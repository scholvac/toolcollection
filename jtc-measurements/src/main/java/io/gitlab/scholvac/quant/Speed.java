package io.gitlab.scholvac.quant;

import javax.measure.Unit;

import systems.uom.unicode.CLDR;

public class Speed extends Measurement<javax.measure.quantity.Speed>{
	public static final Unit<javax.measure.quantity.Speed> METER_PER_SECOND = CLDR.METER_PER_SECOND;
	//	public static final Unit<javax.measure.quantity.Speed> KILOMETER_PER_HOUR = symb(CLDR.KILOMETER.divide(CLDR.HOUR)), "kmH");
	public static final Unit<javax.measure.quantity.Speed> KNOT = CLDR.KNOT;


	static {
		registerType(Speed.class);
		addValidUnits( Speed.class,
				METER_PER_SECOND,
				KNOT);
	}

	public static Speed knots(final float knots) { return new Speed(knots, KNOT); }
	public static Speed meterPerSec(final double meterPerSec) { return new Speed(meterPerSec, METER_PER_SECOND); }

	public Speed() {this(0, METER_PER_SECOND);}
	public Speed(final Number num, final Unit<javax.measure.quantity.Speed> l) { super(num, l); }
	public Speed(final Speed l) { super(l); }
	public Speed copy() { return new Speed(this); }
}