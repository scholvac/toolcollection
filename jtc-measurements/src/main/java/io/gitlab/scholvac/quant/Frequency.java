package io.gitlab.scholvac.quant;


import javax.measure.MetricPrefix;
import javax.measure.Unit;
import javax.measure.quantity.Time;

import systems.uom.unicode.CLDR;

public class Frequency extends Measurement<javax.measure.quantity.Frequency>{
	public static final Unit<javax.measure.quantity.Frequency> Hz = CLDR.HERTZ;
	public static final Unit<javax.measure.quantity.Frequency> KHz = MetricPrefix.KILO(Hz);
	public static final Unit<javax.measure.quantity.Frequency> MHz = MetricPrefix.MEGA(Hz);

	public static final Frequency ZERO = new Frequency(0, Hz);

	static {
		registerType(Frequency.class);
		addValidUnits(Frequency.class,
				Hz,
				symb(MHz, "MHz"),
				symb(KHz, "KHz"));
	}

	public static Frequency hz(final double hz) { return new Frequency(hz, Hz); }

	public Frequency() { this(1, Hz);}
	public Frequency(final double val, final Unit<javax.measure.quantity.Frequency> unit) {
		super(val, unit);
	}

	public double as(final Unit<Time> unit) {
		return new Duration(1./getAs(Hz), Duration.SECOND).getAs(unit);
	}

	public double hz() { return getAs(Hz);}

}