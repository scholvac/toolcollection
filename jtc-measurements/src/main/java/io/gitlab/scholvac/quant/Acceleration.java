package io.gitlab.scholvac.quant;

import javax.measure.Unit;

import systems.uom.unicode.CLDR;

public class Acceleration extends Measurement<javax.measure.quantity.Acceleration>{
	public static final Unit<javax.measure.quantity.Acceleration> METER_PER_SECOND_SQUARED = CLDR.METER_PER_SECOND_SQUARED;
	public static final Unit<javax.measure.quantity.Acceleration> G_FORCE = CLDR.G_FORCE;
	static {
		registerType(Acceleration.class);
		addValidUnits(Acceleration.class,
				symb(METER_PER_SECOND_SQUARED, "m/s²"),
				symb(G_FORCE, "G"));
	}

	public Acceleration() {
		this(0, METER_PER_SECOND_SQUARED);
	}

	public Acceleration(final double value, final Unit<javax.measure.quantity.Acceleration> unit) {
		super(value, unit);
	}

	public static Acceleration mPerSec(final double value) { return new Acceleration(value, METER_PER_SECOND_SQUARED); }
	public static Acceleration g(final double value) { return new Acceleration(value, G_FORCE);}

}