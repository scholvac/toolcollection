package io.gitlab.scholvac.quant;


import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.measure.MetricPrefix;
import javax.measure.Unit;
import javax.measure.quantity.Time;

import systems.uom.unicode.CLDR;
import tech.units.indriya.unit.Units;

/**
 * Represents a measurement of time.
 *
 * <p>This class provides a set of predefined units for time measurements, such as nanoseconds, milliseconds, seconds, minutes, hours, days, weeks, months, years, and centuries.
 * It also allows for the creation of custom time durations using double values and a specified unit.
 *
 * <p>The class provides static factory methods for creating instances of Duration with predefined units:
 * <ul>
 *   <li>{@link #nano(double)}: Creates a Duration instance with the specified value in nanoseconds.</li>
 *   <li>{@link #milli(double)}: Creates a Duration instance with the specified value in milliseconds.</li>
 *   <li>{@link #sec(double)}: Creates a Duration instance with the specified value in seconds.</li>
 *   <li>{@link #min(double)}: Creates a Duration instance with the specified value in minutes.</li>
 *   <li>{@link #hour(double)}: Creates a Duration instance with the specified value in hours.</li>
 * </ul>
 *
 * <p>The class also provides a static method for retrieving the result of a CompletableFuture after a specified duration:
 * <ul>
 *   <li>{@link #getFuture(Duration, CompletableFuture)}: Retrieves the result of the CompletableFuture after the specified duration, or throws an exception if the future is not completed within the specified time.</li>
 * </ul>
 */
public class Duration extends Measurement<Time>{
	public static final Unit<Time> NANOSECONDS 	= MetricPrefix.NANO(Units.SECOND);
	public static final Unit<Time> MILLISECONDS	= MetricPrefix.MILLI(Units.SECOND);
	public static final Unit<Time> CENTISECONDS	= MetricPrefix.CENTI(Units.SECOND);
	public static final Unit<Time> DECISECONDS= MetricPrefix.DECI(Units.SECOND);
	public static final Unit<Time> SECOND 		= CLDR.SECOND;
	public static final Unit<Time> MINUTE 		= CLDR.MINUTE;
	public static final Unit<Time> HOUR 		= CLDR.HOUR;
	public static final Unit<Time> DAY	 		= CLDR.DAY;
	public static final Unit<Time> WEEK 		= CLDR.WEEK;
	public static final Unit<Time> MONTH 		= CLDR.MONTH;
	public static final Unit<Time> YEAR         = CLDR.YEAR;
	public static final Unit<Time> CENTURY 		= CLDR.CENTURY;


	public Duration() { this(0, SECOND);}
	public Duration(final double val, final Unit<Time> unit) {
		super(val, unit);
	}
	static {
		registerType(Duration.class);
		addValidUnits(Duration.class,
				NANOSECONDS,
				MILLISECONDS,
				CENTISECONDS,
				DECISECONDS,
				CLDR.SECOND,
				CLDR.MINUTE,
				CLDR.HOUR,
				CLDR.DAY,
				CLDR.WEEK,
				CLDR.MONTH,
				CLDR.YEAR,
				CLDR.CENTURY);
	}

	/**
	 * Creates a {@code Duration} instance with the specified value in nanoseconds.
	 *
	 * @param value the value in nanoseconds
	 * @return a {@code Duration} instance representing the specified value in nanoseconds
	 */
	public static Duration nano(final double value) {
		return new Duration(value, NANOSECONDS);
	}

	/**
	 * Creates a {@code Duration} instance with the specified value in milliseconds.
	 *
	 * @param value the value in milliseconds
	 * @return a {@code Duration} instance representing the specified value in milliseconds
	 */
	public static Duration milli(final double value) {
		return new Duration(value, MILLISECONDS);
	}

	/**
	 * Creates a {@code Duration} instance with the specified value in centi (1/100) seconds .
	 *
	 * @param value the value in centi seconds
	 * @return a {@code Duration} instance representing the specified value in centiseconds
	 */
	public static Duration centi(final double value) {
		return new Duration(value, CENTISECONDS);
	}

	/**
	 * Creates a {@code Duration} instance with the specified value in seconds.
	 *
	 * @param value the value in seconds
	 * @return a {@code Duration} instance representing the specified value in seconds
	 */
	public static Duration sec(final double value) {
		return new Duration(value, SECOND);
	}

	/**
	 * Creates a {@code Duration} instance with the specified value in minutes.
	 *
	 * @param value the value in minutes
	 * @return a {@code Duration} instance representing the specified value in minutes
	 */
	public static Duration min(final double value) {
		return new Duration(value, MINUTE);
	}

	/**
	 * Creates a {@code Duration} instance with the specified value in hours.
	 *
	 * @param value the value in hours
	 * @return a {@code Duration} instance representing the specified value in hours
	 */
	public static Duration hour(final double value) {
		return new Duration(value, HOUR);
	}


	/**
	 * Retrieves the result of a {@link CompletableFuture} after the specified duration.
	 * If the future is not completed within the specified time, an exception is thrown.
	 *
	 * @param duration the duration to wait for the future
	 * @param future the future to retrieve the result from
	 * @return the result of the future
	 * @throws InterruptedException if the current thread is interrupted while waiting
	 * @throws ExecutionException if the future cannot be retrieved due to an exception
	 * @throws TimeoutException if the future is not completed within the specified time
	 */
	public static <T> T getFuture(final Duration duration, final CompletableFuture<T> future) throws InterruptedException, ExecutionException, TimeoutException {
		return future.get((long)duration.getAs(MILLISECONDS), TimeUnit.MILLISECONDS);
	}

	/**
	 * Converts this Duration to its equivalent Frequency representation.
	 *
	 * @return A Frequency object representing the inverse of this Duration in Hertz.
	 */
	public Frequency getAsFrequency() { return Frequency.hz(1/sec());}

	/**
	 * Gets the duration value in nanoseconds.
	 *
	 * @return The duration in nanoseconds.
	 */
	public final double nano() { return getAs(NANOSECONDS); }

	/**
	 * Gets the duration value in milliseconds.
	 *
	 * @return The duration in milliseconds.
	 */
	public final double milli() { return getAs(MILLISECONDS); }

	/**
	 * Gets the duration value in centiseconds (1/100 of a second).
	 *
	 * @return The duration in centiseconds.
	 */
	public final double centi() { return getAs(CENTISECONDS); }

	/**
	 * Gets the duration value in seconds.
	 *
	 * @return The duration in seconds.
	 */
	public final double sec() { return getAs(SECOND); }

	/**
	 * Gets the duration value in minutes.
	 *
	 * @return The duration in minutes.
	 */
	public final double mins() { return getAs(MINUTE); }

	/**
	 * Gets the duration value in hours.
	 *
	 * @return The duration in hours.
	 */
	public final double hours() { return getAs(HOUR); }

	/**
	 * Gets the duration value in days.
	 *
	 * @return The duration in days.
	 */
	public final double days() { return getAs(DAY); }

	/**
	 * Gets the duration value in weeks.
	 *
	 * @return The duration in weeks.
	 */
	public final double weeks() { return getAs(WEEK); }

	/**
	 * Gets the duration value in months.
	 *
	 * @return The duration in months.
	 */
	public final double months() { return getAs(MONTH); }

	/**
	 * Gets the duration value in years.
	 *
	 * @return The duration in years.
	 */
	public final double years() { return getAs(YEAR); }

	/**
	 * Gets the duration value in centuries.
	 *
	 * @return The duration in centuries.
	 */
	public final double centurys() { return getAs(CENTURY); }


}