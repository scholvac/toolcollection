package io.gitlab.scholvac.quant;



import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.measure.Quantity;
import javax.measure.Unit;

import io.gitlab.scholvac.ReflectionUtils;
import io.gitlab.scholvac.observable.ObservableSupport;
import io.gitlab.scholvac.observable.ObservableSupport.AbstractObservableSupport;
import systems.uom.unicode.CLDR;
import tech.units.indriya.ComparableQuantity;


/**
 * The purpose of the Measurement class is to provide a generic and flexible framework for working with measurements in a Java application. It aims to address the following requirements:
 * <ul>
 * <li> Type Safety: The class is generic, allowing for the creation of specific measurement classes that represent different quantities. This ensures type safety and prevents errors related to incompatible quantities.
 * <li> Unit Management: The class supports working with different units of measurement. It provides methods for adding valid units, getting valid units, and getting units by symbol. This allows for the conversion and comparison of measurements in different units.
 * <li> Serialization and Deserialization: The class provides methods for serializing and deserializing measurements. This enables the storage and retrieval of measurements in a human-readable format, which can be useful for data persistence or communication.
 * <li> Property Change Notifications: The class supports property change notifications. It implements the AbstractObservableSupport interface from the ObservableSupport class, allowing for the observation and reaction to changes in measurements.
 * <li> Extensibility: The class is designed to be extensible. It allows for the creation of specific measurement classes by extending the Measurement class and implementing the necessary methods. This provides flexibility and customization for different types of measurements.
 * <li> Comparison and Arithmetic Operations: The class provides methods for comparing measurements and performing arithmetic operations on measurements. This enables the calculation and manipulation of measurements in a consistent and type-safe manner.
 * </ul>
 * Overall, the Measurement class serves as a foundation for working with measurements in a Java application, providing a flexible and extensible framework for handling different types of measurements.
 */
public class Measurement<Q extends Quantity<Q>> extends WrappedQuantity<Q> implements AbstractObservableSupport, Serializable {

	public static <TYPE extends Measurement, Q extends Quantity<Q>> TYPE of(final Class<TYPE> clazz, final Number num, final Unit<Q> unit){
		try {
			return (TYPE) clazz.newInstance().set(num, unit);
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
		return null;
	}

	private final static Map<Class<? extends Measurement>, List<Unit<?>>> 	sValidUnits = new HashMap<>();
	private final static Map<Class<? extends Measurement>, Unit<?>>			sDefaultUnit = new HashMap<>();
	private final static Map<String, Class<? extends Measurement>>			sNameToTypeMap = new HashMap<>();
	private final static Map<Class<? extends Measurement>, String>			sTypeToNameMap = new HashMap<>();
	private final static Map<String, Unit<?>> 								sSymbolMap = new HashMap<>();//Stores symbols, that have been manually created (by symb(...) method)

	public static final void registerType(final Class<? extends Measurement> clazz) {
		registerType(clazz.getSimpleName(), clazz);
	}
	public static final void registerType(final String name, final Class<? extends Measurement> clazz) {
		sNameToTypeMap.put(name, clazz);
		sTypeToNameMap.put(clazz, name);
	}
	public static final <TYPE extends Quantity<TYPE>> Unit<TYPE> symb(final Unit<TYPE> unit, final String symb){
		try{
			if (unit.getSymbol() == null || unit.getSymbol().isEmpty()) {
				ReflectionUtils.setField(unit, "symbol", symb);
				sSymbolMap.put(symb, unit);
			}
		}catch(final Exception e) {
			e.printStackTrace();
		}
		return unit;
	}
	@SafeVarargs
	public static <TYPE extends Quantity<TYPE>> void addValidUnits(final Class<? extends Measurement> clazz, final Unit<TYPE>... units) {
		sValidUnits.computeIfAbsent(clazz, a -> new ArrayList<>()).addAll(Arrays.asList(units));
		sDefaultUnit.put(clazz, units[0]);
	}
	public static List<Unit<?>> getValidUnits(final Class<? extends Measurement> clazz) {
		return Collections.unmodifiableList(sValidUnits.get(clazz));
	}
	public static Unit<?> getUnitBySymbol(final String symbol){
		//since the CLDR search in a list, we do optimize a little bit and build a dynamic cache.
		return sSymbolMap.computeIfAbsent(symbol, sy -> CLDR.getInstance().getUnit(sy));
	}


	private transient ObservableSupport mPropertySupport = null;


	public Measurement(final Number val, final Unit<Q> unit) {
		super(val, unit);
	}
	public Measurement(final Measurement l) {
		this(l.getValue(), l.getUnit());
	}

	public List<Unit<?>> getValidUnits() {
		return getValidUnits(getClass());
	}


	@Override
	public <T extends WrappedQuantity<Q>> T set(final ComparableQuantity<Q> newDelegate) {
		final ComparableQuantity<Q> oldValue = mDelegate;
		final T val = super.set(newDelegate);
		fireValueChanged(oldValue, newDelegate);
		return val;
	}


	public static String serialize(final Measurement<?> m) {
		final String type = sTypeToNameMap.get(m.getClass());
		return type + "(" + m.getValue() + "[" + m.getUnit() + "])";
	}
	public static Measurement<?> deserialize(final String str){
		final int fidx = str.indexOf('(');
		final String typeName = str.substring(0, fidx);
		final Class<? extends Measurement> clazz = sNameToTypeMap.get(typeName);
		final int secondIdxx = str.indexOf('[');
		final int lidx = str.indexOf(']', fidx);
		final String symb = str.substring(secondIdxx+1, lidx);
		final String valStr = str.substring(fidx+1, secondIdxx);
		final Unit<?> unit = getUnitBySymbol(symb);
		return Measurement.of(clazz, Double.parseDouble(valStr), unit);
	}

	@Override
	public String toString() {
		return serialize(this);
	}
	public String toString(final Unit<Q> unit) {
		return String.format("%1.5f[%s]", getAs(unit), unit.getSymbol());
	}


	public static <Q extends Quantity<Q>, T extends Measurement<Q>> boolean equals(final T a, final T b, final T epsilon) {
		return equals(a, b, epsilon.getAs(epsilon.getUnit()));
	}
	public static <Q extends Quantity<Q>, T extends Measurement<Q>> boolean equals(final T a, final T b, final double absEpsilon) {
		if (a == b) {
			return true;
		}
		if (a == null || b == null) {
			return false;
		}
		final Unit<Q> unit = a.getUnit();
		final double valueA = a.getAs(unit);
		final double valueB = b.getAs(unit);
		return Math.abs(valueA - valueB) < absEpsilon;
	}

	/**
	 * Adds the given measurement to this measurement and returns the result.
	 * The unit of the result is taken into account.
	 *
	 * @param ot The measurement to add.
	 * @return The result of adding the given measurement to this measurement.
	 */
	public <T extends Measurement<Q>> T add(final T ot) {
		return (T) of(getClass(), getValue().doubleValue() + ot.getAs(getUnit()), getUnit());
	}

	/**
	 * Subtracts the given measurement from this measurement and returns the result.
	 * The unit of the result is taken into account.
	 *
	 * @param ot The measurement to subtract.
	 * @return The result of subtracting the given measurement from this measurement.
	 */
	public <T extends Measurement<Q>> T sub(final T ot) {
		return (T) of(getClass(), getValue().doubleValue() - ot.getAs(getUnit()), getUnit());
	}

	/**
	 * Multiplies this measurement by the given measurement and returns the result.
	 * The unit of the result is taken into account.
	 *
	 * @param ot The measurement to multiply by.
	 * @return The result of multiplying this measurement by the given measurement.
	 */
	public <T extends Measurement<Q>> T mul(final T ot) {
		return (T) of(getClass(), getValue().doubleValue() * ot.getAs(getUnit()), getUnit());
	}

	/**
	 * Divides this measurement by the given measurement and returns the result.
	 * The unit of the result is taken into account.
	 *
	 * @param ot The measurement to divide by.
	 * @return The result of dividing this measurement by the given measurement.
	 */
	public <T extends Measurement<Q>> T div(final T ot) {
		return (T) of(getClass(), getValue().doubleValue() / ot.getAs(getUnit()), getUnit());
	}

	/**
	 * Adds the given double value to this measurement and returns the result.
	 * The unit of the result is taken into account.
	 *
	 * @param ot The double value to add.
	 * @return The result of adding the given double value to this measurement.
	 */
	public <T extends Measurement<Q>> T add(final double ot) {
		return (T) of(getClass(), getValue().doubleValue() + ot, getUnit());
	}

	/**
	 * Subtracts the given double value from this measurement and returns the result.
	 * The unit of the result is taken into account.
	 *
	 * @param ot The double value to subtract.
	 * @return The result of subtracting the given double value from this measurement.
	 */
	public <T extends Measurement<Q>> T sub(final double ot) {
		return (T) of(getClass(), getValue().doubleValue() - ot, getUnit());
	}

	/**
	 * Multiplies this measurement by the given double value and returns the result.
	 * The unit of the result is taken into account.
	 *
	 * @param ot The double value to multiply by.
	 * @return The result of multiplying this measurement by the given double value.
	 */
	public <T extends Measurement<Q>> T mul(final double ot) {
		return (T) of(getClass(), getValue().doubleValue() * ot, getUnit());
	}

	/**
	 * Divides this measurement by the given double value and returns the result.
	 * The unit of the result is taken into account.
	 *
	 * @param ot The double value to divide by.
	 * @return The result of dividing this measurement by the given double value.
	 */
	public <T extends Measurement<Q>> T div(final double ot) {
		return (T) of(getClass(), getValue().doubleValue() / ot, getUnit());
	}


	@Override
	public ObservableSupport getObservableSupport() {
		if (mPropertySupport == null) {
			mPropertySupport = new ObservableSupport<>();
		}
		return mPropertySupport;
	}


	public boolean equals(final Measurement<Q> other, final double eps) {
		return Measurement.equals(this, other, eps);
	}

}
