package io.gitlab.scholvac.quant;

import javax.measure.Unit;

import systems.uom.unicode.CLDR;

public class Length extends Measurement<javax.measure.quantity.Length>{
	public static final Unit<javax.measure.quantity.Length> METER = CLDR.METER;
	public static final Unit<javax.measure.quantity.Length> KILOMETER = CLDR.KILOMETER;
	public static final Unit<javax.measure.quantity.Length> CENTIMETER = CLDR.CENTIMETER;
	public static final Unit<javax.measure.quantity.Length> NAUTICAL_MILES = CLDR.NAUTICAL_MILE;
	public static final Unit<javax.measure.quantity.Length> FOOT = CLDR.FOOT;
	public static final Unit<javax.measure.quantity.Length> FURLONG = CLDR.FURLONG;
	public static final Unit<javax.measure.quantity.Length> YARD = CLDR.YARD;
	public static final Unit<javax.measure.quantity.Length> INCH = CLDR.INCH;
	public static final Unit<javax.measure.quantity.Length> ASTRONOMICAL_UNIT = CLDR.ASTRONOMICAL_UNIT;
	public static final Unit<javax.measure.quantity.Length> FATHOM = CLDR.FATHOM;
	public static final Unit<javax.measure.quantity.Length> MILE_SCANDINAVIAN = CLDR.MILE_SCANDINAVIAN;


	static {
		registerType(Length.class);
		addValidUnits( Length.class,
				METER,
				symb(CLDR.MILLIMETER, "mm"),
				symb(CENTIMETER, "cm"),
				symb(KILOMETER, "km"),
				symb(FOOT, "ft"),
				symb(FURLONG, "furlong"),
				symb(YARD, "yd"),
				symb(INCH, "in"),
				symb(NAUTICAL_MILES, "mi"),
				symb(ASTRONOMICAL_UNIT, "ua"),
				symb(FATHOM, "fm"),
				symb(MILE_SCANDINAVIAN, "dkMi"));
	}

	public static Length meter(final double meter) { return new Length(meter, METER); }
	public static Length foot(final double foot) { return new Length(foot, FOOT); }

	public Length() {this(0, METER);}
	public Length(final Number num, final Unit<javax.measure.quantity.Length> l) { super(num, l); }
	public Length(final Length l) { super(l); }
	public Length copy() { return new Length(this); }
}