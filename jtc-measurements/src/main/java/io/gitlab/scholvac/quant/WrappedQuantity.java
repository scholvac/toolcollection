package io.gitlab.scholvac.quant;


import java.util.Objects;

import javax.measure.Quantity;
import javax.measure.Unit;

import tech.units.indriya.ComparableQuantity;
import tech.units.indriya.quantity.Quantities;
import tech.uom.lib.common.function.UnitSupplier;
import tech.uom.lib.common.function.ValueSupplier;



public class WrappedQuantity<Q extends Quantity<Q>> implements ComparableQuantity<Q>, UnitSupplier<Q>, ValueSupplier<Number> {

	protected ComparableQuantity<Q> 	mDelegate;

	public WrappedQuantity(final Unit<Q> unit) {
		this(0.0, unit);
	}
	public WrappedQuantity(final Number val, final Unit<Q> unit) {
		mDelegate = Quantities.getQuantity(val, unit);
	}
	private WrappedQuantity() {
		mDelegate = null;
	}

	public void setValue(final Number newVal) {
		if (newVal.equals(getValue()))
			return ; //does not change anything...
		set(mDelegate = Quantities.getQuantity(newVal, mDelegate.getUnit(), mDelegate.getScale()));
	}
	public void setUnit(final Unit<Q> unit) {
		if (unit == getUnit())
			return ; //do nothing as it does not change anything....
		set(Quantities.getQuantity(mDelegate.getValue(), unit, mDelegate.getScale()));
	}
	public <T extends WrappedQuantity<Q>> T set(final Number num, final Unit<Q> unit) {
		return set(Quantities.getQuantity(num, unit));
	}
	public <T extends WrappedQuantity<Q>> T convertTo(final Unit unit) {
		try {
			final ComparableQuantity<Q> tmp = mDelegate.to(unit);
			return set(tmp);
		}catch(final Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public <T extends WrappedQuantity<Q>> T set(final ComparableQuantity<Q> newDelegate) {
		mDelegate = newDelegate;
		return (T)this;
	}

	public double getAs(final Unit<Q> unit) {
		if (getUnit().equals(unit))
			return getValue().doubleValue();
		return mDelegate.to(unit).getValue().doubleValue();
	}


	@Override
	public Quantity<Q> negate() { return mDelegate.negate(); }
	@Override
	public Number getValue() { return mDelegate.getValue(); }
	@Override
	public Unit<Q> getUnit() { return mDelegate.getUnit(); }
	@Override
	public Scale getScale() { return mDelegate.getScale(); }
	@Override
	public boolean isEquivalentTo(final Quantity<Q> that) { return mDelegate.isEquivalentTo(that); }
	@Override
	public int compareTo(final Quantity<Q> o) { return mDelegate.compareTo(o); }
	@Override
	public ComparableQuantity<Q> add(final Quantity<Q> that) { return mDelegate.add(that); }
	@Override
	public ComparableQuantity<Q> subtract(final Quantity<Q> that) { return mDelegate.subtract(that);}
	@Override
	public ComparableQuantity<?> divide(final Quantity<?> that) { return mDelegate.divide(getValue());}
	@Override
	public ComparableQuantity<Q> divide(final Number that) { return mDelegate.divide(that); }
	@Override
	public ComparableQuantity<?> multiply(final Quantity<?> multiplier) { return mDelegate.multiply(multiplier); }
	@Override
	public ComparableQuantity<Q> multiply(final Number multiplier) { return mDelegate.multiply(multiplier);}
	@Override
	public ComparableQuantity<?> inverse() { return mDelegate.inverse(); }
	@Override
	public <T extends Quantity<T>> ComparableQuantity<T> inverse(final Class<T> quantityClass) { return mDelegate.inverse(quantityClass); }
	@Override
	public ComparableQuantity<Q> to(final Unit<Q> unit) { return mDelegate.to(unit); }
	@Override
	public <T extends Quantity<T>> ComparableQuantity<T> asType(final Class<T> type) throws ClassCastException { return mDelegate.asType(type); }
	@Override
	public boolean isGreaterThan(final Quantity<Q> that) {return mDelegate.isGreaterThan(that);}
	@Override
	public boolean isGreaterThanOrEqualTo(final Quantity<Q> that) {return mDelegate.isGreaterThanOrEqualTo(that);}
	@Override
	public boolean isLessThan(final Quantity<Q> that) { return mDelegate.isLessThan(that); }
	@Override
	public boolean isLessThanOrEqualTo(final Quantity<Q> that) {return mDelegate.isLessThanOrEqualTo(that);}
	@Override
	public <T extends Quantity<T>, E extends Quantity<E>> ComparableQuantity<E> divide(final Quantity<T> that, final Class<E> asTypeQuantity) { return mDelegate.divide(that, asTypeQuantity);}
	@Override
	public <T extends Quantity<T>, E extends Quantity<E>> ComparableQuantity<E> multiply(final Quantity<T> that, final Class<E> asTypeQuantity) {return mDelegate.multiply(that, asTypeQuantity); }
	@Override
	public int hashCode() {
		return Objects.hash(mDelegate);
	}
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final WrappedQuantity other = (WrappedQuantity) obj;
		return compareTo(other) == 0;
	}
}
