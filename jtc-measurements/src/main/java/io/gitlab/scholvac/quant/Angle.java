package io.gitlab.scholvac.quant;


import javax.measure.Unit;

import systems.uom.unicode.CLDR;

/**
 * Represents an angle measurement.
 * This class extends the Measurement class for the Angle quantity.
 */
public class Angle extends Measurement<javax.measure.quantity.Angle> {

	/**
	 * Unit representing degrees.
	 */
	public static final Unit<javax.measure.quantity.Angle> DEGREE = CLDR.DEGREE;

	/**
	 * Unit representing radians.
	 */
	public static final Unit<javax.measure.quantity.Angle> RADIAN = CLDR.RADIAN;

	/**
	 * Unit representing arc minutes.
	 */
	public static final Unit<javax.measure.quantity.Angle> ARC_MINUTE = CLDR.ARC_MINUTE;

	/**
	 * Unit representing arc seconds.
	 */
	public static final Unit<javax.measure.quantity.Angle> ARC_SECOND = CLDR.ARC_SECOND;

	/**
	 * Unit representing full revolutions (360 degrees).
	 */
	public static final Unit<javax.measure.quantity.Angle> REVOLUTION_ANGLE = CLDR.REVOLUTION_ANGLE;

	/**
	 * Creates an Angle instance with the specified value in degrees.
	 *
	 * @param degrees The angle value in degrees.
	 * @return An Angle object representing the specified degrees.
	 */
	public static Angle deg(final double degrees) { return new Angle(degrees, DEGREE); }

	/**
	 * Creates an Angle instance with the specified value in radians.
	 *
	 * @param radians The angle value in radians.
	 * @return An Angle object representing the specified radians.
	 */
	public static Angle rad(final double radians) { return new Angle(radians, RADIAN); }


	static {
		registerType(Angle.class);
		addValidUnits(Angle.class,
				symb(DEGREE, "deg"),
				RADIAN,
				symb(ARC_MINUTE, "arc min"),
				symb(ARC_SECOND, "arc sec"),
				symb(REVOLUTION_ANGLE, "rev"));
	}
	/**
	 * Constructs an Angle with a default value of 0 degrees.
	 */
	public Angle() {
		this(0.0, DEGREE);
	}

	/**
	 * Constructs an Angle with the specified value and unit.
	 *
	 * @param value The angle value.
	 * @param unit The angle unit.
	 */
	public Angle(final double value, final Unit<javax.measure.quantity.Angle> unit) {
		super(value, unit);
	}

	/**
	 * Gets the angle value in degrees.
	 *
	 * @return The angle value in degrees.
	 */
	public double deg() { return getAs(DEGREE); }

	/**
	 * Gets the angle value in radians.
	 *
	 * @return The angle value in radians.
	 */
	public double rad() { return getAs(RADIAN); }
}