package io.gitlab.scholvac.quant;


import javax.measure.MetricPrefix;
import javax.measure.Unit;

import systems.uom.unicode.CLDR;

public class Information extends Measurement<systems.uom.quantity.Information>{
	public static final Unit<systems.uom.quantity.Information> BIT = CLDR.BIT;
	public static final Unit<systems.uom.quantity.Information> MBit = MetricPrefix.MEGA(CLDR.BIT);
	public static final Unit<systems.uom.quantity.Information> KBit = MetricPrefix.KILO(CLDR.BIT);
	public static final Unit<systems.uom.quantity.Information> GBit = MetricPrefix.GIGA(CLDR.BIT);

	public static final Unit<systems.uom.quantity.Information> BYTE = CLDR.BYTE;
	public static final Unit<systems.uom.quantity.Information> KB = symb(BYTE.multiply(1024), "KB");
	public static final Unit<systems.uom.quantity.Information> MB = symb(KB.multiply(1024), "MB");
	public static final Unit<systems.uom.quantity.Information> GB = symb(MB.multiply(1024), "GB");
	public static final Unit<systems.uom.quantity.Information> TB = symb(GB.multiply(1024), "TB");
	public static final Unit<systems.uom.quantity.Information> PB = symb(GB.multiply(1024), "PB");



	static {
		registerType(Information.class);
		addValidUnits(Information.class, MB, GB, TB, PB, BYTE, BIT, MBit, KBit, GBit);
	}

	public static Information mb(final double mb) {
		return new Information(mb, MB);
	}

	public Information() { this(0, MB); }
	public Information(final double value, final Unit<systems.uom.quantity.Information> unit) {
		super(value, unit);
	}
	public Information(final Information other) {
		super(other);
	}
}