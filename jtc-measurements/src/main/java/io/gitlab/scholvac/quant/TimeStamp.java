package io.gitlab.scholvac.quant;


import javax.measure.MetricPrefix;
import javax.measure.Unit;
import javax.measure.quantity.Time;

import systems.uom.unicode.CLDR;
import tech.units.indriya.unit.Units;

/**
 * Represents a timestamp with various time units and utility methods.
 * This class extends the Measurement class for the Time quantity.
 */
public class TimeStamp extends Measurement<Time> {

	/**
	 * Unit representing nanoseconds.
	 */
	public static final Unit<Time> NANOSECONDS = MetricPrefix.NANO(Units.SECOND);

	/**
	 * Unit representing milliseconds.
	 */
	public static final Unit<Time> MILLISECONDS = MetricPrefix.MILLI(Units.SECOND);

	/**
	 * Unit representing centiseconds.
	 */
	public static final Unit<Time> CENTISECONDS = MetricPrefix.CENTI(Units.SECOND);

	/**
	 * Unit representing deciseconds.
	 */
	public static final Unit<Time> DECISECONDS = MetricPrefix.DECI(Units.SECOND);

	/**
	 * Unit representing seconds.
	 */
	public static final Unit<Time> SECOND = CLDR.SECOND;

	/**
	 * Unit representing minutes.
	 */
	public static final Unit<Time> MINUTE = CLDR.MINUTE;

	/**
	 * Unit representing hours.
	 */
	public static final Unit<Time> HOUR = CLDR.HOUR;

	/**
	 * Unit representing days.
	 */
	public static final Unit<Time> DAY = CLDR.DAY;

	/**
	 * Unit representing weeks.
	 */
	public static final Unit<Time> WEEK = CLDR.WEEK;

	/**
	 * Unit representing months.
	 */
	public static final Unit<Time> MONTH = CLDR.MONTH;

	/**
	 * Unit representing years.
	 */
	public static final Unit<Time> YEAR = CLDR.YEAR;

	/**
	 * Unit representing centuries.
	 */
	public static final Unit<Time> CENTURY = CLDR.CENTURY;

	/**
	 * Creates a TimeStamp representing the current time.
	 *
	 * @return A TimeStamp object set to the current time.
	 */
	public static TimeStamp now() { return epoch(System.currentTimeMillis()); }

	/**
	 * Creates a TimeStamp with the specified number of nanoseconds.
	 *
	 * @param ns The number of nanoseconds.
	 * @return A TimeStamp object representing the specified nanoseconds.
	 */
	public static TimeStamp nano(final double ns) { return new TimeStamp(ns, NANOSECONDS);}

	/**
	 * Creates a TimeStamp with the specified number of milliseconds.
	 *
	 * @param ns The number of milliseconds.
	 * @return A TimeStamp object representing the specified milliseconds.
	 */
	public static TimeStamp milli(final double ns) { return new TimeStamp(ns, MILLISECONDS);}

	/**
	 * Creates a TimeStamp with the specified number of centiseconds.
	 *
	 * @param ns The number of centiseconds.
	 * @return A TimeStamp object representing the specified centiseconds.
	 */
	public static TimeStamp centi(final double ns) { return new TimeStamp(ns, CENTISECONDS);}

	/**
	 * Creates a TimeStamp with the specified number of deciseconds.
	 *
	 * @param ns The number of deciseconds.
	 * @return A TimeStamp object representing the specified deciseconds.
	 */
	public static TimeStamp deci(final double ns) { return new TimeStamp(ns, DECISECONDS);}

	/**
	 * Creates a TimeStamp with the specified number of seconds.
	 *
	 * @param s The number of seconds.
	 * @return A TimeStamp object representing the specified seconds.
	 */
	public static TimeStamp second(final double s) { return new TimeStamp(s, SECOND);}

	/**
	 * Creates a TimeStamp with the specified number of minutes.
	 *
	 * @param m The number of minutes.
	 * @return A TimeStamp object representing the specified minutes.
	 */
	public static TimeStamp minute(final double m) { return new TimeStamp(m, MINUTE);}

	/**
	 * Creates a TimeStamp with the specified number of hours.
	 *
	 * @param h The number of hours.
	 * @return A TimeStamp object representing the specified hours.
	 */
	public static TimeStamp hour(final double h) { return new TimeStamp(h, HOUR);}

	/**
	 * Creates a TimeStamp with the specified number of days.
	 *
	 * @param d The number of days.
	 * @return A TimeStamp object representing the specified days.
	 */
	public static TimeStamp day(final double d) { return new TimeStamp(d, DAY);}

	/**
	 * Creates a TimeStamp with the specified number of weeks.
	 *
	 * @param w The number of weeks.
	 * @return A TimeStamp object representing the specified weeks.
	 */
	public static TimeStamp week(final double w) { return new TimeStamp(w, WEEK);}

	/**
	 * Creates a TimeStamp with the specified number of months.
	 *
	 * @param m The number of months.
	 * @return A TimeStamp object representing the specified months.
	 */
	public static TimeStamp month(final double m) { return new TimeStamp(m, MONTH);}

	/**
	 * Creates a TimeStamp with the specified number of years.
	 *
	 * @param y The number of years.
	 * @return A TimeStamp object representing the specified years.
	 */
	public static TimeStamp year(final double y) { return new TimeStamp(y, YEAR);}

	/**
	 * Creates a TimeStamp with the specified number of centuries.
	 *
	 * @param c The number of centuries.
	 * @return A TimeStamp object representing the specified centuries.
	 */
	public static TimeStamp century(final double c) { return new TimeStamp(c, CENTURY);}

	/**
	 * Creates a TimeStamp with the specified epoch time in milliseconds.
	 *
	 * @param val The epoch time in milliseconds.
	 * @return A TimeStamp object representing the specified epoch time.
	 */
	public static TimeStamp epoch(final double val) { return new TimeStamp(val, MILLISECONDS);}

	static {
		registerType(TimeStamp.class);
		addValidUnits(TimeStamp.class,
				NANOSECONDS,
				MILLISECONDS,
				CENTISECONDS,
				DECISECONDS,
				CLDR.SECOND,
				CLDR.MINUTE,
				CLDR.HOUR,
				CLDR.DAY,
				CLDR.WEEK,
				CLDR.MONTH,
				CLDR.YEAR,
				CLDR.CENTURY);
	}
	/**
	 * Constructs a TimeStamp with a default value of 0 seconds.
	 */
	public TimeStamp() { this(0, SECOND);}

	/**
	 * Constructs a TimeStamp with the specified value and unit.
	 *
	 * @param val The time value.
	 * @param unit The time unit.
	 */
	public TimeStamp(final double val, final Unit<Time> unit) {
		super(val, unit);
	}

	/**
	 * Gets the time value in nanoseconds.
	 *
	 * @return The time value in nanoseconds.
	 */
	public double getNs() { return getAs(NANOSECONDS);}

	/**
	 * Gets the time value in milliseconds.
	 *
	 * @return The time value in milliseconds.
	 */
	public double getMs() { return getAs(MILLISECONDS);}

	/**
	 * Gets the time value in centiseconds.
	 *
	 * @return The time value in centiseconds.
	 */
	public double getCs() { return getAs(CENTISECONDS);}

	/**
	 * Gets the time value in deciseconds.
	 *
	 * @return The time value in deciseconds.
	 */
	public double getDs() { return getAs(DECISECONDS);}

	/**
	 * Gets the time value in seconds.
	 *
	 * @return The time value in seconds.
	 */
	public double getSecs() { return getAs(SECOND);}

	/**
	 * Gets the time value in minutes.
	 *
	 * @return The time value in minutes.
	 */
	public double getMins() { return getAs(MINUTE);}

	/**
	 * Gets the time value in hours.
	 *
	 * @return The time value in hours.
	 */
	public double getHours() { return getAs(HOUR);}

	/**
	 * Gets the time value in days.
	 *
	 * @return The time value in days.
	 */
	public double getDays() { return getAs(DAY);}


	public TimeStamp add(final Duration duration) {
		return add(duration.getAs(getUnit()));
	}
	public TimeStamp sub(final Duration duration) {
		return sub(duration.getAs(getUnit()));
	}


}