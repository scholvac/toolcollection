package io.gitlab.scholvac.quant;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class AngleTest {

	private static final double DELTA = 1e-10;

	@Test
	void testDefaultConstructor() {
		final Angle angle = new Angle();
		assertEquals(0.0, angle.deg(), DELTA);
		assertEquals(0.0, angle.rad(), DELTA);
	}

	@Test
	void testConstructorWithValueAndUnit() {
		final Angle angle = new Angle(90, Angle.DEGREE);
		assertEquals(90.0, angle.deg(), DELTA);
		assertEquals(Math.PI / 2, angle.rad(), DELTA);
	}

	@Test
	void testDegStaticMethod() {
		final Angle angle = Angle.deg(180);
		assertEquals(180.0, angle.deg(), DELTA);
		assertEquals(Math.PI, angle.rad(), DELTA);
	}

	@Test
	void testRadStaticMethod() {
		final Angle angle = Angle.rad(Math.PI);
		assertEquals(180.0, angle.deg(), DELTA);
		assertEquals(Math.PI, angle.rad(), DELTA);
	}

	@Test
	void testDegMethod() {
		final Angle angle = new Angle(45, Angle.DEGREE);
		assertEquals(45.0, angle.deg(), DELTA);
	}

	@Test
	void testRadMethod() {
		final Angle angle = new Angle(Math.PI / 4, Angle.RADIAN);
		assertEquals(Math.PI / 4, angle.rad(), DELTA);
	}

	@Test
	void testConversionDegreeToRadian() {
		final Angle angle = Angle.deg(180);
		assertEquals(Math.PI, angle.rad(), DELTA);
	}

	@Test
	void testConversionRadianToDegree() {
		final Angle angle = Angle.rad(Math.PI / 2);
		assertEquals(90.0, angle.deg(), DELTA);
	}

	@Test
	void testZeroAngle() {
		final Angle angle = Angle.deg(0);
		assertEquals(0.0, angle.deg(), DELTA);
		assertEquals(0.0, angle.rad(), DELTA);
	}

	@Test
	void testNegativeAngle() {
		final Angle angle = Angle.deg(-90);
		assertEquals(-90.0, angle.deg(), DELTA);
		assertEquals(-Math.PI / 2, angle.rad(), DELTA);
	}

	@Test
	void testLargeAngle() {
		final Angle angle = Angle.deg(720);
		assertEquals(720.0, angle.deg(), DELTA);
		assertEquals(4 * Math.PI, angle.rad(), DELTA);
	}
}