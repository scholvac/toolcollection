package io.gitlab.scholvac.quant;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;

class DurationTest {

	private static final double DELTA = 1e-10;

	@Test
	void testDefaultConstructor() {
		final Duration duration = new Duration();
		assertEquals(0.0, duration.sec(), DELTA);
	}

	@Test
	void testConstructorWithValueAndUnit() {
		final Duration duration = new Duration(5, Duration.SECOND);
		assertEquals(5.0, duration.sec(), DELTA);
	}

	@Test
	void testFactoryMethods() {
		assertEquals(1e-9, Duration.nano(1).sec(), DELTA);
		assertEquals(0.001, Duration.milli(1).sec(), DELTA);
		assertEquals(0.01, Duration.centi(1).sec(), DELTA);
		assertEquals(1.0, Duration.sec(1).sec(), DELTA);
		assertEquals(60.0, Duration.min(1).sec(), DELTA);
		assertEquals(3600.0, Duration.hour(1).sec(), DELTA);
	}

	@Test
	void testGetAsFrequency() {
		final Duration duration = Duration.sec(2);
		final Frequency frequency = duration.getAsFrequency();
		assertEquals(0.5, frequency.hz(), DELTA);
	}

	@Test
	void testConversionMethods() {
		final Duration duration = Duration.sec(1);
		assertEquals(1e9, duration.nano(), DELTA);
		assertEquals(1000, duration.milli(), DELTA);
		assertEquals(100, duration.centi(), DELTA);
		assertEquals(1, duration.sec(), DELTA);
		assertEquals(1.0 / 60, duration.mins(), DELTA);
		assertEquals(1.0 / 3600, duration.hours(), DELTA);
		assertEquals(1.0 / (24 * 3600), duration.days(), DELTA);
		assertEquals(1.0 / (7 * 24 * 3600), duration.weeks(), DELTA);
		// Note: months and years might be approximate due to varying lengths
		assertTrue(duration.months() > 0 && duration.months() < 1);
		assertTrue(duration.years() > 0 && duration.years() < 1);
		assertTrue(duration.centurys() > 0 && duration.centurys() < 1);
	}

	@Test
	@Timeout(1)
	void testGetFutureSuccess() throws InterruptedException, ExecutionException, TimeoutException {
		final CompletableFuture<String> future = CompletableFuture.supplyAsync(() -> {
			try {
				Thread.sleep(100);
			} catch (final InterruptedException e) {
				e.printStackTrace();
			}
			return "Done";
		});

		final String result = Duration.getFuture(Duration.milli(500), future);
		assertEquals("Done", result);
	}

	@Test
	void testGetFutureTimeout() {
		final CompletableFuture<String> future = CompletableFuture.supplyAsync(() -> {
			try {
				Thread.sleep(1000);
			} catch (final InterruptedException e) {
				e.printStackTrace();
			}
			return "Done";
		});

		assertThrows(TimeoutException.class, () -> Duration.getFuture(Duration.milli(100), future));
	}
}