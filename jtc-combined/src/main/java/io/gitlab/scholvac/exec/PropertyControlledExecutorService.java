package io.gitlab.scholvac.exec;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.AbstractExecutorService;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import io.gitlab.scholvac.CommonUtils;
import io.gitlab.scholvac.IDisposeable;
import io.gitlab.scholvac.observable.IObservableObject.ValueChangeEvent;
import io.gitlab.scholvac.prop.IProperty;

/**
 * An executor service whose behaviour is controlled through properties.
 * <br>
 * The executor service uses a delegate pattern. Whenever one of the properties changes, the unterlying executor service is rebuild.
 */
public class PropertyControlledExecutorService extends AbstractExecutorService implements ScheduledExecutorService, IDisposeable {

	private final Object 					mLock = new Object();
	private ExecutorService					mExecutor;

	private final IProperty<String>			mName;
	private final IProperty<Boolean>		mExecuteAsync;
	private final IProperty<Boolean> 		mScheduled;
	private final IProperty<Integer>		mNumberOfWorkers;
	private	final IProperty<Boolean>		mDaemonThread;

	private final IDisposeable				mDisposables;


	public PropertyControlledExecutorService(final IProperty<String> name, final IProperty<Boolean> async, final IProperty<Boolean> scheduled, final IProperty<Integer> threadCount, final IProperty<Boolean> daemonThreads) {
		mName = name;
		mExecuteAsync = async;
		mScheduled = scheduled;
		mNumberOfWorkers = threadCount;
		mDaemonThread = daemonThreads;

		mDisposables = IDisposeable.createMulti(mName.addPropertyChangeListener(this::disposeExecutor))
				.add(mExecuteAsync.addPropertyChangeListener(this::disposeExecutor))
				.add(mScheduled.addPropertyChangeListener(this::disposeExecutor))
				.add(mNumberOfWorkers.addPropertyChangeListener(this::disposeExecutor))
				.add(mDaemonThread.addPropertyChangeListener(this::disposeExecutor))
				;

	}

	private void disposeExecutor(final ValueChangeEvent evt) {
		synchronized (mLock) {
			mExecutor = null;
		}
	}
	public boolean isAsync() {
		return mExecuteAsync.get();
	}
	public boolean isScheduled() {
		return mScheduled.get();
	}

	@Override
	public void dispose() throws Exception {
		mDisposables.dispose();
	}
	@Override
	public boolean isDisposed() {
		return mDisposables.isDisposed();
	}

	private ExecutorService getExecutorService() {
		synchronized (mLock) {
			if (mExecutor == null) {
				if (!isAsync())
					mExecutor = new DirectExecutorService();
				else if (isScheduled())
					mExecutor = Executors.newScheduledThreadPool(mNumberOfWorkers.get(), CommonUtils.createThreadFactory(mName.getName(), mDaemonThread.get()));
				else
					mExecutor = Executors.newFixedThreadPool(mNumberOfWorkers.get(), CommonUtils.createThreadFactory(mName.getName(), mDaemonThread.get()));
			}
			return mExecutor;
		}
	}

	@Override
	public void execute(final Runnable command) {
		final ExecutorService delegate = getExecutorService();
		if (delegate != null)
			delegate.execute(command);
		else
			command.run();
	}

	@Override
	public void shutdown() {
		if (mExecutor != null)
			mExecutor.shutdown();
	}
	@Override
	public List<Runnable> shutdownNow() {
		if (mExecutor != null)
			return mExecutor.shutdownNow();
		return new ArrayList<>();
	}
	@Override
	public boolean isShutdown() {
		if (mExecutor != null)
			return mExecutor.isShutdown();
		return true;
	}
	@Override
	public boolean isTerminated() {
		if (mExecutor != null)
			return mExecutor.isTerminated();
		return true;
	}
	@Override
	public boolean awaitTermination(final long timeout, final TimeUnit unit) throws InterruptedException {
		if (mExecutor != null)
			return mExecutor.awaitTermination(timeout, unit);
		return true;
	}
	@Override
	public <T> Future<T> submit(final Callable<T> task) {
		final ExecutorService delegate = getExecutorService();
		return delegate.submit(task);
	}
	@Override
	public <T> Future<T> submit(final Runnable task, final T result) {
		final ExecutorService delegate = getExecutorService();
		return delegate.submit(task, result);
	}
	@Override
	public Future<?> submit(final Runnable task) {
		final ExecutorService delegate = getExecutorService();
		return delegate.submit(task);
	}
	@Override
	public <T> List<Future<T>> invokeAll(final Collection<? extends Callable<T>> tasks) throws InterruptedException {
		final ExecutorService delegate = getExecutorService();
		return delegate.invokeAll(tasks);
	}
	@Override
	public <T> List<Future<T>> invokeAll(final Collection<? extends Callable<T>> tasks, final long timeout, final TimeUnit unit)
			throws InterruptedException {
		final ExecutorService delegate = getExecutorService();
		return delegate.invokeAll(tasks, timeout, unit);
	}
	@Override
	public <T> T invokeAny(final Collection<? extends Callable<T>> tasks)
			throws InterruptedException, ExecutionException {
		final ExecutorService delegate = getExecutorService();
		return delegate.invokeAny(tasks);
	}
	@Override
	public <T> T invokeAny(final Collection<? extends Callable<T>> tasks, final long timeout, final TimeUnit unit)
			throws InterruptedException, ExecutionException, TimeoutException {
		final ExecutorService delegate = getExecutorService();
		return delegate.invokeAny(tasks, timeout, unit);
	}

	@Override
	public ScheduledFuture<?> schedule(final Runnable command, final long delay, final TimeUnit unit) {
		if (isScheduled() == false) throw new UnsupportedOperationException("The scheduler service is not a scheduled service");
		final ScheduledExecutorService delegate = (ScheduledExecutorService)getExecutorService();
		return delegate.schedule(command, delay, unit);
	}

	@Override
	public <V> ScheduledFuture<V> schedule(final Callable<V> callable, final long delay, final TimeUnit unit) {
		if (isScheduled() == false) throw new UnsupportedOperationException("The scheduler service is not a scheduled service");
		final ScheduledExecutorService delegate = (ScheduledExecutorService)getExecutorService();
		return delegate.schedule(callable, delay, unit);
	}

	@Override
	public ScheduledFuture<?> scheduleAtFixedRate(final Runnable command, final long initialDelay, final long period, final TimeUnit unit) {
		if (isScheduled() == false) throw new UnsupportedOperationException("The scheduler service is not a scheduled service");
		final ScheduledExecutorService delegate = (ScheduledExecutorService)getExecutorService();
		return delegate.scheduleAtFixedRate(command, initialDelay, period, unit);
	}

	@Override
	public ScheduledFuture<?> scheduleWithFixedDelay(final Runnable command, final long initialDelay, final long delay, final TimeUnit unit) {
		if (isScheduled() == false) throw new UnsupportedOperationException("The scheduler service is not a scheduled service");
		final ScheduledExecutorService delegate = (ScheduledExecutorService)getExecutorService();
		return delegate.scheduleWithFixedDelay(command, initialDelay, delay, unit);
	}
}