package io.gitlab.scholvac.exec;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import io.gitlab.scholvac.CommonUtils;
import io.gitlab.scholvac.prop.IProperty;
import io.gitlab.scholvac.prop.IPropertyContext;
import io.gitlab.scholvac.prop.PropertyDescriptor;
import io.gitlab.scholvac.prop.impl.PropertyContext;

public class ExecutorUtils2 extends ExecutorUtils {

	public static final PropertyDescriptor<String>		REL_DESC_NAME 			= PropertyDescriptor.create("Name", "Executor").description("The name of the executor threads");
	public static final PropertyDescriptor<Boolean>		REL_DESC_ASYNC 			= PropertyDescriptor.create("Async", true).description("Whether the generated executor shall execute the tasks asynchron or synchron");
	public static final PropertyDescriptor<Integer> 	REL_DESC_THREAD_COUNT 	= PropertyDescriptor.create("Thread Count", 1).description("The number of threads used for async execution. Ignored if 'Async' is false");
	public static final PropertyDescriptor<Boolean>		REL_DESC_DEAMON			= PropertyDescriptor.create("Deamon", true).description("Whether the thread shall keep the application alive (false) or not (true)");

	public static final int 							UI_SCHEDULER_THREAD_COUNT 	= 2;

	private static final boolean NO_SCHEDULER_SERVICE = false;

	private static ScheduledExecutorService 			sUIScheduler 	= null;
	private static Map<String, ExecutorService> 		sNamedExecutors = new HashMap<>();

	/** Provides a system wide ui scheduler, that shall be reused by as many UI elements as possible, instead of creating a new execution service */
	public static synchronized ScheduledExecutorService getUIScheduler() {
		if (sUIScheduler == null)
			sUIScheduler = Executors.newScheduledThreadPool(UI_SCHEDULER_THREAD_COUNT, CommonUtils.createThreadFactory("UIScheduler", true));
		return sUIScheduler;
	}


	public static ScheduledExecutorService createScheduledExecutorService(final PropertyContext context) {
		return createScheduledExecutorService(context.getProperty(REL_DESC_NAME), context.getProperty(REL_DESC_THREAD_COUNT), context.getProperty(REL_DESC_DEAMON));
	}

	private static ScheduledExecutorService createScheduledExecutorService(final IProperty<String> name, final IProperty<Integer> threadCount, final IProperty<Boolean> deamon) {
		return new PropertyControlledExecutorService(name, REL_DESC_ASYNC.createNewInstance(), IProperty.of(true), threadCount, deamon);
	}


	public static PropertyControlledExecutorService createExecutorService(final String name) {
		return createExecutorService(name, true, 1, true);
	}

	public static PropertyControlledExecutorService createExecutorService(final String name, final boolean async, final int threadCount, final boolean daemon) {
		return new PropertyControlledExecutorService(IProperty.of(name), IProperty.of(async), IProperty.of(NO_SCHEDULER_SERVICE), IProperty.of(threadCount), IProperty.of(daemon));
	}

	public static PropertyControlledExecutorService createExecutorService(final String name, final IPropertyContext context) {
		return new PropertyControlledExecutorService(IProperty.of(name), context.getProperty(REL_DESC_ASYNC), IProperty.of(NO_SCHEDULER_SERVICE), context.getProperty(REL_DESC_THREAD_COUNT), context.getProperty(REL_DESC_DEAMON));
	}

	public static PropertyControlledExecutorService createExecutorService(final IProperty<String> name, final IProperty<Boolean> async, final IProperty<Integer> threadCount, final IProperty<Boolean> daemon) {
		return new PropertyControlledExecutorService(name, async, IProperty.of(false), threadCount, daemon);
	}

}
