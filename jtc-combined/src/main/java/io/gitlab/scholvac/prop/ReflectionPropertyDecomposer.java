package io.gitlab.scholvac.prop;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import io.gitlab.scholvac.ReflectionManager;
import io.gitlab.scholvac.prop.PropertyDecomposer.IPropertyDecomposer;
import io.gitlab.scholvac.prop.impl.FunctionalProperty;
import io.gitlab.scholvac.rcore.RClassifier;
import io.gitlab.scholvac.rcore.RClassifier.RField;

public class ReflectionPropertyDecomposer implements IPropertyDecomposer {

	private static final ReflectionManager sOfficialReflectionManager = ReflectionManager.get();
	private static final ReflectionManager sInternalReflectionManager = new ReflectionManager();

	private static final class ReflectionProperty extends FunctionalProperty {

		private final RField mField;
		private final Object mInstance;

		public ReflectionProperty(final Object instance, final RField field) {
			super(field.getName(), () -> field.get(instance), field.isReadOnly() ? null : newValue -> field.set(instance, newValue));
			mField = field;
			mInstance = instance;
		}

		@Override
		public Class getType() {
			return mField.getType();
		}
	}





	@Override
	public Collection<IProperty<?>> decompose(final Object value) {
		if (value == null)
			return null;
		final RClassifier classifier = getClassifier(value);
		if (classifier == null)
			return null;

		final List<IProperty<?>> out = new ArrayList<>();
		final Collection<RField> fields = classifier.getAllFields();
		for (final RField field : fields) {
			if (IProperty.class.isAssignableFrom(field.getType())){
				out.add(IProperty.class.cast(field.get(value)));
			}else {
				final ReflectionProperty prop = new ReflectionProperty(value, field);
				out.add(prop);
			}
		}
		return out;
	}


	protected static RClassifier getClassifier(final Object value) {
		final Class<?> clazz = value.getClass();
		RClassifier cl = null;
		if (sOfficialReflectionManager.containsClassifier(clazz))
			cl = sOfficialReflectionManager.getClassifier(clazz);
		if (cl == null) {
			if (false == sInternalReflectionManager.containsClassifier(clazz))
				sInternalReflectionManager.register(clazz);
			cl = sInternalReflectionManager.getClassifier(clazz);
		}
		return cl;
	}
}
