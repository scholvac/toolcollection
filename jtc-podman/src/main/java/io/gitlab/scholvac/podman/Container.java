package io.gitlab.scholvac.podman;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Consumer;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.log4j.Log4j;

@Getter
@RequiredArgsConstructor
@Log4j
@ToString
public class Container {


	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Description{
		private String 		name;
		private String 		image;

		private String 		hostname;
		private String 		network;
		private boolean 	privileged = false;
		private String[]	publish; //[[ip:][hostPort]:]containerPort[/protocol]
		private String 		restart; //no, on-failure[:max_retries], always, unless-stopped
		private boolean 	remove;
		private String[] 	volumes; //[source-volume|host-dir:]container-dir[:options]  - Options: rw|ro...
		private String[] 	envs;
		private String		workdir;
		private Map<String,String> 	labels;

		public void addEnvironmentVariable(final String name, final String val) {
			addEnvironmentVariable(name+"="+val);
		}
		public void addEnvironmentVariable(final String string) {
			if (envs == null)
				envs = new String[] {string};
			else {
				envs = Arrays.copyOf(envs, envs.length+1);
				envs[envs.length-1] = string;
			}
		}

		public void addLabel(final String key, final String value) {
			if (labels == null) labels = new HashMap<>();
			labels.put(key, value);
		}
		public void addVolume(final String source, final String guest) {
			final String str = source + ":" + guest;
			addVolume(str);
		}
		public void addVolume(final String str) {
			if (volumes == null) {
				volumes = new String[] {str};
			}else {
				volumes = Arrays.copyOf(volumes, volumes.length+1);
				volumes[volumes.length-1] = str;
			}
		}

		public void publish(final int host, final int guest) {
			final String pub = host+":"+guest;
			publish(pub);
		}

		private void publish(final String pub) {
			if (publish == null)
				publish = new String[] {pub};
			else {
				publish = Arrays.copyOf(publish, envs.length+1);
				publish[publish.length-1] = pub;
			}

		}
	}

	public enum State{
		created, exited, paused, running, stopping, stopped, unknown
	}
	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Status {
		@Data
		@Builder
		@NoArgsConstructor
		@AllArgsConstructor
		public static class Port {
			private String host_ip;
			private int container_port;
			private int host_port;
			private int range;
			private String protocol;
		}
		@Data
		@Builder
		@NoArgsConstructor
		@AllArgsConstructor
		public static class Size{
			private int rootFsSize;
			private int rwSize;
		}
		private String AutoRemove;
		private String[] Command;
		private String Created;
		private String CreatedAt;
		private String CreatedHuman;
		private String ExitCode;
		private String Exited;
		private String ExitedAt;
		private String Id;
		private String Image;
		private String ImageID;
		private boolean IsInfra;
		private Map<String,String> Labels;
		private String[] Mounts;
		private String[] Names;
		private String[] Networks;
		//		private String[] Namespaces;
		private String Pid;
		private String Pod;
		private String PodName;
		private Port[] Ports;
		private String RunningFor;
		private Size Size;
		private String StartedAt;
		private State State;
		private String Status;


		public String getName() { return (Names != null && Names.length > 0) ? Names[0] : null;		}
	}

	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class SubNet {
		String subnet;
		String gateway;
	}

	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Network {
		String name;
		String id;
		String driverM;
		String network_interface;
		String created;
		SubNet[] subnets;
		boolean ipv6_enabled;
		boolean internal;
		boolean dns_enabled;
		Map<String, String> ipam_options;
	}

	@AllArgsConstructor
	@NoArgsConstructor
	@Data
	public static class Log{
		@Getter
		private List<String> logLines;
	}

	private Status			status;
	@Setter
	private Description		description;
	private Log				log;

	public Container(final Status s) { status = s;}

	private List<Consumer<Container>> changeListener = new ArrayList<>();

	public boolean isValid() {
		return getId() != null && getId().isEmpty() == false;
	}

	public String getId() {
		return getStatus() != null ? getStatus().getId() : null;
	}


	public boolean update(final Container currentContainer, final Log logs) {
		boolean changed = false;
		if (false == Objects.deepEquals(status, currentContainer.getStatus())){
			changed = true;
			status = currentContainer.status;
		}
		if (logs != null) {
			this.log = logs;
			changed = true;
		}
		if (changed && changeListener.isEmpty() == false)
			changeListener.forEach(l -> l.accept(Container.this));

		return changed;
	}

	public String getLabel(final String key) {
		if (getStatus() == null) return null;
		return getStatus().getLabels().get(key);
	}

	public String getName() { return getStatus() != null ? getStatus().getName() : null; }


}
