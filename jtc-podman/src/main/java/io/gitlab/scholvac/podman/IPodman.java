package io.gitlab.scholvac.podman;

import java.util.Arrays;
import java.util.Collection;

import io.gitlab.scholvac.podman.Container.Description;
import io.gitlab.scholvac.podman.Container.Log;
import io.gitlab.scholvac.podman.Container.Network;
import io.gitlab.scholvac.podman.Container.State;

public interface IPodman {

	boolean start();
	boolean stop();

	Container run(final Description desc);
	Container find(final Description desc);

	Container stop(final Container container);
	Container remove(final Container container);

	Collection<Container> getAllContainer();
	Container update(final Container toBeUpdated, boolean updateLog);

	boolean createNetwork(String name, String driver, boolean ipv6Enabled, boolean internal, String[] dnsServer);
	Collection<Network> getNetworks();

	Log readLogs(Container toBeUpdated, int numberOfLines);



	default Network getNetworkByName(final String name) {
		return getNetworks().stream()
				.filter(it -> name.equals(it.getName()))
				.findFirst()
				.orElse(null);
	}
	default Network getNetworkByID(final String id) {
		return getNetworks().stream()
				.filter(it -> id.equals(it.getId()))
				.findFirst()
				.orElse(null);
	}

	default Container findById(final String id) {
		return getAllContainer().stream()
				.filter(it -> it.getStatus().getId().equals(id))
				.findFirst()
				.orElse(null);
	}
	default Container findByName(final String name) {
		return getAllContainer().stream()
				.filter(it -> Arrays.asList(it.getStatus().getNames()).contains(name))
				.findFirst()
				.orElse(null);
	}
	default Container stopAndRemove(final Container container) {
		if (container == null) return null;
		if (container.getStatus().getState() == State.running)
			stop(container);
		remove(container);
		return container;
	}

}