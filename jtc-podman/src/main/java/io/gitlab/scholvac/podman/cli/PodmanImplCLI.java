package io.gitlab.scholvac.podman.cli;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.buildobjects.process.ExternalProcessFailureException;
import org.buildobjects.process.ProcBuilder;
import org.buildobjects.process.ProcResult;

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import io.gitlab.scholvac.log.ILogger;
import io.gitlab.scholvac.log.SLog;
import io.gitlab.scholvac.podman.Container;
import io.gitlab.scholvac.podman.Container.Description;
import io.gitlab.scholvac.podman.Container.Log;
import io.gitlab.scholvac.podman.Container.Network;
import io.gitlab.scholvac.podman.Container.Status;
import io.gitlab.scholvac.podman.IPodman;

public class PodmanImplCLI implements IPodman{
	private static final ILogger		log = SLog.getLogger("PodmanCli");

	private static final int 			DEFAULT_TIMEOUT_MILLIS = 15_000;
	private static final int 			OK_EXIT_VALUE = 0;

	private ObjectMapper mJSONMapper;


	@Override
	public boolean start() {
		try {
			final ProcResult res = new ProcBuilder("podman", "machine", "start")
					.withTimeoutMillis(DEFAULT_TIMEOUT_MILLIS)
					.run();
			System.out.println("Start - Result = " + res.getExitValue());
			return true;
		}catch(final ExternalProcessFailureException epfe) {
			if (epfe.getStderr().contains(""))
				return true;
			epfe.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean stop() {
		try {
			final ProcResult res = new ProcBuilder("podman", "machine", "stop")
					.withTimeoutMillis(DEFAULT_TIMEOUT_MILLIS)
					.run();
			System.out.println("Start - Result = " + res.getExitValue());
			return true;
		}catch(final ExternalProcessFailureException epfe) {
			if (epfe.getStderr().contains(""))
				return true;
			epfe.printStackTrace();
		}
		return false;
	}

	@Override
	public Container run(final Description pod) {
		ProcBuilder builder = new ProcBuilder("podman", "run")
				.withNoTimeout()
				.withArg("--detach")
				;

		if (pod.getName() != null)
			builder = builder.withArgs("--name", pod.getName());
		if (pod.getHostname() != null)
			builder = builder.withArgs("--hostname", pod.getHostname());
		if (pod.getPublish() != null)
			for (int i = 0; i < pod.getPublish().length; i++)
				builder = builder.withArgs("--publish", pod.getPublish()[i]);
		if (pod.getVolumes() != null)
			for (int i = 0; i < pod.getVolumes().length; i++)
				builder = builder.withArgs("--volume", pod.getVolumes()[i]);
		if (pod.isPrivileged())
			builder = builder.withArg("--privileged");
		if (pod.isRemove())
			builder = builder.withArg("--rm");
		if (pod.getEnvs() != null)
			for (int i = 0; i < pod.getEnvs().length; i++)
				builder = builder.withArgs("-e", pod.getEnvs()[i]);
		if (pod.getNetwork() != null) {
			checkAndCreateNetwork(pod.getNetwork());
			builder = builder.withArgs("--network", pod.getNetwork());
		}
		if (pod.getWorkdir() != null)
			builder = builder.withArgs("--workdir", pod.getWorkdir());
		if (pod.getLabels() != null && pod.getLabels().isEmpty() == false) {
			for (final Entry<String, String> e : pod.getLabels().entrySet()) {
				builder = builder.withArg("-l").withArg(e.getKey() + "=" + e.getValue());
			}
		}


		final ProcBuilder theBuilder = builder.withArg(pod.getImage());
		log.debug("Run podman {}", theBuilder.getCommandLine());
		final ProcResult res = theBuilder.run();
		final String id = res.getOutputString().trim();//.substring(0, 12);

		Container c = findById(id);
		if (c == null)
			c = findByName(pod.getName());
		if (c != null)
			c.setDescription(pod);
		return c;
	}

	private void checkAndCreateNetwork(final String name) {
		final Network network = getNetworkByName(name);
		if (network == null) {
			createNetwork(name, "bridge", false, false, null);
		}
	}

	@Override
	public boolean createNetwork(final String name, final String driver, final boolean ipv6Enabled, final boolean internal, final String[] dnsServer) {
		ProcBuilder builder = new ProcBuilder("podman", "network", "create")
				.withArgs("-d", driver)
				.withNoTimeout();
		if (ipv6Enabled)
			builder = builder.withArg("--ipv6");
		if (internal)
			builder = builder.withArg("--internal");
		if (dnsServer != null && dnsServer.length > 0)
			builder = builder.withArg("--dns " + String.join(",", dnsServer));
		builder= builder.withArg(name);
		System.out.println(builder.getCommandLine());
		final ProcResult res = builder.run();
		if (res.getExitValue() == OK_EXIT_VALUE)
			return true;
		return false;
	}

	@Override
	public Container stop(final Container container) {
		if (container == null || !container.isValid())
			return null;
		final ProcBuilder builder = new ProcBuilder("podman", "stop", container.getId())
				.withNoTimeout();
		final ProcResult res = builder.run();
		if (res.getExitValue() == OK_EXIT_VALUE)
			return container;
		return null;
	}

	@Override
	public Container remove(final Container container) {
		if (container == null || !container.isValid())
			return null;
		//check if the container still exists, otherwise
		final ProcBuilder builder = new ProcBuilder("podman", "rm", container.getId())
				.withNoTimeout()
				.withExpectedExitStatuses(0, 1)
				;
		final ProcResult res = builder.run();
		if (res.getExitValue() == OK_EXIT_VALUE)
			return container;
		return null;
	}

	@Override
	public Container find(final Description desc) {
		if (desc == null) return null;
		return findByName(desc.getName());
	}


	@Override
	public Collection<Container> getAllContainer() {
		final ProcBuilder pb = new ProcBuilder("podman", "ps", "-a", "--size", "--format=json")
				.withNoTimeout();
		final ProcResult res = pb.run();

		final String json = res.getOutputString();
		try {
			final Status[] status = getJSONMapper().readValue(json, Container.Status[].class);
			return Stream.of(status).map(Container::new).collect(Collectors.toList());
		} catch (final JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
	}

	public Map Inspect(final Container c){
		final ProcBuilder pb = new ProcBuilder("podman", "inspect", "--format=json", c.getId());
		final ProcResult res = pb.run();

		final String json = res.getOutputString();
		try {
			final Map[] status = getJSONMapper().readValue(json, Map[].class);
			return status[0];
		} catch (final JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Collection<Network> getNetworks() {
		final ProcBuilder builder = new ProcBuilder("podman", "network", "ls", "--format=json").withNoTimeout() ;
		final ProcResult res = builder.run();

		final String json = res.getOutputString();
		try {
			final Network[] status = getJSONMapper().readValue(json, Container.Network[].class);
			return Stream.of(status).collect(Collectors.toList());
		} catch (final JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
	}


	protected ObjectMapper getJSONMapper() {
		if (mJSONMapper == null){
			mJSONMapper = new ObjectMapper(new JsonFactory());

			mJSONMapper.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);
			mJSONMapper.setVisibility(PropertyAccessor.GETTER, Visibility.NONE);
			mJSONMapper.setVisibility(PropertyAccessor.SETTER, Visibility.NONE);

			mJSONMapper.enable(SerializationFeature.INDENT_OUTPUT);
			mJSONMapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			mJSONMapper.enable(DeserializationFeature.FAIL_ON_INVALID_SUBTYPE);
			mJSONMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		}
		return mJSONMapper;
	}

	@Override
	public Container update(final Container toBeUpdated, final boolean updateLog) {
		final String id = toBeUpdated.getId();
		if (id == null)
			return null;
		final Container currentContainer = getAllContainer().stream().filter(it -> id.equals(it.getId())).findFirst().orElse(null);
		if (currentContainer == null)
			return null;

		Log logs = null;
		if (updateLog) {
			logs = readLogs(toBeUpdated, 8000);
		}
		toBeUpdated.update(currentContainer, logs);

		return toBeUpdated;
	}

	@Override
	public Log readLogs(final Container toBeUpdated, final int numberOfLines) {
		final ProcBuilder builder = new ProcBuilder("podman", "logs", "--tail", ""+numberOfLines, toBeUpdated.getId()).withNoTimeout() ;
		System.out.println(builder.getCommandLine());
		final ProcResult res = builder.run();
		final Log l = new Log(Arrays.asList(res.getOutputString().split("\n")));
		return l;
	}

	public static void main(final String[] args) {
		final IPodman ip = new PodmanImplCLI();
		final Collection<Container> cont = ip.getAllContainer();
		System.out.println("fertig");
	}

}
