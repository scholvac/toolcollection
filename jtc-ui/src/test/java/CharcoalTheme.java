


import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.metal.DefaultMetalTheme;

/**
 * This class describes a theme using gray colors.
 *
 * @author Steve Wilson
 */
public class CharcoalTheme extends DefaultMetalTheme {

	@Override
	public String getName() { return "Charcoal"; }

	private final ColorUIResource primary1 = new ColorUIResource(66, 33, 66);
	private final ColorUIResource primary2 = new ColorUIResource(90, 86, 99);
	private final ColorUIResource primary3 = new ColorUIResource(99, 99, 99);

	private final ColorUIResource secondary1 = new ColorUIResource(0, 0, 0);
	private final ColorUIResource secondary2 = new ColorUIResource(51, 51, 51);
	private final ColorUIResource secondary3 = new ColorUIResource(102, 102, 102);

	private final ColorUIResource black = new ColorUIResource(222, 222, 222);
	private final ColorUIResource white = new ColorUIResource(0, 0, 0);

	@Override
	protected ColorUIResource getPrimary1() { return primary1; }
	@Override
	protected ColorUIResource getPrimary2() { return primary2; }
	@Override
	protected ColorUIResource getPrimary3() { return primary3; }

	@Override
	protected ColorUIResource getSecondary1() { return secondary1; }
	@Override
	protected ColorUIResource getSecondary2() { return secondary2; }
	@Override
	protected ColorUIResource getSecondary3() { return secondary3; }

	@Override
	protected ColorUIResource getBlack() { return black; }
	@Override
	protected ColorUIResource getWhite() { return white; }

}
