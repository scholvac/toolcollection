

import java.io.File;

import io.gitlab.scholvac.anno.uml.ReflectionSupport;

public class DemoStructures {

	public static class PrivateSingleClass {
		@ReflectionSupport
		private int			integerValue;
		@ReflectionSupport
		private boolean		booleanValue;
		@ReflectionSupport
		private String		stringValue;
		@ReflectionSupport
		private File		fileValue = new File(".");
	}

	public static class RecursiveClass {
		@ReflectionSupport
		private int 			id;
		@ReflectionSupport
		private String			name;
		@ReflectionSupport
		double 					value;

		float					privateValue;

		@ReflectionSupport
		private RecursiveClass	parent;
		@ReflectionSupport
		private RecursiveClass	left;
		@ReflectionSupport
		private RecursiveClass	right;
	}


	public static RecursiveClass createTree(final int layer) {
		return createTree(layer, "Root", 0, null);
	}
	private static RecursiveClass createTree(final int layer, final String nPrefix, final int id, final RecursiveClass p) {
		final RecursiveClass rc = new RecursiveClass();
		rc.id = id;
		rc.name = nPrefix + "_" + rc.id;
		rc.value = id;
		rc.parent = p;

		if (layer > 0) {
			rc.left = createTree(layer-1, "left_", id+1, rc);
			rc.right = createTree(layer-1, "right_", id+1, rc);
		}
		return rc;
	}
}
