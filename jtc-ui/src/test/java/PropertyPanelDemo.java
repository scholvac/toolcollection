

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;

import io.gitlab.scholvac.QualifiedName;
import io.gitlab.scholvac.prop.IProperty;
import io.gitlab.scholvac.prop.IPropertyContext;
import io.gitlab.scholvac.prop.PropertyDescriptor;
import io.gitlab.scholvac.prop.impl.PropertyContext;
import io.gitlab.scholvac.quant.Acceleration;
import io.gitlab.scholvac.quant.Angle;
import io.gitlab.scholvac.quant.Duration;
import io.gitlab.scholvac.quant.Frequency;
import io.gitlab.scholvac.quant.Information;
import io.gitlab.scholvac.quant.Length;
import io.gitlab.scholvac.quant.TimeStamp;
import io.gitlab.scholvac.ui.IUIElementFactory;
import io.gitlab.scholvac.ui.prop.PropertySheet;
import io.gitlab.scholvac.ui.prop.TabbedPropertySheet;
import io.gitlab.scholvac.ui.prop.TreePropertySheet;
import io.gitlab.scholvac.ui.prop.edt.impl.NumberEditor;

public class PropertyPanelDemo {

	public enum DemoEnumeration {
		DEMO_1, DEMO_2, DEMO_3, DEMO_4
	}

	public static void setupPropertyStore(final IPropertyContext ps) {
		setFqn(ps, "Root:PropertySheed:Basic:Boolean", "Boolean Property", true, true)
		.annotate(PropertySheet.ANNOTATION__HEADER, "Basic");
		setFqn(ps, "Root:PropertySheed:Basic:Byte", "Byte Property", true, (byte)42);
		setFqn(ps, "Root:PropertySheed:Basic:Short", "Short Property", true, (short)42);
		setFqn(ps, "Root:PropertySheed:Basic:Integer-RO", "Integer Property - Readonly", false, 42);
		setFqn(ps, "Root:PropertySheed:Basic:Long", "long Property", true, (long)42);
		setFqn(ps, "Root:PropertySheed:Basic:Double", "Double Property", true, 42.42);
		setFqn(ps, "Root:PropertySheed:Basic:Float", "Float Property", true, 42.42f);
		setFqn(ps, "Root:PropertySheed:Basic:String", "String Property", true, "Hello PropertySheet");
		setFqn(ps, "Root:PropertySheed:Basic:DemoEnum", "Demo Enumeration Property", true, DemoEnumeration.DEMO_1);

		setFqn(ps, "Root:PropertySheed:Java:Date", "Date Property", true, new Date());
		setFqn(ps, "Root:PropertySheed:Java:Color", "Color Property", true, Color.RED)
		.annotate(PropertySheet.ANNOTATION__HEADER, "Java");
		setFqn(ps, "Root:PropertySheed:Java:File", "File Property", true, new File("./.classpath"));
		setFqn(ps, "Root:PropertySheed:Java:Point2D", "Point2D Property", true, new Point2D.Double(1, 2));
		setFqn(ps, "Root:PropertySheed:Java:Rectangle", "Rectangle Property", true, new Rectangle(0, 0, 10, 10));

		setFqn(ps, "Root:PropertySheed:Measurement:Acceleration", "Acceleration Property", true, new Acceleration(1, Acceleration.G_FORCE))
		.annotate(PropertySheet.ANNOTATION__HEADER, "Measurements");
		setFqn(ps, "Root:PropertySheed:Measurement:Angle", "Angle Property", true, Angle.deg(10));
		setFqn(ps, "Root:PropertySheed:Measurement:Duration", "Duration Property", true, Duration.sec(17));
		setFqn(ps, "Root:PropertySheed:Measurement:Frequency", "Frequency Property", true, Frequency.hz(10));
		setFqn(ps, "Root:PropertySheed:Measurement:Information", "Information Property", true, Information.mb(10));
		setFqn(ps, "Root:PropertySheed:Measurement:Length", "Length Property", true, Length.meter(10));
		setFqn(ps, "Root:PropertySheed:Measurement:Timestamp", "Timestamp Property", true, TimeStamp.epoch(0));

		setFqn(ps, "Root:PropertySheed:Constraint:Float-minMax", "Float Property With min (20) and max (40)", true, 42.42f);
		ps.getPropertyFQN("Root:PropertySheed:Constraint:Float-minMax").annotate(NumberEditor.ANNOTATION__MIN_VAULE, 20);
		ps.getPropertyFQN("Root:PropertySheed:Constraint:Float-minMax").annotate(NumberEditor.ANNOTATION__MAX_VALUE, 40);
		setFqn(ps, "Root:PropertySheed:Constraint:Float-AllowNull", "Float Property that may be null", true, 1f);
		ps.getPropertyFQN("Root:PropertySheed:Constraint:Float-AllowNull").annotate(NumberEditor.ANNOTATION__ALLOW_NULL);
		ps.getPropertyFQN("Root:PropertySheed:Constraint:Float-AllowNull").annotate(PropertySheet.ANNOTATION__HEADER, "Constraints");
		ps.getPropertyFQN("Root:PropertySheed:Constraint:Float-AllowNull").set(null);
		setFqn(ps, "Root:PropertySheed:Constraint:Float-Formated", "Float Property with a format indicator", true, 1f);
		ps.getPropertyFQN("Root:PropertySheed:Constraint:Float-Formated").annotate(NumberEditor.ANNOTATION__ALLOW_NULL); //to see the format indication
		ps.getPropertyFQN("Root:PropertySheed:Constraint:Float-Formated").annotate(NumberEditor.ANNOTATION__FORMAT, "00000.000##");
		ps.getPropertyFQN("Root:PropertySheed:Constraint:Float-Formated").set(null);


		setFqn(ps, "Root:PropertySheed:Complex:Simple1", "Complex structure", true, new DemoStructures.PrivateSingleClass());
		setFqn(ps, "Root:PropertySheed:Complex:Simple2", "Complex structure2", true, new DemoStructures.PrivateSingleClass());
		setFqn(ps, "Root:PropertySheed:Complex:Recursive", "Recursive...", true, DemoStructures.createTree(5));
		//
		//		setFqn(ps, "Root:PropertySheed:Arrays:PrimitiveArray", "Primitive Array", true, new int[] {1,2,3});
		//		setFqn(ps, "Root:PropertySheed:Arrays:ComplexArray", "Complex Array", true, new PrivateSingleClass[] {new PrivateSingleClass()});
		//		//		setFqn(ps, "Root:PropertySheed:Arrays:RecursiveArray", "Recursive", true, new RecursiveClass[] { DemoStructures.createTree(2)});
		//
		//		setFqn(ps, "Root:PropertySheed:Lists:PrimitiveList", "Primitive List", true, Arrays.asList(1, 2, 3));
		//		setFqn(ps, "Root:PropertySheed:Lists:ComplexList", "Complex List", true, Arrays.asList(new PrivateSingleClass()));
		//		//		setFqn(ps, "Root:PropertySheed:Lists:RecursiveArray", "Recursive", true, Arrays.asList(DemoStructures.createTree(2)));
		//
		//		setFqn(ps, "Root:PropertySheed:Sets:PrimitiveSet", "Primitive Set", true, Arrays.asList(1, 2, 3));
		//		setFqn(ps, "Root:PropertySheed:Sets:ComplexSet", "Complex", true, new HashSet<>(Arrays.asList(new PrivateSingleClass())));
		//		//		setFqn(ps, "Root:PropertySheed:Sets:RecursiveSet", "Recursive", true, new HashSet<>(Arrays.asList(DemoStructures.createTree(2))));

	}
	private static <T> IProperty<T> setFqn(final IPropertyContext ps, final String name, final String desc, final boolean editable, final T value) {
		final PropertyDescriptor<T> d = PropertyDescriptor.createFQN(QualifiedName.fromSeperator(name), desc, editable, value);
		final IProperty<T> prop = ps.getProperty(d);
		if (editable) {
			prop.set(value);
		}
		return prop;
	}
	public static void main(final String[] args) {
		final JFrame frame = new JFrame("New Number Editor Demo");
		//		final MetalTheme theme = new CharcoalTheme();
		//		try {
		//			MetalLookAndFeel.setCurrentTheme(theme);
		//			UIManager.setLookAndFeel(MetalLookAndFeel.class.getName());
		//			if (frame != null) {
		//				SwingUtilities.updateComponentTreeUI(frame);
		//			} else {
		//				SwingXUtilities.updateAllComponentTreeUIs();
		//			}
		//		} catch (final Exception e1) {
		//			e1.printStackTrace();
		//		}
		final IUIElementFactory factory = IUIElementFactory.INSTANCE;

		final IPropertyContext ps = new PropertyContext();
		setupPropertyStore(ps);

		final PropertyContext sheetConfig = new PropertyContext("SheetConfig");


		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout(0, 0));

		final JTabbedPane tabbedPane = new JTabbedPane(SwingConstants.TOP);
		frame.getContentPane().add(tabbedPane);

		final PropertySheet sheet = new PropertySheet(factory, null, sheetConfig);
		tabbedPane.addTab("Property Sheet", null, new JScrollPane(sheet), null);
		final IProperty[] sheetProperties = ps.getContext(QualifiedName.from("Root", "PropertySheed")).getAllProperties(true).toArray(IProperty[]::new);
		sheet.asyncUpdateProperties(sheetProperties);


		final TabbedPropertySheet tabbedSheet = new TabbedPropertySheet(factory, null, sheetConfig);
		tabbedPane.addTab("Tabbed Sheet", null, tabbedSheet, null);
		final List<IPropertyContext> contexts = ps.getAllContexts(true).collect(Collectors.toList());
		tabbedSheet.asyncUpdateProperties(contexts);

		final TreePropertySheet treeSheet = new TreePropertySheet(factory, null, sheetConfig);
		tabbedPane.addTab("Tree Sheet", null, treeSheet, null);
		treeSheet.asyncUpdateProperties(ps.getContext("Root"));

		final PropertySheet psConfig = new PropertySheet(factory, null, new PropertyContext());
		psConfig.syncUpdateProperties(sheetConfig.getAllProperties(true).toArray(IProperty[]::new));
		frame.getContentPane().add(new JScrollPane(psConfig), BorderLayout.NORTH);

		tabbedPane.setSelectedIndex(0);
		frame.setSize(640, 880);
		frame.setVisible(true);
	}
}
