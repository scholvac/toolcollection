package io.gitlab.scholvac.ui.prop.edt.impl;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.function.Function;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.WindowConstants;

import io.gitlab.scholvac.IDisposeable;
import io.gitlab.scholvac.StringUtils;
import io.gitlab.scholvac.func.FunctionUtils;
import io.gitlab.scholvac.prop.IProperty;
import io.gitlab.scholvac.ui.IUIElementFactory;
import io.gitlab.scholvac.ui.LineEdit;

public class TextEditor<ValueT> extends AbstractPropertyEditor<ValueT> {

	public static final String ANNOTATION__ALLOW_NULL					= "AllowNull";
	public static final String ANNOTATION__PROMT_TEXT 					= "PromtText";
	public static final String NULL_INDICATOR 							= "null";




	protected LineEdit<ValueT> 		mLineEdt;
	private IDisposeable			mValueDisposeable;


	public TextEditor(final IProperty<ValueT> prop, final Function<IProperty<?>, IProperty<?>> propertyWrapper, final Function<String, ValueT> toContent, final Function<ValueT, String> toString) {
		super(prop, propertyWrapper);
		mLineEdt = new LineEdit<>(LineEdit.DEFAULT_VALIDATION_DELAY, wrapToContent(toContent), toString);
		addConditions(prop, mLineEdt);
		mLineEdt.getPromt().setText(getPromtText());
	}
	protected Function<String, ValueT> wrapToContent(Function<String, ValueT> toContent) {
		final IProperty<ValueT> property = getProperty();
		toContent = handleNullValueAnnotation(property, toContent);
		return toContent;
	}

	protected void addConditions(final IProperty<ValueT> prop, final LineEdit<ValueT> edt) {

	}
	private <NumberT> Function<String, NumberT> handleNullValueAnnotation(final IProperty<NumberT> prop, final Function<String, NumberT> func){
		if (prop.hasAnnotation(ANNOTATION__ALLOW_NULL)) {
			return FunctionUtils.ifThenElse(cond -> NULL_INDICATOR.equalsIgnoreCase(cond) || StringUtils.isNullOrEmpty(cond), str_true -> null, func);
		}
		return func;
	}
	protected String getPromtText() {
		final String annoText = getProperty().getAnnotation(ANNOTATION__PROMT_TEXT);
		if (annoText != null)
			return annoText;
		final Class<ValueT> clazz = getProperty().getType();
		if (clazz != null)
			return "new " + clazz.getSimpleName();
		return "";
	}

	@Override
	protected Component createEditor(final IProperty<ValueT> property, final IUIElementFactory factory) {
		mLineEdt.addContentListener(this::updateFromEditor);
		return mLineEdt.getComponent();
	}

	private void updateFromEditor(final ValueT newValue) {
		runInThisThread(() -> set(newValue));
	}
	private void updateFromProperty(final ValueT newValue) {
		if (mLineEdt.hasFocus() == false)
			runInThisThread(() -> mLineEdt.setValue(newValue));
	}

	@Override
	protected void onEditorShown(final Component editor, final IProperty<ValueT> property) {
		mValueDisposeable = property.addValueChangeListener(pcl -> {
			updateFromProperty(property.get());
		});
		updateFromProperty(property.get());
	}

	@Override
	protected void onEditorHidden(final Component editor, final IProperty<ValueT> property) {
		mValueDisposeable = IDisposeable.dispose(mValueDisposeable);
	}


	public static class StringEditor extends TextEditor<String> {
		public StringEditor(final IProperty<String> prop, final Function<IProperty<?>, IProperty<?>> propertyWrapper) {
			super(prop, propertyWrapper, str -> str, str -> str);
		}
	}


	public static void main(final String[] args) {
		final IProperty<String> strProp = IProperty.of("String", "String Property", true, "");

		final JFrame frame = new JFrame("New String Editor Demo");
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));

		final GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		frame.getContentPane().setLayout(gridBagLayout);

		final JLabel lblNewLabel = new JLabel("String");
		final GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 1;
		frame.getContentPane().add(lblNewLabel, gbc_lblNewLabel);

		final TextEditor edtByte = new StringEditor(strProp, null);
		final GridBagConstraints gbc_edtByte = new GridBagConstraints();
		gbc_edtByte.insets = new Insets(0, 0, 5, 0);
		gbc_edtByte.fill = GridBagConstraints.HORIZONTAL;
		gbc_edtByte.gridx = 1;
		gbc_edtByte.gridy = 1;
		frame.getContentPane().add(edtByte.getEditor(IUIElementFactory.INSTANCE), gbc_edtByte);

		final JLabel lblNewLabel_1 = new JLabel("String-Copy");
		final GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_1.gridx = 0;
		gbc_lblNewLabel_1.gridy = 2;
		frame.getContentPane().add(lblNewLabel_1, gbc_lblNewLabel_1);

		final TextEditor edtInteger = new StringEditor(strProp, null);
		final GridBagConstraints gbc_edtInteger = new GridBagConstraints();
		gbc_edtInteger.insets = new Insets(0, 0, 5, 0);
		gbc_edtInteger.fill = GridBagConstraints.HORIZONTAL;
		gbc_edtInteger.gridx = 1;
		gbc_edtInteger.gridy = 2;
		frame.getContentPane().add(edtInteger.getEditor(IUIElementFactory.INSTANCE), gbc_edtInteger);

		frame.setSize(640, 480);
		frame.setVisible(true);
	}
}
