package io.gitlab.scholvac.ui.impl;

import java.awt.Component;
import java.util.function.Function;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;

public class FunctionCellListRenderer<T> extends DefaultListCellRenderer {
	public final Function<T, String> mToString;
	public FunctionCellListRenderer(final Function<T, String> toStr) {
		mToString = toStr;
	}
	@Override
	public Component getListCellRendererComponent(final JList<?> list, final Object value, final int index, final boolean isSelected,
			final boolean cellHasFocus) {
		final Component c = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
		if (c != null && value != null) {
			if (c instanceof JLabel)
				((JLabel) c).setText(mToString.apply((T) value));
		}
		return c;
	}
}