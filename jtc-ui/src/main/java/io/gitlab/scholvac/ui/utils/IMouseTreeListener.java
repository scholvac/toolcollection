package io.gitlab.scholvac.ui.utils;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JTree;
import javax.swing.tree.TreePath;

public interface IMouseTreeListener {

	void mouseClicked(MouseEvent e, TreePath treePath);

	void mousePressed(MouseEvent e, TreePath treePath);

	void mouseReleased(MouseEvent e, TreePath treePath);


	public static class MouseTreeAdapter implements IMouseTreeListener {
		@Override
		public void mouseClicked(final MouseEvent e, final TreePath treePath) { }
		@Override
		public void mousePressed(final MouseEvent e, final TreePath treePath) { }
		@Override
		public void mouseReleased(final MouseEvent e, final TreePath treePath) { }
	}

	public class MouseTreeListenerImpl implements MouseListener{

		private final IMouseTreeListener			mDelegate;
		private final JTree							mTree;

		public MouseTreeListenerImpl(final IMouseTreeListener l, final JTree t) {
			mDelegate = l;
			mTree = t;
		}

		private TreePath getTreePath(final MouseEvent e)  {
			return mTree.getPathForLocation(e.getX(), e.getY());
		}
		@Override
		public void mouseClicked(final MouseEvent e) {
			mDelegate.mouseClicked(e, getTreePath(e));
		}

		@Override
		public void mousePressed(final MouseEvent e) {
			mDelegate.mousePressed(e, getTreePath(e));
		}

		@Override
		public void mouseReleased(final MouseEvent e) {
			mDelegate.mouseReleased(e, getTreePath(e));
		}

		@Override
		public void mouseEntered(final MouseEvent e) {
			//not supported
		}

		@Override
		public void mouseExited(final MouseEvent e) {
			//not yet supported
		}

	}

	static MouseTreeListenerImpl register(final JTree tree, final IMouseTreeListener listener) {
		final MouseTreeListenerImpl mtli = new MouseTreeListenerImpl(listener, tree);
		tree.addMouseListener(mtli);
		return mtli;
	}
}
