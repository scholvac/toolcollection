package io.gitlab.scholvac.ui;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.font.TextAttribute;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.DocumentFilter;
import javax.swing.text.JTextComponent;

import io.gitlab.scholvac.Debouncer;
import io.gitlab.scholvac.IDisposeable;
import io.gitlab.scholvac.prop.IProperty;
import io.gitlab.scholvac.prop.IPropertyContext;
import io.gitlab.scholvac.prop.PropertyDescriptor;
import io.gitlab.scholvac.prop.impl.PropertyContext;
import io.gitlab.scholvac.ui.utils.UIUtils;
import io.gitlab.scholvac.validate.IValidator;
import io.gitlab.scholvac.validate.IValidator.SEVERITY;
import io.gitlab.scholvac.validate.IValidator.ValidationMessage;
import io.gitlab.scholvac.validate.IValidator.ValidationResult;

public class LineEdit<CONTENT_TYPE> {

	/** Default time in milliseconds, to wait for a new user input, before start to validate the input */
	public static long DEFAULT_VALIDATION_DELAY = 300;

	public static final PropertyDescriptor<Color> DESC_ERROR_PROMT_COLOR = PropertyDescriptor.create("Error Promt Color", Color.RED)
			.description("Color of warning messages, shown with the message promt");
	public static final PropertyDescriptor<Font> DESC_ERROR_PROMT_FONT = PropertyDescriptor.create("Error Promt Font", Font.class)
			.defaultValue(() -> UIUtils.getDefaultTextPromtFont().deriveFont(Collections.singletonMap( TextAttribute.WEIGHT, TextAttribute.WEIGHT_BOLD)))
			.description("Font to use when showing an error with the message promt");

	public static final PropertyDescriptor<Color> DESC_WARN_PROMT_COLOR = PropertyDescriptor.create("Warning Promt Color", Color.RED)
			.description("Color of warning messages, shown with the message promt");
	public static final PropertyDescriptor<Font> DESC_WARN_PROMT_FONT = PropertyDescriptor.create("Warning Promt Font", Font.class)
			.defaultValue(() -> UIUtils.getDefaultTextPromtFont().deriveFont(Collections.singletonMap( TextAttribute.POSTURE, TextAttribute.POSTURE_OBLIQUE)))
			.description("Font to use when showing an warning with the message promt");

	interface IErrorHandler {

	}

	static class ValueResult<T> extends ValidationResult {
		T value;
	}

	private final JTextComponent				mComponent;

	private boolean 							mApplyIntermediateResults = true;

	private Function<CONTENT_TYPE, String> 		mToString;
	private Function<String, CONTENT_TYPE> 		mToContent;
	private List<Function<String, String>>		mMapper;

	private IErrorHandler						mErrorHandler;

	private List<IValidator<String>>			mTextValidators = null;
	private List<IValidator<CONTENT_TYPE>>		mContentValidators = null;
	private List<Consumer<CONTENT_TYPE>>		mContentListener = null;

	private final Debouncer<String, String> 	mDebouncer;
	private final TextPrompt					mTextPrompt;
	private IPropertyContext 					mContext;

	private String 								mShadowToolTip;
	private Font 								mShadowFont;
	private Color 								mShadowForeground;

	private String 								mLastText;



	public LineEdit(final long liveValidationDelayMs, final Function<String, CONTENT_TYPE> toContent, final Function<CONTENT_TYPE, String> toString) {
		this(new JTextField(), liveValidationDelayMs, toContent, toString);
	}
	public LineEdit(final JTextComponent component, final long liveValidationDelayMs, final Function<String, CONTENT_TYPE> toContent, final Function<CONTENT_TYPE, String> toString) {
		mComponent = component;
		mToContent = toContent;
		mToString = toString;
		mDebouncer = new Debouncer<>(this::liveValidateOrApply, liveValidationDelayMs);
		mTextPrompt = new TextPrompt("", mComponent);
		mTextPrompt.setForeground(Color.GRAY);
		mTextPrompt.setFont(mComponent.getFont().deriveFont(Collections.singletonMap( TextAttribute.WEIGHT, TextAttribute.WEIGHT_BOLD))); //let's have a bold text promt

		mShadowFont = mComponent.getFont();
		mShadowForeground = mComponent.getForeground();


		final Document document = mComponent.getDocument();
		if (document instanceof AbstractDocument) {
			((AbstractDocument)document).setDocumentFilter(new DocumentFilter() {
				@Override
				public void insertString(final FilterBypass fb, final int offset, final String string, final AttributeSet attr) throws BadLocationException {
					final String filteredString = filterString(string);
					super.insertString(fb, offset, filteredString, attr);
				}
				@Override
				public void replace(final FilterBypass fb, final int offset, final int length, final String text, final AttributeSet attrs) throws BadLocationException {
					final String filteredString = filterString(text);
					super.replace(fb, offset, length, filteredString, attrs);
					mComponent.setCaretPosition(offset+filteredString.length());
				}
			});
		}
		document.addDocumentListener(new DocumentListener() {
			@Override public void removeUpdate(final DocumentEvent e) { changedUpdate(e); }
			@Override public void insertUpdate(final DocumentEvent e) { changedUpdate(e); }
			@Override
			public void changedUpdate(final DocumentEvent e) {
				try {
					final String newText = e.getDocument().getText(0, e.getDocument().getLength());
					mDebouncer.call("validate", newText);
				} catch (final BadLocationException e1) {
					e1.printStackTrace();
				}
			}
		});
		mComponent.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(final FocusEvent e) {
				applyInputChange(getText());
			}
		});
		if (mComponent instanceof JTextField) {
			((JTextField)mComponent).addActionListener(al -> applyInputChange(getText()));
		}
	}

	public String getText() {
		return mComponent.getText();
	}

	public void setText(final String text) {
		mComponent.setText(text);
	}

	public JTextComponent getComponent() { return mComponent;}

	/**
	 * Depending on {@link #mApplyIntermediateResults} either the apply input (incl. validation) or only the validation is called
	 * @param txt
	 */
	private void liveValidateOrApply(final String txt) {
		if (mApplyIntermediateResults)
			applyInputChange(txt);
		else
			liveValidate(txt);
	}

	protected void applyInputChange(final String text) {
		//If updated in Background and focus active (no updates forwarded to UI) do not send the old text.
		if (text != null && text.equals(mLastText))
			return ;
		final ValueResult<CONTENT_TYPE> validResult = liveValidate(text);

		if (mContentListener != null && validResult.has(SEVERITY.ERROR) == false) {
			for (int i = 0; i < mContentListener.size(); i++)
				mContentListener.get(i).accept(validResult.value);
		}
	}

	protected String filterString(final String input) {
		if (mMapper == null)
			return input;
		String res = input;
		for (int i = 0; i < mMapper.size(); i++)
			res = mMapper.get(i).apply(res);
		return res;
	}

	private ValueResult<CONTENT_TYPE> liveValidate(final String txt) {
		final ValueResult<CONTENT_TYPE> result = new ValueResult<>();

		if (mTextValidators != null)
			IValidator.validate(mTextValidators, txt, result);

		try {
			result.value = mToContent.apply(txt);
		}catch(final Throwable t) {
			result.add(SEVERITY.ERROR, t);
		}
		if (mContentValidators != null)
			IValidator.validate(mContentValidators, result.value, result);

		handleValidationResult(result);

		return result;
	}

	public CONTENT_TYPE getValue() {
		return liveValidate(getText()).value;
	}
	public void setValue(final CONTENT_TYPE content) {
		if (content != null){
			mLastText = mToString != null ? mToString.apply(content) : ""+content;
			setText(mLastText);
		}else {
			setText("");
		}
	}

	public IDisposeable addContentValidator(final IValidator<CONTENT_TYPE> validator) {
		if (mContentValidators == null) mContentValidators = new ArrayList<>();
		mContentValidators.add(validator);
		return IDisposeable.create(() -> removeContentValidator(validator));
	}
	public boolean removeContentValidator(final IValidator<CONTENT_TYPE> validator) {
		if (mContentValidators != null && mContentValidators.remove(validator)) {
			if (mContentValidators.isEmpty()) mContentValidators = null;
			return true;
		}
		return false;
	}
	public IDisposeable addContentListener(final Consumer<CONTENT_TYPE> consumer) {
		if (consumer != null) {
			if (mContentListener == null) mContentListener = new LinkedList<>();
			mContentListener.add(consumer);
			return IDisposeable.create(() -> removeContentListener(consumer));
		}
		return null;
	}
	public boolean removeContentListener(final Consumer<CONTENT_TYPE> consumer) {
		if (consumer != null && mContentListener != null) {
			if (mContentListener.remove(consumer)) {
				if (mContentListener.isEmpty()) mContentListener = null;
				return true;
			}
		}
		return false;
	}

	public void setFont(final Font f) {
		mShadowFont = f;
		mComponent.setFont(f);
	}

	public void setToolTipText(final String text) {
		mShadowToolTip = text;
		mComponent.setToolTipText(text);
	}

	public void setForeground(final Color fg) {
		mShadowForeground = fg;
		mComponent.setForeground(fg);
	}


	private void handleValidationResult(final ValidationResult result) {
		if (result == null || result.hasMessages() == false)
			clearResults();
		else
			showResults(result);
	}

	private void showResults(final ValidationResult result) {
		final List<ValidationMessage<?>> msgList = result.get(SEVERITY.ERROR);
		if (result.has(SEVERITY.ERROR)) {
			mComponent.setForeground(getErrorPromtColor().get());
			mComponent.setFont(getErrorPromtFont().get());
		}else if (result.has(SEVERITY.WARNING)) {
			mComponent.setForeground(getWarnPromtColor().get());
			mComponent.setFont(getWarnPromtFont().get());
		}else {
			mComponent.setForeground(mShadowForeground);
			mComponent.setFont(mShadowFont);
		}

		mComponent.setToolTipText(createTooltip(mShadowToolTip, result));
	}

	private String createTooltip(final String origTT, final ValidationResult result) {
		final StringBuilder sb = new StringBuilder();
		sb.append("<html>");
		result.get(SEVERITY.ERROR).stream().forEach(vr -> sb.append(listEntry(vr, getErrorPromtColor().get())));
		result.get(SEVERITY.WARNING).stream().forEach(vr -> sb.append(listEntry(vr, getWarnPromtColor().get())));
		result.get(SEVERITY.INFO).stream().forEach(vr -> sb.append(listEntry(vr, mShadowForeground)));
		sb.append("</html>");
		return sb.toString();
	}

	private String listEntry(final ValidationMessage<?> vr, final Color color) {
		return String.format("<li style=\"color:rgb(%d,%d,%d);\"> [%s] : %s </li>", color.getRed(), color.getGreen(), color.getBlue(), vr.getSeverity().name(), vr.getMessage());
	}

	protected IPropertyContext getPropertyContext() {
		if (mContext == null) mContext = new PropertyContext();
		return mContext;
	}

	protected IProperty<Color> getErrorPromtColor() {
		return getPropertyContext().getProperty(DESC_ERROR_PROMT_COLOR);
	}

	protected IProperty<Font> getErrorPromtFont() {
		return getPropertyContext().getProperty(DESC_ERROR_PROMT_FONT);
	}
	protected IProperty<Color> getWarnPromtColor() {
		return getPropertyContext().getProperty(DESC_WARN_PROMT_COLOR);
	}

	protected IProperty<Font> getWarnPromtFont() {
		return getPropertyContext().getProperty(DESC_WARN_PROMT_FONT);
	}

	private void clearResults() {
		SwingUtilities.invokeLater(() -> {
			mComponent.setToolTipText(mShadowToolTip);
			mComponent.setForeground(mShadowForeground);
			mComponent.setFont(mShadowFont);
		});
	}

	public LineEdit<CONTENT_TYPE> map(final Function<String, String> mapper){
		if (mMapper == null) mMapper = new ArrayList<>();
		mMapper.add(mapper);
		return this;
	}
	public LineEdit<CONTENT_TYPE> removeMapping(final Function<String, String> mapper){
		if (mapper != null && mMapper != null) {
			if (mMapper.remove(mapper) && mMapper.isEmpty())
				mMapper = null;
		}
		return this;
	}

	public TextPrompt getPromt() { return mTextPrompt;}

	public static void main(final String[] args) {
		final JFrame frame = new JFrame("LineEdit Demo");
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));

		final JTextField tf = new JTextField();
		final LineEdit<Double> dblEdt = new LineEdit<>(tf, 300, Double::parseDouble, dbl -> String.format("%1.5f", dbl));
		dblEdt.mTextPrompt.setText("Moinsen");
		dblEdt.map(str -> str.replace(",", "."));
		dblEdt.setValue(42.3);

		frame.add(dblEdt.getComponent());

		final LineEdit<String> dblEdt1 = new LineEdit<>(new JTextArea(), 300, it -> it, dbl -> dbl);
		dblEdt1.setPromtText("Insert some multi line text");
		frame.add(dblEdt1.getComponent());

		//		final LineEdit2<Double> dblEdt2 = new LineEdit2<>(new JTextArea(), 300, Double::parseDouble, dbl -> String.format("%1.5f", dbl));
		//		frame.add(dblEdt2.getComponent());



		frame.setSize(640, 480);
		frame.setVisible(true);

	}

	public void setPromtText(final String promtTxt) {
		mTextPrompt.setText(promtTxt);
	}
	public boolean hasFocus() {
		return mComponent.hasFocus();
	}


}
