package io.gitlab.scholvac.ui.utils;

import java.awt.Component;
import java.awt.event.HierarchyEvent;
import java.awt.event.HierarchyListener;
import java.util.function.Consumer;
import java.util.function.Supplier;

import io.gitlab.scholvac.IDisposeable;

public class VisibilityUtil {

	public interface IVisiblityListener<CompType extends Component> {
		void onShowing(final CompType component);
		void onHide(final CompType component);
	}

	public static <CompType extends Component> IDisposeable registerShowingListener(final CompType component, final Consumer<CompType> onShowing, final Consumer<CompType> onHide) {
		return registerShowingListener(component, new IVisiblityListener<CompType>() {
			@Override
			public void onShowing(final CompType component) {
				if (onShowing != null)
					onShowing.accept(component);
			}
			@Override
			public void onHide(final CompType component) {
				if (onHide != null)
					onHide.accept(component);
			}
		});
	}
	public static <CompType extends Component> IDisposeable whileVisible(final CompType component, final Supplier<IDisposeable> disposableSupplier) {
		return registerShowingListener(component, new IVisiblityListener<CompType>() {
			IDisposeable mDisposeable= null;
			@Override
			public void onShowing(final CompType component) {
				mDisposeable = disposableSupplier.get();
			}

			@Override
			public void onHide(final CompType component) {
				mDisposeable = IDisposeable.dispose(mDisposeable);
			}
		});
	}

	public static <CompType extends Component> IDisposeable registerShowingListener(final CompType component, final IVisiblityListener<CompType> iVisiblityListener) {
		if (component == null || iVisiblityListener == null)
			return null;
		final HierarchyListener hl = new HierarchyListener() {
			@Override
			public void hierarchyChanged(final HierarchyEvent e) {
				if ((e.getChangeFlags() & HierarchyEvent.SHOWING_CHANGED) != 0 ) {
					if (component.isShowing())
						iVisiblityListener.onShowing(component);
					else
						iVisiblityListener.onHide(component);
				}
			}
		};
		component.addHierarchyListener(hl);
		return IDisposeable.create(() -> component.removeHierarchyListener(hl));
	}
}
