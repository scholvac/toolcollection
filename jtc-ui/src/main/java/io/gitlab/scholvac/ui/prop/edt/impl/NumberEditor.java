package io.gitlab.scholvac.ui.prop.edt.impl;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.WindowConstants;

import io.gitlab.scholvac.IDisposeable;
import io.gitlab.scholvac.StringUtils;
import io.gitlab.scholvac.func.FunctionUtils;
import io.gitlab.scholvac.prop.IProperty;
import io.gitlab.scholvac.ui.IUIElementFactory;
import io.gitlab.scholvac.ui.LineEdit;

public class NumberEditor<NumberT extends Number> extends AbstractPropertyEditor<NumberT> {

	public static final String ANNOTATION__MIN_VAULE 					= "MinValue";
	public static final String ANNOTATION__MAX_VALUE					= "MaxValue";
	public static final String ANNOTATION__FORMAT						= "Format";
	public static final String ANNOTATION__ALLOW_NULL					= "AllowNull";
	private static final String ANNOTATION__LOCALE 						= "Locale";

	public static final String NULL_INDICATOR 							= "null";


	protected LineEdit<NumberT> 		mLineEdt;
	private IDisposeable				mValueDisposeable;


	public NumberEditor(final IProperty<NumberT> prop, final Function<IProperty<?>, IProperty<?>> propertyWrapper, final Function<String, NumberT> toContent, final Function<NumberT, String> toString) {
		super(prop, propertyWrapper);
		mLineEdt = new LineEdit<>(LineEdit.DEFAULT_VALIDATION_DELAY, wrapToContent(toContent), toString);
		addConditions(prop, mLineEdt);
		mLineEdt.getPromt().setText(getPromtText());
	}

	protected void addConditions(final IProperty<NumberT> prop, final LineEdit<NumberT> edt) {
		if (hasAnnotation(ANNOTATION__MIN_VAULE) && getAnnotation(ANNOTATION__MIN_VAULE) instanceof Number) {
			final Number min = getAnnotation(ANNOTATION__MIN_VAULE);
			if (min != null) {
				edt.addContentValidator((content, result) -> ifSucceed(content, c -> c != null && c.doubleValue() < min.doubleValue(), c -> result.error("Value " + c + " is less than minimum bound of " + min)));
			}
		}
		if (hasAnnotation(ANNOTATION__MAX_VALUE) && getAnnotation(ANNOTATION__MAX_VALUE) instanceof Number) {
			final Number max = getAnnotation(ANNOTATION__MAX_VALUE);
			if (max != null) {
				edt.addContentValidator((content, result) -> ifSucceed(content, c -> c != null && c.doubleValue() > max.doubleValue(), c -> result.error("Value " + c + " is more than maximum bound of " + max)));
			}
		}
	}


	private static <T> void ifSucceed(final T content, final Predicate<T> condition, final Consumer<T> failed) {
		if (condition.test(content)) {
			failed.accept(content);
		}
	}

	protected Function<String, NumberT> wrapToContent(Function<String, NumberT> toContent) {
		final IProperty<NumberT> property = getProperty();
		toContent = handleNullValueAnnotation(property, toContent);
		return toContent;
	}

	private <NumberT> Function<String, NumberT> handleNullValueAnnotation(final IProperty<NumberT> prop, final Function<String, NumberT> func){
		if (hasAnnotation(ANNOTATION__ALLOW_NULL)) {
			return FunctionUtils.ifThenElse(cond -> NULL_INDICATOR.equalsIgnoreCase(cond) || StringUtils.isNullOrEmpty(cond), str_true -> null, func);
		}
		return func;
	}
	protected String getPromtText() {
		final Class<NumberT> clazz = getProperty().getType();
		if (clazz != null) {
			return "new " + clazz.getSimpleName();
		}
		return "";
	}

	@Override
	protected Component createEditor(final IProperty<NumberT> property, final IUIElementFactory factory) {
		mLineEdt.addContentListener(this::updateFromEditor);
		return mLineEdt.getComponent();
	}

	private void updateFromEditor(final NumberT newValue) {
		runInThisThread(() -> set(newValue));
	}
	private void updateFromProperty(final NumberT newValue) {
		if (mLineEdt.hasFocus() == false) {
			runInThisThread(() -> mLineEdt.setValue(newValue));
		}
	}

	@Override
	protected void onEditorShown(final Component editor, final IProperty<NumberT> property) {
		mValueDisposeable = property.addValueChangeListener(pcl -> {
			updateFromProperty(property.get());
		});
		updateFromProperty(property.get());
	}

	@Override
	protected void onEditorHidden(final Component editor, final IProperty<NumberT> property) {
		mValueDisposeable = IDisposeable.dispose(mValueDisposeable);
	}



	public static class ByteEditor extends NumberEditor<Byte> {
		public ByteEditor(final IProperty<Byte> prop, final Function<IProperty<?>, IProperty<?>> propertyWrapper) {
			super(prop, propertyWrapper, Byte::parseByte, b -> Byte.toString(b));
		}

	}
	public static class ShortEditor extends NumberEditor<Short> {
		public ShortEditor(final IProperty<Short> prop, final Function<IProperty<?>, IProperty<?>> propertyWrapper) {
			super(prop, propertyWrapper, Short::parseShort, b -> Short.toString(b));
		}
	}
	public static class IntegerEditor extends NumberEditor<Integer> {
		public IntegerEditor(final IProperty<Integer> prop, final Function<IProperty<?>, IProperty<?>> propertyWrapper) {
			super(prop, propertyWrapper, Integer::parseInt, b -> Integer.toString(b));
		}
	}
	public static class LongEditor extends NumberEditor<Long> {
		public LongEditor(final IProperty<Long> prop, final Function<IProperty<?>, IProperty<?>> propertyWrapper) {
			super(prop, propertyWrapper, Long::parseLong, b -> Long.toString(b));
		}
	}

	protected static class DecimalEditor<T extends Number> extends NumberEditor<T>{

		public DecimalEditor(final IProperty<T> prop, final Function<IProperty<?>, IProperty<?>> propertyWrapper, final Function<String, T> toContent, final Function<T, String> toString) {
			super(prop, propertyWrapper, toContent, wrapToString(prop, toString));
		}

		private static <T extends Number> Function<T, String> wrapToString(final IProperty<T> prop, final Function<T, String> toString) {
			//TODO: change to a not static function
			final String format = prop.getAnnotation(ANNOTATION__FORMAT, (String)null);
			if (StringUtils.isNullOrEmpty(format) == false){
				final DecimalFormat df = new DecimalFormat(format);
				return val -> df.format(val);
			}
			return toString;
		}

		@Override
		protected String getPromtText() {
			final String format = getAnnotation(ANNOTATION__FORMAT, (String)null);
			if (format == null) {
				return super.getPromtText();
			}
			return super.getPromtText() + " ("+ format + ")";
		}

		@Override
		protected Function<String, T> wrapToContent(final Function<String, T> toContent) {
			return FunctionUtils.concatenate(this::prepareString, super.wrapToContent(toContent));
		}

		protected String prepareString(String in) {
			if (in != null){
				in = in.replaceAll("_", "");
				in = in.replaceFirst(getDecimalSeperator(), ".");
			}
			return in;
		}
		private String mDecimalSeperator = null;
		public String getDecimalSeperator() {
			if (mDecimalSeperator == null) {
				mDecimalSeperator = DecimalFormatSymbols.getInstance(getLocale()).getDecimalSeparator()+"";
				if (mDecimalSeperator.equals("."))
					mDecimalSeperator = "\\.";
			}
			return mDecimalSeperator;
		}


	}
	public static class FloatEditor extends DecimalEditor<Float> {
		public FloatEditor(final IProperty<Float> prop, final Function<IProperty<?>, IProperty<?>> propertyWrapper) {
			super(prop, propertyWrapper, Float::parseFloat, b -> Float.toString(b));
			mLineEdt.setText(ANNOTATION__LOCALE);
		}
	}
	public static class DoubleEditor extends DecimalEditor<Double> {
		public DoubleEditor(final IProperty<Double> prop, final Function<IProperty<?>, IProperty<?>> propertyWrapper) {
			super(prop, propertyWrapper, Double::parseDouble, b -> Double.toString(b));
		}
	}

	private Locale mLocale = null;
	public Locale getLocale() {
		if (mLocale == null) {
			mLocale = retreiveLocale();
		}
		return mLocale;
	}


	protected Locale retreiveLocale() {
		final Object obj = getAnnotation(ANNOTATION__LOCALE);
		if (obj instanceof Locale) {
			return (Locale)obj;
		}
		if (obj instanceof String) {
			return Locale.forLanguageTag(obj.toString());
		}
		return Locale.getDefault();
	}


	public static void main(final String[] args) {

		final DecimalFormat tf = new DecimalFormat("00000.##");
		System.out.println(tf.format(3933.93339));


		final IProperty<Byte> byteProp = IProperty.of("Byte", "Byte Property", true, (byte)42);
		final IProperty<Integer> integerProp = IProperty.of("Integer", "IntegerProperty", false, 42);
		final IProperty<Double> doubleProp = IProperty.of("Double", "IntegerProperty", true, 42.42);
		final IProperty<Double> doubleProp2 = IProperty.of("Double2", "IntegerProperty", true, 42.42);

		byteProp.annotate(ANNOTATION__ALLOW_NULL);
		byteProp.annotate(ANNOTATION__MIN_VAULE, (byte)15);
		byteProp.annotate(ANNOTATION__MAX_VALUE, (byte)20);

		doubleProp.annotate(ANNOTATION__FORMAT, "00.00##");


		new Thread(() -> {
			while(true) {
				try {Thread.sleep(100);}catch(final Exception e) {}
				doubleProp2.set(doubleProp2.get() + 0.5);
			}
		}).start();



		final JFrame frame = new JFrame("New Number Editor Demo");
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));

		final GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		frame.getContentPane().setLayout(gridBagLayout);

		final JLabel lblNewLabel = new JLabel("Byte");
		final GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 1;
		frame.getContentPane().add(lblNewLabel, gbc_lblNewLabel);

		final NumberEditor edtByte = new ByteEditor(byteProp, null);
		final GridBagConstraints gbc_edtByte = new GridBagConstraints();
		gbc_edtByte.insets = new Insets(0, 0, 5, 0);
		gbc_edtByte.fill = GridBagConstraints.HORIZONTAL;
		gbc_edtByte.gridx = 1;
		gbc_edtByte.gridy = 1;
		frame.getContentPane().add(edtByte.getEditor(IUIElementFactory.INSTANCE), gbc_edtByte);

		final JLabel lblNewLabel_1 = new JLabel("Integer");
		final GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_1.gridx = 0;
		gbc_lblNewLabel_1.gridy = 2;
		frame.getContentPane().add(lblNewLabel_1, gbc_lblNewLabel_1);

		final NumberEditor edtInteger = new IntegerEditor(integerProp, null);
		final GridBagConstraints gbc_edtInteger = new GridBagConstraints();
		gbc_edtInteger.insets = new Insets(0, 0, 5, 0);
		gbc_edtInteger.fill = GridBagConstraints.HORIZONTAL;
		gbc_edtInteger.gridx = 1;
		gbc_edtInteger.gridy = 2;
		frame.getContentPane().add(edtInteger.getEditor(IUIElementFactory.INSTANCE), gbc_edtInteger);

		final JLabel lblNewLabel_2 = new JLabel("Double");
		final GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.insets = new Insets(0, 0, 0, 5);
		gbc_lblNewLabel_2.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_2.gridx = 0;
		gbc_lblNewLabel_2.gridy = 3;
		frame.getContentPane().add(lblNewLabel_2, gbc_lblNewLabel_2);

		final NumberEditor edtDouble = new DoubleEditor(doubleProp, null);
		final GridBagConstraints gbc_edtDouble = new GridBagConstraints();
		gbc_edtDouble.fill = GridBagConstraints.HORIZONTAL;
		gbc_edtDouble.gridx = 1;
		gbc_edtDouble.gridy = 3;
		frame.getContentPane().add(edtDouble.getEditor(IUIElementFactory.INSTANCE), gbc_edtDouble);

		final NumberEditor edtDouble2 = new DoubleEditor(doubleProp2, null);
		final GridBagConstraints gbc_edtDouble2 = new GridBagConstraints();
		gbc_edtDouble2.fill = GridBagConstraints.HORIZONTAL;
		gbc_edtDouble2.gridx = 1;
		gbc_edtDouble2.gridy = 4;
		frame.getContentPane().add(edtDouble2.getEditor(IUIElementFactory.INSTANCE), gbc_edtDouble2);

		frame.setSize(640, 480);
		frame.setVisible(true);
	}
}
