package io.gitlab.scholvac.ui.prop.edt.impl;

import java.awt.BorderLayout;
import java.awt.Component;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import org.jdesktop.swingx.JXTaskPane;

import io.gitlab.scholvac.prop.IProperty;
import io.gitlab.scholvac.prop.IPropertyContext;
import io.gitlab.scholvac.prop.impl.FunctionalProperty;
import io.gitlab.scholvac.ui.IUIElementFactory;
import io.gitlab.scholvac.ui.prop.PropertySheet;

public class CollectionPropertyEditor<ValueT, CollectionT extends Collection<ValueT>> extends AbstractPropertyEditor<CollectionT> {

	private static final int DEFAULT_MAX_ELEMENT_COUNT = 100;
	private static final int SHOW_ALL_ELEMENTS = -1;

	private JXTaskPane 			mTaskPane;
	private PropertySheet 		mSheet;
	private final int	 		mMaxElementsToShow;


	public CollectionPropertyEditor(final IProperty prop, final Function<IProperty<?>, IProperty<?>> propertyWrapper, final IUIElementFactory uiFactory, final Comparator<IProperty<?>> paramComperator, final IPropertyContext sheetConfig) {
		this(prop, propertyWrapper, uiFactory, DEFAULT_MAX_ELEMENT_COUNT, paramComperator, sheetConfig);
	}


	public CollectionPropertyEditor(final IProperty property, final Function<IProperty<?>, IProperty<?>> propertyWrapper, final IUIElementFactory uiFactory, final Integer maxElements, final Comparator<IProperty<?>> paramComperator, final IPropertyContext sheetConfig) {
		super(property, propertyWrapper);
		mMaxElementsToShow = maxElements != null ? maxElements : SHOW_ALL_ELEMENTS;

		mTaskPane = new JXTaskPane(property.getName() + "[" + get().size() + "]");
		mTaskPane.setLayout(new BorderLayout());

		mSheet = new PropertySheet(uiFactory, paramComperator, sheetConfig);
		mTaskPane.add(mSheet, BorderLayout.CENTER);

		mSheet.asyncUpdateProperties(createDelegateVariables());
	}


	private List<IProperty<?>> createDelegateVariables() {
		final List<IProperty<?>> listOfProperties = new ArrayList<>(get().size());
		int idx = 0;
		final String desc = getProperty().getDescription();
		final boolean editable = getProperty().isEditable() && get() instanceof List;
		for (final ValueT v : get()) {
			final int _idx = idx;
			final Supplier<ValueT> getter =()-> ((List<ValueT>)get()).get(_idx);
			final Consumer<ValueT> setter = editable ? newVal -> ((List<ValueT>)get()).set(_idx, newVal):null;
			final FunctionalProperty<ValueT> prop = new FunctionalProperty<>(idx+"", desc, editable, getter, setter);
			listOfProperties.add(prop);
			idx++;
			if (mMaxElementsToShow > 0 && idx > mMaxElementsToShow)
				break;
		}
		return listOfProperties;
	}


	@Override
	protected Component createEditor(final IProperty<CollectionT> property, final IUIElementFactory factory) {
		return mTaskPane;
	}

}
