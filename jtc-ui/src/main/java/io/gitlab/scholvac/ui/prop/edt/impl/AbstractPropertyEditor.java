package io.gitlab.scholvac.ui.prop.edt.impl;

import java.awt.Component;
import java.awt.event.HierarchyEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Supplier;

import javax.swing.JComponent;

import io.gitlab.scholvac.IDisposeable;
import io.gitlab.scholvac.StringUtils;
import io.gitlab.scholvac.log.ILogger;
import io.gitlab.scholvac.log.SLog;
import io.gitlab.scholvac.observable.ObservableSupport;
import io.gitlab.scholvac.observable.ObservableSupport.AbstractObservableSupport;
import io.gitlab.scholvac.prop.IAnnotatedObject;
import io.gitlab.scholvac.prop.IProperty;
import io.gitlab.scholvac.ui.IUIElementFactory;
import io.gitlab.scholvac.ui.prop.edt.IPropertyEditor;
import io.gitlab.scholvac.ui.utils.UIUtils;

public abstract class AbstractPropertyEditor<ValueT> implements IPropertyEditor<ValueT>, AbstractObservableSupport<ValueT>, IAnnotatedObject{


	protected static final ILogger						LOG = SLog.getLogger(AbstractPropertyEditor.class);

	private final ObservableSupport<ValueT>				mObservableSupport = new ObservableSupport<>();

	private final IProperty<ValueT> 					mProperty;
	private final Function<IProperty<?>, IProperty<?>> 	mPropertyWrapper;
	/**
	 * Additional annotations, that shall be handled similar to the property annotation itself but apply only to this editor.
	 */
	private final Map<String, Object> 					mEditorAnnotations = new HashMap<>();

	private List<IDisposeable>							mDisposables;
	private List<IDisposeable>							mDisposeOnHidden;
	private List<Supplier<IDisposeable>>				mWhileActive;
	private Component									mEditor;



	public AbstractPropertyEditor(final IProperty<ValueT> prop, final Function<IProperty<?>, IProperty<?>> propertyWrapper) {
		mProperty = prop;
		mPropertyWrapper = propertyWrapper;
	}

	protected Function<IProperty<?>, IProperty<?>> getWrapper(){ return mPropertyWrapper;}

	@Override
	public ObservableSupport<ValueT> getObservableSupport() {
		return mObservableSupport;
	}
	@Override
	public boolean isDisposed() {
		return mDisposables == null;
	}

	@Override
	public void dispose() throws Exception {
		IDisposeable.dispose(mDisposables);
		mDisposables = null;
	}

	@Override
	public IProperty<ValueT> getProperty() { return mProperty; }
	protected ValueT get() { return getProperty().get(); }
	protected void set(final ValueT value) { getProperty().set(value);}
	public boolean isReadOnly() { return !getProperty().isEditable();}

	protected IDisposeable disposeOnDispose(final IDisposeable disp) {
		if (mDisposables == null) {
			mDisposables = new ArrayList<>();
		}
		if (disp != null && mDisposables.contains(disp) == false) {
			mDisposables.add(disp);
		}
		return disp;
	}
	protected void disposeWhenHidden(final IDisposeable disp) {
		if (mDisposeOnHidden == null) {
			mDisposeOnHidden = new ArrayList<>();
		}
		mDisposeOnHidden.add(disp);
	}

	/**
	 * Adds an Supplier that is executed whenever the editor is shown.
	 * The supplier shall return an {@link IDisposeable} that will be disposed as soon as the editor('s component) becomes hidden.
	 *
	 * @param actionWhileShown
	 */
	protected IDisposeable whileShown(final Supplier<IDisposeable> actionWhileShown) {
		if (mWhileActive == null) {
			mWhileActive = new ArrayList<>();
		}
		mWhileActive.add(actionWhileShown);
		return IDisposeable.create(() -> removeWhileShown(actionWhileShown));
	}

	private void removeWhileShown(final Supplier<IDisposeable> actionWhileShown) {
		if (mWhileActive.remove(actionWhileShown)) {
			if (mWhileActive.isEmpty()) {
				mWhileActive = null;
			}
		}
	}

	@Override
	public Component getEditor(final IUIElementFactory factory) {
		if (mEditor == null) {
			mEditor = createEditor(getProperty(), factory);
			if (mEditor != null) {
				mEditor.setEnabled(!isReadOnly());
				mEditor.addHierarchyListener(e -> {
					if ((e.getChangeFlags() & HierarchyEvent.SHOWING_CHANGED) != 0) {
						if (e.getComponent().isDisplayable()) {
							_onEditorShown(mEditor, mProperty);
						} else {
							_onEditorHidden(mEditor, mProperty);
						}
					}
				});
			}
		}
		return mEditor;
	}
	protected void _onEditorShown(final Component editor, final IProperty<ValueT> property) {
		if (mWhileActive != null) {
			mWhileActive.stream()
			.map(Supplier::get) // do the actual action to be performed on show
			.filter(it -> it != null)
			.forEach(it -> disposeWhenHidden(it));
		}
		onEditorShown(editor, property);
	}


	private void _onEditorHidden(final Component edt, final IProperty<ValueT> prop) {
		onEditorHidden(edt, prop);
		if (mDisposeOnHidden != null) {
			IDisposeable.dispose(mDisposeOnHidden);
			mDisposeOnHidden = null;
		}
	}


	protected abstract Component createEditor(final IProperty<ValueT> property, final IUIElementFactory factory);
	protected void onEditorShown(final Component editor, final IProperty<ValueT> property) { }
	protected void onEditorHidden(final Component editor, final IProperty<ValueT> property) { }


	protected void runInEvtThread(final Runnable action) {
		UIUtils.ensureInEvtThread( () -> {
			synchronized (mProperty) {
				try {
					action.run();
				}catch(final Exception e) {
					e.printStackTrace();
				}finally {

				}
			}
		});
	}
	protected synchronized void runInThisThread(final Runnable action) {
		synchronized (mProperty) {
			try {
				action.run();
			}catch(final Exception e) {
				e.printStackTrace();
			}finally {
			}
		}
	}

	protected void assignTooltip(final IProperty<ValueT> property, final JComponent slider) {
		//TODO: provide a more elegant tooltip provider with dynamic tooltip support.
		if (StringUtils.notNullOrEmpty(property.getDescription())) {
			slider.setToolTipText(property.getDescription());
		}
	}

	@Override
	public <AnnotationType> void addAnnotation(final String key, final AnnotationType value) {
		mEditorAnnotations.put(key, value);
	}
	@Override
	public boolean hasAnnotation(final String key) {
		return getProperty().hasAnnotation(key) || mEditorAnnotations.containsKey(key);
	}
	@SuppressWarnings("unchecked")
	@Override
	public <AnnotationType> AnnotationType getAnnotation(final String key) {
		final AnnotationType ano = getProperty().getAnnotation(key);
		if (ano != null) {
			return ano;
		}
		return (AnnotationType) mEditorAnnotations.get(key);
	}
	@SuppressWarnings("unchecked")
	@Override
	public <AnnotationType> AnnotationType removeAnnotation(final String key) {
		return (AnnotationType) mEditorAnnotations.remove(key);
	}
	@Override
	public Map<String, Object> getAnnotations() {
		final HashMap<String, Object> ano = new HashMap<>(getProperty().getAnnotations());
		ano.putAll(mEditorAnnotations);
		return ano;
	}
}
