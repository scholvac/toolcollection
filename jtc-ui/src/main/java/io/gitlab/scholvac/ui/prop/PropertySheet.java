package io.gitlab.scholvac.ui.prop;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.function.Function;
import java.util.stream.Stream;

import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.jdesktop.swingx.JXTaskPane;
import org.jdesktop.swingx.JXTaskPaneContainer;

import io.gitlab.scholvac.IDisposeable;
import io.gitlab.scholvac.ReflectionUtils;
import io.gitlab.scholvac.StringUtils;
import io.gitlab.scholvac.exec.ExecutorUtils;
import io.gitlab.scholvac.observable.IObservableObject;
import io.gitlab.scholvac.prop.IProperty;
import io.gitlab.scholvac.prop.IPropertyContext;
import io.gitlab.scholvac.prop.PropertyDecomposer;
import io.gitlab.scholvac.prop.PropertyDescriptor;
import io.gitlab.scholvac.prop.ReflectionPropertyDecomposer;
import io.gitlab.scholvac.prop.impl.DelegateProperty;
import io.gitlab.scholvac.prop.impl.PropertyContext;
import io.gitlab.scholvac.ui.IUIElementFactory;
import io.gitlab.scholvac.ui.prop.edt.IPropertyEditor;
import io.gitlab.scholvac.ui.prop.edt.impl.CollectionPropertyEditor;
import io.gitlab.scholvac.ui.utils.VisibilityUtil;

public class PropertySheet extends JPanel {

	public interface IFullWidthEditor {
	}


	public static PropertyDescriptor<Boolean>		DESC_SINGLE_OCCURENCE = PropertyDescriptor.create("SingleOccurence", true)
			.description("Whether a property can occure more than one time per sheet or not");
	public static PropertyDescriptor<Boolean>		DESC_SHOW_UNKNOWN_AS_LABEL = PropertyDescriptor.create("ShowUnknowEditorAsLabel", true)
			.description("If set to true, unknown properties (e.g. no editor available) will be shown as an readonly label");
	public static PropertyDescriptor<Boolean>		DESC_SHOW_LABELS = PropertyDescriptor.create("ShowLabels", true)
			.description("If the property name shall be shown as a label, together with the editor for this property");
	public static PropertyDescriptor<Boolean>		DESC_DECOMPOSE_UNKNOWN = PropertyDescriptor.create("DecomposeUnknown", true)
			.description("If TRUE the dialog decomposes a type into sub types, via reflection api");
	public static PropertyDescriptor<Integer> 		DESC_MAX_DEPTH = PropertyDescriptor.create("Maximum Depth", 3)
			.description("How many sub properties (in case of complex properties) shall be displayed");
	public static PropertyDescriptor<Boolean>		DESC_HANDLE_COLLECTIONS = PropertyDescriptor.create("Handle Collections", true)
			.description("Whether to show the values of a collection or not");
	public static PropertyDescriptor<Integer>		DESC_MAX_COLLECTION_ELEMENTS = PropertyDescriptor.create("Maximum Collection Elements", 25)
			.description("The (maximum) amount of elements of a collection that are shown in the sheet. Only active if 'Handle Collection = true'");
	public static PropertyDescriptor<Integer>		DESC_LABEL_DISTANCE = PropertyDescriptor.create("Label Distance", 20)
			.description("The distance (pixel) between the label and the value editor");

	public static final String			ANNOTATION__HEADER = "PropertySheetHeader";

	private static ExecutorService					sExecutor = ExecutorUtils.getOrCreate("PropertySheetExecutor", 2);
	static ExecutorService getExecutor() { return sExecutor; }

	private IUIElementFactory						mUIElementFactory;
	private Comparator<IProperty<?>>				mPropertyComparator;

	private final int								mRemainingSubSheets;
	private final IProperty<Boolean>				mShowUnknownEditorAsLabel;
	private final IProperty<Boolean>				mShowLabels;
	private final IProperty<Integer>				mLabelEditorDistance;
	private final IProperty<Boolean>				mDecomposeUnknownObjects;
	private final IProperty<Boolean>				mHandleCollections;
	private final IProperty<Integer>				mMaxCollectionElements;
	private final IProperty<Boolean> 				mSingleOccurence;
	private final IPropertyContext 					mSheetConfiguration;

	private Font 									mHeaderFont;
	private Runnable								mReloadFunction = null;
	private Function<IProperty<?>, IProperty<?>> 	mWrapperFunction;


	static {
		PropertyDecomposer.get().addProvider(new ReflectionPropertyDecomposer());
	}

	public PropertySheet() {
		this(IUIElementFactory.INSTANCE, IProperty.NAME_COMPARATOR);
	}
	public PropertySheet(final IUIElementFactory factory, final Comparator<IProperty<?>> comparator) {
		this(factory, comparator, new PropertyContext());
	}
	public PropertySheet(final IUIElementFactory factory, final Comparator<IProperty<?>> comparator, final IPropertyContext sheetConfig) {
		this(factory, comparator, sheetConfig, -1);
	}
	private PropertySheet(final IUIElementFactory factory, final Comparator<IProperty<?>> comparator, final IPropertyContext sheetConfig, final int depth) {
		mUIElementFactory = factory;
		mPropertyComparator = comparator;
		mSheetConfiguration = sheetConfig;

		mRemainingSubSheets = depth < 0 ? sheetConfig.getProperty(DESC_MAX_DEPTH).get() : depth;

		mSingleOccurence = sheetConfig.getProperty(DESC_SINGLE_OCCURENCE);
		mShowUnknownEditorAsLabel = sheetConfig.getProperty(DESC_SHOW_UNKNOWN_AS_LABEL);
		mShowLabels = sheetConfig.getProperty(DESC_SHOW_LABELS);
		mDecomposeUnknownObjects = sheetConfig.getProperty(DESC_DECOMPOSE_UNKNOWN);
		mHandleCollections = sheetConfig.getProperty(DESC_HANDLE_COLLECTIONS);
		mMaxCollectionElements = sheetConfig.getProperty(DESC_MAX_COLLECTION_ELEMENTS);

		mLabelEditorDistance = sheetConfig.getProperty(DESC_LABEL_DISTANCE);

		VisibilityUtil.whileVisible(this, () -> IObservableObject.observe(Arrays.asList(mSingleOccurence, mShowUnknownEditorAsLabel, mShowLabels, mDecomposeUnknownObjects, mHandleCollections, mMaxCollectionElements, mLabelEditorDistance), evt -> doReload()));

		final GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, Double.MIN_VALUE};

		setLayout(gridBagLayout);
	}

	//----------- configuration ------------
	public void setLabelEditorDistance(final int is) { mLabelEditorDistance.set(is);}
	public int getLabelEditorDistance() { return mLabelEditorDistance.get();}
	public void setUIFactory(final IUIElementFactory factory) { mUIElementFactory = factory; }
	public IUIElementFactory getUIFactory() { return mUIElementFactory; }
	public Comparator<IProperty<?>> getPropertyComparator() { return mPropertyComparator; }
	public void setPropertyComparator(final Comparator<IProperty<?>> mPropertyComparator) { this.mPropertyComparator = mPropertyComparator; }

	public boolean isShowUnknownEditorAsLabel() { return mShowUnknownEditorAsLabel.get(); }
	public void setShowUnknownEditorAsLabel(final boolean mShowUnknownEditorAsLabel) { this.mShowUnknownEditorAsLabel.set(mShowUnknownEditorAsLabel); }
	public boolean isShowLabels() { return mShowLabels.get(); }
	public void setShowLabels(final boolean mShowLabels) { this.mShowLabels.set(mShowLabels); }

	//--------- Public interface -------------
	public void syncUpdateProperties(final IProperty<?> ...references) {
		syncUpdateProperties(Arrays.asList(references));
	}
	public void syncUpdateProperties(final Collection<IProperty<?>> references) {
		final Future<?> future = asyncUpdateProperties(references);
		try {
			future.get();
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
	}

	public Future<?> asyncUpdateProperties(final IProperty<?> ...references) {
		return asyncUpdateProperties(Arrays.asList(references));
	}
	public Future<?> asyncUpdateProperties(final Stream<IProperty> stream) {
		return asyncUpdateProperties(stream.toArray(IProperty[]::new));
	}
	public Future<?> asyncUpdatePropertiesFromObject(final Object value) {
		if (value instanceof IProperty) {
			return asyncUpdateProperties((IProperty)value);
		} else if (value instanceof Collection) {
			final Object obj = ((Collection)value).iterator().next();
			if (obj instanceof IProperty) {
				return asyncUpdateProperties((Collection<IProperty<?>>)value);
			}
		}
		else {
			final List<IProperty<?>> collection = PropertyDecomposer.get().listProperties(value);
			if (collection != null) {
				return asyncUpdateProperties(collection);
			}
		}
		return null;
	}
	public Future<?> asyncUpdateProperties(final Collection<IProperty<?>> references) {
		mReloadFunction = () -> updateProperties(references, new HashSet<>());
		return sExecutor.submit(() -> updateProperties(references, new HashSet<>()));
	}


	public void setPropertyWrapper(final Function<IProperty<?>, IProperty<?>> wrapper) {
		mWrapperFunction = wrapper;
	}
	public Function<IProperty<?>, IProperty<?>> getWrapper() { return mWrapperFunction;}

	//-------- Protected interface / specialisation -------------------
	protected IProperty<?> wrapProperty(final IProperty<?> prop) {
		if (mWrapperFunction != null) {
			try {
				return mWrapperFunction.apply(prop);
			}catch(final Throwable t) {
				t.printStackTrace();
				return prop;
			}
		}
		return prop;
	}
	protected IPropertyEditor<?> getEditor(final IUIElementFactory factory, final IProperty<?> property, final Set<Object> shownValues) {
		IPropertyEditor<?> edt = factory.getPropertyEditor(property, this::wrapProperty);
		if (edt == null) {
			edt = handleNoEditorWithDecompose(factory, property, shownValues);
		}
		if (edt == null) {
			edt = handleCollections(factory, property);
		}
		if (edt == null) {
			edt = handleNoEditorWithLabel(factory, property);
		}
		return edt;
	}

	private IPropertyEditor<?> handleCollections(final IUIElementFactory factory, final IProperty property) {
		final Class<?> type = property.getType();
		if (ReflectionUtils.isMany(type)==false || mHandleCollections.get() == false) {
			return null;
		}
		if (List.class.isAssignableFrom(type)) {
			final Function<IProperty<?>, IProperty<?>> wrapper = this::wrapProperty;
			return new CollectionPropertyEditor(property, wrapper, factory, mMaxCollectionElements.get(), mPropertyComparator, mSheetConfiguration);
		}
		return null;
	}

	private IPropertyEditor<?> handleNoEditorWithDecompose(final IUIElementFactory factory, final IProperty<?> property, final Set<Object> shownValues) {
		if (mDecomposeUnknownObjects.get() == false) {
			return null;
		}
		//first try the official provider
		final Object toDecompose = property.get();
		Collection<IProperty<?>> decomposed = null;
		if (toDecompose != null) {
			if ((decomposed == null || decomposed.isEmpty())) {
				decomposed = PropertyDecomposer.get().listProperties(toDecompose);
			}
		}
		if (decomposed != null && decomposed.isEmpty() == false && mRemainingSubSheets > 0) {
			if (mSingleOccurence.get() && skipAsDoubleOccurence(property, shownValues)) {
				return null;
			}
			return new SubSheetEditor(property.getName(), property, decomposed, shownValues);
		}
		return null;
	}
	private boolean skipAsDoubleOccurence(final IProperty<?> prop, final Set<Object> shownValues) {
		final Object val = prop.get();
		if (shownValues.contains(val)) {
			return true;
		}
		shownValues.add(val);
		return false;
	}

	@SuppressWarnings({"rawtypes", "unchecked"})
	protected IPropertyEditor<?> handleNoEditorWithLabel(final IUIElementFactory factory, final IProperty<?> property) {
		if (mShowUnknownEditorAsLabel.get()) {
			final DelegateProperty strProp = new DelegateProperty(property){
				@Override
				public boolean isEditable() { return false; }
				@Override
				public Object get() {
					final Object obj = super.get();
					return obj == null ? " --- " : obj.toString();
				}
				@Override
				public Class getType() { return String.class; }
			};
			return factory.getPropertyEditor(strProp, this::wrapProperty);

		}
		return null;
	}
	protected JLabel createLabel(final IPropertyEditor<?> editor) {
		if (editor.getProperty() == null) {
			return null;
		}
		final String name = editor.getProperty().getName();
		if (StringUtils.isNullOrEmpty(name)) {
			return mUIElementFactory.createLabel("Unknown");
		}
		String desc = editor.getProperty().getDescription();
		if (desc == null) {
			desc = name;
		}
		final JLabel lbl = mUIElementFactory.createLabel(name);
		lbl.setToolTipText(desc);
		return lbl;
	}



	//-------------------- Private --------------------------
	private void doReload() {
		if (mReloadFunction != null) {
			getExecutor().execute(mReloadFunction);
		}
	}
	private void updateProperties(final Collection<IProperty<?>> references, final Set<Object> shownValues) {
		SwingUtilities.invokeLater(() -> {
			final List<IPropertyEditor<?>> properties = prepareProperties(references != null ? new ArrayList<>(references) : new ArrayList<>(), shownValues);
			showProperties(properties);
		});
	}
	private List<IPropertyEditor<?>> prepareProperties(final List<IProperty<?>> properties, final Set<Object> shownValues) {
		if (mPropertyComparator != null) {
			properties.sort(mPropertyComparator);
		}

		final List<IPropertyEditor<?>> editorList = new ArrayList<>(properties.size());
		for (IProperty<?> prop : properties) {
			if (prop == null) {
				continue;
			}

			final List<IPropertyEditor<?>> annotationEditors = collectAnnotationEditors(prop);
			if (annotationEditors != null && annotationEditors.isEmpty() == false) {
				editorList.addAll(annotationEditors);
			}

			prop = wrapProperty(prop);
			if (prop == null) {
				continue;
			}
			final IPropertyEditor<?> editor = getEditor(mUIElementFactory, prop, shownValues);
			if (editor == null) {
				continue;
			}
			editorList.add(editor);
		}
		return editorList;
	}

	protected List<IPropertyEditor<?>> collectAnnotationEditors(final IProperty<?> prop) {
		final String header = prop.getAnnotation(ANNOTATION__HEADER, (String)null);
		if (header == null) {
			return null;
		}
		final IPropertyEditor<?> edt = new HeaderEditor(mUIElementFactory, header, getHeaderFont());
		return Arrays.asList(edt);
	}

	protected Font getHeaderFont() {
		if (mHeaderFont == null) {
			final JLabel tmp = mUIElementFactory.createLabel("foo");
			mHeaderFont = tmp.getFont().deriveFont(Font.BOLD | Font.ITALIC);
		}
		return mHeaderFont;
	}

	private void showProperties(final List<IPropertyEditor<?>> editors) {
		removeAll();
		if (editors.isEmpty()) {
			return ;
		}

		int gridY = 0;
		final GridBagConstraints gc_label = new GridBagConstraints();
		gc_label.gridx = 0;
		gc_label.gridy = gridY;
		gc_label.fill = GridBagConstraints.NONE;
		gc_label.anchor = GridBagConstraints.WEST;
		gc_label.insets = new Insets(1, 1, 1, getLabelEditorDistance());
		gc_label.weighty = 0;

		final GridBagConstraints gc_editor = new GridBagConstraints();
		gc_editor.gridx = 1;
		gc_editor.gridy = gridY;
		gc_editor.fill = GridBagConstraints.BOTH;
		gc_editor.anchor = GridBagConstraints.NORTHWEST;
		gc_editor.weighty = 0;

		final boolean showLabels = isShowLabels();
		for (final IPropertyEditor<?> editor : editors) {
			final Component edtComp = editor.getEditorComponent();
			if (edtComp != null) {
				gc_editor.gridy = gridY;
				gc_label.gridy = gridY;
				if (editor instanceof IFullWidthEditor) {
					gc_label.gridwidth = 2;
					gc_label.fill = GridBagConstraints.BOTH;
					gc_label.insets = new Insets(1, 1, 1, 1);
					add(edtComp, gc_label);
					gc_label.gridwidth = 1;
					gc_label.insets = new Insets(1, 1, 1, getLabelEditorDistance());
					gc_label.fill = GridBagConstraints.NONE;
				}else {
					if (showLabels) {
						final JLabel lblName = createLabel(editor);
						if (lblName != null) {
							add(lblName, gc_label);
						}
					}
					add(edtComp, gc_editor);
				}
				gridY++;
			}
		}
		//add space that fill up the empty space
		gc_editor.weighty = 10;
		gc_label.weighty = 10;
		gc_editor.gridy = gridY;
		gc_label.gridy = gridY;
		add(Box.createVerticalStrut(1), gc_label);
		add(Box.createVerticalStrut(1), gc_editor);

		updateUI();
	}



	private static class HeaderEditor implements IPropertyEditor, IFullWidthEditor {
		private final JLabel mLabel;

		public HeaderEditor(final IUIElementFactory factory, final String header, final Font headerFont) {
			mLabel = factory.createLabel(header);
			mLabel.setFont(headerFont);
		}
		@Override
		public boolean isDisposed() { return true; }
		@Override
		public void dispose() throws Exception {}
		@Override
		public IDisposeable addValueChangeListener(final IValueChangeListener listener) { return null;}
		@Override
		public void removeValueChangeListener(final IValueChangeListener listener) { }
		@Override
		public IProperty getProperty() { return null; }
		@Override
		public Component getEditor(final IUIElementFactory factory) { return mLabel; }
		@Override
		public Map<String, Object> getAnnotations() { return null; }
		@Override
		public <AnnotationType> void addAnnotation(final String key, final AnnotationType value) { }
		@Override
		public <AnnotationType> AnnotationType getAnnotation(final String key) { return null; }
		@Override
		public <AnnotationType> AnnotationType removeAnnotation(final String key) { return null;}
	}


	private class SubSheetEditor implements IPropertyEditor, IFullWidthEditor{
		private final JXTaskPaneContainer mTaskPaneContainer;
		private final JXTaskPane 			mTaskPane;
		private PropertySheet		mSheet;
		private final IProperty<?> 		mParent;

		public SubSheetEditor(final String header, final IProperty<?> parent, final Collection<IProperty<?>> properties, final Set<Object> shownValues) {
			mParent = parent;
			mTaskPaneContainer = new JXTaskPaneContainer();
			mTaskPane = new JXTaskPane(header);
			mTaskPane.setLayout(new BorderLayout());
			if (mRemainingSubSheets > 0) {
				mSheet = new PropertySheet(mUIElementFactory, mPropertyComparator, mSheetConfiguration, mRemainingSubSheets - 1);
				//				mSheet.asyncUpdateProperties(properties);
				mSheet.updateProperties(properties, shownValues);
				mTaskPane.add(mSheet, BorderLayout.CENTER);
			}
			mTaskPaneContainer.add(mTaskPane);
		}
		@Override
		public boolean isDisposed() { return mSheet != null; }
		@Override
		public void dispose() throws Exception {}
		@Override
		public IDisposeable addValueChangeListener(final IValueChangeListener listener) { return null;}
		@Override
		public void removeValueChangeListener(final IValueChangeListener listener) { }
		@Override
		public IProperty getProperty() { return mParent; }
		@Override
		public Component getEditor(final IUIElementFactory factory) { return mTaskPane; }
		@Override
		public Map<String, Object> getAnnotations() { return null; }
		@Override
		public <AnnotationType> void addAnnotation(final String key, final AnnotationType value) { }
		@Override
		public <AnnotationType> AnnotationType getAnnotation(final String key) { return null; }
		@Override
		public <AnnotationType> AnnotationType removeAnnotation(final String key) { return null; }

	}




}
