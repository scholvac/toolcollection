package io.gitlab.scholvac.ui.prop.edt.impl;

import java.awt.Component;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.List;
import java.util.Vector;
import java.util.function.Function;
import java.util.function.Supplier;

import javax.measure.Quantity;
import javax.measure.Unit;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.WindowConstants;

import org.jdesktop.swingx.renderer.DefaultListRenderer;

import io.gitlab.scholvac.StringUtils;
import io.gitlab.scholvac.prop.IProperty;
import io.gitlab.scholvac.prop.PropertyDescriptor;
import io.gitlab.scholvac.quant.Frequency;
import io.gitlab.scholvac.quant.Measurement;
import io.gitlab.scholvac.resources.IconManager;
import io.gitlab.scholvac.ui.IUIElementFactory;
import io.gitlab.scholvac.ui.prop.edt.IPropertyEditor;

public class MeasurementEditor<Q extends Quantity<Q>, ValueT extends Measurement<Q>> extends AbstractPropertyEditor<ValueT> {

	public static final String ANNOTATION__NO_CONVERT_BUTTON 					= "NoConvertButton";
	public static final String ANNOTATION__NO_UNIT_SELECTOR 					= "NoUnitSelector";

	private final IProperty<Double> 			mQuantity;
	private final IProperty<Unit<Q>> 			mUnit;
	private boolean 							mInUpdate = false;

	public MeasurementEditor(final IProperty<ValueT> prop, final Function<IProperty<?>, IProperty<?>> propertyWrapper) {
		super(prop, propertyWrapper);


		final Supplier<Double> quantityGetter = () -> {
			final ValueT val = get();
			return val != null ? val.getValue().doubleValue() : Double.NaN;
		};
		final Supplier<Unit<Q>> unitGetter = () -> {
			final ValueT val = get();
			return val != null ? val.getUnit() : (Unit<Q>) getValidUnits().get(0);
		};
		mQuantity = IProperty.of(quantityGetter.get());
		mUnit = IProperty.of(unitGetter.get());

		//		whileShown(() -> prop.addValueChangeListener(it -> mQuantity.set(quantityGetter.get())));
		//		whileShown(() -> prop.addValueChangeListener(it -> {
		//			mUnit.set(unitGetter.get());
		//		}));
	}


	/**
	 * @wbp.parser.entryPoint
	 */
	@Override
	protected Component createEditor(final IProperty<ValueT> property, final IUIElementFactory factory) {

		final IPropertyEditor<?> numberEdit = factory.getPropertyEditor(mQuantity, getWrapper());

		final JPanel panel = factory.createJPanel();
		final GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{ 0, 0, 0, 0};
		gbl_panel.rowHeights = new int[]{0, 0};
		gbl_panel.columnWeights = new double[]{1.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{1.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);

		final GridBagConstraints gbc_valueEdt = new GridBagConstraints();
		gbc_valueEdt.insets = new Insets(0, 0, 0, 5);
		gbc_valueEdt.fill = GridBagConstraints.HORIZONTAL;
		gbc_valueEdt.gridx = 0;
		gbc_valueEdt.gridy = 0;

		panel.add(numberEdit.getEditorComponent(), gbc_valueEdt);
		int gridx = 1;

		if (hasAnnotation(ANNOTATION__NO_UNIT_SELECTOR) == false) {
			final JComboBox<Unit<?>> unitCB = new JComboBox<>(new DefaultComboBoxModel<>(new Vector<>(getValidUnits())));
			unitCB.setRenderer(new DefaultListRenderer( unit -> ((Unit<?>)unit).getSymbol()));
			final Font symbolFont = new Font("Tahoma", Font.PLAIN, 8);
			unitCB.setFont(symbolFont);
			unitCB.setSelectedItem(mUnit.get());
			final GridBagConstraints gbc_unitCB = new GridBagConstraints();
			gbc_unitCB.insets = new Insets(0, 0, 0, 5);
			gbc_unitCB.fill = GridBagConstraints.HORIZONTAL;
			gbc_unitCB.gridx = gridx++;
			gbc_unitCB.gridy = 0;
			panel.add(unitCB, gbc_unitCB);

			unitCB.addActionListener(al -> {
				final Unit unit = (Unit)unitCB.getSelectedItem();
				mUnit.set(unit);
			});
			disposeWhenHidden(mUnit.addValueChangeListener( evt -> {
				final Unit<Q> unit = evt.getNewValue();
				unitCB.setSelectedItem(unit);
			}));
		}
		if (hasAnnotation(ANNOTATION__NO_CONVERT_BUTTON) == false) {
			final JButton convertBtn = factory.createJButton();
			convertBtn.setIcon(IconManager.get().getIcon("icons/convert.png", 12));
			convertBtn.addActionListener(al -> {
				final JPopupMenu pm = new JPopupMenu();
				getValidUnits().stream()
				.filter(it -> it != mUnit.get())
				.filter(it -> !StringUtils.isNullOrEmpty(it.getSymbol()))
				.forEach( unit -> {
					final JMenuItem mi = factory.createJMenuItem(unit.getSymbol());
					//connect actions from UI
					mi.addActionListener(al2 -> convertTo((Unit<Q>) unit));
					pm.add(mi);
					if (unit == mUnit.get())
					{
						mi.setEnabled(false); //no need to allow conversion to same unit.
					}
				});
				pm.show(panel, convertBtn.getX(), convertBtn.getY());
			});
			final GridBagConstraints gbc_convertBtn = new GridBagConstraints();
			gbc_convertBtn.gridx = gridx++;
			gbc_convertBtn.gridy = 0;
			panel.add(convertBtn, gbc_convertBtn);
		}

		//connect actions from UI
		disposeWhenHidden(mUnit.addValueChangeListener(evt -> {
			if (!mInUpdate) {
				set(mQuantity.get(), evt.getNewValue());
			}
		}));
		disposeWhenHidden(mQuantity.addValueChangeListener(evt -> {
			if (!mInUpdate) {
				set(evt.getNewValue(), mUnit.get());
			}
		}));

		//connect update from model

		disposeWhenHidden(getProperty().addValueChangeListener(evt -> {
			mInUpdate = true;
			mQuantity.set(evt.getNewValue().getValue().doubleValue());
			mUnit.set(evt.getNewValue().getUnit());
			mInUpdate = false;
		}));


		return panel;
	}



	private void set(final double value, final Unit<Q> unit) {
		get().set(value, unit);
	}


	private void convertTo(final Unit<Q> unit) {
		get().convertTo(unit);
		mUnit.set(unit);
	}


	private List<Unit<?>> getValidUnits() {
		return Measurement.getValidUnits(getProperty().getType());
	}


	public static void main(final String[] args) {
		final IProperty<Frequency> withValue = IProperty.of("NonNull", "Frequency", true, Frequency.hz(17));
		final IProperty<Frequency> nullValue = PropertyDescriptor.create("NullValue", "", Frequency.class).createNewInstance();

		final JFrame frame = new JFrame("New Measurement Editor Demo");
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));

		final GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		frame.getContentPane().setLayout(gridBagLayout);

		final JLabel lblNewLabel = new JLabel("Measurement");
		final GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 1;
		frame.getContentPane().add(lblNewLabel, gbc_lblNewLabel);

		final MeasurementEditor edtByte = new MeasurementEditor<>(withValue, null);
		final GridBagConstraints gbc_edtByte = new GridBagConstraints();
		gbc_edtByte.insets = new Insets(0, 0, 5, 0);
		gbc_edtByte.fill = GridBagConstraints.HORIZONTAL;
		gbc_edtByte.gridx = 1;
		gbc_edtByte.gridy = 1;
		frame.getContentPane().add(edtByte.getEditor(IUIElementFactory.INSTANCE), gbc_edtByte);

		final JLabel lblNewLabel_1 = new JLabel("NullValue");
		final GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_1.gridx = 0;
		gbc_lblNewLabel_1.gridy = 2;
		frame.getContentPane().add(lblNewLabel_1, gbc_lblNewLabel_1);

		final MeasurementEditor edtNullVal = new MeasurementEditor<>(nullValue, null);
		final GridBagConstraints gbc_edtInteger = new GridBagConstraints();
		gbc_edtInteger.insets = new Insets(0, 0, 5, 0);
		gbc_edtInteger.fill = GridBagConstraints.HORIZONTAL;
		gbc_edtInteger.gridx = 1;
		gbc_edtInteger.gridy = 2;
		frame.getContentPane().add(edtNullVal.getEditor(IUIElementFactory.INSTANCE), gbc_edtInteger);

		frame.setSize(640, 480);
		frame.setVisible(true);
	}

}
