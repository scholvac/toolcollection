package io.gitlab.scholvac.ui.utils;

import java.awt.Component;
import java.awt.Font;
import java.io.File;
import java.util.function.Function;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreeModel;

import org.jdesktop.swingx.JXTree;
import org.jdesktop.swingx.renderer.DefaultTreeRenderer;
import org.jdesktop.swingx.renderer.StringValue;

import io.gitlab.scholvac.ui.IUIElementFactory;

public class UIUtils {

	public static void ensureInEvtThread(final Runnable r) {
		if (SwingUtilities.isEventDispatchThread())
			r.run();
		else
			SwingUtilities.invokeLater(r);
	}

	public static Font getDefaultTextPromtFont() {
		return (Font) UIManager.getLookAndFeelDefaults()
				.put("defaultFont", new Font("Arial", Font.BOLD, 14));
	}

	public static File createFileChooser(final IUIElementFactory factory, final Component parent, final String title, final int dialogType, final int selectionMode, final File directory, final String[] supportedFileExtensions, final boolean appendFirstExtension, final boolean askOverwrite) {
		final JFileChooser chooser = factory.createJFileChooser();
		chooser.setFileSelectionMode(selectionMode);
		chooser.setDialogType(dialogType);
		chooser.setDialogTitle(title);
		if (supportedFileExtensions != null && supportedFileExtensions.length > 0) {
			chooser.setFileFilter(new FileNameExtensionFilter(title, supportedFileExtensions));
		}
		if (directory != null)
			chooser.setCurrentDirectory(directory.isDirectory() ? directory : directory.getParentFile());

		final int result = chooser.showOpenDialog(parent);
		if (result == JFileChooser.APPROVE_OPTION) {
			File resultFile = chooser.getSelectedFile();
			if (appendFirstExtension && resultFile.getName().lastIndexOf('.') < 0 && supportedFileExtensions != null && supportedFileExtensions.length > 0) {
				resultFile = new File(resultFile.getAbsolutePath() + "." + supportedFileExtensions[0]);
			}
			if (askOverwrite && resultFile.exists()) {
				final int dialogResult = JOptionPane.showConfirmDialog (parent, "Would you like to overwrite the file: " + resultFile.getName(),"Warning", JOptionPane.YES_NO_OPTION);
				if(dialogResult == JOptionPane.YES_OPTION){
					return resultFile;
				}
				return null; //do not overwrite
			}
			return resultFile;
		}
		return null;
	}

	public static <T> JXTree createJTree(final IUIElementFactory factory,final TreeModel treeModel, final Class<T> typeHint, final Function<T, String> labelProvider) {
		final TreeCellRenderer tr = new DefaultTreeRenderer(new StringValue() {
			@Override
			public String getString(final Object obj) {
				if (obj != null && typeHint.isAssignableFrom(obj.getClass()))
					return labelProvider.apply((T)obj);
				return null;
			}
		});
		return createJTree(factory, treeModel, tr);
	}
	public static JXTree createJTree(final IUIElementFactory factory,final TreeModel treeModel, final TreeCellRenderer renderer) {
		final JXTree tree = factory.createJTree();
		tree.setModel(treeModel);
		tree.setCellRenderer(renderer);
		return tree;
	}

}
