package io.gitlab.scholvac.ui.prop.edt;

import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;

import io.gitlab.scholvac.prop.IProperty;

public interface IPropertyEditorProvider {
	<T> IPropertyEditor<T> provide(final IProperty<T> property, Function<IProperty<?>, IProperty<?>> propertyWrapper);



	public static class DefaultPropertyEditorProvider<T> implements IPropertyEditorProvider {
		private final Predicate<T> 																			mFilter;
		private final Predicate<Class<T>>																	mClassFilter;
		private final BiFunction<IProperty<T>, Function<IProperty<?>, IProperty<?>>, IPropertyEditor<T>> 	mFunction;

		public DefaultPropertyEditorProvider(final Predicate<T> mFilter, final BiFunction<IProperty<T>, Function<IProperty<?>, IProperty<?>>, IPropertyEditor<T>> mFunction) {
			this(mFilter, null, mFunction);
		}
		public DefaultPropertyEditorProvider(final Class<T> cfilter, final BiFunction<IProperty<T>, Function<IProperty<?>, IProperty<?>>, IPropertyEditor<T>> mFunction) {
			this(null, c -> c == cfilter, mFunction);
		}
		public DefaultPropertyEditorProvider(final Predicate<T> mFilter, final Predicate<Class<T>> cfilter, final BiFunction<IProperty<T>, Function<IProperty<?>, IProperty<?>>, IPropertyEditor<T>> mFunction) {
			this.mFilter = mFilter;
			this.mClassFilter = cfilter;
			this.mFunction = mFunction;
		}

		@Override
		public <A> IPropertyEditor<A> provide(final IProperty<A> property, final Function<IProperty<?>, IProperty<?>> propertyWrapper) {
			if (mClassFilter != null) {
				if (mClassFilter.test((Class<T>) property.getType()))
					return (IPropertyEditor<A>) mFunction.apply((IProperty<T>) property, propertyWrapper);
			}
			if (mFilter != null) {
				final A value = property.get();
				if (value != null && mFilter.test((T) value))
					return (IPropertyEditor<A>) mFunction.apply((IProperty<T>) property, propertyWrapper);
			}
			return null;
		}
	}
}
