package io.gitlab.scholvac.ui.prop;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import org.jdesktop.swingx.JXFindBar;
import org.jdesktop.swingx.JXTree;
import org.jdesktop.swingx.search.Searchable;

import io.gitlab.scholvac.prop.IContextAccess;
import io.gitlab.scholvac.prop.IProperty;
import io.gitlab.scholvac.prop.IPropertyContext;
import io.gitlab.scholvac.prop.impl.PropertyContext;
import io.gitlab.scholvac.resources.IconManager;
import io.gitlab.scholvac.ui.IUIElementFactory;
import io.gitlab.scholvac.ui.utils.UIUtils;
import io.gitlab.scholvac.ui.utils.VisibilityUtil;

public class TreePropertySheet extends JPanel {
	private final IPropertyContext 	mSheetConfig;

	private JPanel 					mTreeAndSearchPanel;
	private JPanel 					mContentPanel;
	private final ContextTreeModel 	mTreeModel;

	private final PropertySheet 	mPropertySheet;


	public TreePropertySheet(final IUIElementFactory factory, final Comparator<IProperty<?>> comparator) {
		this(factory, comparator, new PropertyContext());
	}

	/**
	 * @wbp.parser.constructor
	 */
	public TreePropertySheet(final IUIElementFactory factory, final Comparator<IProperty<?>> comparator, final PropertyContext sheetConfig) {
		setLayout(new BorderLayout());
		mSheetConfig = sheetConfig;

		mTreeModel = new ContextTreeModel();
		mPropertySheet = new PropertySheet(factory, comparator);

		VisibilityUtil.whileVisible(this, () -> mSheetConfig.addValueChangeListener(pcl -> setup(factory, mPropertySheet)));

		setup(factory, mPropertySheet);
	}

	public void setPropertyWrapper(final Function<IProperty<?>, IProperty<?>> wrapper) {
		mPropertySheet.setPropertyWrapper(wrapper);
	}
	public Function<IProperty<?>, IProperty<?>> getWrapper() { return mPropertySheet.getWrapper();}

	private void setup(final IUIElementFactory factory, final PropertySheet ps) {
		removeAll();

		final JSplitPane splitPane = factory.createJSplitPane();
		add(splitPane, BorderLayout.CENTER);

		mTreeAndSearchPanel = factory.createJPanel();
		splitPane.setLeftComponent(new JScrollPane(mTreeAndSearchPanel));
		mTreeAndSearchPanel.setLayout(new BorderLayout(0, 0));

		mContentPanel = factory.createJPanel();
		splitPane.setRightComponent(new JScrollPane(mContentPanel));
		mContentPanel.setLayout(new BorderLayout(0, 0));
		mContentPanel.add(ps, BorderLayout.CENTER);

		final JTextField txtBreadCrum = factory.createJTextField();
		txtBreadCrum.setEditable(false);
		if (mSheetConfig.get("ShowBredCrum", true))
			mContentPanel.add(txtBreadCrum, BorderLayout.NORTH);

		final JXTree hierarchy = UIUtils.createJTree(IUIElementFactory.INSTANCE, mTreeModel, PropertyContext.class, PropertyContext::getName);
		final SmallFindBar searchBar = new SmallFindBar(hierarchy.getSearchable());
		final boolean showBreadCrum =mSheetConfig.get("EnableSearch", true);
		if (showBreadCrum)
			mTreeAndSearchPanel.add(searchBar, BorderLayout.NORTH);
		mTreeAndSearchPanel.add(hierarchy, BorderLayout.CENTER);

		hierarchy.addTreeSelectionListener(new TreeSelectionListener() {
			@Override
			public void valueChanged(final TreeSelectionEvent e) {
				final IPropertyContext ctx = (IPropertyContext) e.getPath().getLastPathComponent();
				if (showBreadCrum) {
					//					final String breadCrum = String.join(".", Stream.of(e.getPath().getPath()).map(obj -> IContextAccess.class.cast(obj)).map(IPropertyContextProvider::getContextName).collect(Collectors.toList()));
					//					txtBreadCrum.setText(breadCrum);
				}
				ps.asyncUpdateProperties(ctx.getAllProperties(false));
			}
		});

		updateUI();
	}

	//--------- Public interface -------------

	public void asyncUpdateProperties(final IPropertyContext contexts) {
		PropertySheet.getExecutor().execute(() -> updateContexts(contexts));
	}

	public void syncUpdateProperties(final IPropertyContext contexts) {
		final Future<?> future = PropertySheet.getExecutor().submit(() -> updateContexts(contexts));
		try {
			future.get();
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
	}

	private void updateContexts(final IContextAccess context) {
		mTreeModel.setRoot(context);
	}


	static class ContextTreeModel implements TreeModel{

		private IContextAccess					mRootContext;
		private final List<TreeModelListener>	mListener = new ArrayList<>();

		public void setRoot(final IContextAccess ctx) {
			mRootContext = ctx;
			fireTreeChanged(mRootContext);
		}
		@Override
		public Object getRoot() {
			return mRootContext;
		}

		@Override
		public Object getChild(final Object parent, final int index) {
			final List<IPropertyContext> sorted = getSortedContextList(parent);
			return sorted.get(index);
		}
		@Override
		public int getChildCount(final Object parent) {
			return getSortedContextList(parent).size();
		}
		@Override
		public boolean isLeaf(final Object node) {
			return getChildCount(node) == 0;
		}
		@Override
		public void valueForPathChanged(final TreePath path, final Object newValue) {
			System.out.println("ValueForPathChanged");
		}
		@Override
		public int getIndexOfChild(final Object parent, final Object child) {
			return getSortedContextList(parent).indexOf(child);
		}
		@Override
		public void addTreeModelListener(final TreeModelListener l) {
			mListener.add(l);
		}
		@Override
		public void removeTreeModelListener(final TreeModelListener l) {
			mListener.remove(l);
		}

		private void fireTreeChanged(final IContextAccess...path) {
			final TreePath tp = new TreePath(path);
			if (mListener.isEmpty() == false)
				mListener.forEach(l -> l.treeStructureChanged(new TreeModelEvent(this, tp)));
		}
		private List<IPropertyContext> getSortedContextList(final Object parent) {
			final IContextAccess ctx = optTyped(parent);
			if (ctx == null)
				return Collections.emptyList();
			final List<IPropertyContext> children = ctx.getAllContexts(false).collect(Collectors.toList());
			children.sort((a,b)->String.CASE_INSENSITIVE_ORDER.compare(a.getName(), b.getName()));
			return children;
		}
		private IContextAccess optTyped(final Object parent) {
			if (parent instanceof IContextAccess)
				return (IContextAccess)parent;
			return null;
		}
	}

	static class SmallFindBar extends JXFindBar {
		public SmallFindBar(final Searchable searchable) {
			super(searchable);
		}
		@Override
		protected void build() {
			findNext.setText("");
			findNext.setIcon(IconManager.get().getIcon("icons/caret-down.png", IconManager.SMALL_ICON_SIZE));
			findNext.setPreferredSize(new Dimension(18, 18));
			findPrevious.setText("");
			findPrevious.setIcon(IconManager.get().getIcon("icons/caret-up.png", IconManager.SMALL_ICON_SIZE));
			findPrevious.setPreferredSize(new Dimension(18, 18));


			final GridBagLayout gridBagLayout = new GridBagLayout();
			gridBagLayout.columnWidths = new int[]{0, 16, 0, 0, 0};
			gridBagLayout.rowHeights = new int[]{0, 0};
			gridBagLayout.columnWeights = new double[]{1.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
			gridBagLayout.rowWeights = new double[]{0.0, Double.MIN_VALUE};
			setLayout(gridBagLayout);

			final GridBagConstraints gbc_textField = new GridBagConstraints();
			gbc_textField.insets = new Insets(0, 0, 0, 5);
			gbc_textField.fill = GridBagConstraints.HORIZONTAL;
			gbc_textField.gridx = 0;
			gbc_textField.gridy = 0;
			add(searchField, gbc_textField);
			searchField.setColumns(10);

			final GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
			gbc_btnNewButton.insets = new Insets(0, 0, 0, 5);
			gbc_btnNewButton.gridx = 1;
			gbc_btnNewButton.gridy = 0;
			add(findNext, gbc_btnNewButton);

			final GridBagConstraints gbc_btnNewButton_1 = new GridBagConstraints();
			gbc_btnNewButton_1.insets = new Insets(0, 0, 0, 5);
			gbc_btnNewButton_1.gridx = 2;
			gbc_btnNewButton_1.gridy = 0;
			add(findPrevious, gbc_btnNewButton_1);
		}
		@Override
		protected void init() {
			super.init();
			searchLabel.setText("");
			findNext.setText("");
			findNext.setIcon(IconManager.get().getIcon("icons/caret-down.png", IconManager.SMALL_ICON_SIZE));
			findNext.setSize(25,25);
			findPrevious.setText("");
			findPrevious.setIcon(IconManager.get().getIcon("icons/caret-up.png", IconManager.SMALL_ICON_SIZE));
			findPrevious.setSize(18, 18);
		}
	}
}
