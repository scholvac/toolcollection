package io.gitlab.scholvac.ui.prop.edt.impl;

import java.awt.Component;
import java.util.function.Function;

import javax.swing.JComboBox;

import io.gitlab.scholvac.prop.IProperty;
import io.gitlab.scholvac.rcore.RClassifier.REnum;
import io.gitlab.scholvac.ui.IUIElementFactory;

public class REnumEditor<T extends Enum<?>> extends AbstractPropertyEditor<T> {

	private REnum 					mClassifier;
	private JComboBox<T> 			mComboBox;

	public REnumEditor(final IProperty<T> property, final Function<IProperty<?>, IProperty<?>> propertyWrapper, final REnum classifier) {
		super(property, propertyWrapper);
		mClassifier = classifier;
	}

	@Override
	protected Component createEditor(final IProperty property, final IUIElementFactory factory) {
		mComboBox = new JComboBox<>((T[])mClassifier.getEnumConstants());
		mComboBox.setSelectedItem(property.get());
		mComboBox.setEnabled(property.isEditable());

		mComboBox.addActionListener(al -> runInEvtThread(() -> {
			set((T) mComboBox.getSelectedItem());
		}));
		return mComboBox;
	}

	@Override
	protected void onEditorShown(final Component editor, final IProperty<T> property) {
		disposeOnDispose(property.addPropertyChangeListener(pcl -> runInThisThread(() -> mComboBox.setSelectedItem(pcl.getNewValue()))));
	}
}
