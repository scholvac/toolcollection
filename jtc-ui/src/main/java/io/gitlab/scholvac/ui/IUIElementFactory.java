package io.gitlab.scholvac.ui;

import java.awt.Component;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionListener;
import java.util.function.Function;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.SwingUtilities;

import org.jdesktop.swingx.JXTree;

import io.gitlab.scholvac.prop.IProperty;
import io.gitlab.scholvac.ui.impl.DefaultUIElementFactory;
import io.gitlab.scholvac.ui.prop.edt.IPropertyEditor;
import io.gitlab.scholvac.ui.prop.edt.IPropertyEditorProvider;

public interface IUIElementFactory {

	IUIElementFactory	INSTANCE = new DefaultUIElementFactory();

	JPanel 				createJPanel();
	JScrollPane 		createJScrollPane(final Component view);
	JTabbedPane 		createJTabbedPane();
	JSplitPane 			createJSplitPane();

	JXTree 				createJTree();

	JButton 			createJButton();
	JToggleButton 		createJToggleButton();

	JLabel 				createLabel(final String txt);
	JTextField 			createJTextField(final String txt, final int columns);
	JSpinner 			createJSpinner();
	JCheckBox  			createJCheckbox();
	<T> JComboBox<T> 	createJComboBox();
	JTextArea 			createJTextArea();

	JDialog createJDialog(final Dialog parent);
	JDialog createJDialog(final Frame parent);
	JDialog createJDialog(final Window parent);
	JFileChooser createJFileChooser();

	JMenuItem createJMenuItem(final String label);



	////////////////////////////////////////////////////////////////////////////////
	//			property manager methods
	////////////////////////////////////////////////////////////////////////////////

	<T> IPropertyEditor<T> getPropertyEditor(final IProperty<T> property, Function<IProperty<?>, IProperty<?>> propertyWrapper);
	void addPropertyEditorProvider(final IPropertyEditorProvider provider);
	void removePropertyEditorProvider(final IPropertyEditorProvider provider);

	default <T> IPropertyEditor<T> getPropertyEditor(final IProperty<T> property){
		return getPropertyEditor(property, it -> it);
	}
	default <T> Component getPropertyEditorComponent(final IProperty<T> property) {
		return getPropertyEditorComponent(property, it -> it);
	}
	default <T> Component getPropertyEditorComponent(final IProperty<T> property, final Function<IProperty<?>, IProperty<?>> propertyWrapper) {
		if (property == null)
		{
			return new JTextField("Editor-Dummy"); //for window builder only
		}
		final IPropertyEditor<T> editor = getPropertyEditor(property, propertyWrapper);
		if (editor == null) {
			return null;
		}
		return editor.getEditorComponent();
	}


	////////////////////////////////////////////////////////////////////////////////
	//			real defaults
	////////////////////////////////////////////////////////////////////////////////

	default JButton createJButton(final String label, final ActionListener alistener) {
		final JButton btn = createJButton();
		btn.setText(label);
		btn.addActionListener(alistener);
		return btn;
	}


	default JTextField createJTextField() { return createJTextField(0);}
	default JTextField createJTextField(final String txt) {
		return createJTextField(txt, 0);
	}
	default JTextField createJTextField(final int columns) {
		return createJTextField(null, columns);
	}


	default JCheckBox createJCheckbox(final boolean selected) {
		final JCheckBox cb = createJCheckbox();
		cb.setSelected(selected);
		return cb;
	}
	default JCheckBox createJCheckbox(final String txt, final boolean selected) {
		final JCheckBox cb = createJCheckbox();
		cb.setText(txt);
		cb.setSelected(selected);
		return cb;
	}
	/** Helper method that decides if the runnable need to be forwarded into the
	 * AWT Thread or not.
	 * This method is usefull for debugging, otherwise SwingUtils.invokeLater, does the
	 * same job.
	 *
	 * @param runnable
	 */
	static void runInAWTThread(final Runnable runnable) {
		if (SwingUtilities.isEventDispatchThread()) {
			runnable.run();
		} else {
			SwingUtilities.invokeLater(runnable);
		}
	}
	boolean showQuestionDialogWithCheckbox(Window object, String string, String string2,
			IProperty<Boolean> mAskConfirmation);

}
