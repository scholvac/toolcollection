package io.gitlab.scholvac.ui;

import java.util.ArrayList;
import java.util.List;

import io.gitlab.scholvac.service.IService;
import io.gitlab.scholvac.service.ServiceManager;

public interface ILabelProvider {
	public static class Info {
		public final String label;
		public final String description;
		public final String icon;
		public Info(final String label, final String description, final String icon) {
			this.label = label;
			this.description = description;
			this.icon = icon;
		}
		public static Info fromClass(final Object instance) {
			if (instance == null) return new Info("Unknown", null, null);
			return new Info(instance.getClass().getSimpleName(), null, null);
		}
	}
	Info describe(final Object object);




	public static class LabelProvider implements ILabelProvider, IService {
		public static LabelProvider get() { return ServiceManager.get(LabelProvider.class); }

		private List<ILabelProvider> 	mDelegates = new ArrayList<>();

		public void addLabelProvider(final ILabelProvider provider) {
			if (provider != null)
				mDelegates.add(0, provider);
		}
		public void removeLabelProvider(final ILabelProvider provider) {
			mDelegates.remove(provider);
		}
		@Override
		public Info describe(final Object object) {
			if (object == null)
				return null;
			for (int i = 0; i < mDelegates.size(); i++) {
				final ILabelProvider provider = mDelegates.get(i);
				final Info info = provider.describe(object);
				if (info != null)
					return info;
			}
			return null;
		}
	}
}
