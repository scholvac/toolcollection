package io.gitlab.scholvac.ui.impl;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.Window;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToggleButton;

import org.jdesktop.swingx.JXTree;

import io.gitlab.scholvac.log.ILogger;
import io.gitlab.scholvac.log.SLog;
import io.gitlab.scholvac.prop.IProperty;
import io.gitlab.scholvac.ui.IUIElementFactory;
import io.gitlab.scholvac.ui.prop.edt.IPropertyEditor;
import io.gitlab.scholvac.ui.prop.edt.IPropertyEditorProvider;
import io.gitlab.scholvac.ui.prop.edt.impl.StandardPropertyEditorProvider;

public class DefaultUIElementFactory implements IUIElementFactory {

	protected static final ILogger					LOG = SLog.getLogger(DefaultUIElementFactory.class);

	private final List<IPropertyEditorProvider>		mProviders = new ArrayList<>();

	public DefaultUIElementFactory() {
		addPropertyEditorProvider(new StandardPropertyEditorProvider());
	}

	@Override
	public <T> IPropertyEditor<T> getPropertyEditor(final IProperty<T> property, final Function<IProperty<?>, IProperty<?>> propertyWrapper) {
		if (property == null) {
			return null;
		}
		return syncGetPropertyEditor(property, propertyWrapper);
	}
	public <T> IPropertyEditor<T> syncGetPropertyEditor(final IProperty<T> property, final Function<IProperty<?>, IProperty<?>> propertyWrapper) {
		for (final IPropertyEditorProvider mProvider : mProviders) {
			try {
				final IPropertyEditor<T> editor = mProvider.provide(property, propertyWrapper);
				if (editor != null)
					return editor;
			}catch(final Throwable t) {
				LOG.warn("PropertyEditorProvider failed to check for valid property {}", property.getName());
				t.printStackTrace();
			}
		}
		LOG.trace("Could not find an editor for property: {}", property.getName());
		return null;
	}

	@Override
	public void addPropertyEditorProvider(final IPropertyEditorProvider provider) {
		if (provider != null && !mProviders.contains(provider))
			mProviders.add(0, provider); //default provider shall be checked last
	}

	@Override
	public void removePropertyEditorProvider(final IPropertyEditorProvider provider) {
		mProviders.remove(provider);
	}

	@Override
	public JPanel createJPanel() {
		return new JPanel();
	}

	@Override
	public JScrollPane createJScrollPane(final Component view) {
		return new JScrollPane(view);
	}

	@Override
	public JTabbedPane createJTabbedPane() {
		return new JTabbedPane();
	}

	@Override
	public JSplitPane createJSplitPane() {
		return new JSplitPane();
	}

	@Override
	public JButton createJButton() {
		return new JButton();
	}

	@Override
	public JToggleButton createJToggleButton() {
		return new JToggleButton();
	}

	@Override
	public JLabel createLabel(final String txt) {
		return new JLabel(txt);
	}

	@Override
	public JTextField createJTextField(final String txt, final int columns) {
		return new JTextField(txt, columns);
	}

	@Override
	public JSpinner createJSpinner() {
		return new JSpinner();
	}

	@Override
	public JCheckBox createJCheckbox() {
		return new JCheckBox();
	}

	@Override
	public <T> JComboBox<T> createJComboBox() {
		return new JComboBox<>();
	}

	@Override
	public JTextArea createJTextArea() {
		return new JTextArea();
	}

	@Override
	public JDialog createJDialog(final Dialog parent) {
		return new JDialog(parent);
	}

	@Override
	public JDialog createJDialog(final Frame parent) {
		return new JDialog(parent);
	}

	@Override
	public JDialog createJDialog(final Window parent) {
		return new JDialog(parent);
	}

	@Override
	public JFileChooser createJFileChooser() {
		return new JFileChooser();
	}

	@Override
	public JMenuItem createJMenuItem(final String label) {
		return new JMenuItem(label);
	}

	@Override
	public JXTree createJTree() {
		return new JXTree();
	}

	@Override
	public boolean showQuestionDialogWithCheckbox(final Window parent, final String title, final String message, final IProperty<Boolean> showAgainProperty) {
		final Component edtComp = getPropertyEditorComponent(showAgainProperty, it->it);
		final JPanel showAgainPanel = createJPanel();
		showAgainPanel.setLayout(new BorderLayout());
		showAgainPanel.add(edtComp, BorderLayout.WEST);
		showAgainPanel.add(createLabel("Do not show this message again"), BorderLayout.CENTER);

		final Object[] params = { message, showAgainPanel };
		final int result = JOptionPane.showConfirmDialog(null, params, title, JOptionPane.YES_NO_OPTION);
		if (result != JOptionPane.YES_OPTION) {
			return false;
		}
		return true;
	}

}