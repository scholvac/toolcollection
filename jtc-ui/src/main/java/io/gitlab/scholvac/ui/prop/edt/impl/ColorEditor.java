package io.gitlab.scholvac.ui.prop.edt.impl;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JButton;
import javax.swing.JColorChooser;

import io.gitlab.scholvac.prop.IProperty;
import io.gitlab.scholvac.ui.IUIElementFactory;
import io.gitlab.scholvac.ui.utils.VisibilityUtil;

public class ColorEditor extends AbstractPropertyEditor<Color> {

	/**
	 * @wbp.parser.entryPoint
	 */
	public ColorEditor(final IProperty<Color> prop) {
		super(prop, null);
	}

	/**
	 * @wbp.parser.entryPoint
	 */
	@Override
	protected Component createEditor(final IProperty<Color> property, final IUIElementFactory factory) {
		final JButton btn = factory.createJButton();
		btn.setBackground(property.get());

		VisibilityUtil.whileVisible(btn, () -> property.addValueChangeListener(pcl -> btn.setBackground(property.get()), true));

		btn.addActionListener(al ->{
			final Color color = JColorChooser.showDialog(btn, "Choose Color", null);
			if (color != null)
				property.set(color);
		});

		return btn;
	}
}
