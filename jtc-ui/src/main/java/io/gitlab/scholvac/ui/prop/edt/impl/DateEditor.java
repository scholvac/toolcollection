package io.gitlab.scholvac.ui.prop.edt.impl;

import java.awt.Component;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;

import io.gitlab.scholvac.prop.IProperty;
import io.gitlab.scholvac.ui.IUIElementFactory;
import io.gitlab.scholvac.ui.utils.VisibilityUtil;

public class DateEditor extends AbstractPropertyEditor<Date> {

	/**
	 * @wbp.parser.entryPoint
	 */
	public DateEditor(final IProperty<Date> prop) {
		super(prop, null);
	}

	/**
	 * @wbp.parser.entryPoint
	 */
	@Override
	protected Component createEditor(final IProperty<Date> property, final IUIElementFactory factory) {
		final SpinnerDateModel dateModel = new SpinnerDateModel(new Date(), null, null, Calendar.DAY_OF_MONTH);

		final JSpinner spinner = new JSpinner(dateModel);
		spinner.setEditor(new JSpinner.DateEditor(spinner));//, getDefaultPattern(spinner.getLocale())));

		VisibilityUtil.whileVisible(spinner, () -> property.addValueChangeListener(pcl -> dateModel.setValue(pcl.getNewValue()), true));

		return spinner;
	}

}
