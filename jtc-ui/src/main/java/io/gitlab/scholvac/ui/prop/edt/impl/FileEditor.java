package io.gitlab.scholvac.ui.prop.edt.impl;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.File;
import java.util.Arrays;
import java.util.function.Function;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import javax.swing.filechooser.FileNameExtensionFilter;

import io.gitlab.scholvac.other.ExtendedFile;
import io.gitlab.scholvac.other.ExtendedFile.Direction;
import io.gitlab.scholvac.other.ExtendedFile.Type;
import io.gitlab.scholvac.prop.IProperty;
import io.gitlab.scholvac.ui.IUIElementFactory;

public class FileEditor extends AbstractPropertyEditor<File> {

	//Short-cuts to not include the ExtendedFile
	public static final Type		TYPE_DIRECTORY 					= Type.DIRECTORY;
	public static final Type		TYPE_FILE						= Type.FILE;
	public static final Type		TYPE_BOTH						= Type.BOTH;
	public static final Direction	DIRECTION_OPEN					= Direction.OPEN;
	public static final Direction	DIRECTION_SAVE					= Direction.SAVE;


	public static final String 		ANNOTATION__FileType 			= "FileType";
	public static final String		ANNOTATION__FileDirection 		= "Direction";
	/** Accepts either one String (for one extension) or a Array of Strings. Each Extension String has to have the
	 * following format:
	 *
	 * 		DESCRIPTION,EXTENSION(,EXTENSION)*
	 * Like:
	 * 		"Office Documents,docx,xlsx"
	 * if only one element is found (e.g. no ',') the element is used as extension
	 * 		"*" == ",*"
	 */
	public static final String		ANNOTATION__FileExtensions		= "Extensions";




	private final IProperty<String>				mPathProperty;
	private Type								mType = Type.FILE;
	private Direction							mDirection = Direction.OPEN;
	private String[]							mExtensions = {"*"};


	public FileEditor(final IProperty<File> prop, final Function<IProperty<?>, IProperty<?>> propertyWrapper) {
		super(prop, propertyWrapper);

		if (prop.get() != null && prop.get() instanceof ExtendedFile) {
			mType = ((ExtendedFile)prop.get()).getType();
			mDirection = ((ExtendedFile)prop.get()).getDirection();
		}
		//overwrite with annotation values
		mType = getAnnotation(ANNOTATION__FileType, mType);
		mDirection = getAnnotation(ANNOTATION__FileDirection, mDirection);
		try {
			mExtensions = getAnnotation(ANNOTATION__FileExtensions, mExtensions);
		}catch(final ClassCastException cce) {
			final String ext = getAnnotation(ANNOTATION__FileExtensions, null);
			if (ext != null) {
				mExtensions = new String[] {ext};
			}
		}

		//ensure valid values
		if (mType == null) {
			mType = Type.BOTH;
		}
		if (mDirection == null) {
			mDirection = Direction.OPEN;
		}
		if (mExtensions == null || mExtensions.length == 0) {
			mExtensions = new String[] {"*"};
		}

		mPathProperty = IProperty.of("path", "The absolute path of the file", true, getPath(prop));
		disposeOnDispose(prop.addValueChangeListener(evt -> {
			mPathProperty.set(getPath(prop));
		}));
		disposeOnDispose(mPathProperty.addValueChangeListener(evt -> {
			setPath(prop, evt.getNewValue());
		}));

		//		mPathProperty = new FunctionalProperty<>( , true, () -> getPath(prop), newVal -> {
		//			setPath(prop, newVal);
		//			FunctionalProperty<>.this.fireValueChanged(null, newVal);
		//		});
		//		disposeOnDispose(prop.addPropertyChangeListener(evt->mPathProperty.fireValueChanged(null, evt.getNewValue()+"")));

	}
	private void setPath(final IProperty<File> prop, final String newVal) {
		System.out.println("NewVal = " + newVal);
		if (newVal != null) {
			if ("-.-".equals(newVal)) {
				prop.set(null);
			} else {
				prop.set(new File(newVal));
			}
		}else {
			prop.set(null);
		}
	}
	private String getPath(final IProperty<File> prop) {
		if (prop.get() == null) {
			return "-.-";
		}
		return prop.get().getAbsolutePath();
	}

	/**
	 * @wbp.parser.entryPoint
	 */
	@Override
	protected Component createEditor(final IProperty<File> property, final IUIElementFactory factory) {
		final JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout(0, 0));

		panel.add(factory.getPropertyEditorComponent(mPathProperty, getWrapper()), BorderLayout.CENTER);

		final JButton btn = new JButton("...");
		panel.add(btn, BorderLayout.EAST);

		btn.addActionListener(al -> showDialog(panel) );
		return panel;
	}

	private void showDialog(final JPanel panel) {
		final JFileChooser jf = new JFileChooser(get());
		jf.setMultiSelectionEnabled(false);
		jf.setCurrentDirectory(get());
		jf.setDialogType(mDirection == Direction.OPEN ? JFileChooser.OPEN_DIALOG : JFileChooser.SAVE_DIALOG);

		jf.setFileSelectionMode(mType.getJFileSelectionMode());
		if (mType == Type.FILE) { //otherwise extension do not make any sense...?!
			for (final String ext : mExtensions) {
				final String[] split = ext.split(",");
				if (split.length == 1) {
					jf.addChoosableFileFilter(new FileNameExtensionFilter("", split[0].trim()));
				}else {
					for (int i = 0; i < split.length; i++) {
						split[i] = split[i].trim();
					}
					jf.addChoosableFileFilter(new FileNameExtensionFilter(split[0], Arrays.copyOfRange(split, 1, split.length)));
				}
			}
		}
		int res = 0;
		if (mDirection == Direction.OPEN) {
			jf.setDialogTitle("Open " + (mType == Type.DIRECTORY ? "Directory" : "File"));
			res = jf.showOpenDialog(panel);
		}else {
			jf.setDialogTitle("Save " + (mType == Type.DIRECTORY ? "Directory" : "File"));
			res = jf.showSaveDialog(panel);
		}
		if (res == JFileChooser.APPROVE_OPTION) {
			final File f = jf.getSelectedFile();
			mPathProperty.set(f != null ? f.getAbsolutePath() : null);
		}
	}


	public static void main(final String[] args) {
		final IProperty<File> fileProp = IProperty.of("File", "String Property", true, new File("pom.xml"));

		final JFrame frame = new JFrame("New FileEditor Editor Demo");
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));

		final GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		frame.getContentPane().setLayout(gridBagLayout);

		final JLabel lblNewLabel = new JLabel("File");
		final GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 1;
		frame.getContentPane().add(lblNewLabel, gbc_lblNewLabel);

		final FileEditor edtByte = new FileEditor(fileProp, null);
		final GridBagConstraints gbc_edtByte = new GridBagConstraints();
		gbc_edtByte.insets = new Insets(0, 0, 5, 0);
		gbc_edtByte.fill = GridBagConstraints.HORIZONTAL;
		gbc_edtByte.gridx = 1;
		gbc_edtByte.gridy = 1;
		frame.getContentPane().add(edtByte.getEditor(IUIElementFactory.INSTANCE), gbc_edtByte);



		frame.setSize(640, 480);
		frame.setVisible(true);
	}
}
