package io.gitlab.scholvac.ui.prop.edt.impl;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.function.Function;

import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.WindowConstants;

import io.gitlab.scholvac.prop.IProperty;
import io.gitlab.scholvac.ui.IUIElementFactory;

public class BooleanEditor extends AbstractPropertyEditor<Boolean> {

	private JCheckBox mCheckbox;

	public BooleanEditor(final IProperty<Boolean> prop, final Function<IProperty<?>, IProperty<?>> wrapper) {
		super(prop, wrapper);
	}

	@Override
	protected Component createEditor(final IProperty<Boolean> property, final IUIElementFactory factory) {
		mCheckbox = factory.createJCheckbox(get());

		mCheckbox.addActionListener(al -> runInThisThread(() -> {
			set(mCheckbox.isSelected());
		}));


		return mCheckbox;
	}

	@Override
	protected void onEditorShown(final Component editor, final IProperty<Boolean> property) {
		disposeWhenHidden(property.addValueChangeListener(pcl -> {
			runInEvtThread(()->mCheckbox.setSelected(get()));
		}, true));

	}

	public static void main(final String[] args) {
		final IProperty<Boolean> boolProp = IProperty.of("Boolean", "String Property", true, true);

		final JFrame frame = new JFrame("New String Editor Demo");
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));

		final GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		frame.getContentPane().setLayout(gridBagLayout);

		final JLabel lblNewLabel = new JLabel("Boolean");
		final GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 1;
		frame.getContentPane().add(lblNewLabel, gbc_lblNewLabel);

		final BooleanEditor edtByte = new BooleanEditor(boolProp, null);
		final GridBagConstraints gbc_edtByte = new GridBagConstraints();
		gbc_edtByte.insets = new Insets(0, 0, 5, 0);
		gbc_edtByte.fill = GridBagConstraints.HORIZONTAL;
		gbc_edtByte.gridx = 1;
		gbc_edtByte.gridy = 1;
		frame.getContentPane().add(edtByte.getEditor(IUIElementFactory.INSTANCE), gbc_edtByte);

		final JLabel lblNewLabel_1 = new JLabel("Boolean-cpy");
		final GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_1.gridx = 0;
		gbc_lblNewLabel_1.gridy = 2;
		frame.getContentPane().add(lblNewLabel_1, gbc_lblNewLabel_1);

		final BooleanEditor edtInteger = new BooleanEditor(boolProp, null);
		final GridBagConstraints gbc_edtInteger = new GridBagConstraints();
		gbc_edtInteger.insets = new Insets(0, 0, 5, 0);
		gbc_edtInteger.fill = GridBagConstraints.HORIZONTAL;
		gbc_edtInteger.gridx = 1;
		gbc_edtInteger.gridy = 2;
		frame.getContentPane().add(edtInteger.getEditor(IUIElementFactory.INSTANCE), gbc_edtInteger);

		frame.setSize(640, 480);
		frame.setVisible(true);
	}
}
