package io.gitlab.scholvac.ui.prop;

import java.awt.BorderLayout;
import java.util.Collection;
import java.util.Comparator;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.swing.Icon;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import io.gitlab.scholvac.StringUtils;
import io.gitlab.scholvac.prop.IProperty;
import io.gitlab.scholvac.prop.IPropertyContext;
import io.gitlab.scholvac.prop.impl.PropertyContext;
import io.gitlab.scholvac.ui.IUIElementFactory;

public class TabbedPropertySheet extends JPanel {

	public enum TabLocation  {
		LEFT(SwingConstants.LEFT), TOP(SwingConstants.TOP), RIGHT(SwingConstants.RIGHT), BOTTOM(SwingConstants.BOTTOM);

		int code;
		TabLocation(final int l) { code = l;}
	}

	private final IUIElementFactory 			mFactory;
	private final Comparator<IProperty<?>> 		mComparator;
	private final IPropertyContext 				mSheetConfig;

	private final JTabbedPane					mTabPane;

	public TabbedPropertySheet() { this(IUIElementFactory.INSTANCE, IProperty.NAME_COMPARATOR); }

	public TabbedPropertySheet(final IUIElementFactory factory, final Comparator<IProperty<?>> comparator) {
		this(factory, comparator, new PropertyContext());
	}

	public TabbedPropertySheet(final IUIElementFactory factory, final Comparator<IProperty<?>> comparator, final IPropertyContext sheetConfig) {
		mFactory = factory;
		mComparator = comparator;
		mSheetConfig = sheetConfig;

		mTabPane = factory.createJTabbedPane();
		setLayout(new BorderLayout());
		add(mTabPane, BorderLayout.CENTER);

		final IProperty<TabLocation> locTab = sheetConfig.getProperty("TabLocation", TabLocation.LEFT);
		locTab.addValueChangeListener(pcl -> mTabPane.setTabPlacement(pcl.getNewValue().code), true);
	}


	//--------- Public interface -------------
	public void asyncUpdateProperties(final Collection<IPropertyContext> contexts) {
		PropertySheet.getExecutor().execute(() -> updateContexts(contexts));
	}

	public void syncUpdateProperties(final Collection<IPropertyContext> contexts) {
		final Future<?> future = PropertySheet.getExecutor().submit(() -> updateContexts(contexts));
		try {
			future.get();
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
	}

	private void updateContexts(final Collection<IPropertyContext> contexts) {
		SwingUtilities.invokeLater(() -> _updateContexts(contexts));
	}
	private void _updateContexts(final Collection<IPropertyContext> contexts) {
		mTabPane.removeAll();
		if (contexts.isEmpty())
			return ;

		for (final IPropertyContext ctx : contexts) {
			String ctxName = ctx.getName();
			if (StringUtils.isNullOrEmpty(ctxName)) ctxName = "Unnamed";
			final String desc = ctx.getDescription();
			final Icon icon = null;

			final PropertySheet ps = new PropertySheet(mFactory, mComparator, mSheetConfig);
			final IProperty[] properties = ctx.getAllProperties(false).toArray(IProperty[]::new);
			if (properties.length > 0) {
				ps.asyncUpdateProperties(properties);
				mTabPane.addTab(ctxName, icon, new JScrollPane(ps), desc);
			}
		}

		updateUI();
	}
}
