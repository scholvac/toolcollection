package io.gitlab.scholvac.ui.prop.edt.impl;

import java.awt.Color;
import java.io.File;
import java.util.Date;
import java.util.function.Function;

import io.gitlab.scholvac.ReflectionManager;
import io.gitlab.scholvac.other.ExtendedFile;
import io.gitlab.scholvac.prop.IProperty;
import io.gitlab.scholvac.quant.Measurement;
import io.gitlab.scholvac.rcore.RClassifier;
import io.gitlab.scholvac.rcore.RClassifier.REnum;
import io.gitlab.scholvac.ui.prop.edt.IPropertyEditor;
import io.gitlab.scholvac.ui.prop.edt.IPropertyEditorProvider;
import io.gitlab.scholvac.ui.prop.edt.impl.NumberEditor.ByteEditor;
import io.gitlab.scholvac.ui.prop.edt.impl.NumberEditor.DoubleEditor;
import io.gitlab.scholvac.ui.prop.edt.impl.NumberEditor.FloatEditor;
import io.gitlab.scholvac.ui.prop.edt.impl.NumberEditor.IntegerEditor;
import io.gitlab.scholvac.ui.prop.edt.impl.NumberEditor.LongEditor;
import io.gitlab.scholvac.ui.prop.edt.impl.NumberEditor.ShortEditor;
import io.gitlab.scholvac.ui.prop.edt.impl.TextEditor.StringEditor;

public class StandardPropertyEditorProvider implements IPropertyEditorProvider {

	ReflectionManager	mRefMgr= ReflectionManager.get();
	ReflectionManager	mInternalEnumManager = new ReflectionManager();

	@SuppressWarnings("unchecked")
	@Override
	public <T> IPropertyEditor<T> provide(final IProperty<T> property, final Function<IProperty<?>, IProperty<?>> propertyWrapper) {
		final Class<?> clazz = property.getType();
		if (clazz == String.class) {
			return (IPropertyEditor<T>) new StringEditor((IProperty<String>) property, propertyWrapper);
		}
		if (clazz == byte.class || clazz == Byte.class) {
			return (IPropertyEditor<T>) new ByteEditor((IProperty<Byte>) property, propertyWrapper);
		}
		if (clazz == short.class || clazz == Short.class) {
			return (IPropertyEditor<T>) new ShortEditor((IProperty<Short>) property, propertyWrapper);
		}
		if (clazz == int.class || clazz == Integer.class) {
			return (IPropertyEditor<T>) new IntegerEditor((IProperty<Integer>) property, propertyWrapper);
		}
		if (clazz == long.class || clazz == Long.class) {
			return (IPropertyEditor<T>) new LongEditor((IProperty<Long>) property, propertyWrapper);
		}
		if (clazz == float.class || clazz == Float.class) {
			return (IPropertyEditor<T>) new FloatEditor((IProperty<Float>) property, propertyWrapper);
		}
		if (clazz == double.class || clazz == Double.class) {
			return (IPropertyEditor<T>) new DoubleEditor((IProperty<Double>) property, propertyWrapper);
		} else if (clazz == boolean.class || clazz == Boolean.class) {
			return (IPropertyEditor<T>) new io.gitlab.scholvac.ui.prop.edt.impl.BooleanEditor((IProperty<Boolean>) property, propertyWrapper);
		} else if (clazz == File.class || clazz == ExtendedFile.class) {
			return (IPropertyEditor<T>)new FileEditor((IProperty<File>) property, propertyWrapper);
		} else if (clazz == Color.class) {
			return (IPropertyEditor<T>) new ColorEditor((IProperty<Color>) property);
		} else if (clazz == Date.class) {
			return (IPropertyEditor<T>) new DateEditor((IProperty<Date>)property);
		}
		//		else if (clazz == Point2D.class || clazz == Point2D.Double.class)
		//			return (IPropertyEditor<T>) new Point2DPropertyEditor((IProperty<Point2D>) property, propertyWrapper);
		//		else if (clazz == Rectangle.class)
		//			return (IPropertyEditor<T>) new RectanglePropertyEditor((IProperty<Rectangle>) property, propertyWrapper);
		//
		final T value = property.get();
		if (value != null && Measurement.class.isAssignableFrom(clazz)) {
			return new MeasurementEditor(property, propertyWrapper);
		}

		//	---------------	Registered (Reflection) Enumerations	---------------------

		if (clazz != null && clazz.isEnum()) {
			RClassifier classifier = null;
			//first check if the enum has been registered by the user
			if (mRefMgr.containsClassifier(clazz)) {
				classifier = mRefMgr.getClassifier(clazz);
			}
			if (classifier == null) {
				//if it has not been registered we use an internal reflection manager to reuse the capabilities but do not interferre with the global manager.
				if (mInternalEnumManager.containsClassifier(clazz) == false) {
					mInternalEnumManager.register(clazz);
				}
				classifier = mInternalEnumManager.getClassifier(clazz);
			}
			if (classifier instanceof REnum) {
				return new REnumEditor(property, propertyWrapper, (REnum)classifier);
			}
		}
		return null;
	}



}
