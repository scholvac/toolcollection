package io.gitlab.scholvac.ui.prop.edt;

import java.awt.Component;

import io.gitlab.scholvac.IDisposeable;
import io.gitlab.scholvac.observable.IObservableObject;
import io.gitlab.scholvac.prop.IAnnotatedObject;
import io.gitlab.scholvac.prop.IProperty;
import io.gitlab.scholvac.ui.IUIElementFactory;

public interface IPropertyEditor<T> extends IDisposeable, IObservableObject<T>, IAnnotatedObject
{
	default Component getEditorComponent() { return getEditor(IUIElementFactory.INSTANCE);}

	IProperty<T> getProperty();
	Component getEditor(IUIElementFactory factory);
}
