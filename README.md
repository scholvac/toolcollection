[![pipeline status](https://gitlab.com/scholvac/toolcollection/badges/develop-2.0/pipeline.svg)](https://gitlab.com/scholvac/toolcollection/-/commits/develop-2.0) [![Latest Release](https://gitlab.com/scholvac/toolcollection/-/badges/release.svg)](https://gitlab.com/scholvac/toolcollection/-/releases)

# ToolCollection

Collection of frequently used java tools