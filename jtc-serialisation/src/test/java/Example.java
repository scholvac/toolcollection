import java.util.Map;

import org.apache.fury.Fury;
import org.apache.fury.config.Language;

public class Example {
	public static class SomeClass {
		SomeClass f1;
		Map<String, String> f2;
		Map<String, String> f3;
	}
	public static void main(final String[] args) {
		final SomeClass object = new SomeClass();
		// Note that Fury instances should be reused between
		// multiple serializations of different objects.
		final Fury fury = Fury.builder().withLanguage(Language.JAVA)
				// Allow to deserialize objects unknown types,
				// more flexible but less secure.
				//				.withMetaContextShare(true)
				.requireClassRegistration(false)
				.build();
		// Registering types can reduce class name serialization overhead, but not mandatory.
		// If secure mode enabled, all custom types must be registered.
		//		fury.register(SomeClass.class);
		final byte[] bytes = fury.serialize(object);
		System.out.println(fury.deserialize(bytes));
	}
}