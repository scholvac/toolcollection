package io.gitlab.scholvac.resources;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collection;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

class ResourceManagerTest {

	private ResourceManager resourceManager;
	@TempDir
	Path tempDir;

	@BeforeEach
	void setUp() {
		final File homeDir = tempDir.resolve("home").toFile();
		final Collection<File> resourcePaths = Arrays.asList(
				tempDir.resolve("resources1").toFile(),
				tempDir.resolve("resources2").toFile()
				);
		resourceManager = new ResourceManager(homeDir, resourcePaths, getClass());
	}

	@Test
	void testGetResource() throws IOException {
		// Create a test resource file
		final File resourceFile = tempDir.resolve("resources1/test.txt").toFile();
		resourceFile.getParentFile().mkdirs();
		Files.write(resourceFile.toPath(), "test content".getBytes());

		final URL resource = resourceManager.getResource("test.txt");
		assertNotNull(resource);
		assertEquals(resourceFile.toURI().toURL(), resource);
	}

	@Test
	void testGetHomeDirectory() {
		final File homeDir = resourceManager.getHomeDirectory();
		assertEquals(tempDir.resolve("home").toFile(), homeDir);
	}

	@Test
	void testGetLookupPaths() {
		final Collection<File> lookupPaths = resourceManager.getLookupPaths();
		assertEquals(2, lookupPaths.size());
		assertTrue(lookupPaths.contains(tempDir.resolve("resources1").toFile()));
		assertTrue(lookupPaths.contains(tempDir.resolve("resources2").toFile()));
	}

	@Test
	void testFindFile() throws IOException {
		// Create test files
		final File homeFile = tempDir.resolve("home/home.txt").toFile();
		final File resource1File = tempDir.resolve("resources1/resource1.txt").toFile();
		final File resource2File = tempDir.resolve("resources2/resource2.txt").toFile();

		homeFile.getParentFile().mkdirs();
		resource1File.getParentFile().mkdirs();
		resource2File.getParentFile().mkdirs();

		Files.write(homeFile.toPath(), "home content".getBytes());
		Files.write(resource1File.toPath(), "resource1 content".getBytes());
		Files.write(resource2File.toPath(), "resource2 content".getBytes());

		assertEquals(homeFile, resourceManager.findFile("home.txt"));
		assertEquals(resource1File, resourceManager.findFile("resource1.txt"));
		assertEquals(resource2File, resourceManager.findFile("resource2.txt"));
		assertNull(resourceManager.findFile("non-existent.txt"));
	}

	@Test
	void testResolveInHome() {
		final File resolved = resourceManager.resolveInHome("test.txt");
		assertEquals(tempDir.resolve("home/test.txt").toFile(), resolved);
	}

	@Test
	void testUnpackFileToHome() throws IOException {
		// Create a test resource file
		final File resourceFile = tempDir.resolve("resources1/unpack.txt").toFile();
		resourceFile.getParentFile().mkdirs();
		Files.write(resourceFile.toPath(), "unpack content".getBytes());

		final File unpackedFile = resourceManager.unpackFileToHome("unpack.txt");
		assertTrue(unpackedFile.exists());
		assertEquals("unpack content", Files.readAllLines(unpackedFile.toPath()).get(0));
	}

	@Test
	void testGetStream() throws IOException {
		// Create a test resource file
		final File resourceFile = tempDir.resolve("resources1/stream.txt").toFile();
		resourceFile.getParentFile().mkdirs();
		Files.write(resourceFile.toPath(), "stream content".getBytes());

		try (InputStream stream = resourceManager.getStream("stream.txt")) {
			assertNotNull(stream);
			final byte[] data = new byte[stream.available()];
			stream.read(data);
			assertEquals("stream content", new String(data));
		}
	}
}