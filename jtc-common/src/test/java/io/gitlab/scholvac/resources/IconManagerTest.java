package io.gitlab.scholvac.resources;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.awt.image.BufferedImage;
import java.net.URL;

import javax.swing.ImageIcon;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

class IconManagerTest {

	private IconManager iconManager;

	@Mock
	private ResourceManager resourceManager;

	@BeforeEach
	void setUp() {
		MockitoAnnotations.openMocks(this);
		iconManager = new IconManager(resourceManager);
	}

	@Test
	void testGetIcon() {
		final BufferedImage image = new BufferedImage(100, 100, BufferedImage.TYPE_INT_ARGB);
		final ImageIcon icon = IconManager.getIcon(image);
		assertNotNull(icon);
		assertEquals(100, icon.getIconWidth());
		assertEquals(100, icon.getIconHeight());
	}

	@Test
	void testGetScaledIcon() {
		final BufferedImage image = new BufferedImage(100, 100, BufferedImage.TYPE_INT_ARGB);
		final ImageIcon icon = iconManager.getIcon(image, IconManager.SMALL_ICON_SIZE);
		assertNotNull(icon);
		assertEquals(IconManager.SMALL_ICON_SIZE, icon.getIconWidth());
		assertEquals(IconManager.SMALL_ICON_SIZE, icon.getIconHeight());
	}

	@Test
	void testGetScaledImage() {
		final BufferedImage image = new BufferedImage(100, 100, BufferedImage.TYPE_INT_ARGB);
		final BufferedImage scaledImage = IconManager.getScaledImage(image, IconManager.SMALL_ICON_SIZE, true);
		assertNotNull(scaledImage);
		assertEquals(IconManager.SMALL_ICON_SIZE, scaledImage.getWidth());
		assertEquals(IconManager.SMALL_ICON_SIZE, scaledImage.getHeight());
	}

	@Test
	void testToBufferedImage() {
		final BufferedImage image = new BufferedImage(100, 100, BufferedImage.TYPE_INT_ARGB);
		final BufferedImage result = IconManager.toBufferedImage(image);
		assertSame(image, result);
	}

	@Test
	void testGetImage() throws Exception {
		final String path = "test/image.png";
		final URL mockUrl = new URL("file:test/image.png");
		final BufferedImage mockImage = new BufferedImage(100, 100, BufferedImage.TYPE_INT_ARGB);

		when(resourceManager.getResource(null, path)).thenReturn(mockUrl);
		when(resourceManager.getResource(IconManagerTest.class, path)).thenReturn(mockUrl);

		// Mock the behavior of getImage(URL)
		final IconManager spyIconManager = spy(iconManager);
		doReturn(mockImage).when(spyIconManager).getImage(mockUrl);

		BufferedImage result = spyIconManager.getImage(path);
		assertNotNull(result);
		assertEquals(100, result.getWidth());
		assertEquals(100, result.getHeight());

		result = spyIconManager.getImage(IconManagerTest.class, path);
		assertNotNull(result);
		assertEquals(100, result.getWidth());
		assertEquals(100, result.getHeight());
	}

	@Test
	void testGetImageWithSize() throws Exception {
		final String path = "test/image.png";
		final URL mockUrl = new URL("file:test/image.png");
		final BufferedImage mockImage = new BufferedImage(100, 100, BufferedImage.TYPE_INT_ARGB);

		when(resourceManager.getResource(null, path)).thenReturn(mockUrl);
		when(resourceManager.getResource(IconManagerTest.class, path)).thenReturn(mockUrl);

		// Mock the behavior of getImage(URL, int)
		final IconManager spyIconManager = spy(iconManager);
		doReturn(mockImage).when(spyIconManager).getImage(eq(mockUrl), anyInt());

		BufferedImage result = spyIconManager.getImage(path, IconManager.SMALL_ICON_SIZE);
		assertNotNull(result);
		assertEquals(100, result.getWidth());
		assertEquals(100, result.getHeight());

		result = spyIconManager.getImage(IconManagerTest.class, path, IconManager.SMALL_ICON_SIZE);
		assertNotNull(result);
		assertEquals(100, result.getWidth());
		assertEquals(100, result.getHeight());
	}
}