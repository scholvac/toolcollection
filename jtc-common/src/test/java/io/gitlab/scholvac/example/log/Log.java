package io.gitlab.scholvac.example.log;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.LoggerConfig;
import org.junit.jupiter.api.Test;

import io.gitlab.scholvac.log.ILogger;
import io.gitlab.scholvac.log.LogAppender;
import io.gitlab.scholvac.log.LogAppender.ILogAppender;
import io.gitlab.scholvac.log.LogAppender.LogEvent;
import io.gitlab.scholvac.log.SLog;

public class Log {

	static class ListAppender extends AbstractAppender {

		private final List<LogEvent> log;

		public ListAppender(final String name, final List<LogEvent> testLog) {
			super(name, null, null);
			this.log = testLog;
		}

		@Override
		public void append(final org.apache.logging.log4j.core.LogEvent logEvent) {
			//			log.add(new TestLogEvent(logEvent));
			logEvent.getMessage().getFormattedMessage();
			System.out.println("The Event...");
		}
	}

	private static class Log4J2Appender implements java.lang.reflect.InvocationHandler {
		private static final Method sGetLevel;
		private static final Method sGetMessage;
		private static final Method sGetLoggerName;
		private static final Method sGetThreadName;
		private static final Method sGetMessageFormatedMessage;
		static {
			Method l = null, m = null, lo = null, th = null, mm = null;
			try {
				final Class<?> clazz = Class.forName("org.apache.logging.log4j.core.LogEvent");
				l = clazz.getDeclaredMethod("getLevel", null);
				m = clazz.getMethod("getMessage", null);
				lo = clazz.getMethod("getLoggerName", null);
				th = clazz.getMethod("getThreadName", null);
				final Class<?> msgClazz = Thread.currentThread().getContextClassLoader().loadClass("org.apache.logging.log4j.message.Message");
				mm = msgClazz.getMethod("getFormattedMessage", null);
			} catch (ClassNotFoundException | NoSuchMethodException | SecurityException e) {
				e.printStackTrace();
			}
			sGetLevel = l;
			sGetMessage = m;
			sGetLoggerName = lo;
			sGetThreadName = th;
			sGetMessageFormatedMessage = mm;
		}
		public boolean isStarted() {
			return true;
		}

		@Override
		public Object invoke(final Object proxy, final Method method, final Object[] args) throws Throwable {
			final String n = method.getName();
			if ("equals".equals(n))
				return super.equals(args[0]);
			if ("isStarted".equals(n))
				return true;
			if ("getName".equals(n))
				return "MyReflectionAppender";
			if ("append".equals(n)) {
				final Object evt = args[0];
				final Object l = sGetLevel.invoke(evt, null);
				final Object msg = sGetMessage.invoke(evt, null);
				final Object msgStr = sGetMessageFormatedMessage.invoke(msg, null);
				final Object lo= sGetLoggerName.invoke(evt, null);
				final Object th= sGetThreadName.invoke(evt, null);
				System.out.println(msgStr);
				//				handleDoAppend(new LogEvent(m.toString(), l.toString(), th.toString(), lo.toString()));
			}
			return null;
		}

	}


	public static void main(final String[] args) throws Throwable {
		final ILogger logger = SLog.getLogger("Foo");
		logger.info("First");

		final LoggerContext loggerContext = (LoggerContext) LogManager.getContext(false);
		final Configuration configuration = loggerContext.getConfiguration();
		final LoggerConfig rootLoggerConfig = configuration.getLoggerConfig("");



		final Log4J2Appender app = new Log4J2Appender();
		final Class<?> appenderClazz = Thread.currentThread().getContextClassLoader().loadClass("org.apache.logging.log4j.core.Appender");
		final Object appender = java.lang.reflect.Proxy.newProxyInstance(
				LogAppender.class.getClassLoader(),
				new java.lang.Class[] {appenderClazz},
				app );
		rootLoggerConfig.addAppender((Appender) appender, Level.ALL, null);
		logger.info("Second");
	}

	@Test
	public void testImplLogAppender() {

	}


	@Test
	public void testLogAppender() {
		final List<String> messages = new ArrayList<>();
		final ILogAppender appender = new ILogAppender() {
			@Override
			public void doAppend(final LogEvent event) {
				messages.add(event.message);
			}
		};
		SLog.addLogAppender(appender);

		final ILogger log = SLog.getLogger("MyLogger1");
		log.trace("Trace");
		log.debug("Debug");
		log.info("Info");
		log.warn("Warn");
		log.error("error");

		assertEquals(5, messages.size());
	}
}
