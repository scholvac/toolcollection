package io.gitlab.scholvac;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class QualifiedName_Tests {

	@Test
	public void testEquals() {
		final QualifiedName n1 = QualifiedName.from("hallo", "welt");
		final QualifiedName n2 = QualifiedName.from("hallo", "welt");
		final QualifiedName n3 = QualifiedName.from("hallo", "welt", "wie", "gehts", "?");

		assertEquals(n1, n2);
		assertNotEquals(n1, n3);
	}

	@Test
	public void testStartWith() {
		final QualifiedName n1 = QualifiedName.from("hallo");
		final QualifiedName n2 = QualifiedName.from("hallo", "welt");
		final QualifiedName n3 = QualifiedName.from("hallo", "welt", "wie", "gehts", "?");
		final QualifiedName n4 = QualifiedName.from("Foosar");

		assertTrue(n1.startsWith(n1));
		assertTrue(n2.startsWith(n1));
		assertTrue(n3.startsWith(n1));
		assertTrue(n3.startsWith(n2));

		assertFalse(n1.startsWith(n2));
		assertFalse(n3.startsWith(n4));
	}
	@Test
	void testFromSeperator() {
		final String input = "a:b:c";
		final QualifiedName qn = QualifiedName.fromSeperator(input);
		assertEquals("a", qn.getSegment(0));
		assertEquals("b", qn.getSegment(1));
		assertEquals("c", qn.getSegment(2));
	}

	@Test
	void testFromSeperatorWithDefaultSeparator() {
		final String input = "a:b:c";
		final QualifiedName qn = QualifiedName.fromSeperator(input);
		assertEquals("a", qn.getSegment(0));
		assertEquals("b", qn.getSegment(1));
		assertEquals("c", qn.getSegment(2));
	}

	@Test
	void testFromSeperatorWithEmptyInput() {
		final String input = "";
		final QualifiedName qn = QualifiedName.fromSeperator(input);
		assertFalse(qn.isEmpty());
	}

	@Test
	void testFromSeperatorWithEmptyInputAndDefaultSeparator1() {
		final String input = "";
		final QualifiedName qn = QualifiedName.fromSeperator(input, QualifiedName.DEFAULT_SEPARATOR);
		assertFalse(qn.isEmpty());
	}

	@Test
	void testFromSeperatorWithEmptyInputAndCustomSeparator2() {
		final String input = "";
		final String customSeparator = ":";
		final QualifiedName qn = QualifiedName.fromSeperator(input, customSeparator);
		assertFalse(qn.isEmpty());
	}

	@Test
	void testFromSeperatorWithEmptyInputAndDefaultSeparator() {
		final String input = "";
		final QualifiedName qn = QualifiedName.fromSeperator(input, QualifiedName.DEFAULT_SEPARATOR);
		assertFalse(qn.isEmpty());
	}

	@Test
	void testFromSeperatorWithEmptyInputAndCustomSeparator() {
		final String input = "";
		final String customSeparator = ":";
		final QualifiedName qn = QualifiedName.fromSeperator(input, customSeparator);
		assertFalse(qn.isEmpty());
	}

	@Test
	void testFromSeperatorWithSingleSegment() {
		final String input = "a";
		final QualifiedName qn = QualifiedName.fromSeperator(input);
		assertEquals("a", qn.getSegment(0));
	}

	@Test
	void testFromSeperatorWithSingleSegmentAndDefaultSeparator() {
		final String input = "a";
		final QualifiedName qn = QualifiedName.fromSeperator(input, QualifiedName.DEFAULT_SEPARATOR);
		assertEquals("a", qn.getSegment(0));
	}

	@Test
	void testFromSeperatorWithSingleSegmentAndCustomSeparator() {
		final String input = "a";
		final String customSeparator = ":";
		final QualifiedName qn = QualifiedName.fromSeperator(input, customSeparator);
		assertEquals("a", qn.getSegment(0));
	}

	@Test
	void testFromSeperatorWithMultipleSegments() {
		final String input = "a:b:c";
		final QualifiedName qn = QualifiedName.fromSeperator(input);
		assertEquals("a", qn.getSegment(0));
		assertEquals("b", qn.getSegment(1));
		assertEquals("c", qn.getSegment(2));
	}

	@Test
	void testFromSeperatorWithMultipleSegmentsAndDefaultSeparator() {
		final String input = "a:b:c";
		final QualifiedName qn = QualifiedName.fromSeperator(input, QualifiedName.DEFAULT_SEPARATOR);
		assertEquals("a", qn.getSegment(0));
		assertEquals("b", qn.getSegment(1));
		assertEquals("c", qn.getSegment(2));
	}

	@Test
	void testFromSeperatorWithMultipleSegmentsAndCustomSeparator() {
		final String input = "a:b:c";
		final String customSeparator = ":";
		final QualifiedName qn = QualifiedName.fromSeperator(input, customSeparator);
		assertEquals("a", qn.getSegment(0));
		assertEquals("b", qn.getSegment(1));
		assertEquals("c", qn.getSegment(2));
	}

	@Test
	void testFrom() {
		final String[] input = {"a", "b", "c"};
		final QualifiedName qn = QualifiedName.from(input);
		assertEquals("a", qn.getSegment(0));
		assertEquals("b", qn.getSegment(1));
		assertEquals("c", qn.getSegment(2));
	}

	@Test
	void testFromWithEmptyInput() {
		final String[] input = {};
		final QualifiedName qn = QualifiedName.from(input);
		assertTrue(qn.isEmpty());
	}
}
