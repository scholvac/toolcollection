package io.gitlab.scholvac.scheduler;

import java.util.function.Consumer;
import java.util.function.Function;

public interface ITask {

	default float getPriority() { return 100f;}
	default String getName() { return null;}

	void execute(final ExecutionContext ctx) throws Exception;

	/**
	 * Returns the next (absolute) update time when this task shall be sceduled again.
	 *
	 * @param ctx The current execution context, containing (among others) the current scheduler time to calculate the absolute time
	 * @return a timestamp > {@link ExecutionContext#getCurrentTime()} to rescedule the task or {@link Double#NaN} to not rescedule the task.
	 */
	double getNextTimestamp(final ExecutionContext ctx);


	public abstract class AbstractTask implements ITask {
		private float		mPriority = 0;
		private String		mName;
		private String		mDescription;

		public AbstractTask() {
			this(null, null, 100f);
		}
		public AbstractTask(final String name) {
			this(name, null, 100f);
		}
		public AbstractTask(final String name, final String desc) {
			this(name, desc, 100f);
		}
		public AbstractTask(final String name, final String desc, final float prio) {
			mName = name;
			mDescription = desc;
			mPriority = prio;
		}

		@Override
		public float getPriority() {
			return mPriority;
		}
		public void setPriority(final float mPriority) {
			this.mPriority = mPriority;
		}
		@Override
		public String getName() {
			return mName;
		}
		public void setName(final String mName) {
			this.mName = mName;
		}
		public String getDescription() {
			return mDescription;
		}
		public void setDescription(final String mDescription) {
			this.mDescription = mDescription;
		}
	}

	public abstract class PeriodicTask extends AbstractTask {

		private double		mFirstActivation;
		private final double		mUpdateInterval;

		public PeriodicTask(final String name, final double first, final double interval) {
			super(name);
			mFirstActivation = first;
			mUpdateInterval = interval;
		}
		protected double getFirstActivation() { return mFirstActivation;}
		protected double getUpdateInterval(final ExecutionContext ctx) { return mUpdateInterval;}


		@Override
		public double getNextTimestamp(final ExecutionContext ctx) {
			final double fa = getFirstActivation();
			if (fa > 0) {
				mFirstActivation = -1;
				return fa;
			}
			final double na = ctx.getCurrentTime() + getUpdateInterval(ctx);
			return na;
		}
	}
	public abstract class AbstractOneTimeTask extends AbstractTask {
		private double		mFirstActivation;

		public AbstractOneTimeTask(final String name, final double first) {
			super(name);
			mFirstActivation = first;
		}
		protected double getFirstActivation() { return mFirstActivation;}

		@Override
		public double getNextTimestamp(final ExecutionContext ctx) {
			final double fa = getFirstActivation();
			if (fa > 0) {
				mFirstActivation = -1;
				return fa;
			}
			return Double.NaN;
		}
	}



	static ITask createPeriodic(final String name, final double first, final double interval, final Consumer<ExecutionContext> consumer) {
		return new PeriodicTask(name, first, interval) {
			@Override
			public void execute(final ExecutionContext ctx) throws Exception {
				consumer.accept(ctx);
			}
		};
	}
	static ITask create(final String name, final double ts, final Runnable action) {
		return new AbstractTask(name) {
			@Override
			public double getNextTimestamp(final ExecutionContext ctx) {
				return ts;
			}

			@Override
			public void execute(final ExecutionContext ctx) throws Exception {
				action.run();
			}
		};
	}
	static ITask create(final String name, final double ts, final Consumer<ExecutionContext> action) {
		return new AbstractTask(name) {
			@Override
			public double getNextTimestamp(final ExecutionContext ctx) {
				return ts;
			}

			@Override
			public void execute(final ExecutionContext ctx) throws Exception {
				action.accept(ctx);
			}
		};
	}
	static ITask create(final String name, final Consumer<ExecutionContext> action, final Function<ExecutionContext, Double> updateTimeProvider) {
		return new AbstractTask(name) {
			@Override
			public double getNextTimestamp(final ExecutionContext ctx) {
				final Double next = updateTimeProvider.apply(ctx);
				return next != null ? next : Double.NaN;
			}

			@Override
			public void execute(final ExecutionContext ctx) throws Exception {
				action.accept(ctx);
			}
		};
	}
}
