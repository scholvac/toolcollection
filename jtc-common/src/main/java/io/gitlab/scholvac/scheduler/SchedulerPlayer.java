package io.gitlab.scholvac.scheduler;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Stream;

import io.gitlab.scholvac.IDisposeable;

public class SchedulerPlayer  {

	public interface IExitCondition {
		boolean shallExit(final Scheduler scheduler);
	}

	public interface Listener extends Scheduler.ISchedulerListener {
		void onStart();
		void onStop();
	}

	public final Scheduler								mScheduler;
	private Runner										mRunner = null;

	private double 										mDesiredTimeFactor;
	private double										mWallClockDuration = 0;
	private boolean										mAlive = false;
	private List<IExitCondition>						mExitConditions = null;
	private CompletableFuture<Boolean>					mFinishFuture; // used to wait til the last job has been executed

	private final Set<Listener>							mListener = new HashSet<>();


	private class Runner extends Thread {
		double simTimeLastResume ;
		double wallClockLastResume;

		public Runner() {
			super("SchedulerPlayer_" + mScheduler.getName());
		}
		@Override
		public void run() {
			mAlive = true;

			final Scheduler scheduler = getScheduler();

			if (scheduler.getCurrentTime() < 0) {
				scheduler.setTime(scheduler.getNextTimestamp());
			}

			simTimeLastResume = scheduler.getCurrentTime() < 0 ? 0 : scheduler.getCurrentTime(); //[sec]
			wallClockLastResume = System.currentTimeMillis(); //[ms]


			long ttw = 0;

			for (final Listener l : mListener) {
				try {
					l.onStart();
				}catch(final Exception e) {
					e.printStackTrace();
				}
			}

			while(mAlive) {
				final double wallDurationSinceResumeInMilli = System.currentTimeMillis() - wallClockLastResume; //[ms]
				final double wallDurationSinceResumeInSeconds = wallDurationSinceResumeInMilli * 0.001 * mDesiredTimeFactor;//[sec]
				final double simuExpectedSinceResume = simTimeLastResume + wallDurationSinceResumeInSeconds; //[sec]

				final double next = mScheduler.getNextTimestamp();
				if (next != next) {
					ttw = 100;
					simTimeLastResume = scheduler.getCurrentTime() < 0 ? 0 : scheduler.getCurrentTime(); //[sec]
					wallClockLastResume = System.currentTimeMillis(); //[ms]
				}else if (next > simuExpectedSinceResume) {
					final double diff = next - simuExpectedSinceResume; //[sec]
					ttw = (long)(diff * 1000); //[ms]
				}else {
					ttw = 0;
				}

				if (ttw >= 1) {
					try {
						Thread.sleep(ttw);
					} catch (final InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if (next == next)
					mScheduler.runUntil(next);

				if (mExitConditions != null) {
					for (int i = 0; i < mExitConditions.size(); i++) {
						final IExitCondition cond = mExitConditions.get(i);
						if (cond.shallExit(scheduler)){
							mAlive = false;
							break;
						}
					}
				}
			}

			for (final Listener l : mListener) {
				try {
					l.onStop();
				}catch(final Exception e) {
					e.printStackTrace();
				}
			}
			mWallClockDuration += System.currentTimeMillis() - wallClockLastResume;
			if (mFinishFuture != null)
				mFinishFuture.complete(false);
		}
	}

	public SchedulerPlayer(final Scheduler scheduler) {
		this(scheduler, 1);
	}
	public SchedulerPlayer(final Scheduler scheduler, final double factor) {
		mScheduler = scheduler;
		mDesiredTimeFactor = factor;
	}

	public IDisposeable addListener(final Listener listener) {
		if (mListener.add(listener)) {
			mScheduler.addListener(listener);
			return IDisposeable.create(() -> removeListener(listener));
		}
		return null;
	}
	public boolean removeListener(final Listener listener) {
		if (mListener.remove(listener)) {
			mScheduler.removeListener(listener);
			return true;
		}
		return false;
	}
	public void start() {
		if (isRunning())
			return ;
		mRunner = createRunner();
		mRunner.start();
	}
	protected Runner createRunner() {
		return new Runner();
	}
	public void start(final float tf) {
		setDesiredTimeFactor(tf);
		start();
	}

	public void stopPlayer() {
		stopPlayer(false);
	}
	public void stopPlayer(final boolean wait) {
		if (!isRunning())
			return ;
		mAlive = false;
		if (wait)
			try {
				mRunner.join();
				mRunner = null;
			} catch (final InterruptedException e) {
				e.printStackTrace();
			}
	}
	public Scheduler getScheduler() { return mScheduler; }
	public boolean isRunning() { return mAlive;}

	public void startBlocking(final double timeFactor, final IExitCondition...conditions) {
		setDesiredTimeFactor(timeFactor);
		if (conditions != null)
			Stream.of(conditions).forEach(this::addExitCondition);
		mFinishFuture = new CompletableFuture<>();
		start();
		try {
			mFinishFuture.get();
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
	}
	public void addExitCondition(final IExitCondition condition) {
		if (condition != null) {
			if (mExitConditions == null)
				mExitConditions = new ArrayList<>();
			mExitConditions.add(condition);
		}
	}

	public void setDesiredTimeFactor(final double tf) {
		if (isRunning()) {
			stopPlayer(true);
			mDesiredTimeFactor = tf;
			start();
		}else
			mDesiredTimeFactor = tf;
	}
	public double getDesiredTimeFactor() { return mDesiredTimeFactor; }
	public boolean isAlive() { return mAlive;  }

	public double getWallClockDurationSinceLastResume() {
		if (mRunner != null)
			return System.currentTimeMillis()- mRunner.wallClockLastResume;
		return 0;
	}
	public double getWallClockDuration() {
		if (mRunner == null || mAlive == false)
			return mWallClockDuration;
		return mWallClockDuration + System.currentTimeMillis() - mRunner.wallClockLastResume;
	}
	public double getSimulationTime() {
		return mScheduler.getCurrentTime();
	}

	public float getCurrentTimeFactor() {
		if (mRunner == null || mScheduler == null)
			return 0;
		final double wallTime = mAlive == false ? mWallClockDuration : mWallClockDuration + (System.currentTimeMillis() - mRunner.wallClockLastResume);
		final double simuTime = mScheduler.getCurrentTime() * 1000.;
		return (float)(simuTime/wallTime);
	}


}
