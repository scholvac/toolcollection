package io.gitlab.scholvac.scheduler;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import io.gitlab.scholvac.IDisposeable;
import io.gitlab.scholvac.IDisposeable.AbstractDisposeable;
import io.gitlab.scholvac.exec.ExecutorUtils;
import io.gitlab.scholvac.log.ILogger;
import io.gitlab.scholvac.log.SLog;
import io.gitlab.scholvac.stat.MiniStat;

public class Scheduler {

	private static final ILogger	LOG = SLog.getLogger(Scheduler.class);

	public enum NOTIFICATION_TYPE {
		PRE_TASK_EXECUTION,
		POST_TASK_EXECUTION,
		TASK_FAILURE,
		NEXT_TIMESTAMP,
		NEW_TASK
	}

	public interface ISchedulerListener {
		void schedulerEvent(final NOTIFICATION_TYPE type, ITaskContext context, Object optional);
	}

	public interface ITaskContext {
		default String getName() { return getTask().getName();}
		ITask getTask();
		MiniStat getStatistic();
	}
	protected static class TaskContext implements ITaskContext
	{
		public final ITask 				task;
		public final ExecutionContext	exec;
		public final MiniStat			stat;

		public TaskList 				taskList;
		private double					nextUpdate = Double.NaN;
		private int						mErrorCounter = 0;
		private Exception 				mLastError = null;


		public TaskContext(final ITask t, final Scheduler sched) {
			task = t;
			exec = new ExecutionContext(t, sched);
			stat = new MiniStat(t.getName());
		}

		public float getPriority() {
			return task.getPriority();
		}

		public double getNextUpdateTime() {
			if (nextUpdate != nextUpdate) {
				nextUpdate = task.getNextTimestamp(exec);
			}
			return nextUpdate;
		}

		public void execute(final Scheduler scheduler, final double currentTime) throws Exception {
			exec.setCurrentTime(currentTime);
			try {
				stat.tic();
				task.execute(exec);
			}catch(final Exception e){
				mErrorCounter++;
				mLastError = e;
				throw e;
			}finally {
				stat.tac();
				nextUpdate = Double.NaN;
				taskList = null;
			}
		}

		@Override
		public ITask getTask() { return task; }
		@Override
		public MiniStat getStatistic() { return stat; }

		@Override
		public String toString() {
			return String.format("TaskContext[%s, next=%1.3f, priority=%1.2f, errors=%d]", task.getName(), nextUpdate, getPriority(), mErrorCounter);
		}
	}

	protected static class TaskList {
		public double									time;
		public TreeMap<Float, LinkedList<TaskContext>>	tasks = new TreeMap<>();

		public TaskList(final double t) {
			time = t;
		}

		public boolean isEmpty() {
			return tasks.isEmpty();
		}
		public void add(final TaskContext ctx) {
			synchronized (tasks) {
				final float prio = ctx.getPriority();
				LinkedList<TaskContext> list = tasks.get(prio);
				if (list == null)
					tasks.put(prio, list = new LinkedList<>());
				LOG.trace("add task {} to tasklist {} with prio {}", ctx, this, prio);
				list.add(ctx);
				ctx.taskList = this;
			}

		}

		public boolean remove(final TaskContext ctx) { //returns if the tasklist is empty after the tasks has been removed
			synchronized (tasks) {
				LOG.trace("remove task {}, from taskList {}", ctx, this);
				Float toRemove = null;
				for (final Entry<Float, LinkedList<TaskContext>> e : tasks.entrySet()) {
					if (e.getValue().remove(ctx) && e.getValue().isEmpty()) {
						toRemove = e.getKey();
						break;
					}
				}
				if (toRemove != null){
					tasks.remove(toRemove);
				}
				return tasks.isEmpty();
			}
		}

		public TaskContext popNext() {
			synchronized (tasks) {
				final Entry<Float, LinkedList<TaskContext>> entry = tasks.firstEntry();
				final LinkedList<TaskContext> tcl = entry.getValue();
				final TaskContext tc = tcl.removeFirst();
				if (tcl.isEmpty()) {
					tasks.pollFirstEntry();
				}
				return tc;
			}
		}

		public LinkedList<TaskContext> popNextTaskList() {
			synchronized (tasks) {
				if (tasks.isEmpty())
					return null;
				return tasks.pollFirstEntry().getValue();
			}
		}
	}

	private double									mTime = -1;
	private final TreeMap<Double, TaskList>			mTaskLists = new TreeMap<>();
	private final LinkedList<TaskList>				mUnusedTaskLists = new LinkedList<>();
	private final Map<ITask, TaskContext> 			mTaskContextMap;

	private final ExecutorService					mExecutorService;

	private final String 							mName;
	private final boolean 							mAsync;
	private final int								mThreadCount;
	private final boolean			 				mDemonThread;
	private Set<ISchedulerListener>					mListener = null;


	public Scheduler() {
		this("Scheduler");
	}

	public Scheduler(final String name) {
		this(name, false, 1, true);
	}


	public Scheduler(final String name, final boolean async, final int threadCount, final boolean asDemon) {
		mName = name;
		mAsync = async;
		if (async) {
			LOG.warn("ASYNC scheduling is experimental");
		}
		mThreadCount = threadCount;
		mDemonThread = asDemon;
		mExecutorService = ExecutorUtils.createExecutorService(mName, true, mThreadCount, mDemonThread); //always use a different thread to execute a task
		if (mAsync) {
			mTaskContextMap = new ConcurrentHashMap<>();
		}else
			mTaskContextMap = new HashMap<>(); //faster
	}


	public ITaskContext schedule(final ITask task) {
		ITaskContext tc = getTaskContext(task);
		if (tc == null)
			tc = createTaskContext(task);
		return reschedule(tc);
	}
	public double getCurrentTime() {
		return mTime;
	}
	protected void setTime(final double timeStamp) {
		mTime = timeStamp;
	}

	public double runUntil(final double timeStamp) {
		if (getCurrentTime() > timeStamp)
			return getCurrentTime();

		while(hasNextTask())
			runNextTask();
		double nextTimestamp = getNextTimestamp();
		while(nextTimestamp <= timeStamp && !Double.isNaN(nextTimestamp)) {
			notifyListener(NOTIFICATION_TYPE.NEXT_TIMESTAMP, null, getNextTimestamp());
			Entry<Double, TaskList> entry = null;
			synchronized (mTaskLists) {
				entry = mTaskLists.pollFirstEntry();
				if (entry == null)
					break;
				setTime(entry.getKey());
			} //no need for further synchronization, if time == mTime: the job lies in the past
			_executeTaskList(entry.getValue());
			nextTimestamp = getNextTimestamp();
		}
		return getCurrentTime();
	}



	public double getNextTimestamp() {
		synchronized (mTaskLists) {
			return mTaskLists.isEmpty() ? Double.NaN :  mTaskLists.firstKey();
		}
	}
	public boolean hasNextTimestamp() {
		return !Double.isNaN(getNextTimestamp());
	}
	public void runNextTask() {
		TaskList tl = null;
		synchronized (mTaskLists) {
			tl = mTaskLists.get(getCurrentTime());
		}
		if (tl == null)
			return ; //shall not happen, if the user asked for hasNextTask before
		final TaskContext tc = tl.popNext();
		if (tl.isEmpty()){
			synchronized (mTaskLists) {
				mTaskLists.remove(getCurrentTime());
			}
		}
		_executeTask(tc);
	}

	public boolean hasNextTask() {
		synchronized (mTaskLists) {
			return mTaskLists.containsKey(getCurrentTime());
		}
	}

	private void _executeTaskList(final TaskList value) {
		//we still need to hold the priority order, e.g. first execute tasks with lowest order...
		LinkedList<TaskContext> tasks = value.popNextTaskList();

		while(tasks != null) {
			for (final TaskContext tc : tasks) {
				final Future f = mExecutorService.submit(() -> _doExecute(tc));
				try {
					f.get();
				} catch (InterruptedException | ExecutionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			tasks = value.popNextTaskList();
		}

		value.time = Double.NaN;
		synchronized (mUnusedTaskLists) {
			mUnusedTaskLists.add(value);
		}
	}
	private void _executeTask(final TaskContext tc) {
		mExecutorService.execute(() -> _doExecute(tc));
	}

	private void _doExecute(final TaskContext tc) {
		notifyListener(NOTIFICATION_TYPE.PRE_TASK_EXECUTION, tc, null);
		try {
			tc.execute(this, getCurrentTime());
			notifyListener(NOTIFICATION_TYPE.PRE_TASK_EXECUTION, tc, null);
		}catch(final Throwable e) {
			e.printStackTrace();
			notifyListener(NOTIFICATION_TYPE.TASK_FAILURE, tc, e);
		}finally {
			_reschedule(tc);//may be executed again
		}
	}

	public IDisposeable addListener(final ISchedulerListener listener) {
		if (listener == null)
			return null;
		if (mListener == null)
			mListener = new HashSet<>();
		mListener.add(listener);
		return new AbstractDisposeable() {
			@Override
			protected boolean doDispose() {
				removeListener(listener);
				return true;
			}
		};
	}
	public void removeListener(final ISchedulerListener listener) {
		if (mListener == null || listener == null) return ;
		if (mListener.remove(listener))
			if (mListener.isEmpty())
				mListener = null;
	}
	private void notifyListener(final NOTIFICATION_TYPE type, final TaskContext context, final Object extra) {
		if (mListener == null)
			return ;
		for (final ISchedulerListener l : mListener) {
			try {
				l.schedulerEvent(type, context, extra);
			}catch(final Exception e) {
				e.printStackTrace();
				LOG.error("Failed to notify listener {}", l);
			}
		}
	}

	public ITaskContext reschedule(final ITaskContext context) {
		((TaskContext)context).nextUpdate = Double.NaN;
		_reschedule((TaskContext)context);
		return context;
	}
	protected void _reschedule(final TaskContext ctx) {
		if (ctx == null)
			return ;
		if (ctx.taskList != null && ctx.taskList.remove(ctx)) {
			remove(ctx.taskList);
		}

		final double nextUpdate = ctx.getNextUpdateTime();

		if (nextUpdate < 0 || nextUpdate != nextUpdate){
			//			LOG.trace("Task {} is not scheduled again", ctx);
			return ;
		}
		if (nextUpdate <= getCurrentTime()) {
			LOG.warn("Task {} is scheduled for past = {}, current = {}", ctx, nextUpdate, getCurrentTime());
			return ;
		}
		final TaskList tl = getOrCreateTaskList(nextUpdate);
		tl.add(ctx);

		notifyListener(NOTIFICATION_TYPE.NEW_TASK, ctx, null);
	}

	private void remove(final TaskList taskList) {
		if (taskList == null)
			return ;
		synchronized (mTaskLists) {
			if (mTaskLists.remove(taskList.time) != null) {
				taskList.time = Double.NaN;
				synchronized (mUnusedTaskLists) {
					mUnusedTaskLists.add(taskList);
				}
			}
		}
	}

	protected TaskList getOrCreateTaskList(final double time) {
		TaskList tl = mTaskLists.get(time);
		if (tl == null) {
			synchronized (mUnusedTaskLists) {
				if (!mUnusedTaskLists.isEmpty()) {
					tl = mUnusedTaskLists.removeFirst();
					tl.time = time;
				}
			}
			if (tl == null)
				tl = new TaskList(time);
			synchronized (mTaskLists) {
				mTaskLists.put(time, tl);
			}
		}
		return tl;
	}

	protected TaskContext getTaskContext(final ITask t) {
		return mTaskContextMap.get(t);
	}
	protected TaskContext createTaskContext(final ITask t) {
		final TaskContext tc = new TaskContext(t, this);
		mTaskContextMap.put(t, tc);
		return tc;
	}

	public String getName() { return mName; }

	public void remove(final ITaskContext task) {
		for (final TaskList tl: mTaskLists.values()) {
			if (tl.remove((TaskContext) task)) {
			}
		}
	}



}
