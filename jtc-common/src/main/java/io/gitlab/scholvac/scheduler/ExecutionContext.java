package io.gitlab.scholvac.scheduler;

public class ExecutionContext {

	private final Scheduler		mScheduler;
	private final ITask			mTask;

	private double				mCurrentTime = 0;
	private double				mLastTime = -1;

	public ExecutionContext(final ITask task, final Scheduler scheduler) {
		mScheduler = scheduler;
		mTask = task;
	}

	public Scheduler getScheduler() {
		return mScheduler;
	}

	public ITask getTask() {
		return mTask;
	}

	public double getCurrentTime() {
		return mCurrentTime;
	}

	public double getLastTime() {
		return mLastTime;
	}

	void setCurrentTime(final double time) {
		mLastTime = mCurrentTime;
		mCurrentTime = time;
	}
	public double getDeltaTime() {
		return mCurrentTime - mLastTime;
	}


}