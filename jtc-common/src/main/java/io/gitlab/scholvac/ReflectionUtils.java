package io.gitlab.scholvac;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.stream.Stream;


public class ReflectionUtils {
	public interface IClassLookup {
		Class<?> lookupClassByName(final String name) throws ClassNotFoundException;
	}

	private static int						debugMode = -1;
	private static LinkedList<IClassLookup> sClassLookups; //TODO: Do we really need this?

	static {
		sClassLookups = new LinkedList<>();
		addClassLookup(cn -> {
			final ClassLoader cl = Thread.currentThread().getContextClassLoader();
			return cl.loadClass(cn);
		});
	}


	public static void addClassLookup(final IClassLookup lookup) { //not classloader to support the functional interface
		if (lookup != null) sClassLookups.add(lookup);
	}
	public static boolean removeClassClassLookup(final IClassLookup lookup) {
		return sClassLookups.remove(lookup);
	}

	public static boolean startedInDebugMode() { return isStartedInDebugMode();}
	public static boolean isStartedInDebugMode() {
		if (debugMode < 0) {
			debugMode = java.lang.management.ManagementFactory.getRuntimeMXBean().getInputArguments().toString().indexOf("-agentlib:jdwp");
		}
		return debugMode > 0;
	}
	public static boolean callMethod(final Object target, final Collection<String> methodNames, final Object...parameters) {
		return _callMethod(target, methodNames, parameters);
	}
	public static boolean callMethod(final Object target, final String methodName, final Object...parameters) {
		return _callMethod(target, Collections.singleton(methodName), parameters);
	}

	public static Object callMethodWithReturn(final Object target, final String methodName, final Object...parameters) {
		try {
			return _callMethod(target.getClass(), target, Collections.singleton(methodName), parameters);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
		return null;
	}

	private static boolean _callMethod(final Object target, final Collection<String> methodNames, final Object[] parameters) {
		try {
			_callMethod(target.getClass(), target, methodNames, parameters);
			return true;
		}catch(final Throwable t) {
			return false;
		}
	}
	private static Object _callMethod(final Class<?> tc, final Object target, final Collection<String> methodNames, final Object[] parameters) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		final int paramCount = parameters != null ? parameters.length : 0;
		Class[] paramTypes = null;
		if (paramCount > 0) {
			paramTypes = new Class[paramCount];
			for (int i = 0; i < parameters.length; i++) paramTypes[i] = parameters[i] != null ? parameters[i].getClass() : null;
		}
		return callMethod(tc, target, methodNames, parameters, paramTypes);
	}

	public static Object callMethod(final Class<?> tc, final Object target, final Collection<String> methodNames, final Object[] parameters, final Class[] parameterTypes) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		final int paramCount = parameters != null ? parameters.length : 0;
		if (paramCount > 0) {
			if (parameters.length != paramCount || parameterTypes.length != paramCount)
				throw new IllegalArgumentException("Expect parameters and parameter types to have the same size");
		}

		final Method method = Stream.concat(Stream.of(tc.getDeclaredMethods()), Stream.of(tc.getMethods())) //Stream.of(tc.getDeclaredMethods())
				.filter(it -> it.getParameterCount() == paramCount)
				.filter(it -> methodNames.contains(it.getName()))
				.filter(it -> {
					for (int i = 0; i < paramCount; i++) {
						if (parameters[i] != null)
							if (!inherits(parameterTypes[i], it.getParameterTypes()[i]))
								return false;
					}
					return true;
				})
				.findFirst().orElse(null);

		if (method == null) {

			return false;
		}


		method.setAccessible(true);// yes: that's some kind of evil but if it was public there would be no need for this method

		return method.invoke(target, parameters);
	}

	public static boolean inherits(final Class<?> child, final Class<?> parent) {
		if (child == null || parent == null) return false;
		if (child == parent) return true;
		return parent.isAssignableFrom(child);
	}
	public static boolean setField(final Object target, final String fieldName, final Object value) {
		return _setField(target, Collections.singleton(fieldName), value);
	}
	public static boolean setField(final Object target, final Collection<String> fieldNames, final Object value) {
		return _setField(target, fieldNames, value);
	}

	private static boolean _setField(final Object target, final Collection<String> fieldNames, final Object value) {
		if (target == null || fieldNames == null || fieldNames.isEmpty())
			return false;
		Class<? extends Object> tc = target.getClass();
		Field field = null;
		while(tc != null) {
			for (final String fieldName : fieldNames) {
				try {
					field = tc.getField(fieldName);
					if (field != null)
						break;
				} catch (NoSuchFieldException | SecurityException e) {}
				try {
					field = tc.getDeclaredField(fieldName);
					if (field != null)
						break;
				} catch (NoSuchFieldException | SecurityException e) {}
			}
			if (field != null)
				break;
			tc = tc.getSuperclass();
		}
		if (field == null) {
			return false;
		}
		try {
			field.setAccessible(true);
			field.set(target, value);
			return true;
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
			return false;
		}
	}
	public static Object callStaticMethod(final String clazzName, final String methodName, final Object[] parameter) throws ClassNotFoundException, ReflectiveOperationException, RuntimeException {
		final Class<?> clazz = getClassByName(clazzName);
		return _callMethod(clazz, null, Arrays.asList(methodName), parameter);
	}

	public static Method findFirstMethod(final Class<?> clazz, final String[] names, final Class<?> returnType, final Class<?> _argType) {
		final Class<?>[] argType = _argType == null ? new Class<?>[] {} : new Class<?>[] {_argType};
		for (final String name : names) {
			try {
				Method m = clazz.getMethod(name, argType);
				if (m != null && inherits(returnType, m.getReturnType()))
					return m;
				m = clazz.getDeclaredMethod(name, argType);
				if (m != null && inherits(returnType, m.getReturnType()))
					return m;
			} catch (NoSuchMethodException | SecurityException e) {}
		}
		for (final Method m : clazz.getDeclaredMethods()) {
			if (argType == null) { //getter
				if (m.getParameterCount() != 0)
					continue;
			} else if (m.getParameterCount() != 1 || !inherits(m.getParameterTypes()[0], _argType))
				continue;
		}
		return null;
	}
	public static Class<?> getCaller(final int history) {
		final StackTraceElement ste = Thread.currentThread().getStackTrace()[2+history];
		try {
			final String className = ste.getClassName();
			return getClassByName(className);
		} catch (final Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	public static Class<?> getClassByName(final String className) {
		synchronized (sClassLookups) {
			for (final IClassLookup lookup : sClassLookups) {
				try {
					final Class<?> clazz = lookup.lookupClassByName(className);
					if (clazz != null)
						return clazz;
				}catch(final ClassNotFoundException cnfe) {
					//return this exception as we expect some classes not to be found by the default lookup
				}
			}
		}
		return null;
	}
	public static boolean isMany(final Class<?> type) {
		if (type.isArray())
			return true;
		if (Collection.class.isAssignableFrom(type))
			return true;
		return false;
	}

}

