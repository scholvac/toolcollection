package io.gitlab.scholvac;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * A class representing a qualified name, which is a sequence of segments separated by a specified separator.
 *
 * @author Scholvac
 */
public class QualifiedName implements Comparable<QualifiedName>, Serializable {

	/**
	 * The default separator used when creating a {@code QualifiedName} instance.
	 */
	public static final String DEFAULT_SEPARATOR = ":";

	/**
	 * The list of segments that make up the qualified name.
	 */
	private final List<String> mSegments;

	/**
	 * Constructs a new {@code QualifiedName} instance from a string using the default separator.
	 *
	 * @param str the string to parse
	 * @return a new {@code QualifiedName} instance
	 */
	public static QualifiedName fromSeperator(final String str) {
		return fromSeperator(str, DEFAULT_SEPARATOR);
	}

	/**
	 * Constructs a new {@code QualifiedName} instance from a string using the specified separator.
	 *
	 * @param str the string to parse
	 * @param seperator the separator to use
	 * @return a new {@code QualifiedName} instance
	 */
	public static QualifiedName fromSeperator(final String str, final String seperator) {
		final String[] split = str.split(seperator);
		if (split.length == 0)
			return from(str);
		return from(split);
	}

	/**
	 * Constructs a new {@code QualifiedName} instance from an array of segments.
	 *
	 * @param segments the array of segments
	 * @return a new {@code QualifiedName} instance
	 */
	public static QualifiedName from(final String... segments) {
		return new QualifiedName(Arrays.asList(segments));
	}

	/**
	 * Private constructor to prevent instantiation from outside the class.
	 *
	 * @param segments the list of segments
	 */
	private QualifiedName(final List<String> segments) {
		mSegments = segments;
	}

	/**
	 * Checks if the qualified name is empty.
	 *
	 * @return {@code true} if the qualified name is empty, {@code false} otherwise
	 */
	public boolean isEmpty() {
		return mSegments.isEmpty();
	}

	/**
	 * Returns the number of segments in the qualified name.
	 *
	 * @return the number of segments
	 */
	public int size() {
		return mSegments.size();
	}

	/**
	 * Returns the length of the qualified name.
	 *
	 * @return the length of the qualified name
	 */
	public int length() {
		return size();
	}

	/**
	 * Skips the first n segments of the qualified name.
	 *
	 * @param num the number of segments to skip
	 * @return a new {@code QualifiedName} instance with the first n segments skipped
	 */
	public QualifiedName skipFront(final int num) {
		final int start = Math.min(num, size());
		return new QualifiedName(mSegments.subList(start, size()));
	}

	/**
	 * Skips the last n segments of the qualified name.
	 *
	 * @param num the number of segments to skip
	 * @return a new {@code QualifiedName} instance with the last n segments skipped
	 */
	public QualifiedName skipLast(final int num) {
		final int min = Math.max(0, size() - num);
		return new QualifiedName(mSegments.subList(0, min));
	}

	/**
	 * Adds new segments to the qualified name.
	 *
	 * @param segments the new segments to add
	 * @return a new {@code QualifiedName} instance with the new segments added
	 */
	public QualifiedName add(final String... segments) {
		return add(Arrays.asList(segments));
	}

	/**
	 * Adds a new {@code QualifiedName} instance to the current qualified name.
	 *
	 * @param qn the {@code QualifiedName} instance to add
	 * @return a new {@code QualifiedName} instance with the new {@code QualifiedName} instance added
	 */
	public QualifiedName add(final QualifiedName qn) {
		return add(qn.mSegments);
	}

	/**
	 * Adds a list of new segments to the current qualified name.
	 *
	 * @param segments the list of new segments to add
	 * @return a new {@code QualifiedName} instance with the new segments added
	 */
	public QualifiedName add(final List<String> segments) {
		final ArrayList<String> newL = new ArrayList<>(mSegments);
		newL.addAll(segments);
		return new QualifiedName(newL);
	}

	/**
	 * Returns the segment at the specified index.
	 *
	 * @param idx the index of the segment to retrieve
	 * @return the segment at the specified index
	 */
	public String getSegment(final int idx) {
		return mSegments.get(idx);
	}

	/**
	 * Returns a string representation of the qualified name.
	 *
	 * @return a string representation of the qualified name
	 */
	@Override
	public String toString() {
		return toString(DEFAULT_SEPARATOR);
	}

	/**
	 * Returns a string representation of the qualified name using the specified separator.
	 *
	 * @param seperator the separator to use
	 * @return a string representation of the qualified name using the specified separator
	 */
	public String toString(final String seperator) {
		return String.join(seperator, mSegments);
	}

	/**
	 * Returns the last segment of the qualified name.
	 *
	 * @return the last segment of the qualified name
	 */
	public String last() {
		return getSegment(size() - 1);
	}

	/**
	 * Returns the first segment of the qualified name.
	 *
	 * @return the first segment of the qualified name
	 */
	public String first() {
		return getSegment(0);
	}

	/**
	 * Appends a new {@code QualifiedName} instance to the current qualified name.
	 *
	 * @param qualifiedName the {@code QualifiedName} instance to append
	 * @return a new {@code QualifiedName} instance with the new {@code QualifiedName} instance appended
	 */
	public QualifiedName append(final QualifiedName qualifiedName) {
		return append(qualifiedName.mSegments);
	}

	/**
	 * Appends a list of new segments to the current qualified name.
	 *
	 * @param segments the list of new segments to append
	 * @return a new {@code QualifiedName} instance with the new segments appended
	 */
	public QualifiedName append(final String... segments) {
		return append(Arrays.asList(segments));
	}

	/**
	 * Appends a list of new segments to the current qualified name.
	 *
	 * @param segments the list of new segments to append
	 * @return a new {@code QualifiedName} instance with the new segments appended
	 */
	public QualifiedName append(final List<String> segments) {
		final List<String> newSegments = new ArrayList<>(mSegments);
		newSegments.addAll(segments);
		return new QualifiedName(newSegments);
	}

	/**
	 * Skips the first segment of the qualified name.
	 *
	 * @return a new {@code QualifiedName} instance with the first segment skipped
	 */
	public QualifiedName skipFirst() {
		return skipFront(1);
	}

	/**
	 * Skips the last segment of the qualified name.
	 *
	 * @return a new {@code QualifiedName} instance with the last segment skipped
	 */
	public QualifiedName skipLast() {
		return skipLast(1);
	}

	/**
	 * Checks if the qualified name starts with another qualified name.
	 *
	 * @param other the qualified name to compare against
	 * @return {@code true} if the qualified name starts with the other qualified name, {@code false} otherwise
	 */
	public boolean startsWith(final QualifiedName other) {
		if (size() < other.size())
			return false;
		for (int i = 0; i < other.size(); i++)
			if (getSegment(i).equals(other.getSegment(i)) == false)
				return false;
		return true;
	}

	/**
	 * Returns the hash code of the qualified name.
	 *
	 * @return the hash code of the qualified name
	 */
	@Override
	public int hashCode() {
		return Objects.hash(mSegments);
	}

	/**
	 * Checks if the qualified name is equal to another object.
	 *
	 * @param obj the object to compare against
	 * @return {@code true} if the qualified name is equal to the other object, {@code false} otherwise
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final QualifiedName other = (QualifiedName) obj;
		return Objects.equals(mSegments, other.mSegments);
	}

	/**
	 * Compares the qualified name to another qualified name using their string representations.
	 *
	 * @param o the qualified name to compare against
	 * @return the result of the comparison
	 */
	@Override
	public int compareTo(final QualifiedName o) {
		return toString().compareTo(o.toString());
	}
}
