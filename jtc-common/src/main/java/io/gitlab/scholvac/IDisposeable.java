package io.gitlab.scholvac;

import java.awt.Component;
import java.awt.event.HierarchyEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * An interface for objects that can be disposed.
 */
public interface IDisposeable {

	IDisposeable EMPTY = create(() -> {});

	/**
	 * Holds an instance of {@link IDisposable} and allows changing the instance while the holder instance remains stable.
	 * This way, the disposable can be used in anonymous classes.
	 */
	public static class DisposableHolder {
		//used to hold an IDisposable instance and change the IDisposable while the holder instance remains
		//stable, this way the disposable can be used in anonymouse classes

		private IDisposeable disposable;

		/**
		 * Disposes the current disposable instance.
		 */
		public void dispose() {
			IDisposeable.saveDispose(disposable);
		}

		/**
		 * Updates the current disposable instance with a new one.
		 *
		 * @param newDisposable the new disposable instance to update with
		 */
		public void update(final IDisposeable newDisposable) {
			dispose();
			disposable = newDisposable;
		}
	}

	/**
	 * An abstract class that provides a basic implementation of the {@link IDisposable} interface.
	 */
	public static abstract class AbstractDisposeable implements IDisposeable {
		protected boolean mDisposed = false;
		public AbstractDisposeable() { }
		@Override
		public boolean isDisposed() { return mDisposed; }
		@Override
		public void dispose() throws Exception {
			if (isDisposed() == false)
				mDisposed = doDispose();
		}
		/**
		 * Overwrite this method to only get disposed once.
		 *
		 * @return true, if the disposal was successful.
		 */
		protected boolean doDispose() {
			return false;
		}
	}
	public static class MultiDisposable extends AbstractDisposeable{
		private final Set<IDisposeable> mDisposables = new HashSet<>();
		public MultiDisposable() { this(new ArrayList<>());}
		public MultiDisposable(final IDisposeable ...disposeables) { this(Arrays.asList(disposeables));}
		public MultiDisposable(final Collection<IDisposeable> disposables) { mDisposables.addAll(mDisposables);}
		public MultiDisposable add(final IDisposeable disp) { mDisposables.add(disp); return this;}

		@Override
		protected boolean doDispose() {
			IDisposeable.dispose(new ArrayList<>(mDisposables));
			return true;
		}
	}

	public static class RunnableDisposeable extends AbstractDisposeable {
		private final Runnable mRunnanle;

		public RunnableDisposeable(final Runnable r) {
			mRunnanle = r;
		}
		@Override
		protected boolean doDispose() {
			mRunnanle.run();
			return true;
		}
	}

	static MultiDisposable createMulti(final IDisposeable ...disposeables) {
		return new MultiDisposable(disposeables);
	}
	static IDisposeable create(final Runnable r) {
		return new RunnableDisposeable(r);
	}





	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	/**
	 * Returns whether this object has been disposed.
	 *
	 * @return true if this object has been disposed, false otherwise
	 */
	boolean isDisposed();

	/**
	 * Disposes this object and any resources it holds.
	 *
	 * @throws Exception if an error occurs during disposal
	 */
	void dispose() throws Exception;

	/**
	 * Disposes the given disposable object and any resources it holds.
	 *
	 * @param disp the disposable object to dispose
	 * @return the given disposable object if it was successfully disposed, null otherwise
	 */
	static IDisposeable dispose(final IDisposeable disp) {
		return saveDispose(disp) ? null : disp;
	}

	/**
	 * Saves the given disposable objects and any resources they hold.
	 *
	 * @param disp the disposable objects to save
	 * @return true if all disposable objects were successfully saved, false otherwise
	 */
	static boolean saveDispose(final IDisposeable... disp) {
		if (disp == null || disp.length == 0) return false;
		boolean res = true;
		for (final IDisposeable element : disp)
			if (element != null)
				res &= element.saveDispose();
		return res;
	}

	/**
	 * Disposes the given list of disposable objects and any resources they hold.
	 *
	 * @param disposeables the list of disposable objects to dispose
	 * @return the given list of disposable objects after disposal
	 */
	static List<IDisposeable> dispose(final List<IDisposeable> disposeables) {
		while (!disposeables.isEmpty()) {
			final IDisposeable disp = disposeables.remove(0);
			dispose(disp);
		}
		return disposeables;
	}

	/**
	 * Disposes the given disposable object and any resources it holds when the given component is hidden.
	 *
	 * @param compo the component to observe
	 * @param disp   the disposable object to dispose when the component is hidden
	 */
	static void disposeOnHide(final Component compo, final IDisposeable disp) {
		compo.addHierarchyListener(e -> {
			if ((e.getChangeFlags() & HierarchyEvent.SHOWING_CHANGED) == HierarchyEvent.SHOWING_CHANGED)
				if (compo.isShowing() == false)
					IDisposeable.dispose(disp);
		});
	}

	/**
	 * Disposes the given disposable object and any resources it holds when the given component is not yet disposed.
	 * @return true if the dispose method has been called successfully, false otherwise.
	 */
	default boolean saveDispose() {
		if (!isDisposed()){
			try {
				dispose();
				return true;
			}catch(final Exception e) {
				e.printStackTrace();
				return false;
			}
		}
		return false;
	}

}
