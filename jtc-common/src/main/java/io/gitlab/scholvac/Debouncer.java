//taken from: https://itecnote.com/tecnote/java-implementing-debounce-in-java/ (07.08.2022)
package io.gitlab.scholvac;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


/**
 * A generic debouncing mechanism that can be used with any type T and payload type PAYLOAD.
 * It delays the execution of a function until a certain period of time has passed without any further calls.
 *<br>
 *Debouncing is a technique used to avoid unnecessary function calls by delaying the execution of a function until a certain period of time has passed without any further calls.
 *
 * @param <T> The type of the key used to identify the task.
 * @param <PAYLOAD> The type of the payload passed to the function.
 */
public class Debouncer <T, PAYLOAD> {
	/**
	 * The interface that must be implemented by the function to be debounced.
	 *
	 * @param <PAYLOAD> The type of the payload passed to the function.
	 */
	@FunctionalInterface
	public interface Callback<PAYLOAD> {
		/**
		 * The function to be debounced.
		 *
		 * @param payload The payload passed to the function.
		 */
		void call(PAYLOAD t);
	}

	private static final ScheduledExecutorService 	mScheduler = CommonUtils.getSystemScheduler(); //reuse an existing scheduler

	private final ConcurrentHashMap<T, TimerTask> 	mDelayedMap = new ConcurrentHashMap<>();
	private final Callback<PAYLOAD> 				mCallback;
	private final long 								mInterval;

	/**
	 * Constructs a new debouncer with the specified callback and interval.
	 *
	 * @param c The callback to be debounced.
	 * @param interval The interval between function calls, in milliseconds.
	 */
	public Debouncer(final Callback<PAYLOAD> c, final long interval) {
		this.mCallback = c;
		this.mInterval = interval;
	}

	/**
	 * Gets the interval between function calls, in milliseconds.
	 *
	 * @return The interval between function calls.
	 */
	public long getInterval() { return mInterval; }

	/**
	 * Calls the specified function with the given key and payload, debouncing the calls.
	 *
	 * @param key The key used to identify the task.
	 * @param payload The payload passed to the function.
	 */
	public void call(final T key, final PAYLOAD payload) {
		final TimerTask task = new TimerTask(key, payload);

		TimerTask prev;
		do {
			prev = mDelayedMap.putIfAbsent(key, task);
			if (prev == null)
				mScheduler.schedule(task, mInterval, TimeUnit.MILLISECONDS);
		} while (prev != null && !prev.extend(payload)); // Exit only if new task was added to map, or existing task was extended successfully
	}


	//	/**
	//	 * Terminates all scheduled tasks and shuts down the scheduler.
	//	 */
	//	public void terminate() {
	//		mScheduler.
	//	}

	/**
	 * The task that wakes up when the wait time elapses.
	 */
	private class TimerTask implements Runnable {
		private final T mKey;
		private PAYLOAD mPayload;
		private long mDueTime;
		private final Object mLock = new Object();

		/**
		 * Constructs a new timer task with the specified key and payload.
		 *
		 * @param key The key used to identify the task.
		 * @param payload The payload passed to the function.
		 */
		public TimerTask(final T key, final PAYLOAD payload) {
			this.mKey = key;
			this.mPayload = payload;
			extend(payload);
		}

		/**
		 * Checks if the task has already been shut down, and if not, updates the task with the new payload and reschedules it.
		 *
		 * @param payload The new payload to be used by the task.
		 * @return True if the task has been extended successfully, and false otherwise.
		 */
		public boolean extend(final PAYLOAD payload) {
			synchronized (mLock) {
				if (mDueTime < 0) // Task has been shutdown
					return false;
				mDueTime = System.currentTimeMillis() + mInterval;
				mPayload = payload;
				return true;
			}
		}

		/**
		 * Executes the task when the wait time elapses.
		 */
		@Override
		public void run() {
			synchronized (mLock) {
				final long remaining = mDueTime - System.currentTimeMillis();
				if (remaining > 0) { // Re-schedule task
					mScheduler.schedule(this, remaining, TimeUnit.MILLISECONDS);
				} else { // Mark as terminated and invoke callback
					mDueTime = -1;
					try {
						mCallback.call(mPayload);
					} finally {
						mDelayedMap.remove(mKey);
					}
				}
			}
		}
	}


}