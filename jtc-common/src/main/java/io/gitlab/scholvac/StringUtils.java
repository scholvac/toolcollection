package io.gitlab.scholvac;

public class StringUtils {

	public static boolean isNullOrEmpty(final String str) {
		return str == null || str.isEmpty();
	}
	public static boolean notNullOrEmpty(final String str) {
		return str != null && str.isEmpty() == false;
	}
	public static void checkNotNullOrEmpty(final String str) {
		if (str == null)
			throw new NullPointerException("Expected a non null string");
		if (str.isEmpty())
			throw new IllegalArgumentException("Expected a not empty string");
	}

	public static String appendPrefixToEachLine(final String prefix, final String content) {
		return prefix + content.replaceAll("(\r\n|\n|\r)", "$1" + prefix);
	}
	public static String toFirstUpper(final String name) {
		if (isNullOrEmpty(name))
			return name;
		if (Character.isUpperCase(name.charAt(0)))
			return name;
		return Character.toUpperCase(name.charAt(0)) + name.substring(1);
	}
}
