package io.gitlab.scholvac.resources;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import io.gitlab.scholvac.ReflectionUtils;
import io.gitlab.scholvac.service.IService;
import io.gitlab.scholvac.service.ServiceManager;


/**
 * The IconManager class is responsible for managing, loading, and manipulating icons and images.
 * It provides functionality to load, cache, scale, and convert images for use in the application.
 * This class implements the IService interface and can be accessed as a singleton through the ServiceManager.
 *
 * <p>Key features:
 * <ul>
 *   <li>Loading images from various sources (files, resources, URLs)</li>
 *   <li>Caching images for improved performance</li>
 *   <li>Scaling images to different sizes</li>
 *   <li>Converting between Image and BufferedImage</li>
 *   <li>Creating ImageIcons from images</li>
 * </ul>
 *
 * <p>Usage example:
 * <pre>
 * IconManager iconManager = IconManager.get();
 * BufferedImage image = iconManager.getImage("path/to/image.png", IconManager.LARGE_ICON_SIZE);
 * ImageIcon icon = iconManager.getIcon("path/to/icon.png", IconManager.SMALL_ICON_SIZE);
 * </pre>
 */
public class IconManager implements IService {

	/** Default size for application icons */
	public static final int DEFAULT_APPLICATION_ICON_SIZE = 64;

	/** Size constant for small icons */
	public static final int SMALL_ICON_SIZE = 16;

	/** Size constant for medium icons */
	public static final int MID_ICON_SIZE = 32;

	/** Size constant for large icons */
	public static final int LARGE_ICON_SIZE = 64;

	/**
	 * Retrieves the singleton instance of IconManager.
	 *
	 * @return The IconManager instance
	 */
	public static IconManager get() {
		return ServiceManager.get(IconManager.class);
	}

	private ResourceManager 				mResourceManager;
	private Map<String, BufferedImage> 		mImageCache = new ConcurrentHashMap<>();


	public IconManager() {
		this(ResourceManager.get());
	}

	public IconManager(ResourceManager resMgr) {
		if (resMgr == null)
			resMgr = ResourceManager.get();
		mResourceManager = resMgr;
	}

	/**
	 * Creates an ImageIcon from the given Image.
	 *
	 * @param image The source Image
	 * @return An ImageIcon, or null if the input is null
	 */
	public static ImageIcon getIcon(final Image image) {
		if (image == null)
			return null;
		return new ImageIcon(image);
	}

	/**
	 * Creates a scaled ImageIcon from the given Image.
	 *
	 * @param image The source Image
	 * @param size The desired size of the icon
	 * @return A scaled ImageIcon, or null if the input is null
	 */
	public ImageIcon getIcon(final Image image, final int size) {
		if (image == null)
			return null;
		return getIcon(getScaledImage(image, size, false));
	}

	/**
	 * Scales an Image to the specified size.
	 *
	 * @param image The source Image
	 * @param size The desired size of the scaled image
	 * @param keepAspect If true, maintains the aspect ratio of the original image
	 * @return A scaled BufferedImage
	 */
	public static BufferedImage getScaledImage(final Image image, final int size, final boolean keepAspect) {
		if (image == null) return null;
		if (image.getWidth(null) == size && image.getHeight(null) == size) return toBufferedImage(image);
		if (!keepAspect && (image.getWidth(null) == size || image.getHeight(null) == size)) return toBufferedImage(image);

		final BufferedImage source = toBufferedImage(image);
		final int w = source.getWidth();
		final int h = source.getHeight();

		final float scale = keepAspect ? Math.min((float) size / w, (float) size / h) : (float) size / Math.max(w, h);
		final int scaledWidth = Math.round(w * scale);
		final int scaledHeight = Math.round(h * scale);

		final BufferedImage result = new BufferedImage(size, size, BufferedImage.TYPE_INT_ARGB);
		final Graphics2D g2d = result.createGraphics();
		g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g2d.drawImage(source, (size - scaledWidth) / 2, (size - scaledHeight) / 2, scaledWidth, scaledHeight, null);
		g2d.dispose();

		return result;
	}

	/**
	 * Converts a given Image into a BufferedImage
	 *
	 * @param img The Image to be converted
	 * @return The converted BufferedImage
	 */
	public static BufferedImage toBufferedImage(final Image img) {
		if (img instanceof BufferedImage) return (BufferedImage) img;

		final BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);
		final Graphics2D bGr = bimage.createGraphics();
		bGr.drawImage(img, 0, 0, null);
		bGr.dispose();

		return bimage;
	}

	/**
	 * Retrieves a scaled image from the specified path.
	 *
	 * @param path The path to the image resource
	 * @param size The desired size of the image
	 * @return A BufferedImage of the specified size
	 */
	public BufferedImage getImage(final String path, final int size) {
		final BufferedImage img = getImage(null, path, size);
		if (img == null)
			return getImage(ReflectionUtils.getCaller(1), path, size);
		return img;
	}

	/**
	 * Retrieves an image from the specified path.
	 *
	 * @param path The path to the image resource
	 * @return A BufferedImage
	 */
	public BufferedImage getImage(final String path) {
		final BufferedImage img = getImage(null, path);
		if (img == null)
			return getImage(ReflectionUtils.getCaller(1), path);
		return img;
	}

	/**
	 * Retrieves an image from the specified path, using the given class as context.
	 *
	 * @param caller The class to use as context for resource loading
	 * @param path The path to the image resource
	 * @return A BufferedImage
	 */
	public BufferedImage getImage(final Class<?> caller, final String path) {
		return getImage(mResourceManager.getResource(caller, path));
	}

	/**
	 * Retrieves a scaled image from the specified path, using the given class as context.
	 *
	 * @param caller The class to use as context for resource loading
	 * @param path The path to the image resource
	 * @param size The desired size of the image
	 * @return A BufferedImage of the specified size
	 */
	public BufferedImage getImage(final Class<?> caller, final String path, final int size) {
		return getImage(mResourceManager.getResource(caller, path), size);
	}

	/**
	 * Creates a scaled ImageIcon from the specified path.
	 *
	 * @param path The path to the image resource
	 * @param size The desired size of the icon
	 * @return An ImageIcon of the specified size
	 */
	public ImageIcon getIcon(final String path, final int size) {
		BufferedImage img = getImage(null, path, size);
		if (img == null)
			img = getImage(ReflectionUtils.getCaller(1), path, size);
		return getIcon(img);
	}
	/**
	 * Creates a scaled ImageIcon from the specified path, using the given class as context.
	 *
	 * @param caller The class to use as context for resource loading
	 * @param path The path to the image resource
	 * @param size The desired size of the icon
	 * @return An ImageIcon of the specified size
	 */
	public ImageIcon getIcon(final Class<?> caller, final String path, final int size) {
		return getIcon(getImage(caller, path, size));
	}

	/**
	 * Creates a scaled ImageIcon from the specified URL.
	 *
	 * @param url The URL of the image resource
	 * @param size The desired size of the icon
	 * @return An ImageIcon of the specified size
	 */
	public ImageIcon getIcon(final URL url, final int size) {
		return getIcon(getImage(url, size));
	}

	/**
	 * Retrieves an image from the specified URL.
	 *
	 * @param url The URL of the image resource
	 * @return A BufferedImage
	 */
	public BufferedImage getImage(final URL url) {
		final String key = url.toString();
		BufferedImage bimg = mImageCache.get(key); //do not use return mImageCache.computeIfAbsent(key, this::saveLoadImage); as it result in a recursive update.
		if (bimg == null) {
			bimg = saveLoadImage(key);
			mImageCache.put(key, bimg);
		}
		return bimg;
	}

	/**
	 * Retrieves a scaled image from the specified URL.
	 *
	 * @param url The URL of the image resource
	 * @param size The desired size of the image
	 * @return A BufferedImage of the specified size
	 */
	public BufferedImage getImage(final URL url, final int size) {
		if (url == null)
			return null;

		final String key = url + "#" + size;
		BufferedImage bimg = mImageCache.get(key);
		if (bimg == null) {
			bimg = saveLoadImage(key);
			mImageCache.put(key, bimg);
		}
		return bimg;
	}

	private BufferedImage saveLoadImage(final String key) {
		try {
			return loadImage(key);
		}catch (ExecutionException | IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	/**
	 * Loads an image from the specified key, which can include size and aspect ratio information.
	 *
	 * @param key The key containing image URL and optional size and aspect ratio information
	 * @return A BufferedImage
	 * @throws ExecutionException If there's an error during image loading
	 * @throws IOException If there's an I/O error during image loading
	 */
	protected BufferedImage loadImage(final String key) throws ExecutionException, IOException {
		final String[] sp = key.split("#");
		final URL imgURL = new URL(sp[0]);
		if (sp.length == 1) {
			return ImageIO.read(imgURL);
		}
		final BufferedImage img = getImage(imgURL);
		final int size = Integer.parseInt(sp[1]);
		boolean keepAspect = false;
		if (sp.length > 2)
			keepAspect = Boolean.parseBoolean(sp[2]);
		return getScaledImage(img, size, keepAspect);
	}
}
