package io.gitlab.scholvac.resources;

public class ZipFileException extends Exception {

	public ZipFileException() {
	}

	public ZipFileException(final String arg0) {
		super(arg0);
	}

	public ZipFileException(final Throwable arg0) {
		super(arg0);
	}

	public ZipFileException(final String arg0, final Throwable arg1) {
		super(arg0, arg1);
	}

}
