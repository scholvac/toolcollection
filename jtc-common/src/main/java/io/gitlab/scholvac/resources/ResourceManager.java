package io.gitlab.scholvac.resources;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import io.gitlab.scholvac.service.IService;
import io.gitlab.scholvac.service.ServiceManager;

/**
 * A utility class for managing resources such as files and directories.
 * It provides methods for loading resources from the classpath, local directory, and specified search paths.
 * It also provides methods for unpacking files and directories to the home directory.
 */
public class ResourceManager implements IService {

	/**
	 * Returns the singleton instance of {@link ResourceManager}.
	 *
	 * @return the singleton instance of {@link ResourceManager}
	 */
	public static ResourceManager get() { return ServiceManager.get(ResourceManager.class); }

	private final Class<?>					mLoaderClass;
	private File 							mHomeDirectory;
	private final Collection<File> 			mResourcePaths;

	private Class<?> 						_mCallerClass; //temporary variable, that stores an external provided clazz as alternative to mLoaderClass.

	private final Map<String, URL> 			mCache = new HashMap<>();

	/**
	 * Constructs a new instance of {@link ResourceManager} with the default application name,
	 * no specified resource paths, and the {@link ResourceManager} class as the loader class.
	 */
	public ResourceManager() {
		this("DefaultApplication", null, ResourceManager.class);
	}
	/**
	 * Constructs a new instance of {@link ResourceManager} with the specified application name,
	 * resource paths, and loader class.
	 *
	 * @param appName the name of the application
	 * @param resourcePaths the collection of resource paths
	 * @param clazz the loader class
	 */
	public ResourceManager(final String appName, final Collection<File> resourcePaths, final Class<?> clazz) {
		this(new File(System.getProperty("user.home") + "/" + appName), resourcePaths, clazz);
	}
	/**
	 * Constructs a new instance of {@link ResourceManager} with the specified home directory,
	 * resource paths, and loader class.
	 *
	 * @param homeDirectory the home directory
	 * @param resourcePaths the collection of resource paths
	 * @param clazz the loader class
	 */
	public ResourceManager(final File homeDirectory, final Collection<File> resourcePaths, final Class<?> clazz) {
		mHomeDirectory = homeDirectory;
		if (mHomeDirectory == null)
			mHomeDirectory = new File(".");
		mResourcePaths = resourcePaths == null ? Collections.EMPTY_LIST : resourcePaths;
		mLoaderClass = clazz == null ? ResourceManager.class : clazz;
		if (ServiceManager.has(ResourceManager.class) == false)
			ServiceManager.register(mHomeDirectory.getAbsolutePath(), this);
	}


	/**
	 * Returns the URL of the specified resource.
	 *
	 * @param name the name of the resource
	 * @return the URL of the specified resource
	 */
	public URL getResource(final String name) {
		return getResource(null, name);
	}

	/**
	 * Returns the URL of the specified resource with the specified caller class.
	 *
	 * @param caller the caller class
	 * @param name the name of the resource
	 * @return the URL of the specified resource
	 */
	public synchronized URL getResource(final Class<?> caller, final String name) {
		try {
			_mCallerClass = caller;
			return mCache.computeIfAbsent(name, this::findURL);
		}catch (final Throwable e) {
			e.printStackTrace(); //others may help debugging
			return null;
		}finally {
			_mCallerClass = null;
		}
	}

	/**
	 * Returns the home directory of the application.
	 *
	 * @return the home directory of the application
	 */
	public File getHomeDirectory() { return mHomeDirectory;}
	/**
	 * Returns the collection of lookup paths for resources.
	 *
	 * @return the collection of lookup paths for resources
	 */
	public Collection<File> getLookupPaths() { return mResourcePaths;}

	/**
	 * Finds and returns the URL of the specified resource.
	 *
	 * @param key the name of the resource
	 * @return the URL of the specified resource
	 */
	private URL findURL(final String key) {
		//local dir, e.g. next to jar
		final File file = findFile(key);
		if (file != null && file.exists())
			try {
				return file.toURI().toURL();
			} catch (final MalformedURLException e) {
				e.printStackTrace();
			}
		//search in classpath
		URL url = mLoaderClass.getClassLoader().getResource(key);
		if (url != null)
			return url;
		final ClassLoader ccl = Thread.currentThread().getContextClassLoader();
		if (ccl != null && ccl != mLoaderClass.getClassLoader())
			url = ccl.getResource(key);
		if (url != null)
			return url;
		url = mLoaderClass.getResource(key);
		if (url != null)
			return url;
		if (_mCallerClass != null) {
			url = _mCallerClass.getClassLoader().getResource(key);
			if (url != null)
				return url;
			url = _mCallerClass.getResource(key);
			if (url != null)
				return url;
		}
		return url;
	}

	/**
	 * Finds and returns the file of the specified resource.
	 *
	 * @param key the name of the resource
	 * @return the file of the specified resource
	 */
	public File findFile(final String key) {
		final File inLocalDir = new File(key);
		if (inLocalDir.exists()) {
			return inLocalDir;
		}
		//in home dir
		final File inHomeDir = new File(getHomeDirectory(), key);
		if (inHomeDir.exists()) {
			return inHomeDir;
		}
		//in search paths
		final Collection<File> lookupPaths = getLookupPaths();
		if (lookupPaths != null) {
			for (final File lup : lookupPaths) {
				final File inLup = new File(lup, key);
				if (inLup.exists()){
					return inLup;
				}
			}
		}
		return null;
	}

	/**
	 * Resolves the specified resource name relative to the home directory.
	 *
	 * @param name the name of the resource
	 * @return the resolved resource name
	 */
	public File resolveInHome(final String name) {
		return new File(getHomeDirectory(), name);
	}


	/**
	 * Unpacks the specified file to the home directory.
	 *
	 * @param resourceName the name of the resource file
	 * @return the unpacked file
	 * @throws IOException if an I/O error occurs
	 */
	public File unpackFileToHome(final String resourceName) throws IOException {
		final File dest = resolveInHome(resourceName);
		return unpackFileTo(resourceName, dest);
	}

	/**
	 * Unpacks the specified file to the specified destination directory.
	 *
	 * @param resourceName the name of the resource file
	 * @param dest the destination directory
	 * @return the unpacked file
	 * @throws IOException if an I/O error occurs
	 */
	public File unpackFileTo(final String resourceName, final File _dest) throws IOException {
		final File dest = _dest.isDirectory() ? new File(_dest, resourceName) : _dest;
		if (!dest.exists()) {
			final URL url = getResource(resourceName);
			if (url == null)
				throw new IOException("Could not find Resource: " + resourceName);
			final File parent = dest.getParentFile();
			if (!parent.exists())
				try {
					parent.mkdirs();
				} catch (final Exception e) {
				}

			copy(url, new FileOutputStream(dest));
		}
		return dest;
	}

	/**
	 * Unpacks the specified directory to the home directory.
	 *
	 * @param resourceDirectoryName the name of the resource directory
	 * @throws IOException if an I/O error occurs
	 */
	public void unpackDirectoryToHome(final String resourceDirectoryName) throws IOException {
		unpackDirectoryTo(resourceDirectoryName , resolveInHome(resourceDirectoryName));
	}
	/**
	 * Unpacks the specified directory to the specified destination directory.
	 *
	 * @param resourceDirectoryName the name of the resource directory
	 * @param dest the destination directory
	 * @throws IOException if an I/O error occurs
	 */
	public void unpackDirectoryTo(final String resourceDirectoryName, final File dest) throws IOException {

		if (!dest.exists()) {
			final URL url = getResource(resourceDirectoryName);
			if (url == null)
				throw new IOException("Could not find Resource: " + resourceDirectoryName);
			if (!dest.exists())
				try {
					dest.mkdirs();
				} catch (final Exception e) {
				}
			try {
				FileOperations.copyDir(new File(url.toURI()), dest, false);
			} catch (final URISyntaxException e) {
				e.printStackTrace();
			}
		}
	}


	/**
	 * Copies all bytes from a URL to an output stream.
	 *
	 * @param from the URL to read from
	 * @param to the output stream
	 * @throws IOException if an I/O error occurs
	 */
	public static void copy(final URL from, final OutputStream to) throws IOException {
		final InputStream in = from.openStream();
		final BufferedOutputStream out = new BufferedOutputStream(to);

		try {
			final byte buffer[] = new byte[1024];
			int n_bytes;
			for (;;) {
				n_bytes = in.read(buffer);
				if (n_bytes == -1)
					break;
				out.write(buffer, 0, n_bytes);
			}
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (final IOException e) {
					e.printStackTrace();
				}
			}
			if (out != null) {
				try {
					out.close();
				} catch (final IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	/**
	 * Returns an input stream for the specified resource.
	 *
	 * @param resourceName the name of the resource
	 * @return the input stream for the specified resource
	 * @throws IOException if an I/O error occurs
	 */
	public InputStream getStream(final String resourceName) throws IOException {
		final URL url = getResource(resourceName);
		if (url == null)
			return null;
		return url.openStream();
	}
}
