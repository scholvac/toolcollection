package io.gitlab.scholvac.resources;

import java.io.File;

import io.gitlab.scholvac.StringUtils;

public class FileUtils {

	public static String getFileExtension(final File file) {
		return getFileExtension(file.getName());
	}
	/**
	 * Returns the <a href="http://en.wikipedia.org/wiki/Filename_extension">file extension</a> for
	 * the given file name, or the empty string if the file has no extension. The result does not
	 * include the '{@code .}'.
	 *
	 * <p><b>Note:</b> This method simply returns everything after the last '{@code .}' in the file's
	 * name as determined by {@link File#getName}. It does not account for any filesystem-specific
	 * behavior that the {@link File} API does not already account for. For example, on NTFS it will
	 * report {@code "txt"} as the extension for the filename {@code "foo.exe:.txt"} even though NTFS
	 * will drop the {@code ":.txt"} part of the name when the file is actually created on the
	 * filesystem due to NTFS's <a href="https://goo.gl/vTpJi4">Alternate Data Streams</a>.
	 *
	 * @since 11.0
	 */
	public static String getFileExtension(final String fullName) {
		StringUtils.checkNotNullOrEmpty(fullName);
		final String fileName = new File(fullName).getName();
		final int dotIndex = fileName.lastIndexOf('.');
		return (dotIndex == -1) ? "" : fileName.substring(dotIndex + 1);
	}

	/**
	 * Ensures that the returned file has the provided extension
	 * @param file The file to check for the correct extension
	 * @param extension the expected file extension
	 * @return a new instance of a file that has the requested exte3nsion
	 */
	public static File ensureExtension(final File file, final String extension) {
		final String pointLessExtension = extension.startsWith(".") ? extension.substring(1) : extension;
		if (getFileExtension(file.getName()).equals(pointLessExtension))
			return new File(file.getAbsolutePath());
		if (extension.startsWith("."))
			return new File(file, extension);

		return new File(file, "."+extension);
	}


	public static void rollFile(final File file, final int numberOfRolls) {
		rollFile(file, "_%d", numberOfRolls);
	}

	/**
	 * Rolls a file, e.g. rename the file with a given postfix
	 * @param file
	 * @param format
	 * @param numberOfRolls
	 */
	public static void rollFile(final File file, final String format, final int numberOfRolls) {
		if (file.exists() == false) return ;

		for (int i = numberOfRolls; i >= 0; i--) {
			final String srcName = file.getAbsolutePath() + String.format(format, i);
			final String dstName = file.getAbsolutePath() + String.format(format, i+1);
			final File srcFile = new File(srcName);
			if (srcFile.exists())
				FileOperations.rename(srcName, dstName, true);
		}
		final String srcName = file.getAbsolutePath();
		final String dstName = file.getAbsolutePath() + String.format(format, 0);
		FileOperations.rename(srcName, dstName, true);
	}
	public static boolean hasExtension(final File f, final String ...extensions) {
		final String ext = getFileExtension(f);
		for (int i = 0; i < extensions.length; i++)
			if (ext.equalsIgnoreCase(extensions[i]))
				return true;
		if (ext.startsWith(".") == false) {
			final String ext2 = "."+ext;
			for (int i = 0; i < extensions.length; i++)
				if (ext2.equalsIgnoreCase(extensions[i]))
					return true;
		}
		return false;
	}

}
