package io.gitlab.scholvac.func;

public interface Functionals {

	@FunctionalInterface
	public interface SupplierWithExceptions<ValueT> {
		ValueT get() throws Exception;
	}
	@FunctionalInterface
	public interface ConsumerWithExceptions<ValueT> {
		void accept(ValueT value) throws Exception;
	}

	@FunctionalInterface
	public interface FunctionWithException<InputT, OutputT, ExceptionT extends Exception>{
		OutputT apply(InputT input) throws ExceptionT;
	}

}
