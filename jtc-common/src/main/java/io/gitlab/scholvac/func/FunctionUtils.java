package io.gitlab.scholvac.func;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

import io.gitlab.scholvac.func.Functionals.ConsumerWithExceptions;
import io.gitlab.scholvac.func.Functionals.SupplierWithExceptions;

public class FunctionUtils {

	public static <IN, OUT> Function<IN, OUT> concatenate(final Function<IN, IN> first, final Function<IN, OUT> second) {
		return in -> second.apply(first.apply(in));
	}

	public static <IN, OUT> Function<IN, OUT> ifThenElse(final Predicate<IN> condition, final Function<IN, OUT> trueCase, final Function<IN, OUT> falseCase) {
		return str -> {
			if (condition.test(str)) {
				return trueCase.apply(str);
			}
			return falseCase.apply(str);
		};
	}

	public static void ifThen(final Boolean condition, final Runnable action) {
		if (condition) {
			action.run();
		}
	}

	public static <T> void ifNotNull(final T value, final Consumer<T> consumer) {
		if (value != null) {
			consumer.accept(value);
		}
	}

	public static <T> T createIfNull(final T optionalValue, final Supplier<T> supplier) {
		if (optionalValue != null) {
			return optionalValue;
		}
		return supplier.get();
	}
	public static double ifNaN(final double optionalValue, final Supplier<Double> supplier) {
		if (optionalValue == optionalValue) {
			return optionalValue;
		}
		return supplier.get();
	}

	public static <T> Consumer<T> ifNot(final Predicate<T> pred, final Consumer<T> action) {
		return val -> {
			if (pred.test(val) == false) {
				action.accept(val);
			}
		};
	}

	public static <T> Consumer<T> ifNot(final Predicate<T> pred, final Runnable action) {
		return val -> {
			if (pred.test(val) == false) {
				action.run();
			}
		};
	}



	public static <T> T ignoreExceptions(final SupplierWithExceptions<T> supplier) {
		return ignoreExceptions(supplier, (T)null);
	}
	public static <T> T ignoreExceptions(final SupplierWithExceptions<T> supplier, final T defaultValue) {
		try {
			final T value = supplier.get();
			return value;
		}catch(final Throwable t) {
			return defaultValue;
		}
	}

	public static <T> Throwable ignoreExceptions(final T value, final ConsumerWithExceptions<T> consumer) {
		try {
			consumer.accept(value);
			return null;
		}catch(final Throwable t) {
			return t;
		}
	}

}
