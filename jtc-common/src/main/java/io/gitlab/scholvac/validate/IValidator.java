package io.gitlab.scholvac.validate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

import org.slf4j.event.Level;

import io.gitlab.scholvac.log.ILogger;

public interface IValidator<CONTENT_TYPE> {

	public enum SEVERITY {
		INFO,
		WARNING,
		ERROR,
		FATAL;

		boolean worseThan(final SEVERITY minSeverity) {
			switch (minSeverity) {
			case FATAL: return this == FATAL;
			case ERROR: return this == ERROR || this == FATAL;
			case WARNING: return this == FATAL || this == ERROR || this == WARNING;
			case INFO: return true;
			}
			throw new IllegalArgumentException("Unknown severity: " + minSeverity);
		}
	}
	public static class ValidationMessage<CONTENT_TYPE> {
		private final String 			message;
		private final CONTENT_TYPE 		value;
		private final SEVERITY			severity;
		public ValidationMessage(final String message, final CONTENT_TYPE value, final SEVERITY severity) {
			this.message = message;
			this.value = value;
			this.severity = severity;
		}
		public String getMessage() {
			return message;
		}
		public CONTENT_TYPE getValue() {
			return value;
		}
		public SEVERITY getSeverity() {
			return severity;
		}
	}
	@FunctionalInterface
	public interface IValidationReporter {
		void report(final List<ValidationMessage<?>> messages);
	}
	public static class LogReporter implements IValidationReporter{
		private final ILogger 			mLogger;
		private final Level			mLevel;

		private String 					mFormat = "[%SEVERITY%]:\t %MESSAGE% (%VALUE%)";

		public LogReporter(final ILogger logger) { this(logger, Level.INFO);}
		public LogReporter(final ILogger logger, final Level level) {
			this.mLogger = logger;
			this.mLevel = level;
		}
		public void setFormat(final String format) { mFormat = format;}

		@Override
		public void report(final List<ValidationMessage<?>> messages) {
			for (final ValidationMessage<?> msg : messages) {
				mLogger.log(mLevel, format(msg));
			}
		}
		public String format(final ValidationMessage<?> msg) {
			String str = new String(mFormat);
			str = str.replace("%SEVERITY%", msg.getSeverity().name());
			str = str.replace("%MESSAGE%", msg.getMessage());
			str = str.replace("%VALUE%", ""+msg.getValue());
			return str;
		}
	}
	public static class ToStringReporter implements IValidationReporter{
		private final StringBuilder		mStringBuilder;
		private String 					mFormat = "[%SEVERITY%]:\t %MESSAGE% (%VALUE%)";

		public ToStringReporter() {this(new StringBuilder());}
		public ToStringReporter(final StringBuilder stringBuilder) { mStringBuilder = stringBuilder;}
		public void setFormat(final String format) { mFormat = format;}

		@Override
		public void report(final List<ValidationMessage<?>> messages) {
			mStringBuilder.delete(0, mStringBuilder.length());
			for (final ValidationMessage<?> msg : messages) {
				mStringBuilder.append(format(msg));
			}
		}
		public String format(final ValidationMessage<?> msg) {
			String str = new String(mFormat);
			str = str.replace("%SEVERITY%", msg.getSeverity().name());
			str = str.replace("%MESSAGE%", msg.getMessage());
			str = str.replace("%VALUE%", ""+msg.getValue());
			return str;
		}
		@Override
		public String toString() {
			return mStringBuilder.toString();
		}
	}
	public static class ExceptionReporter<ExceptionT extends RuntimeException> implements IValidationReporter{

		private final Function<String, ExceptionT> 	mExceptionProvider;
		private final ToStringReporter 				mMessageFormatter;


		public ExceptionReporter(final Function<String, ExceptionT> expProvider) {
			this(new ToStringReporter(), expProvider);
		}
		public ExceptionReporter(final ToStringReporter msgFormatter, final Function<String, ExceptionT> expProvider) {
			mMessageFormatter = msgFormatter;
			mExceptionProvider = expProvider;
		}
		@Override
		public void report(final List<ValidationMessage<?>> messages) {
			mMessageFormatter.report(messages);
			final String str = mMessageFormatter.toString();
			final ExceptionT exp = mExceptionProvider.apply(str);
			throw exp;
		}

	}


	public static class ValidationResult {
		private List<ValidationMessage<?>>	mMessages;

		public boolean hasMessages() {
			return mMessages != null && mMessages.isEmpty() == false;
		}

		public void add(final SEVERITY severity, final Throwable t) {
			add(severity, t, Object.class);
		}
		public <T> void add(final SEVERITY severity, final Throwable t, final Class<T> clazz) {
			String msg = t.getMessage();
			if (msg == null || msg.isEmpty() || msg == "null")
				msg = "Catched Exception: " + t.getClass().getName();
			add(new ValidationMessage<>(msg, (T)null, severity));
		}


		public void add(final ValidationMessage<?> msg) {
			if (msg != null) {
				if (mMessages == null) mMessages = new ArrayList<>();
				final ValidationMessage<?> existing = findMessage(msg.message);
				if (existing != null){
					if (existing.severity.compareTo(msg.severity) < 0)
						mMessages.remove(existing);
					else
						return ;//already available
				}
				mMessages.add(msg);
			}
		}
		public ValidationMessage<?> findMessage(final String msg) {
			if (msg != null && mMessages != null) return mMessages.stream().filter(it -> msg.equals(it.message)).findFirst().orElse(null);
			return null;
		}

		public List<ValidationMessage<?>> get(final SEVERITY ...severities) {
			final List<ValidationMessage<?>> msgs = new ArrayList<>();
			if (severities == null || severities.length == 0 || hasMessages() == false)
				return msgs;

			Arrays.sort(severities);//highest severity first
			for (final SEVERITY s : severities) {
				for (final ValidationMessage<?> msg : mMessages)
					if (msg.severity == s)
						msgs.add(msg);
			}
			return msgs;
		}

		public boolean has(final SEVERITY error) {
			return mMessages == null ? false : mMessages.stream().filter(p->p.severity == error).findFirst().isPresent();
		}
		public boolean hasOrWorse(final SEVERITY minSeverity) {
			if (mMessages == null) return false;
			for (int i = 0; i < mMessages.size(); i++) {
				final SEVERITY ms = mMessages.get(i).severity;
				if (ms.worseThan(minSeverity))
					return true;
			}
			return false;
		}

		public void error(final String message) {
			error(null, message);
		}
		public <T> void error(final T value, final String message) {
			add(SEVERITY.ERROR, value, message);
		}
		public void warn(final String message) {
			warn(null, message);
		}
		public <T> void warn(final T value, final String message) {
			add(SEVERITY.WARNING, value, message);
		}
		public void info(final String message) {
			info(null, message);
		}
		public <T> void info(final T value, final String message) {
			add(SEVERITY.INFO, value, message);
		}
		public <T> void add(final SEVERITY severity, final T value, final String message) {
			add(new ValidationMessage<>(message, value, severity));
		}

		public <T> void add(final SEVERITY severity, final String message) {
			add(new ValidationMessage<T>(message, null, severity));
		}
	}



	void validate(CONTENT_TYPE content, ValidationResult result);





	static <T> void validate(final List<IValidator<T>> validatorList, final T content, final ValidationResult result) {
		if (validatorList != null && validatorList.isEmpty() == false)
			validatorList.forEach(val -> validate(val, content, result));
	}

	static <T> void validate(final IValidator<T> validator, final T content, final ValidationResult result) {
		if (validator == null)
			return;
		try {
			validator.validate(content, result);
		}catch(final Throwable e) {
			e.printStackTrace();
		}
	}

	static <ReporterT extends IValidationReporter> IValidationReporter report(final IValidationReporter reporter, final ValidationResult result) {
		return report(reporter, result, SEVERITY.ERROR);
	}
	static IValidationReporter report(final IValidationReporter reporter, final ValidationResult result, final SEVERITY...severities) {
		reporter.report(result.get(severities));
		return reporter;
	}

}
