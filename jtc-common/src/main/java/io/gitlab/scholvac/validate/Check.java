//package io.gitlab.scholvac.validate;
//
//import java.util.function.Function;
//
//
//import io.gitlab.scholvac.validate.IValidator.SEVERITY;
//import io.gitlab.scholvac.validate.IValidator.ToStringReporter;
//import io.gitlab.scholvac.validate.IValidator.ValidationResult;
//
//public class Check<T>
//{
//	public static class RuntimeCheck extends RuntimeException {
//
//		private ValidationResult mResult;
//
//		public RuntimeCheck(final ValidationResult result) {
//			mResult = result;
//		}
//
//		@Override
//		public String getMessage() {
//			return IValidator.report(new ToStringReporter(), mResult).toString();
//		}
//
//
//	}
//
//	private final T mValue;
//	private final IValidator mVaildator;
//
//	public Check(final T value) {
//		mValue = value;
//		mVaildator = IValidator.create();
//	}
//
//	public Check<T> notNull() {
//		mVaildator.notNull(mValue);
//		return this;
//	}
//
//	public <ValueT> Check<T> notNull(final Function<T, ValueT> mapper){
//		mVaildator.notNull(mapper.apply(mValue));
//		return this;
//	}
//	public void throwRuntime() {
//		final ValidationResult result = mVaildator.validate();
//		if (result.has(SEVERITY.ERROR))
//			throw new RuntimeCheck(result);
//	}
//
//	public <ValueT> Check equals(final ValueT expected, final Function<T, ValueT> actualGenerator) {
//		mVaildator.equals(expected, () -> actualGenerator.apply(mValue));
//		return this;
//	}
//
//	public boolean isValid() {
//		return mVaildator.isValid();
//	}
//}
