package io.gitlab.scholvac.validate;

public class RuntimeAssertions {

	public static void precondition(final boolean condition, final String msg, final Object...args) {
		if (!condition) {
			throw new IllegalArgumentException(String.format(msg, args));
		}

	}

	public static void requireNonNull(final Object value, final String message, final Object...args) {
		if (value == null) {
			throw new NullPointerException(String.format(message, args));
		}
	}

}
