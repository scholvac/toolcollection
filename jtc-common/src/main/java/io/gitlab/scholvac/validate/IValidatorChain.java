package io.gitlab.scholvac.validate;

public interface IValidatorChain<ValueT> {

	boolean isValid(final ValueT value);
}
