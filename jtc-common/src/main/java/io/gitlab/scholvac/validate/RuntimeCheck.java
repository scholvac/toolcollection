package io.gitlab.scholvac.validate;

import java.util.function.BiFunction;
import java.util.function.Supplier;

import io.gitlab.scholvac.validate.IValidator.ValidationResult;

public class RuntimeCheck<ValueT> {


	public static class RuntimeCheckException extends RuntimeException{
		public RuntimeCheckException(final String message, final Throwable cause) {
			super(message, cause);
		}
	}


	private final IValidatorChain<ValueT> 		mChain;
	private final ValidationResult				mResult;

	public RuntimeCheck(final IValidatorChain<ValueT> validator, final ValidationResult result) {
		mChain = validator;
		mResult = result;
	}

	public void checkAndThrow(final Supplier<ValueT> supplier) {
		checkAndThrow(supplier.get());
	}
	public void checkAndThrow(final ValueT newValue) {
		checkAndThrow(newValue, RuntimeCheckException::new);
	}

	public <ExceptionT extends RuntimeException> void checkAndThrow(final ValueT newValue, final BiFunction<String, Throwable, ExceptionT> object) {

	}

	public ValidationResult evaluate(final ValueT newValue) {
		// TODO Auto-generated method stub
		return null;
	}
}
