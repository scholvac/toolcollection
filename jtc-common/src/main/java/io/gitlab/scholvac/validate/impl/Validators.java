package io.gitlab.scholvac.validate.impl;

import java.util.Objects;
import java.util.function.Supplier;

import io.gitlab.scholvac.validate.IValidator;
import io.gitlab.scholvac.validate.IValidator.ValidationResult;

public class Validators {
	protected abstract static class MessageValidator<CONTENT_TYPE> implements IValidator<CONTENT_TYPE>{
		private final String mMessage;
		public MessageValidator() { this(null);}
		public MessageValidator(final String msg) {
			mMessage = msg != null ? msg + " " : "";
		}
		protected void error(final ValidationResult result, final String reason) {
			result.add(SEVERITY.ERROR, mMessage + reason);
		}
	}
	public static class StringNotEmptyValidator extends MessageValidator<String>{
		public StringNotEmptyValidator() { this(null);}
		public StringNotEmptyValidator(final String msg) { super(msg); }

		@Override
		public void validate(final String content, final ValidationResult result) {
			if (content == null)
				error(result, "is null");
			else if (content.isEmpty())
				error(result, "is empty");
		}
	}
	public static class NotNull<CType> extends MessageValidator<CType>{
		public NotNull() { this(null);}
		public NotNull(final String msg) { super(msg); }

		@Override
		public void validate(final CType content, final ValidationResult result) {
			if (content == null)
				error(result, "is null");
		}
	}

	public static class IsNull<CType> extends MessageValidator<CType>{
		public IsNull() { this(null);}
		public IsNull(final String msg) { super(msg); }

		@Override
		public void validate(final CType content, final ValidationResult result) {
			if (content != null)
				error(result, "is not null");
		}
	}



	public static class EqualsValidator<CType> extends MessageValidator<CType>{
		private final CType mExepcted;
		public EqualsValidator(final CType expected) { mExepcted = expected;}

		@Override
		public void validate(final CType content, final ValidationResult result) {
			final Class<?> actualClazz = content != null ? content.getClass() : null;
			final Class<?> expectedClazz = mExepcted != null ? mExepcted.getClass() : null;
			if (actualClazz != expectedClazz && actualClazz != null && expectedClazz != null) { //if classes == null we get the expected error
				error(result, "Types of expected and actual value do not match: Expected = " + expectedClazz.getSimpleName() + " Actual = " + actualClazz.getSimpleName());
			}
			if (Objects.equals(mExepcted, content) == false)
				error(result, "Expected: [" + mExepcted+"] but got [" + content + "]");
		}
	}


	public static class LazyValidator<CType> {
		private Supplier<CType> mSupplier;
		private IValidator<CType> mDelegate;
		public LazyValidator(final IValidator<CType> delegate, final Supplier<CType> supplier) {
			if (delegate == null || supplier == null) throw new IllegalArgumentException("Supplier and delegate may not be null");
			mSupplier = supplier;
			mDelegate = delegate;
		}

		public void validate(final ValidationResult result) {
			final CType value = mSupplier.get();
			mDelegate.validate(value, result);
		}
	}
}
