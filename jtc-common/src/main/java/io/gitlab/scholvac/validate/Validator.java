//package io.gitlab.scholvac.validate;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.function.Consumer;
//import java.util.function.Predicate;
//import java.util.function.Supplier;
//
//import io.gitlab.scholvac.log.ILogger;
//import io.gitlab.scholvac.validate.IValidator.IValidationReporter;
//import io.gitlab.scholvac.validate.IValidator.LogReporter;
//import io.gitlab.scholvac.validate.IValidator.SEVERITY;
//import io.gitlab.scholvac.validate.IValidator.ValidationResult;
//
//public class Validator {
//
//
//
//	public static Validator create(final Consumer<Validator> init) {
//		final Validator v = create();
//		init.accept(v);
//		return v;
//	}
//
//	public static Validator create() {
//		return new Validator();
//	}
//
//
//
//
//	public static LogReporter LogReporter(final ILogger logger) {
//		return new LogReporter(logger);
//	}
//
//
//
//
//
//
//
//	private List<LazyValidator<?>> mValidatorChain;
//	private ValidationResult mResult;
//
//	private Validator() {}
//
//
//	public Validator notNullOrEmpty(final String msg, final Object value) {
//		return notNullOrEmpty(msg, () -> value);
//	}
//	public Validator notNullOrEmpty(final Object value) {
//		return notNullOrEmpty(() -> value);
//	}
//	public Validator notNullOrEmpty(final Supplier<?> supplier) {
//		return notNullOrEmpty(null, supplier);
//	}
//	public Validator notNullOrEmpty(final String msg, final Supplier<?> supplier) {
//		return append(new StringNotEmptyValidator(msg), () -> toString(supplier));
//	}
//	private String toString(final Supplier<?> supplier) {
//		final Object value = supplier.get();
//		if (value == null)
//			return null;
//		if (value instanceof String)
//			return (String)value;
//
//		return ""+value;
//	}
//
//
//	public <CType> Validator notNull(final String msg, final CType value) {
//		return notNull(msg, () -> value);
//	}
//	public <CType> Validator notNull(final CType value) {
//		return notNull(() -> value);
//	}
//	public <CType> Validator notNull(final Supplier<CType> supplier) {
//		return notNull(null, supplier);
//	}
//	public <CType> Validator notNull(final String msg, final Supplier<CType> supplier) {
//		return append(new NotNull<>(msg), supplier);
//	}
//
//	public <CType> Validator isNull(final Supplier<CType> supplier) {
//		return isNull(null, supplier);
//	}
//	public <CType> Validator isNull(final String msg, final Supplier<CType> supplier) {
//		return append(new IsNull<>(msg), supplier);
//	}
//
//
//	public <ValueT> Validator equals(final ValueT expected, final ValueT actual) {
//		append(new EqualsValidator(expected), () -> actual);
//		return this;
//	}
//	public <ValueT> Validator equals(final ValueT expected, final Supplier<ValueT> actual) {
//		append(new EqualsValidator(expected), actual);
//		return this;
//	}
//
//
//	public <T> Validator append(final Supplier<T> supplier, final Predicate<T> pred, final Consumer<ValidationResult> reporter) {
//		return append(new LazyValidator<>((content, result) -> {
//			if (pred.test(content) == false)
//				reporter.accept(getResult());
//		}, supplier));
//
//	}
//	public <CType> Validator append(final IValidator<CType> validator, final Supplier<CType> supplier) {
//		return append(new LazyValidator<>(validator, supplier));
//	}
//	public <T> Validator append(final LazyValidator<T> iValidator) {
//		if (mValidatorChain == null)
//			mValidatorChain =new ArrayList<>();
//		if (iValidator != null)
//			mValidatorChain.add(iValidator);
//		mResult = null;
//		return this;
//	}
//
//
//	public boolean isValid() { return validate().has(SEVERITY.ERROR) == false; }
//	public ValidationResult getResult() { return validate();
//
//	}
//	public ValidationResult validate() {
//		if (mResult != null)
//			return mResult;
//		if (mValidatorChain == null || mValidatorChain.isEmpty())
//			return null;
//
//		mResult = new ValidationResult();
//		for(final LazyValidator<?> validator : mValidatorChain) {
//			validator.validate(mResult);
//		}
//		return mResult;
//	}
//
//	public void report(final IValidationReporter reporter) {
//		report(reporter, SEVERITY.ERROR);
//	}
//
//	public void report(final IValidationReporter reporter, final SEVERITY... severities) {
//		final ValidationResult result = getResult();
//		reporter.report(result.get(severities));
//	}
//
//	public void append(final Exception e) {
//		getResult().error(e.getMessage());
//	}
//
//	public void clear() {
//		if (mValidatorChain != null) mValidatorChain.clear();
//		mResult = null;
//	}
//}