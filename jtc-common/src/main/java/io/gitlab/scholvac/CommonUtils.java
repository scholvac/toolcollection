package io.gitlab.scholvac;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;

public class CommonUtils {


	private static final double NANO_TO_MICRO = 1e-3;
	private static final double NANO_TO_MILLI = 1e-6;
	private static final double NANO_TO_SECONDS = 1e-9;
	private static final double NANO_TO_MINUTES = 1.6667e-11;
	private static final double NANO_TO_HOURS = 2.7778-13;
	private static final double NANO_TO_DAYS = 1.1574e-14;
	private static final double NANO_TO_WEEKS = 1.6534e-15;
	private static final double NANO_TO_MONTH = 3.8052e-16;
	private static final double NANO_TO_YEARS = 3.171e-17;
	private static final double NANO_TO_DECADES = 3.171e-18;
	private static final double NANO_TO_MILLENIAS = 3.171e-19;
	private static final int SYSTEM_SCHEDULER_THREAD_COUNT = 1;

	private static ScheduledExecutorService sSystemScheduler;

	public static ThreadFactory createThreadFactory(final String name, final boolean daemon) {
		return new ThreadFactory() {
			private int 		mThreadCounter = 0;
			@Override
			public Thread newThread(final Runnable r) {
				final Thread t = new Thread(r);
				t.setName(name + "_" + (++mThreadCounter));
				t.setDaemon(daemon);
				return t;
			}
		};
	}
	public static double getAs(final Duration duration, final TemporalUnit tu) {
		final double nannos = duration.toNanos();
		return fromNanoSecondTo(nannos, tu);
	}

	public static double fromNanoSecondTo(final double nannos, final TemporalUnit tu) {
		if (tu == ChronoUnit.NANOS)
			return nannos;
		if (tu == ChronoUnit.MICROS)
			return nannos * NANO_TO_MICRO;
		if (tu == ChronoUnit.MILLIS)
			return nannos * NANO_TO_MILLI;
		if (tu == ChronoUnit.SECONDS)
			return nannos * NANO_TO_SECONDS;
		if (tu == ChronoUnit.MINUTES)
			return nannos * NANO_TO_MINUTES;
		if (tu == ChronoUnit.HOURS)
			return nannos * NANO_TO_HOURS;
		if (tu == ChronoUnit.DAYS)
			return nannos * NANO_TO_DAYS;
		if (tu == ChronoUnit.WEEKS)
			return nannos * NANO_TO_WEEKS;
		if (tu == ChronoUnit.MONTHS)
			return nannos * NANO_TO_MONTH;
		if (tu == ChronoUnit.YEARS)
			return nannos * NANO_TO_YEARS;
		if (tu == ChronoUnit.DECADES)
			return nannos * NANO_TO_DECADES;
		if (tu == ChronoUnit.MILLENNIA)
			return nannos * NANO_TO_MILLENIAS;
		throw new IllegalArgumentException("Timeunit : " + tu + " is not supported");
	}
	public static String getSymbol(final ChronoUnit tu) {
		if (tu == ChronoUnit.NANOS)
			return "ns";
		if (tu == ChronoUnit.MICROS)
			return "microsec";
		if (tu == ChronoUnit.MILLIS)
			return "ms";
		if (tu == ChronoUnit.SECONDS)
			return "s";
		if (tu == ChronoUnit.MINUTES)
			return "min";
		if (tu == ChronoUnit.HOURS)
			return "h";
		if (tu == ChronoUnit.DAYS)
			return "d";
		if (tu == ChronoUnit.WEEKS)
			return "weeks";
		if (tu == ChronoUnit.MONTHS)
			return "M";
		if (tu == ChronoUnit.YEARS)
			return "Y";
		if (tu == ChronoUnit.DECADES)
			return "dY";
		if (tu == ChronoUnit.MILLENNIA)
			return "millenia";
		throw new IllegalArgumentException("Timeunit : " + tu + " is not supported");
	}









	/** Provides a system wide ui scheduler, that shall be reused by as many UI elements as possible, instead of creating a new execution service */
	public static synchronized ScheduledExecutorService getSystemScheduler() {
		if (sSystemScheduler == null)
			sSystemScheduler = Executors.newScheduledThreadPool(SYSTEM_SCHEDULER_THREAD_COUNT, CommonUtils.createThreadFactory("SystemScheduler", true));
		return sSystemScheduler;
	}
}
