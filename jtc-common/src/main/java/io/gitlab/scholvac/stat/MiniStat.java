package io.gitlab.scholvac.stat;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.concurrent.TimeUnit;

import io.gitlab.scholvac.CommonUtils;



public class MiniStat {

	private String 		mName;
	private long 		mTic = 0;
	private long 		mToc = 0;

	private int 		mCounter = 0;
	private long 		mSum = 0;
	private long 		mLast = 0;

	private long 		mMax = -Long.MAX_VALUE;
	private long 		mMin =  Long.MAX_VALUE;

	public MiniStat() { this(null);}
	public MiniStat(final String name) {
		mName = name;
	}

	public void reset() {
		mSum = mLast = mTic = mToc = mCounter = 0;
		mMax = -Long.MAX_VALUE;
		mMin =  Long.MAX_VALUE;
	}

	public void tic() {
		mTic = System.nanoTime();
	}

	/**
	 * ends the current measurement (e.g. tic needs to be called before),
	 * increases the counter, calculates the last value (e.g. time between tic() and toc())
	 * and returns the time in milliseconds
	 * @return time between tic and toc in milliseconds
	 */
	public long toc() {
		mToc = System.nanoTime();
		mLast = mToc - mTic;

		mCounter++;
		mSum += mLast;
		mMin = mLast < mMin ? mLast : mMin;
		mMax = mLast > mMax ? mLast : mMax;

		return TimeUnit.NANOSECONDS.toMillis(mLast);
	}
	/** Synonym of toc() */
	public long tac() { return toc();}


	public Duration sum() { return Duration.ofNanos(mSum);}
	public double sum(final TemporalUnit unit) { return CommonUtils.fromNanoSecondTo(mSum, unit); }

	public Duration last() { return Duration.ofNanos(mLast); }
	public double last(final TemporalUnit unit) { return CommonUtils.fromNanoSecondTo(mLast, unit); }

	public int count() { return mCounter; }
	public Duration min() { return Duration.ofNanos(mMin); }

	public double min(final TemporalUnit unit) { return CommonUtils.fromNanoSecondTo(mMin, unit);}

	public Duration max() { return Duration.ofNanos(mMax);}
	public double max(final TemporalUnit unit) { return CommonUtils.fromNanoSecondTo(mMax, unit);}

	public Duration avg() {
		final double avgNS = (double)mSum/mCounter;
		return Duration.ofNanos((long) avgNS);
	}
	public double avg(final TemporalUnit unit) {
		final double avgNS = (double)mSum/mCounter;
		return CommonUtils.fromNanoSecondTo(avgNS, unit);
	}


	@Override
	public String toString() {
		return toString(ChronoUnit.SECONDS);
	}
	public String toString(final ChronoUnit unit) {
		final String sym = CommonUtils.getSymbol(unit);
		return String.format("%s: avg=%1.3f[%s], count=%d, sum=%1.3f[%s], last=%1.3f[%s], min=%1.3f[%s], max=%1.3f[%s]", mName, avg(unit), sym, mCounter, sum(unit), sym, last(unit), sym, min(unit), sym, max(unit), sym);
	}

}
