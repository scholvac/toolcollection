package io.gitlab.scholvac.other;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFileChooser;



public class ExtendedFile extends File {

	public static enum Type {
		DIRECTORY(JFileChooser.DIRECTORIES_ONLY),
		FILE(JFileChooser.FILES_ONLY),
		BOTH(JFileChooser.FILES_AND_DIRECTORIES);

		private final int mJFileSelectionMode;

		Type(final int sm) {
			mJFileSelectionMode = sm;
		}
		public int getJFileSelectionMode() {
			return mJFileSelectionMode;
		}
	}
	public static enum Direction {
		OPEN, SAVE
	}

	public static class Builder {
		ExtendedFile f;

		public Builder(final File file) { f = new ExtendedFile(file); }
		public Builder(final File parent, final String child) { f = new ExtendedFile(parent, child); }
		public Builder(final String parent, final String child) { f = new ExtendedFile(parent, child); }
		public Builder(final String pathname) { f = new ExtendedFile(pathname); }
		public Builder(final URI uri) { f = new ExtendedFile(uri); }

		public Builder type(final Type t) { f.setType(t); return this;}
		public Builder direction(final Direction dir) { f.setDirection(dir); return this;}
		public Builder extension(final String ext) { f.addExtension(ext); return this;}

		public Builder directory() { return type(Type.DIRECTORY); }
		public Builder file() { return type(Type.FILE); }
		public Builder save() { return direction(Direction.SAVE); }
		public Builder open() { return direction(Direction.OPEN); }

		public ExtendedFile build() { return f;}
	}

	public static Builder builder(final File file) {return new Builder(file);}
	public static Builder builder(final File parent, final String child) {return new Builder(parent, child);}
	public static Builder builder(final String parent, final String child) {return new Builder(parent, child);}
	public static Builder builder(final String file) {return new Builder(file);}
	public static Builder builder(final URI file) {return new Builder(file);}

	private Type 					mType;
	private Direction				mDirection;
	private List<String>			mExtensions;

	public ExtendedFile(final File parent, final String child) {
		super(parent, child);
	}

	public void setType(final Type t) { mType = t; }
	public Type getType() { return mType;}

	public void setDirection(final Direction dir) { mDirection = dir; }
	public Direction getDirection() { return mDirection;}

	public void addExtension(final String ext) {
		if (mExtensions == null) mExtensions = new ArrayList<>();
		if (!mExtensions.contains(ext))
			mExtensions.add(ext);
	}

	public ExtendedFile(final String parent, final String child) {
		super(parent, child);
	}

	public ExtendedFile(final String pathname) {
		super(pathname);
	}

	public ExtendedFile(final URI uri) {
		super(uri);
	}

	public ExtendedFile(final File file) { super(file, ""); }

	public boolean delete(final boolean recursive) {
		try{
			_delete(this.toPath(), recursive);
			return true;
		}catch(final Exception e) {
			return false;
		}
	}


	private static void _delete(final Path path, final boolean recursive) throws IOException {
		if (path == null) return ;
		if (!recursive) {
			Files.delete(path);
			return ;
		}
		if (Files.isDirectory(path, LinkOption.NOFOLLOW_LINKS)) { //use path to have the link option
			try (DirectoryStream<Path> entries = Files.newDirectoryStream(path)) {
				for (final Path entry : entries) {
					_delete(entry, recursive);
				}
			}
		}
		Files.delete(path);
	}

}
