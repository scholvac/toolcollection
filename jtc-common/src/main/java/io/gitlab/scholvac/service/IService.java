package io.gitlab.scholvac.service;

public interface IService {

	default String getServiceName() { return getClass().getSimpleName();}

}
