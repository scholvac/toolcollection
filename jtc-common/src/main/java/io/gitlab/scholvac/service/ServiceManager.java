package io.gitlab.scholvac.service;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Optional;

import io.gitlab.scholvac.log.SLog;

public class ServiceManager {

	private static HashMap<String, IService>						mIDServiceMap = new HashMap<>();
	private static HashMap<Class<? extends IService>, IService>		mClazzServiceMap = new HashMap<>();

	public static <ServiceType extends IService> ServiceType register(final ServiceType service) {
		return register(service.getClass().getName(), service);
	}

	public static <ServiceType extends IService> ServiceType register(final String serviceID, final ServiceType service) {
		return register(serviceID, service, false);
	}
	public static <ServiceType extends IService> ServiceType register(final String serviceID, final ServiceType service, final boolean overwrite) {
		if (mIDServiceMap.containsKey(serviceID)) {
			final String postFix = overwrite ? " (overwrite existing)": "";
			SLog.debug("A service with ID {} has already been registered {}", serviceID, postFix);
			if (!overwrite)
				return (ServiceType) mIDServiceMap.get(serviceID);
		}
		mIDServiceMap.put(serviceID, service);
		final Class<? extends IService> clazz = service.getClass();
		mClazzServiceMap.put(clazz, service);
		return service;
	}
	public static <ServiceType extends IService> ServiceType remove(final String serviceID) {
		final ServiceType service = get(serviceID);
		return remove(service);
	}
	public static <ServiceType extends IService> ServiceType remove(final ServiceType service) {
		final Optional<Entry<String, IService>> entry = mIDServiceMap.entrySet().stream().filter(e -> e.getValue() == service).findFirst();
		if (entry.isPresent()) {
			mIDServiceMap.remove(entry.get().getKey());
			final Class<? extends IService> clazz = service.getClass();
			if (mClazzServiceMap.get(clazz) == service) {
				//need to be removed here as well and we need to find another instance, if one exist
				mClazzServiceMap.remove(clazz);
				final Optional<Entry<String, IService>> newClazzService = mIDServiceMap.entrySet().stream().filter(e->e.getValue().getClass() == clazz).findAny();
				if (newClazzService.isPresent())
					mClazzServiceMap.put(clazz, newClazzService.get().getValue()); //if another instance exists, the one becomes the new instance to be returned when searched for clazz
			}
			return service;
		}
		return null;
	}

	public static <ServiceType extends IService> boolean has(final Class<ServiceType> clazz) {
		return mClazzServiceMap.containsKey(clazz);
	}

	@SuppressWarnings("unchecked")
	public static <ServiceType extends IService> ServiceType get(final String serviceID) {
		return (ServiceType) mIDServiceMap.get(serviceID);
	}
	public static <ServiceType extends IService> boolean has(final String serviceID) {
		return mIDServiceMap.containsKey(serviceID);
	}
	@SuppressWarnings("unchecked")
	public static <ServiceType extends IService> ServiceType get(final Class<ServiceType> clazz) {
		final ServiceType service = (ServiceType) mClazzServiceMap.get(clazz);
		if (service != null) {
			return service;
		}
		//create a new instance
		try {
			final ServiceType newService = clazz.newInstance();
			if (newService != null)
				return register(newService);
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
			SLog.error(e, "Failed to instanciiate service of class {}", clazz.getName());
		}
		return null;
	}
}
