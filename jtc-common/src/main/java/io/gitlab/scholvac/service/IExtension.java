package io.gitlab.scholvac.service;

public interface IExtension {
	/**
	 * @return true if an extension point shall support multiple instances of this extension (class). Default is false.
	 */
	default boolean multiInstanceSupport() { return false;}
}
