package io.gitlab.scholvac.service;

import java.util.Collection;

import io.gitlab.scholvac.IDisposeable;

public interface IExtensionPoint<ExtensionT extends IExtension> {

	IDisposeable register(ExtensionT extension);
	void remove(final ExtensionT extension);

	Collection<ExtensionT> getExtensions();
}
