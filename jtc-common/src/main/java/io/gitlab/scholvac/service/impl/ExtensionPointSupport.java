package io.gitlab.scholvac.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import io.gitlab.scholvac.IDisposeable;
import io.gitlab.scholvac.service.IExtension;
import io.gitlab.scholvac.service.IExtensionPoint;

public class ExtensionPointSupport<ExtensionT extends IExtension> implements IExtensionPoint<ExtensionT> {

	private final List<ExtensionT> mExtensions = new ArrayList<>();

	@Override
	public IDisposeable register(final ExtensionT ext) {
		if (ext != null) {
			if (mExtensions.contains(ext) && ext.multiInstanceSupport() == false)
				return IDisposeable.EMPTY;
			mExtensions.add(ext);
			return IDisposeable.create(() -> remove(ext));
		}
		return IDisposeable.EMPTY;
	}

	@Override
	public void remove(final ExtensionT extension) {
		mExtensions.remove(extension);
	}

	@Override
	public Collection<ExtensionT> getExtensions() {
		return Collections.unmodifiableCollection(mExtensions);
	}







	/**
	 * Default implementation of IAnnotated object.
	 * <br>
	 * The interface reduces the IAnnotated object to an provider of an {@link AnnotationSupport} instance.
	 */
	public interface AbstractExtensionPointSupport<ExtensionT extends IExtension> extends IExtensionPoint<ExtensionT>{

		ExtensionPointSupport<ExtensionT> getExtensionPointSupport();

		@Override
		default IDisposeable register(final ExtensionT extension) { return getExtensionPointSupport().register(extension); }
		@Override
		default void remove(final ExtensionT extension) { getExtensionPointSupport().remove(extension); }
		@Override
		default Collection<ExtensionT> getExtensions() { return getExtensionPointSupport().getExtensions(); }
		default Stream<ExtensionT> getExtensionStream() { return getExtensions().stream();}
	}
}
