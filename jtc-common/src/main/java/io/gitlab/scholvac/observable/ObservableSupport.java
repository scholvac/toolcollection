package io.gitlab.scholvac.observable;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.BiConsumer;

import io.gitlab.scholvac.CommonUtils;
import io.gitlab.scholvac.IDisposeable;
import io.gitlab.scholvac.observable.IObservableObject.ValueChangeEvent.IndexedValueChangeEvent;

/**
 * An implementation of {@link IObservableObject} that provides a default implementation of the observable object pattern.
 *
 * @param <ValueT> The type of the value that this observable object holds.
 *
 * @author Scholvac
 */
public class ObservableSupport<ValueT> implements IObservableObject<ValueT>{

	/**
	 * A static thread pool executor service that is used to execute value change notifications asynchronously.
	 *
	 * @since 1.0
	 */
	private static final ExecutorService sPropertyChangeExecutorQueue = Executors.newFixedThreadPool(2, CommonUtils.createThreadFactory("PropertyChangeSupport", true));

	/**
	 * A list that contains all the registered value change listeners.
	 *
	 * @since 1.0
	 */
	private final List<IValueChangeListener<ValueT>> mListeners = new CopyOnWriteArrayList<>();

	/**
	 * A bi-consumer that is used to dispatch value change notifications to the registered value change listeners.
	 *
	 * @since 1.0
	 */
	private BiConsumer<IValueChangeListener<ValueT>, ValueChangeEvent<ValueT>> mEventDispatcher;

	/**
	 * Sets the bi-consumer that is used to dispatch value change notifications to the registered value change listeners.
	 *
	 * @param dispatcher The bi-consumer that is used to dispatch value change notifications.
	 *
	 * @since 1.0
	 */
	public void setValueChangeDispatcher(final BiConsumer<IValueChangeListener<ValueT>, ValueChangeEvent<ValueT>> dispatcher) {
		mEventDispatcher = dispatcher;
	}

	/**
	 * Gets the bi-consumer that is used to dispatch value change notifications to the registered value change listeners.
	 *
	 * @return The bi-consumer that is used to dispatch value change notifications.
	 *
	 * @since 1.0
	 */
	protected BiConsumer<IValueChangeListener<ValueT>, ValueChangeEvent<ValueT>> getEventDispatcher(){
		if (mEventDispatcher == null) {
			//			mEventDispatcher = (listener, evt) -> sPropertyChangeExecutorQueue.execute( () -> listener.valueChanged(evt));
			mEventDispatcher = IValueChangeListener::valueChanged;
		}
		return mEventDispatcher;
	}

	/**
	 * Adds a value change listener to receive notifications when the value of this observable object changes.
	 *
	 * @param listener The listener to add.
	 *
	 * @return A {@code IDisposable} object that can be used to remove the listener later.
	 *
	 * @since 1.0
	 */
	@Override
	public IDisposeable addValueChangeListener(final IValueChangeListener<ValueT> listener) {
		if (listener == null || mListeners.contains(listener)) {
			return IDisposeable.EMPTY;
		}

		mListeners.add(listener);
		return IDisposeable.create(() -> removeValueChangeListener(listener));
	}

	/**
	 * Removes a value change listener that was previously added using {@link #addValueChangeListener(IValueChangeListener)}.
	 *
	 * @param listener The listener to remove.
	 *
	 * @since 1.0
	 */
	@Override
	public void removeValueChangeListener(final IValueChangeListener<ValueT> listener) {
		mListeners.remove(listener);
	}

	/**
	 * Fires a value changed event with the specified old and new values.
	 *
	 * @param oldValue The old value.
	 * @param newValue The new value.
	 *
	 * @since 1.0
	 */
	public void fireValueChanged(final ValueT oldValue, final ValueT newValue) {
		fireValueChanged(null, oldValue, newValue);
	}

	/**
	 * Fires a value changed event with the specified source, old and new values.
	 *
	 * @param object The source of the change.
	 * @param oldValue The old value.
	 * @param newValue The new value.
	 *
	 * @since 1.0
	 */
	public void fireValueChanged(final Object object, final ValueT oldValue, final ValueT newValue) {
		if (hasListener() == false) {
			return ;
		}
		final ValueChangeEvent<ValueT> evt = new ValueChangeEvent<>(object, oldValue, newValue);
		_fireValueChanged(evt);
	}

	/**
	 * Fires an indexed value changed event with the specified property name, index, old and new values.
	 *
	 * @param propertyName The property name.
	 * @param index The index.
	 * @param oldValue The old value.
	 * @param newValue The new value.
	 *
	 * @since 1.0
	 */
	public void fireIndexedValueChaned(final String propertyName, final int index, final ValueT oldValue, final ValueT newValue) {
		if (hasListener() == false) {
			return ;
		}
		final IndexedValueChangeEvent<ValueT> ivce = new IndexedValueChangeEvent<>(propertyName, index, oldValue, newValue);
		_fireValueChanged(ivce);
	}

	/**
	 * Fires a value changed event with the specified event.
	 *
	 * @param evt The event to fire.
	 *
	 * @since 1.0
	 */
	public void fireValueChanged(final ValueChangeEvent<ValueT> evt) {
		if (hasListener() == false) {
			return ;
		}
		_fireValueChanged(evt);
	}

	/** actually fires the value changed event.*/
	private void _fireValueChanged(final ValueChangeEvent<ValueT> evt) {
		final BiConsumer<IValueChangeListener<ValueT>, ValueChangeEvent<ValueT>> dispatcher = getEventDispatcher();
		for (final IValueChangeListener<ValueT> listener : mListeners) {
			dispatcher.accept(listener, evt);
		}
	}

	/**
	 * Checks if there are any registered value change listeners.
	 *
	 * @return {@code true} if there are any registered value change listeners, {@code false} otherwise.
	 *
	 * @since 1.0
	 */
	public boolean hasListener() {
		return mListeners != null && mListeners.isEmpty() == false;
	}

	/**
	 * Default implementation of IAnnotated object.
	 * <br>
	 * The interface reduces the IAnnotated object to an provider of an {@link AnnotationSupport} instance.
	 */
	public interface AbstractObservableSupport<ValueT> extends IObservableObject<ValueT>{
		ObservableSupport<ValueT> getObservableSupport();

		@Override
		default IDisposeable addValueChangeListener(final IValueChangeListener<ValueT> listener) { return getObservableSupport().addValueChangeListener(listener);}

		@Override
		default void removeValueChangeListener(final IValueChangeListener<ValueT> listener) {getObservableSupport().removeValueChangeListener(listener);}

		default void fireValueChanged(final ValueT oldValue, final ValueT newValue) { getObservableSupport().fireValueChanged(oldValue, newValue); }
		default void fireValueChanged(final Object src, final ValueT oldValue, final ValueT newValue) { getObservableSupport().fireValueChanged(src, oldValue, newValue); }
		default void fireValueChanged(final ValueChangeEvent<ValueT> evt) { getObservableSupport().fireValueChanged(evt);}

		default void fireIndexedValueChanged(final String propertyName, final int index, final ValueT oldValue, final ValueT newValue) {
			getObservableSupport().fireIndexedValueChaned(propertyName, index, oldValue, newValue);
		}
	}

}
