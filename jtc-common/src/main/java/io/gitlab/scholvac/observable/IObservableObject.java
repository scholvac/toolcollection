package io.gitlab.scholvac.observable;

import java.io.Serializable;
import java.util.Collection;

import io.gitlab.scholvac.IDisposeable;
import io.gitlab.scholvac.IDisposeable.MultiDisposable;

/**
 * An interface for observable objects that can notify listeners when their value changes.
 *
 * @param <ValueT> The type of the value that this observable object holds.
 *
 */
public interface IObservableObject<ValueT> {

	/**
	 * A serializable event class that represents a value change. It contains the source of the change, the old value, and the new value.
	 *
	 * @param <T> The type of the value that this observable object holds.
	 */
	public class ValueChangeEvent<T> implements Serializable {

		private final Object mSource;
		private final T mOldObject;
		private final T mNewObject;

		/**
		 * Constructs a new {@code ValueChangeEvent} with the specified source, old value, and new value.
		 *
		 * @param src The source of the change.
		 * @param oldObj The old value.
		 * @param newObj The new value.
		 */
		public ValueChangeEvent(final Object src, final T oldObj, final T newObj) {
			mSource = src;
			mOldObject = oldObj;
			mNewObject = newObj;
		}

		/**
		 * Checks if the new value is not null.
		 *
		 * @return {@code true} if the new value is not null, {@code false} otherwise.
		 */
		public boolean hasNew() {
			return getNewValue() != null;
		}

		/**
		 * Gets the new value.
		 *
		 * @return The new value.
		 */
		public T getNewValue() {
			return mNewObject;
		}

		/**
		 * Checks if the old value is not null.
		 *
		 * @return {@code true} if the old value is not null, {@code false} otherwise.
		 */
		public boolean hasOld() {
			return getOldValue() != null;
		}

		/**
		 * Gets the old value.
		 *
		 * @return The old value.
		 */
		public T getOldValue() {
			return mOldObject;
		}

		/**
		 * Gets the source of the change.
		 *
		 * @return The source of the change.
		 */
		public Object getSource() {
			return mSource;
		}

		@Override
		public String toString() {
			return "ValueChangeEvent["+getSource() + ": " + getOldValue() + " -> " + getNewValue() + "]";
		}

		/**
		 * A subclass of {@code ValueChangeEvent} that adds an index to the event.
		 *
		 * @param <IdxValT> The type of the indexed value.
		 */
		public static class IndexedValueChangeEvent<IdxValT> extends ValueChangeEvent<IdxValT> {

			private final int mIndex;

			/**
			 * Constructs a new {@code IndexedValueChangeEvent} with the specified source, index, old value, and new value.
			 *
			 * @param src The source of the change.
			 * @param idx The index of the change.
			 * @param oldObj The old value.
			 * @param newObj The new value.
			 */
			public IndexedValueChangeEvent(final Object src, final int idx, final IdxValT oldObj, final IdxValT newObj) {
				super(src, oldObj, newObj);
				mIndex = idx;
			}

			/**
			 * Gets the index of the change.
			 *
			 * @return The index of the change.
			 */
			public int getIndex() {
				return mIndex;
			}
		}
	}

	/**
	 * An interface for listeners that want to be notified when the value of an observable object changes.
	 *
	 * @param <T> The type of the value that this observable object holds.
	 *
	 */
	public interface IValueChangeListener<T> {

		/**
		 * Called when the value of the observable object changes.
		 *
		 * @param changeEvt The event that contains information about the change.
		 */
		void valueChanged(final ValueChangeEvent<T> changeEvt);
	}

	/**
	 * Adds a listener to receive notifications when the value of this observable object changes.
	 *
	 * @param listener The listener to add.
	 *
	 * @return A {@code IDisposable} object that can be used to remove the listener later.
	 */
	IDisposeable addValueChangeListener(final IValueChangeListener<ValueT> listener);

	/**
	 * Removes a listener that was previously added using {@link #addValueChangeListener(IValueChangeListener)}.
	 *
	 * @param listener The listener to remove.
	 */
	void removeValueChangeListener(final IValueChangeListener<ValueT> listener);

	/**
	 * Adds a listener to receive notifications when the value of this observable object changes. This method is deprecated and should not be used. Use {@link #addValueChangeListener(IValueChangeListener)} instead.
	 *
	 * @param listener The listener to add.
	 *
	 * @return A {@code IDisposable} object that can be used to remove the listener later.
	 */
	@Deprecated
	default IDisposeable addPropertyChangeListener(final IValueChangeListener<ValueT> listener) {
		return addValueChangeListener(listener);
	}

	/**
	 * Removes a listener that was previously added using {@link #addPropertyChangeListener(IValueChangeListener)}. This method is deprecated and should not be used. Use {@link #removeValueChangeListener(IValueChangeListener)} instead.
	 *
	 * @param listener The listener to remove.
	 */
	@Deprecated
	default void removePropertyChangeListener(final IValueChangeListener<ValueT> listener) {
		removeValueChangeListener(listener);
	}

	/**
	 * Handles value changes of a bunch of observable objects with the same listener.
	 *
	 * @param observables The observable objects to observe.
	 * @param listener The listener to add to each observable object.
	 *
	 * @return A {@code IDisposable} object that can be used to remove the listener from all observable objects later.
	 */
	static IDisposeable observe(final Collection<IObservableObject> observables, final IValueChangeListener listener) {
		final MultiDisposable md = new MultiDisposable();
		for (final IObservableObject obj : observables) {
			md.add(obj.addValueChangeListener(listener));
		}
		return md;
	}


}
