package io.gitlab.scholvac.monitor;

import java.util.Collection;
import java.util.function.Consumer;
import java.util.function.Function;

import io.gitlab.scholvac.monitor.DefaultMonitors.MonitorBlock;
import io.gitlab.scholvac.monitor.DefaultMonitors.NullMonitor;

/**
 * Interface for monitoring and controlling the progress of tasks.
 * <p>
 * This interface provides methods for starting and ending tasks, advancing the progress, logging messages, and handling exceptions.
 * It extends {@link AutoCloseable}, allowing it to be used with try-with-resources statements.
 * </p>
 * <p>
 * The interface includes a nested {@link MonitorBlock} class that implements {@link IProgressMonitor} and is used to create a block of code where the progress is monitored.
 * When the block is exited, the {@link MonitorBlock} instance will automatically call {@link #endTask()} on the parent {@link IProgressMonitor} with a specified close message.
 * </p>
 * <p>
 * The interface provides default methods for convenience, such as {@link #advance()}, {@link #advance(int, String, Object...)}, {@link #advance(String, Object...)},
 * {@link #endTask(String, Object...)}, {@link #endTask()}, and {@link #dispose()}.
 * These default methods simplify the usage of the interface by providing alternative ways to call the existing methods.
 * </p>
 */
public interface IProgressMonitor extends AutoCloseable {
	/**
	 * A constant instance of {@link IProgressMonitor} that does nothing.
	 */
	IProgressMonitor NULL_MONITOR = new NullMonitor();

	/**
	 * Returns a new instance of {@link IProgressMonitor} that does nothing.
	 *
	 * @return a new instance of {@link IProgressMonitor} that does nothing
	 */
	static IProgressMonitor NullMonitor() {
		return new NullMonitor();
	}

	/**
	 * Begins a new task with the specified number of subtasks and message.
	 *
	 * @param subtasks the number of subtasks
	 * @param message the message to display
	 * @param objects optional objects to be formatted into the message
	 */
	void beginTask(final int subtasks, final String message, Object... objects);

	/**
	 * Ends the current task.
	 *
	 * @param warn true if a warning should be logged
	 * @param message the message to display
	 * @param objects optional objects to be formatted into the message
	 */
	void endTask(boolean warn, final String message, Object... objects);

	/**
	 * Advances the progress by the specified number of steps.
	 *
	 * @param steps the number of steps to advance
	 */
	void advance(int steps);

	/**
	 * Returns a new instance of {@link MonitorBlock} that starts a new task block with the specified number of tasks, open message, and close message.
	 *
	 * @param numTasks the number of tasks
	 * @param openMsg the message to display when the block is opened
	 * @param closeMsg the message to display when the block is closed
	 * @return a new instance of {@link MonitorBlock}
	 */
	default IProgressMonitor beginTaskBlock(final int numTasks, final String openMsg, final String closeMsg) {
		beginTask(numTasks, openMsg);
		return new MonitorBlock(this, closeMsg);
	}

	/**
	 * Ends the current task without any warning or message.
	 */
	@Override
	default void close() {
		endTask(false, null);
	}


	default <ValueT> void foreach(final Collection<ValueT> collection, final String initMessage, final Function<ValueT, String> startMessage, final Consumer<ValueT> action, final Function<ValueT, String> endMessage, final String exitMessage) {
		final IProgressMonitor sub = beginTaskBlock(collection.size(), initMessage, exitMessage);
		for (final ValueT value : collection) {
			if (startMessage != null) {
				sub.info(startMessage.apply(value));
			}
			try {
				action.accept(value);
				if (endMessage != null) {
					sub.advance(endMessage.apply(value));
				} else {
					sub.advance();
				}
			}catch(final Exception e) {
				sub.error("Failed to execute action for value {}", value);
			}
		}
	}

	/**
	 * Logs an error message.
	 *
	 * @param msg the message to display
	 * @param objects optional objects to be formatted into the message
	 */
	void error(final String msg, Object... objects);

	/**
	 * Logs a warning message.
	 *
	 * @param msg the message to display
	 * @param objects optional objects to be formatted into the message
	 */
	void warn(final String msg, Object... objects);

	/**
	 * Logs an informational message.
	 *
	 * @param msg the message to display
	 * @param objects optional objects to be formatted into the message
	 */
	void info(final String msg, Object... objects);

	/**
	 * Logs a debug message.
	 *
	 * @param msg the message to display
	 * @param objects optional objects to be formatted into the message
	 */
	void debug(final String msg, Object... objects);

	/**
	 * Logs a trace message.
	 *
	 * @param msg the message to display
	 * @param objects optional objects to be formatted into the message
	 */
	void trace(final String msg, Object... objects);

	/**
	 * Advances the progress by one step.
	 */
	default void advance() {
		advance(1);
	}

	/**
	 * Advances the progress by the specified number of steps and logs an informational message.
	 *
	 * @param steps the number of steps to advance
	 * @param msg the message to display
	 * @param objects optional objects to be formatted into the message
	 */
	default void advance(final int steps, final String msg, final Object... objects) {
		advance(steps);
		info(msg, objects);
	}

	/**
	 * Advances the progress by one step and logs an informational message.
	 *
	 * @param msg the message to display
	 * @param objects optional objects to be formatted into the message
	 */
	default void advance(final String msg, final Object... objects) {
		advance(1, msg, objects);
	}

	/**
	 * Ends the current task with an informational message.
	 *
	 * @param msg the message to display
	 * @param objects optional objects to be formatted into the message
	 */
	default void endTask(final String msg, final Object... objects) {
		endTask(false, msg, objects);
	}

	/**
	 * Ends the current task without any warning or message.
	 */
	default void endTask() {
		endTask(false, null, null);
	}

	/**
	 * Performs any necessary cleanup.
	 */
	default void dispose() {
	}
}
