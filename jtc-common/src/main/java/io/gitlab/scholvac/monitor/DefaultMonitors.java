package io.gitlab.scholvac.monitor;

public class DefaultMonitors {
	/**
	 * Implementation that does nothing
	 * @author ScholvaC
	 */
	public static class NullMonitor implements IProgressMonitor {
		@Override
		public void beginTask( final int actionCount, final String message, final Object...objects) {}
		@Override
		public void endTask(final boolean warn, final String message, final Object... objects) {}
		@Override
		public void advance(final int steps) {}

		@Override
		public void error(final String msg, final Object... objects) {}
		@Override
		public void warn(final String msg, final Object... objects) {}
		@Override
		public void info(final String msg, final Object... objects) {}
		@Override
		public void debug(final String msg, final Object... objects) {}
		@Override
		public void trace(final String msg, final Object... objects) {}
	}

	public static class DelegateMonitor implements IProgressMonitor{
		private final IProgressMonitor mDelegate;
		protected DelegateMonitor(final IProgressMonitor delegate) { mDelegate = delegate;}

		@Override public void beginTask( final int actionCount, final String message, final Object...objects) {mDelegate.beginTask(actionCount, message, objects);}
		@Override public void endTask(final boolean warn, final String message, final Object... objects) {mDelegate.endTask(warn, message, objects);}
		@Override public void advance(final int steps) {mDelegate.advance();}

		@Override public void error(final String msg, final Object... objects) {mDelegate.error(msg, objects);}
		@Override public void warn(final String msg, final Object... objects) {mDelegate.warn(msg, objects);}
		@Override public void info(final String msg, final Object... objects) {mDelegate.info(msg, objects);}
		@Override public void debug(final String msg, final Object... objects) {mDelegate.debug(msg, objects);}
		@Override public void trace(final String msg, final Object... objects) {mDelegate.trace(msg, objects);}
	}

	public static class MonitorBlock extends DelegateMonitor implements AutoCloseable{
		private final String mCloseMsg;

		MonitorBlock(final IProgressMonitor m, final String closeMsg) {
			super(m);
			mCloseMsg =closeMsg;
		}
		@Override
		public void close() {
			endTask(mCloseMsg);
		}
	}
}
