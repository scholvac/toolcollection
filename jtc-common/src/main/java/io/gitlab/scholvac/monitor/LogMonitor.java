package io.gitlab.scholvac.monitor;

import io.gitlab.scholvac.log.ILogger;

public class LogMonitor extends AbstractProgressMonitor implements IProgressMonitor {

	private final ILogger		mLogger;
	private String				mIndentation = "";

	public LogMonitor(final ILogger logger) {
		mLogger = logger;
	}

	@Override
	protected void onTaskAdded(final Task task) {
		mIndentation += "\t";
	}
	@Override
	protected void onTaskRemoved(final Task task) {
		mIndentation = mIndentation.substring(1);
	}

	@Override
	public void error(final String msg, final Object... objects) {
		mLogger.error(mIndentation + msg, objects);
	}

	@Override
	public void warn(final String msg, final Object... objects) {
		mLogger.warn(mIndentation + msg, objects);
	}

	@Override
	public void info(final String msg, final Object... objects) {
		mLogger.info(mIndentation + msg, objects);
	}

	@Override
	public void debug(final String msg, final Object... objects) {
		mLogger.debug(mIndentation + msg, objects);
	}

	@Override
	public void trace(final String msg, final Object... objects) {
		mLogger.trace(mIndentation + msg, objects);
	}

}
