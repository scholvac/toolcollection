package io.gitlab.scholvac.monitor;

import java.util.Stack;

import io.gitlab.scholvac.log.SLog;

public abstract class AbstractProgressMonitor implements IProgressMonitor {

	public class Task {
		final int		subtasks;
		int				doneTasks;
		boolean 		warned;

		public Task(final int st) {
			subtasks = st; doneTasks = 0;
		}
		public void advance(final int steps) {
			doneTasks += steps;
			if (doneTasks > subtasks && !warned) {
				monitorWarning("number of subtasks exceeded");
				warned = true;
			}
		}
		public int getSubtaskCount() {
			return subtasks;
		}
		public int getProgress() {
			if (doneTasks < 0) return 0;
			if (doneTasks > subtasks) return subtasks;
			return doneTasks;
		}

	}

	private Stack<Task>		mTaskStack = new Stack<>();

	protected AbstractProgressMonitor() {
		final Task rootTask = new Task(-1);
		rootTask.warned = true;
		mTaskStack.add(rootTask);
	}

	@Override
	public void beginTask(final int subtasks, final String message, final Object... objects) {
		info(message, objects);
		final Task t = new Task(subtasks);
		mTaskStack.push(t);
		onTaskAdded(t);
	}

	protected void onTaskAdded(final Task task) {
		//To be overwritten by subclasses
	}

	@Override
	public void endTask(final boolean warn, final String message, final Object... objects) {
		if (mTaskStack.size() == 1) {
			if (warn) {
				monitorWarning("Try to end the root task");
			}
		}else {
			final Task t = mTaskStack.pop();
			if (t.doneTasks > t.subtasks && warn) {
				monitorWarning("current tasks " + t + " actions did not match the expected number. Got: " + t.doneTasks + " remaining tasks");
			}
			onTaskRemoved(t);
		}
		if (message != null)
			info(message, objects);
	}

	protected void onTaskRemoved(final Task task) {
		//to be overwritten by subclasses
	}

	protected void monitorWarning(final String msg) {
		//to be overwritten by subclasses
		SLog.warn(msg);
	}

	protected Task getCurrentTask() {
		return mTaskStack.peek();
	}

	@Override
	public void advance(final int steps) {
		final Task task = getCurrentTask();
		task.advance(steps);
		onAdvance(task);
	}

	protected void onAdvance(final Task task) {
		//to be overwritten by subclasses
	}

}
