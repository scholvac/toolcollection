package io.gitlab.scholvac.log;

import org.slf4j.Logger;
import org.slf4j.event.Level;

public interface ILogger extends Logger {

	default boolean isEnabled(final Level level) {
		switch (level) {
			case ERROR:
				return isErrorEnabled();
			case WARN:
				return isWarnEnabled();
			case INFO:
				return isInfoEnabled();
			case DEBUG:
				return isDebugEnabled();
			case TRACE:
				return isTraceEnabled();
		}
		return false;
	}

	default void log(final Level level, final String msg) {
		if (!isEnabled(level))
			return ;
		switch (level) {
			case ERROR:
				error(msg);
				return;
			case WARN:
				warn(msg);
				return;
			case INFO:
				info(msg);
				return;
			case DEBUG:
				debug(msg);
				return;
			case TRACE:
				trace(msg);
		}
	}

	default void log(final Level level, final String format, final Object arg) {
		if (!isEnabled(level))
			return ;
		switch (level) {
			case ERROR:
				error(format, arg);
				return;
			case WARN:
				warn(format, arg);
				return;
			case INFO:
				info(format, arg);
				return;
			case DEBUG:
				debug(format, arg);
				return;
			case TRACE:
				trace(format, arg);
		}
	}

	default void log(final Level level, final String format, final Object arg1, final Object arg2) {
		if (!isEnabled(level))
			return ;
		switch (level) {
			case ERROR:
				error(format, arg1, arg2);
				return;
			case WARN:
				warn(format, arg1, arg2);
				return;
			case INFO:
				info(format, arg1, arg2);
				return;
			case DEBUG:
				debug(format, arg1, arg2);
				return;
			case TRACE:
				trace(format, arg1, arg2);
		}
	}

	default void log(final Level level, final String format, final Object... arguments) {
		if (!isEnabled(level))
			return ;
		switch (level) {
			case ERROR:
				error(format, arguments);
				return;
			case WARN:
				warn(format, arguments);
				return;
			case INFO:
				info(format, arguments);
				return;
			case DEBUG:
				debug(format, arguments);
				return;
			case TRACE:
				trace(format, arguments);
		}
	}

	default void log(final Level level, final String msg, final Throwable t) {
		if (!isEnabled(level))
			return ;
		switch (level) {
			case ERROR:
				error(msg, t);
				return;
			case WARN:
				warn(msg, t);
				return;
			case INFO:
				info(msg, t);
				return;
			case DEBUG:
				debug(msg, t);
				return;
			case TRACE:
				trace(msg, t);
		}
	}

}
