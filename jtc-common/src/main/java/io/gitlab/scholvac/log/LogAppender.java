package io.gitlab.scholvac.log;

import java.lang.reflect.Method;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import io.gitlab.scholvac.ReflectionUtils;

public class LogAppender {


	public enum LogLevel {
		FATAL, ERROR, WARN, INFO, DEBUG, TRACE
	}
	public static class LogEvent {
		public final String		message;
		public final LogLevel	level;
		public final String		threadName;
		public final String		loggerName;

		public LogEvent(final String message, final String level, final String threadName, final String loggerName) {
			this.message = message;
			this.level = LogLevel.valueOf(level.toUpperCase());
			this.threadName = threadName;
			this.loggerName = loggerName;
		}
	}

	public interface ILogAppender {
		void doAppend(final LogEvent event);
	}


	static class AbstractLogHandler {
		private List<ILogAppender> 				mAppender = new LinkedList<>();

		public void addAppender(final ILogAppender appender) {
			if (appender == null || mAppender.contains(appender))
				return ;
			mAppender.add(appender);
		}
		public void removeAppender(final ILogAppender appender) {
			mAppender.remove(appender);
		}

		protected void handleDoAppend(final LogEvent evt) {
			mAppender.forEach(app -> app.doAppend(evt));
		}
	}
	private static class Log4JAppender extends AbstractLogHandler implements java.lang.reflect.InvocationHandler {
		private static final Method sGetLevel;
		private static final Method sGetMessage;
		private static final Method sGetLoggerName;
		private static final Method sGetThreadName;
		static {
			Method l = null, m = null, lo = null, th = null;
			try {
				final Class<?> clazz = Thread.currentThread().getContextClassLoader().loadClass("org.apache.logging.log4j.spi.LoggingEvent");
				l = clazz.getDeclaredMethod("getLevel", null);
				m = clazz.getMethod("getMessage", null);
				lo = clazz.getMethod("getLoggerName", null);
				th = clazz.getMethod("getThreadName", null);
			} catch (ClassNotFoundException | NoSuchMethodException | SecurityException e) {
				e.printStackTrace();
			}
			sGetLevel = l;
			sGetMessage = m;
			sGetLoggerName = lo;
			sGetThreadName = th;
		}

		@Override
		public Object invoke(final Object proxy, final Method method, final Object[] args) throws Throwable {
			final String n = method.getName();
			if ("equals".equals(n))
				return super.equals(args[0]);
			if ("doAppend".equals(n)) {
				final Object evt = args[0];
				final Object l = sGetLevel.invoke(evt, null);
				final Object m = sGetMessage.invoke(evt, null);
				final Object lo= sGetLoggerName.invoke(evt, null);
				final Object th= sGetThreadName.invoke(evt, null);
				handleDoAppend(new LogEvent(m.toString(), l.toString(), th.toString(), lo.toString()));
			}
			return null;
		}
	}

	private static class Log4J2Appender extends AbstractLogHandler implements java.lang.reflect.InvocationHandler {
		private static final Method sGetLevel;
		private static final Method sGetMessage;
		private static final Method sGetLoggerName;
		private static final Method sGetThreadName;
		private static final Method sGetMessageFormatedMessage;
		static {
			Method l = null, m = null, lo = null, th = null, mm = null;
			try {
				final Class<?> clazz = Thread.currentThread().getContextClassLoader().loadClass("org.apache.logging.log4j.core.LogEvent");
				l = clazz.getDeclaredMethod("getLevel", null);
				m = clazz.getMethod("getMessage", null);
				lo = clazz.getMethod("getLoggerName", null);
				th = clazz.getMethod("getThreadName", null);
				final Class<?> msgClazz = Thread.currentThread().getContextClassLoader().loadClass("org.apache.logging.log4j.message.Message");
				mm = msgClazz.getMethod("getFormattedMessage", null);
			} catch (ClassNotFoundException | NoSuchMethodException | SecurityException e) {
				e.printStackTrace();
			}
			sGetLevel = l;
			sGetMessage = m;
			sGetLoggerName = lo;
			sGetThreadName = th;
			sGetMessageFormatedMessage = mm;
		}

		@Override
		public Object invoke(final Object proxy, final Method method, final Object[] args) throws Throwable {
			final String n = method.getName();
			if ("equals".equals(n))
				return super.equals(args[0]);
			if ("isStarted".equals(n))
				return true;
			if ("getName".equals(n))
				return "MyReflectionAppender";
			if ("append".equals(n)) {
				final Object evt = args[0];
				final Object l = sGetLevel.invoke(evt, null);
				final Object msg = sGetMessage.invoke(evt, null);
				final Object msgStr = sGetMessageFormatedMessage.invoke(msg, null);
				final Object lo= sGetLoggerName.invoke(evt, null);
				final Object th= sGetThreadName.invoke(evt, null);
				handleDoAppend(new LogEvent(msgStr.toString(), l.toString(), th.toString(), lo.toString()));
			}
			return null;
		}

	}

	private static AbstractLogHandler		sLogHandler = null;
	private static boolean 					sInitialized = false;

	protected static AbstractLogHandler getLogHandler() {
		if (!sInitialized) {
			//first try Log4JHandler
			sLogHandler = initLog4J();
			if (sLogHandler == null) {
				sLogHandler = initLog4J2();
			}
			sInitialized = true;
		}
		return sLogHandler;
	}
	public static boolean append(final ILogAppender appender) {
		final AbstractLogHandler alh = getLogHandler();
		if (alh == null)
			return false;
		alh.addAppender(appender);
		return true;
	}
	public static void remove(final ILogAppender appender) {
		final AbstractLogHandler alh = getLogHandler();
		if (alh != null)
			alh.removeAppender(appender);
	}

	private static AbstractLogHandler initLog4J() {
		if (hasLog4JImplementation() == false)
			return null;
		try {
			final Object rl = ReflectionUtils.callStaticMethod("org.apache.logging.log4j.LogManager", "getRootLogger", null);
			if (rl == null)
				return null;
			final Log4JAppender app = new Log4JAppender();
			final Class<?> appenderClazz = Thread.currentThread().getContextClassLoader().loadClass("org.apache.logging.log4j.core.Appender");
			final Object appender = java.lang.reflect.Proxy.newProxyInstance(
					LogAppender.class.getClassLoader(),
					new java.lang.Class[] {appenderClazz},
					app );
			ReflectionUtils.callMethod(rl, "addAppender", appender);
			return app;
		}catch(final Throwable t) {
			t.printStackTrace();
		}
		return null;
	}

	private static boolean hasLog4JImplementation() {
		try {
			final Class<?> clazz = Thread.currentThread().getContextClassLoader().loadClass("org.apache.logging.log4j.spi.LoggingEvent");
			return clazz != null;
		}catch(final Throwable t) { }
		return false;
	}

	private static AbstractLogHandler initLog4J2() {
		if (false == hasLog4J2Implementation())
			return null;
		//get the root loggerConfig
		try {
			final Object loggerContext = ReflectionUtils.callMethod(ReflectionUtils.getClassByName("org.apache.logging.log4j.LogManager"), null, Collections.singleton("getContext"), new Object[] {false}, new Class[] {boolean.class}); //need to call the full method, since Boolean.isAssignable(boolean) == false?

			final Object configuration = ReflectionUtils.callMethodWithReturn(loggerContext, "getConfiguration", null);
			final Object rootLoggerConfig = ReflectionUtils.callMethodWithReturn(configuration, "getLoggerConfig", ""); //"" is the name of the root logger

			final Log4J2Appender appenderProxy = new Log4J2Appender();
			final Class<?> appenderClazz = ReflectionUtils.getClassByName("org.apache.logging.log4j.core.Appender");

			final Object appender = java.lang.reflect.Proxy.newProxyInstance(
					LogAppender.class.getClassLoader(),
					new java.lang.Class[] {appenderClazz},
					appenderProxy );


			final Class<?> levelClazz = ReflectionUtils.getClassByName("org.apache.logging.log4j.Level");
			final Class<?> filterClazz = ReflectionUtils.getClassByName("org.apache.logging.log4j.core.Filter");
			final Method addAppenderMethod = rootLoggerConfig.getClass().getMethod("addAppender", appenderClazz, levelClazz, filterClazz);
			addAppenderMethod.invoke(rootLoggerConfig,  appender, null, null);

			ReflectionUtils.callMethod(loggerContext, "updateLoggers", null);

			SLog.info("Fara");
			return appenderProxy;
		}catch(final Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private static boolean hasLog4J2Implementation() {
		try {
			final Class<?> clazz = Thread.currentThread().getContextClassLoader().loadClass("org.apache.logging.log4j.core.LogEvent");
			return clazz != null;
		}catch(final Throwable t) { }
		return false;
	}
}
