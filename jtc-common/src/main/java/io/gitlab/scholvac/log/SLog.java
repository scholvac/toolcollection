package io.gitlab.scholvac.log;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.gitlab.scholvac.log.LogAppender.ILogAppender;
import io.gitlab.scholvac.log.LogAppender.LogLevel;
import io.gitlab.scholvac.resources.ResourceManager;

/**
 *
 * There are two ways of logging within SLog.
 * 1) Using the SLog's static methods for easy logging, without need to create a logger for each class. Thereby the GVLog automatically detects the calling class
 * 2) Use the SLog method to create a static logger for a specific classifier.
 *
 * The second method should be used if heavily logging is required and performance becomes an issue, since the static methods of GVLog do need to detect the Loggers name
 * at every method invocation.
 *
 * @author scholvac
 *
 */
public class SLog {

	public interface SLogFactory {
		void initialize(final URL url);
		ILogger createLogger(final String qualifiedName);
	}

	private static SLog theInstance = new SLog();
	private SLogFactory mLoggerFactory = new SLogFactory() {

		@Override
		public void initialize(final URL url) {
		}

		@Override
		public ILogger createLogger(final String qualifiedName) {
			return DelegateLogger.get(LoggerFactory.getLogger(qualifiedName));
		}
	};


	public static SLog getInstance() {
		return theInstance;
	}

	private boolean mInitialized = false;

	private final int		mIntendation = 0;
	private final String	mIntendationStr = "";

	private SLog(){
	}



	public boolean initialize(final File logFile){
		URL url = null;
		if (logFile != null && logFile.exists()){
			try {
				url = logFile.toURI().toURL();
			} catch (final MalformedURLException e) {
				e.printStackTrace();
				url = null;
			}
		}
		return initialize(url) != null;
	}

	public SLog initialize(final URL url) {
		mInitialized = true;
		if (url != null) {
			System.setProperty("log4j.configuration", url.toString());
			System.setProperty("log4j2.configurationFile", url.toString());
		}else {
			initialize();
		}
		if (mLoggerFactory != null)
			mLoggerFactory.initialize(url);
		return this;
	}


	public static String getCallerClassName() {
		//using reflections should be quite fast, but unfortunately oracle removed (not only deprecated!) this method
		//return sun.reflect.Reflection.getCallerClass(callStackDepth).getName();
		final StackTraceElement[] st = Thread.currentThread().getStackTrace();
		return st[3].getClassName();
	}

	public static int getIntendation() {
		if (theInstance == null) return -1;
		return theInstance.mIntendation;//mFormatter.getIntendation();
	}
	public static void setIntendation(final int intendation){


	}



	public static void errorInc(final int intendationDelta, final String message){
		if (theInstance != null) addIndentation(intendationDelta);
		error(message);
	}
	public static void errorDec(final int intendationDelta, final String message){
		if (theInstance != null) addIndentation(-intendationDelta);
		error(message);
	}
	public static void error(final String message, final Object...objects){
		try{
			final Logger l = getLogger(getCallerClassName());
			if (l != null && l.isErrorEnabled())
				l.error(String.format(message, objects));
		}catch(final Exception e){ System.out.println("Failed to Log message: " + message); }
	}
	public static void error(final Throwable t, final String message, final Object...objects){
		try{
			final Logger l = getLogger(getCallerClassName());
			if (l != null && l.isErrorEnabled())
				l.error(String.format(message, objects), t);
		}catch(final Exception e){ System.out.println("Failed to Log message: " + message); }
	}

	public static void error(final String message)
	{
		try{
			final Logger l = getLogger(getCallerClassName());
			if (l != null && l.isErrorEnabled())
				l.error(message);
		}catch(final Exception e){ System.out.println("Failed to Log message: " + message); }
	}


	public static void warn(final int intendationDelta, final String message){
		if (theInstance != null) addIndentation(intendationDelta);
		warn(message);
	}
	public static void warn(final String message, final int intendationDelta){
		warn(message);
		if (theInstance != null) addIndentation(intendationDelta);
	}
	public static void warn(final String message)
	{
		try{
			final Logger l = getLogger(getCallerClassName());
			if (l != null & l.isWarnEnabled())
				l.warn(message);
		}catch(final Exception e){ System.out.println("Failed to Log message: " + message); }
	}

	public static void info(final int intendationDelta, final String message){
		if (theInstance != null) addIndentation(intendationDelta);
		info(message);
	}
	public static void info(final String message, final int intendationDelta){
		info(message);
		if (theInstance != null) addIndentation(intendationDelta);
	}
	public static void info(final String message)
	{
		try{
			final Logger l = getLogger(getCallerClassName());
			if (l != null && l.isInfoEnabled())
				l.info(message);
		}catch(final Exception e){System.out.println("Failed to Log message: " + message); }
	}

	public static void debugInc(final int intendationDelta, final String message, final Object...objects){
		if (theInstance != null) addIndentation(intendationDelta);
		debug(message, objects);
	}
	public static void debugDec(final int intendationDelta, final String message, final Object...objects){
		debug(message, objects);
		if (theInstance != null) addIndentation(intendationDelta);
	}

	public static void debug(final String message, final Object...objects)
	{
		try{
			final Logger l = getLogger(getCallerClassName());
			if (l != null && l.isDebugEnabled())
				l.debug(message, objects);
		}catch(final Exception e){ System.out.println("Failed to Log message: " + message); }
	}


	public static void trace(final int intendationDelta, final String message){
		if (theInstance != null) addIndentation(intendationDelta);
		trace(message);
	}
	public static void trace(final String message, final int intendationDelta){
		trace(message);
		if (theInstance != null) addIndentation(intendationDelta);
	}
	public static void addIndentation(final int intendationDelta) {
		setIntendation(getIntendation() + intendationDelta);
	}

	public static void trace(final String message, final Object...objects)
	{
		try{
			final Logger l = getLogger(getCallerClassName());
			if (l != null && l.isTraceEnabled())
				l.trace(message, objects);
		}catch(final Exception e){ System.out.println("Failed to Log message: " + message); }
	}

	/**
	 * get or create a logger with a qualified name
	 * @note if the GVLog has not been initialized this method will initialize the currently used GVLog instance with default values
	 * @param fqcn
	 * @return logger
	 */
	public static ILogger getLogger(final String fqcn) {
		try{
			if (theInstance != null && !theInstance.mInitialized)
				theInstance.initialize();
			if (theInstance != null && theInstance.mLoggerFactory != null)
				return theInstance.mLoggerFactory.createLogger(fqcn);
			return DelegateLogger.get(LoggerFactory.getLogger(fqcn));
		}catch(Exception | Error err){
			err.printStackTrace();
			return null;
		}
	}

	public static ILogger getLogger(final Class<?> clazz) {
		return getLogger(clazz.getName());
	}


	public void initialize() {
		if (mInitialized)
			return ;
		mInitialized = true;
		//this method is called as automatic init, if no manual initialisation has been done before
		//we try to use a standard configuration file, e.g. <ProjectDir>/log/log4j.xml or use the default configuration
		URL url = ResourceManager.get().getResource("log/log4j2.xml");
		if (url == null)
			url = ResourceManager.get().getResource("log4j2.xml");
		if (url == null)
			url = ResourceManager.get().getResource("log4j2.properties");

		//if none of the above files has been found, we use our own default config
		if (url == null)
			url = ResourceManager.get().getResource("log4j2_base_config.xml");


		initialize(url);
	}



	/**
	 * Allows to register a custumized GVLogFactory to create a new Logger
	 * @warn this method should not be used unless you are sure that you need it, also it will be applied system wide, e.g. changes the logging of the whole system.
	 * @param fac
	 */
	public void registerLogFactory(final SLogFactory fac){
		mLoggerFactory = fac;
	}


	public static boolean addLogAppender(final ILogAppender appender) {
		return LogAppender.append(appender);
	}
	public static void removeLogAppender(final ILogAppender appender) {
		LogAppender.remove(appender);
	}


	public static void setLogLevel(final Class<?> class1, final LogLevel level) {
		setLogLevel(class1.getName(), level);
	}

	public static void setLogLevel(final String fqnc, final LogLevel level) {
		final Logger l = LoggerFactory.getLogger(fqnc);

	}


}