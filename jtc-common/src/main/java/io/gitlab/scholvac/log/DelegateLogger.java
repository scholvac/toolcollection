package io.gitlab.scholvac.log;

import org.slf4j.Logger;
import org.slf4j.Marker;

public class DelegateLogger implements ILogger {


	public static ILogger get(final Logger logger) {
		return new DelegateLogger(logger);
	}

	private final Logger mDelegate;

	private DelegateLogger(final Logger l) {
		mDelegate = l;
	}

	@Override
	public String getName() { return mDelegate.getName();}


	@Override public boolean isTraceEnabled() { return mDelegate.isTraceEnabled();}
	@Override public void trace(final String msg) { mDelegate.trace(msg); }
	@Override public void trace(final String format, final Object arg) { mDelegate.trace(format, arg);}
	@Override public void trace(final String format, final Object arg1, final Object arg2) { mDelegate.trace(format, arg1, arg2);}
	@Override public void trace(final String format, final Object... arguments) { mDelegate.trace(format, arguments); }
	@Override public void trace(final String msg, final Throwable t) { mDelegate.trace(msg, t);}

	@Override public boolean isTraceEnabled(final Marker marker) { return mDelegate.isTraceEnabled(marker); }
	@Override public void trace(final Marker marker, final String msg) { mDelegate.trace(marker, msg);}
	@Override public void trace(final Marker marker, final String format, final Object arg) {mDelegate.trace(marker, format, arg);}
	@Override public void trace(final Marker marker, final String format, final Object arg1, final Object arg2) { mDelegate.trace(marker, format, arg1, arg2);}
	@Override public void trace(final Marker marker, final String format, final Object... argArray) { mDelegate.trace(marker, format, argArray);}
	@Override public void trace(final Marker marker, final String msg, final Throwable t) { mDelegate.trace(marker, msg, t); }


	@Override public boolean isDebugEnabled() { return mDelegate.isDebugEnabled();}
	@Override public void debug(final String msg) { mDelegate.debug(msg); }
	@Override public void debug(final String format, final Object arg) { mDelegate.debug(format, arg);}
	@Override public void debug(final String format, final Object arg1, final Object arg2) { mDelegate.debug(format, arg1, arg2);}
	@Override public void debug(final String format, final Object... arguments) { mDelegate.debug(format, arguments); }
	@Override public void debug(final String msg, final Throwable t) { mDelegate.debug(msg, t);}

	@Override public boolean isDebugEnabled(final Marker marker) { return mDelegate.isDebugEnabled(marker); }
	@Override public void debug(final Marker marker, final String msg) { mDelegate.debug(marker, msg);}
	@Override public void debug(final Marker marker, final String format, final Object arg) {mDelegate.debug(marker, format, arg);}
	@Override public void debug(final Marker marker, final String format, final Object arg1, final Object arg2) { mDelegate.debug(marker, format, arg1, arg2);}
	@Override public void debug(final Marker marker, final String format, final Object... argArray) { mDelegate.debug(marker, format, argArray);}
	@Override public void debug(final Marker marker, final String msg, final Throwable t) { mDelegate.debug(marker, msg, t); }


	@Override public boolean isInfoEnabled() { return mDelegate.isInfoEnabled();}
	@Override public void info(final String msg) { mDelegate.info(msg); }
	@Override public void info(final String format, final Object arg) { mDelegate.info(format, arg);}
	@Override public void info(final String format, final Object arg1, final Object arg2) { mDelegate.info(format, arg1, arg2);}
	@Override public void info(final String format, final Object... arguments) { mDelegate.info(format, arguments); }
	@Override public void info(final String msg, final Throwable t) { mDelegate.info(msg, t);}

	@Override public boolean isInfoEnabled(final Marker marker) { return mDelegate.isInfoEnabled(marker); }
	@Override public void info(final Marker marker, final String msg) { mDelegate.info(marker, msg);}
	@Override public void info(final Marker marker, final String format, final Object arg) {mDelegate.info(marker, format, arg);}
	@Override public void info(final Marker marker, final String format, final Object arg1, final Object arg2) { mDelegate.info(marker, format, arg1, arg2);}
	@Override public void info(final Marker marker, final String format, final Object... argArray) { mDelegate.info(marker, format, argArray);}
	@Override public void info(final Marker marker, final String msg, final Throwable t) { mDelegate.info(marker, msg, t); }


	@Override public boolean isWarnEnabled() { return mDelegate.isWarnEnabled();}
	@Override public void warn(final String msg) { mDelegate.warn(msg); }
	@Override public void warn(final String format, final Object arg) { mDelegate.warn(format, arg);}
	@Override public void warn(final String format, final Object arg1, final Object arg2) { mDelegate.warn(format, arg1, arg2);}
	@Override public void warn(final String format, final Object... arguments) { mDelegate.warn(format, arguments); }
	@Override public void warn(final String msg, final Throwable t) { mDelegate.warn(msg, t);}

	@Override public boolean isWarnEnabled(final Marker marker) { return mDelegate.isWarnEnabled(marker); }
	@Override public void warn(final Marker marker, final String msg) { mDelegate.warn(marker, msg);}
	@Override public void warn(final Marker marker, final String format, final Object arg) {mDelegate.warn(marker, format, arg);}
	@Override public void warn(final Marker marker, final String format, final Object arg1, final Object arg2) { mDelegate.warn(marker, format, arg1, arg2);}
	@Override public void warn(final Marker marker, final String format, final Object... argArray) { mDelegate.warn(marker, format, argArray);}
	@Override public void warn(final Marker marker, final String msg, final Throwable t) { mDelegate.warn(marker, msg, t); }


	@Override public boolean isErrorEnabled() { return mDelegate.isErrorEnabled();}
	@Override public void error(final String msg) { mDelegate.error(msg); }
	@Override public void error(final String format, final Object arg) { mDelegate.error(format, arg);}
	@Override public void error(final String format, final Object arg1, final Object arg2) { mDelegate.error(format, arg1, arg2);}
	@Override public void error(final String format, final Object... arguments) { mDelegate.error(format, arguments); }
	@Override public void error(final String msg, final Throwable t) { mDelegate.error(msg, t);}

	@Override public boolean isErrorEnabled(final Marker marker) { return mDelegate.isErrorEnabled(marker); }
	@Override public void error(final Marker marker, final String msg) { mDelegate.error(marker, msg);}
	@Override public void error(final Marker marker, final String format, final Object arg) {mDelegate.error(marker, format, arg);}
	@Override public void error(final Marker marker, final String format, final Object arg1, final Object arg2) { mDelegate.error(marker, format, arg1, arg2);}
	@Override public void error(final Marker marker, final String format, final Object... argArray) { mDelegate.error(marker, format, argArray);}
	@Override public void error(final Marker marker, final String msg, final Throwable t) { mDelegate.error(marker, msg, t); }
}
