package io.gitlab.scholvac.exec;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import io.gitlab.scholvac.CommonUtils;

public class ExecutorUtils {

	public static final int 							UI_SCHEDULER_THREAD_COUNT 	= 2;

	private static final boolean NO_SCHEDULER_SERVICE = false;

	private static ScheduledExecutorService 			sUIScheduler 	= null;
	private static Map<String, ExecutorService> 		sNamedExecutors = new HashMap<>();

	/** Provides a system wide ui scheduler, that shall be reused by as many UI elements as possible, instead of creating a new execution service */
	public static synchronized ScheduledExecutorService getUIScheduler() {
		if (sUIScheduler == null)
			sUIScheduler = Executors.newScheduledThreadPool(UI_SCHEDULER_THREAD_COUNT, CommonUtils.createThreadFactory("UIScheduler", true));
		return sUIScheduler;
	}

	public static ExecutorService getOrCreate(final String name, final int maxThreads) {
		return getOrCreate(name, maxThreads, true);
	}
	public static ExecutorService getOrCreate(final String name, final int maxThreads, final boolean deamon) {
		return sNamedExecutors.computeIfAbsent(name+"_"+maxThreads+"_"+deamon, n -> createExecutorService(name, true, maxThreads, deamon));
	}

	public static ExecutorService createExecutorService(final String name, final boolean async, final int threadCount, final boolean daemon) {
		if (!async)
			return new DirectExecutorService(name);
		if (threadCount == 1)
			return Executors.newSingleThreadExecutor(CommonUtils.createThreadFactory(name, daemon));
		return Executors.newFixedThreadPool(threadCount, CommonUtils.createThreadFactory(name, daemon));
	}




}
