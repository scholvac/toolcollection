package io.gitlab.scholvac.prop.io;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import io.gitlab.scholvac.prop.IPropertyContext;
import io.gitlab.scholvac.prop.PropertyDescriptor;
import io.gitlab.scholvac.prop.impl.PropertyContext;
import io.gitlab.scholvac.prop.io.IPropertyFileFormat;

public class GenericSerializationTestUtil {

	public static void test_write_and_read_primitives(final IPropertyFileFormat io) throws IOException {
		final IPropertyContext writeCtx = new PropertyContext();
		writeCtx.setFQN(":rootProp", 1.4f);
		writeCtx.setFQN(":ctx1:byteProp", (byte)18);
		writeCtx.setFQN(":ctx1:shortProp", (short)45);
		writeCtx.setFQN(":ctx1:intProp", 4);
		writeCtx.setFQN(":ctx1:longProp", 42l);
		writeCtx.setFQN(":ctx1:floatProp", 38f);
		writeCtx.setFQN(":ctx1:doubleProp", 34.);
		writeCtx.setFQN(":ctx1:stringProp", "Hallo Property File");

		writeCtx.setFQN("ctx1:ctx2:boolean:p1", true);
		writeCtx.setFQN("ctx1:ctx2:boolean:p2", false);

		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		io.write(writeCtx, baos);

		//since we do not know anything about the format, we can just check that something has been saved at all...
		assertTrue(baos.size() > 0);

		final IPropertyContext loadedCtx = io.load(new ByteArrayInputStream(baos.toByteArray()));
		assertNotNull(loadedCtx);

		assertTrue(loadedCtx.hasProperty("rootProp"));
		assertTrue(loadedCtx.hasProperty(PropertyDescriptor.createFQN(":rootProp")));
		assertTrue(loadedCtx.hasPropertyFQN(":rootProp"));

		assertEquals(Float.class, loadedCtx.getPropertyFQN(":rootProp").getType());
		assertEquals(1.4f, loadedCtx.get("rootProp", 93.f));


		assertEquals(Byte.class, loadedCtx.getPropertyFQN("ctx1:byteProp").getType());
		assertEquals((byte)18, loadedCtx.getFQN("ctx1:byteProp", (byte)93));

		assertEquals(Short.class, loadedCtx.getPropertyFQN("ctx1:shortProp").getType());
		assertEquals((short)45, loadedCtx.getFQN("ctx1:shortProp", (short)93));

		assertEquals(Integer.class, loadedCtx.getPropertyFQN("ctx1:intProp").getType());
		assertEquals(4, loadedCtx.getFQN("ctx1:intProp", 93));

		assertEquals(Long.class, loadedCtx.getPropertyFQN("ctx1:longProp").getType());
		assertEquals(42, loadedCtx.getFQN("ctx1:longProp", 93l));

		assertEquals(Float.class, loadedCtx.getPropertyFQN("ctx1:floatProp").getType());
		assertEquals(38.f, loadedCtx.getFQN("ctx1:floatProp", 93f));

		assertEquals(Double.class, loadedCtx.getPropertyFQN("ctx1:doubleProp").getType());
		assertEquals(34., loadedCtx.getFQN("ctx1:doubleProp", 93.));

		assertEquals(String.class, loadedCtx.getPropertyFQN("ctx1:stringProp").getType());
		assertEquals("Hallo Property File", loadedCtx.getFQN("ctx1:stringProp", ""));

		assertEquals(Boolean.class, loadedCtx.getPropertyFQN("ctx1:ctx2:boolean:p1").getType());
		assertEquals(true, loadedCtx.getFQN("ctx1:ctx2:boolean:p1", false));

		assertEquals(Boolean.class, loadedCtx.getPropertyFQN("ctx1:ctx2:boolean:p2").getType());
		assertEquals(false, loadedCtx.getFQN("ctx1:ctx2:boolean:p2", true));
	}
}
