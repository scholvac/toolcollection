package io.gitlab.scholvac.prop.io;

import java.io.IOException;

import org.junit.jupiter.api.Test;

import io.gitlab.scholvac.prop.io.impl.SerializeableFormat;

public class PropertyIO_Serializeable_Tests {


	@Test
	public  void test_write_and_read_primitives() throws IOException {
		GenericSerializationTestUtil.test_write_and_read_primitives(new SerializeableFormat());
	}

}
