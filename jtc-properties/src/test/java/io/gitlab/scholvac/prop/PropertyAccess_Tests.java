package io.gitlab.scholvac.prop;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import io.gitlab.scholvac.QualifiedName;
import io.gitlab.scholvac.prop.impl.PropertyContext;

public class PropertyAccess_Tests {

	@Test
	public void testAccess_Methods() {
		final PropertyContext ctx = new PropertyContext(); // no exception
		assertEquals("", ctx.getName());
		assertEquals(QualifiedName.from(""), ctx.getQualifiedName());

		final IProperty<Integer> p1 = ctx.getProperty("MyProp", 3);
		assertNotNull(p1);

		final IProperty<Integer> p2 = ctx.getProperty(PropertyDescriptor.create("MyProp", 33));
		assertNotNull(p2);
		assertTrue(p1 == p2);

		final IProperty<Integer> p3 = ctx.getPropertyFQN(":MyProp");
		assertNotNull(p3);
		assertTrue(p1 == p3);

		final IProperty<Integer> p4 = ctx.getProperty(PropertyDescriptor.createFQN(":MyProp"));
		assertNotNull(p4);
		assertTrue(p1 == p4);

		final IProperty<Integer> p5 = ctx.getProperty(":MyProp"); //missing FQN postfix, thus is another property
		assertNotNull(p5);
		assertTrue(p1 != p5);

	}


}
