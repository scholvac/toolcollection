package io.gitlab.scholvac.prop;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class BasicProperty_Tests {

	@Test
	void test_create() {
		IProperty<Integer> prop = IProperty.of(42);
		assertNotNull(prop);
		assertNull(prop.getName());
		assertNull(prop.getDescription());
		assertTrue(prop.isEditable());
		assertTrue(prop.getAnnotations().isEmpty());
		assertEquals(42, prop.get());

		prop = IProperty.of("Prop", 52);
		assertNotNull(prop);
		assertEquals("Prop", prop.getName());
		assertNull(prop.getDescription());
		assertTrue(prop.isEditable());
		assertTrue(prop.getAnnotations().isEmpty());
		assertEquals(52, prop.get());

		prop = IProperty.of("Prop2", false, 62);
		assertNotNull(prop);
		assertEquals("Prop2", prop.getName());
		assertNull(prop.getDescription());
		assertFalse(prop.isEditable());
		assertTrue(prop.getAnnotations().isEmpty());
		assertEquals(62, prop.get());

		prop = IProperty.ofType(Integer.class);
		assertNotNull(prop);
		assertNull(prop.getName());
		assertNull(prop.getDescription());
		assertTrue(prop.isEditable());
		assertTrue(prop.getAnnotations().isEmpty());
		assertEquals(null, prop.get());
	}

	@Test
	void test_editable() {
		final IProperty<Integer> prop = IProperty.of(42);
		assertEquals(42, prop.get());

		prop.set(52);
		assertEquals(52, prop.get());
	}

	@Test
	void test_not_editable() {
		final IProperty<Integer> prop = IProperty.of(42, false);
		assertEquals(42, prop.get());

		assertThrows(UnsupportedOperationException.class, () -> prop.set(52));
		assertEquals(42, prop.get()); //has not been changed
	}

	@Test
	void test_type() {
		assertEquals(Integer.class, IProperty.of(1).getType());
		assertEquals(Float.class, IProperty.of(1.f).getType());
		assertEquals(String.class, IProperty.ofType(String.class).getType());
	}

}
