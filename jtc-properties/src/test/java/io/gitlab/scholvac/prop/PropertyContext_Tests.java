package io.gitlab.scholvac.prop;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertThrowsExactly;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;

import io.gitlab.scholvac.QualifiedName;
import io.gitlab.scholvac.prop.exceptions.PropertyCreationException;
import io.gitlab.scholvac.prop.impl.PropertyContext;

public class PropertyContext_Tests {



	@Test
	public void testDefaultCreation() {
		PropertyContext ctx = new PropertyContext(); // no exception
		assertEquals("", ctx.getName());
		assertEquals(QualifiedName.from(""), ctx.getQualifiedName());

		ctx = new PropertyContext("NiceName");
		assertEquals("NiceName", ctx.getName());
		assertEquals(QualifiedName.from("NiceName"), ctx.getQualifiedName());

		assertThrowsExactly(IllegalArgumentException.class, () -> new PropertyContext(null));
	}

	@Test
	public void testHierarchyCreation() {
		final PropertyContext ctx = new PropertyContext();
		assertNull(ctx.getParent());
		assertEquals(ctx, ctx.getRootContext());

		final IPropertyContext child1 = ctx.getContext("c1");
		assertNotNull(child1);
		assertEquals("c1", child1.getName());
		assertEquals(QualifiedName.fromSeperator(":c1", ":"), child1.getQualifiedName());
		final IPropertyContext child1_1 = ctx.getContext("c1");
		assertTrue(child1 == child1_1); //object equals

		assertEquals(ctx, child1.getParent());
		assertEquals(ctx, child1.getRootContext());

		final IPropertyContext child11 = child1.getContext("c11");
		assertNotNull(child11);
		assertEquals("c11", child11.getName());
		assertEquals(QualifiedName.from("", "c1", "c11"), child11.getQualifiedName());
		assertEquals(QualifiedName.fromSeperator(":c1:c11"), child11.getQualifiedName());
		final IPropertyContext child11_1 = ctx.getContext(QualifiedName.fromSeperator("c1:c11"));
		assertTrue(child11 == child11_1); //Object equals

		assertTrue(ctx.hasContext("c1"));
		assertFalse(ctx.hasContext("c11"));
	}


	@Test
	public void getProperties() {
		final PropertyContext ctx = new PropertyContext();
		assertFalse(ctx.hasProperty(PropertyDescriptor.create("MyProperty")));
		assertFalse(ctx.hasProperty("MyProperty")); //shortcut for the above

		final PropertyDescriptor<?> desc = PropertyDescriptor.create("MyObjProperty");
		final IProperty<?> objProp = ctx.getProperty(desc);
		assertNotNull(objProp);
		assertTrue(Object.class == objProp.getType());
		assertNull(objProp.get());
		assertNull(objProp.getDescription());
		assertTrue(objProp.isEditable());
		assertEquals("MyObjProperty", objProp.getName());
		assertNotNull(objProp.getAnnotations());
		assertTrue(objProp.getAnnotations().isEmpty());
		assertFalse(objProp.hasAnnotation("Foo"));
		assertNull(objProp.getAnnotation("fara"));
		assertEquals(42, objProp.getAnnotation("upsala", 42));
	}

	@Test
	public void getProperties_detectWrongType() {
		final PropertyContext ctx = new PropertyContext();
		assertNotNull(ctx.getProperty(PropertyDescriptor.create("MyProperty", Integer.class)));
		assertTrue(ctx.hasProperty("MyProperty")); //shortcut for the above

		final PropertyDescriptor<?> desc = PropertyDescriptor.create("MyProperty", Double.class);
		assertThrows(PropertyCreationException.class, () -> ctx.getProperty(desc));
	}

	@Test
	public void getProperties_detectMakeEditable() {
		final PropertyContext ctx = new PropertyContext();
		assertNotNull(ctx.getProperty(PropertyDescriptor.create("MyProperty", Integer.class).editable(false)));
		assertTrue(ctx.hasProperty("MyProperty")); //shortcut for the above

		final PropertyDescriptor<?> desc = PropertyDescriptor.create("MyProperty", Integer.class).editable(true);
		assertThrows(PropertyCreationException.class, () -> ctx.getProperty(desc));

		//accept if editable not specified
		assertNotNull(ctx.getProperty(PropertyDescriptor.create("MyProperty", Integer.class)));
	}

	@Test
	public void getProperties_addDescription() {
		final PropertyContext ctx = new PropertyContext();
		IProperty<Integer> prop = ctx.getProperty(PropertyDescriptor.create("MyProperty", Integer.class).editable(false));
		assertNotNull(prop);
		assertTrue(ctx.hasProperty("MyProperty")); //shortcut for the above
		assertNull(prop.getDescription());

		final PropertyDescriptor<Integer> desc = PropertyDescriptor.create("MyProperty", Integer.class).description("Some desc");
		prop = ctx.getProperty(desc);
		assertNotNull(prop.getDescription());
	}

	@Test
	public void getProperties_addAnnotations() {
		final PropertyContext ctx = new PropertyContext();
		IProperty<Integer> prop = ctx.getProperty(PropertyDescriptor.create("MyProperty", Integer.class).editable(false));
		assertNotNull(prop);
		assertTrue(ctx.hasProperty("MyProperty")); //shortcut for the above
		assertNull(prop.getDescription());
		assertTrue(prop.getAnnotations().isEmpty());

		PropertyDescriptor<Integer> desc = PropertyDescriptor.create("MyProperty", Integer.class)
				.annotate("TheAnswer", 42);
		prop = ctx.getProperty(desc);
		assertEquals(42, prop.getAnnotation("TheAnswer", Integer.class));


		desc = PropertyDescriptor.create("MyProperty", Integer.class)
				.annotate("TheAnswer", 44);
		prop = ctx.getProperty(desc);
		assertEquals(42, prop.getAnnotation("TheAnswer", Integer.class)); //not changed, if already there
	}

	@Test
	public void getAllProperties_selective() {
		final IPropertyContext ctx = new PropertyContext();
		ctx.setFQN("a1", 1);
		ctx.setFQN("a2", 2);
		ctx.setFQN("sub:b1", 3);

		final List<IProperty> l_1 = ctx.getAllProperties(false).collect(Collectors.toList());
		assertEquals(2, l_1.size());
		assertTrue(l_1.stream().map(IProperty::getName).collect(Collectors.toList()).containsAll(Arrays.asList("a1", "a2")));

		final List<IProperty> l_2 = ctx.getContext("sub").getAllProperties(false).collect(Collectors.toList());
		assertEquals(1, l_2.size());
		assertEquals("b1", l_2.get(0).getName());
	}

	@Test
	public void getAllProperties_recursive() {
		final IPropertyContext ctx = new PropertyContext();
		ctx.setFQN("a1", 1);
		ctx.setFQN("a2", 2);
		ctx.setFQN("sub:b1", 3);
		ctx.setFQN("sub:sub2:c1", 4);

		final List<IProperty> all = ctx.getAllProperties(true).collect(Collectors.toList());
		assertEquals(4, all.size());
		assertTrue(all.stream().map(IProperty::getName).collect(Collectors.toList()).containsAll(Arrays.asList("a1", "a2", "b1", "c1")));
	}
}
