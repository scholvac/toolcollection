package io.gitlab.scholvac.prop.io;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.junit.jupiter.api.Test;

import io.gitlab.scholvac.prop.IPropertyContext;
import io.gitlab.scholvac.prop.PropertyDescriptor;
import io.gitlab.scholvac.prop.impl.PropertyContext;
import io.gitlab.scholvac.prop.io.impl.PropertyFormat;
import io.gitlab.scholvac.resources.ResourceManager;

public class PropertyIO_Properties_Tests {

	@Test
	public void test_write_and_read_primitives() {
		try {
			GenericSerializationTestUtil.test_write_and_read_primitives(new PropertyFormat());
		} catch (final IOException e) {
			fail(e);
		}
	}

	@Test
	public void test_write_with_doc() {
		final IPropertyContext ctx = new PropertyContext();
		ctx.setFQN(":rootProp", 1.4f);
		ctx.getProperty(PropertyDescriptor.create("rootProp").description("Some description in \r\n three \r\n lines \r\nto check if the prefix works")).set(34.25698f);

		final PropertyFormat io = new PropertyFormat();
		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			io.write(ctx, baos);
		}catch (final Exception e) {
			fail(e);
		}

		final String expected = "# Some description in \r\n"
				+ "#  three \r\n"
				+ "#  lines \r\n"
				+ "# to check if the prefix works\n"
				+ "rootProp=34.26f";
		final String actual = new String(baos.toByteArray()).trim();
		assertEquals(expected, actual);
	}

	@Test
	public void test_write() throws IOException {
		final IPropertyContext ctx = new PropertyContext();
		ctx.setFQN(":rootProp", 1.4f);
		ctx.setFQN(":ctx1:byteProp", (byte)18);
		ctx.setFQN(":ctx1:shortProp", (short)45);
		ctx.setFQN(":ctx1:intProp", 4);
		ctx.setFQN(":ctx1:longProp", 422l);
		ctx.setFQN(":ctx1:floatProp", 38f);
		ctx.setFQN(":ctx1:doubleProp", 34.);
		ctx.setFQN(":ctx1:stringProp", "Hallo Property File");

		ctx.setFQN("ctx1:ctx2:boolean:p1", true);
		ctx.setFQN("ctx1:ctx2:boolean:p2", false);

		final PropertyFormat io = new PropertyFormat();
		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		io.write(ctx, baos);


		final String expected = "rootProp=1.4f\n"
				+ "ctx1.byteProp=18b\n"
				+ "ctx1.doubleProp=34.0\n"
				+ "ctx1.floatProp=38.0f\n"
				+ "ctx1.intProp=4\n"
				+ "ctx1.longProp=422l\n"
				+ "ctx1.shortProp=45s\n"
				+ "ctx1.stringProp=Hallo Property File\n"
				+ "ctx1.ctx2.boolean.p1=true\n"
				+ "ctx1.ctx2.boolean.p2=false";
		final String actual = new String(baos.toByteArray()).trim();
		assertEquals(expected, actual);
	}


	@Test
	public void test_Read() throws IOException {
		final IPropertyContext ctx = new PropertyFormat().load(ResourceManager.get().getStream("propertyio-test.properties"));

		assertTrue(ctx.hasProperty("rootProp"));
		assertTrue(ctx.hasProperty(PropertyDescriptor.createFQN(":rootProp")));
		assertTrue(ctx.hasPropertyFQN(":rootProp"));

		assertEquals(Float.class, ctx.getPropertyFQN(":rootProp").getType());
		assertEquals(1.3f, ctx.get("rootProp", 93.f));


		assertEquals(Byte.class, ctx.getPropertyFQN("ctx1:byteProp").getType());
		assertEquals((byte)17, ctx.getFQN("ctx1:byteProp", (byte)93));

		assertEquals(Short.class, ctx.getPropertyFQN("ctx1:shortProp").getType());
		assertEquals((short)34, ctx.getFQN("ctx1:shortProp", (short)93));

		assertEquals(Integer.class, ctx.getPropertyFQN("ctx1:intProp").getType());
		assertEquals(3, ctx.getFQN("ctx1:intProp", 93));

		assertEquals(Long.class, ctx.getPropertyFQN("ctx1:longProp").getType());
		assertEquals(42, ctx.getFQN("ctx1:longProp", 93l));

		assertEquals(Float.class, ctx.getPropertyFQN("ctx1:floatProp").getType());
		assertEquals(37.f, ctx.getFQN("ctx1:floatProp", 93f));

		assertEquals(Double.class, ctx.getPropertyFQN("ctx1:doubleProp").getType());
		assertEquals(33., ctx.getFQN("ctx1:doubleProp", 93.));

		assertEquals(String.class, ctx.getPropertyFQN("ctx1:stringProp").getType());
		assertEquals("Hallo Welt was geht ab; Hallo Property: Nichts geht ab.", ctx.getFQN("ctx1:stringProp", ""));



		assertEquals(Boolean.class, ctx.getPropertyFQN("ctx1:ctx2:boolean:p1").getType());
		assertEquals(true, ctx.getFQN("ctx1:ctx2:boolean:p1", false));

		assertEquals(Boolean.class, ctx.getPropertyFQN("ctx1:ctx2:boolean:p2").getType());
		assertEquals(false, ctx.getFQN("ctx1:ctx2:boolean:p2", true));

		assertEquals(Boolean.class, ctx.getPropertyFQN("ctx1:ctx2:boolean:p3").getType());
		assertEquals(true, ctx.getFQN("ctx1:ctx2:boolean:p3", false));

		assertEquals(Boolean.class, ctx.getPropertyFQN("ctx1:ctx2:boolean:p4").getType());
		assertEquals(false, ctx.getFQN("ctx1:ctx2:boolean:p4", true));

		assertEquals(String.class, ctx.getPropertyFQN("ctx1:ctx2:boolean:p5").getType()); //random strings will not be parsed as boolean == false
		assertEquals("upsala", ctx.getFQN("ctx1:ctx2:boolean:p5", ""));

		assertEquals(Boolean.class, ctx.getPropertyFQN(":ctx1:ctx2:boolean:p4").getType());
		assertEquals(false, ctx.getFQN("ctx1:ctx2:boolean:p4", true));
	}



}
