package io.gitlab.scholvac.collections;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

import io.gitlab.scholvac.observable.IObservableObject;
import io.gitlab.scholvac.observable.ObservableSupport;
import io.gitlab.scholvac.observable.ObservableSupport.AbstractObservableSupport;

public class ObservedHashMap<K, V> extends HashMap<K, V> implements IObservableObject, AbstractObservableSupport {

	protected transient ObservableSupport 	mObservableSupport = new ObservableSupport();

	public ObservedHashMap() {
	}

	public ObservedHashMap(final int initialCapacity, final float loadFactor) {
		super(initialCapacity, loadFactor);
	}

	public ObservedHashMap(final int initialCapacity) {
		super(initialCapacity);
	}

	public ObservedHashMap(final Map<? extends K, ? extends V> m) {
		super(m);
	}

	@Override
	public ObservableSupport getObservableSupport() {
		return mObservableSupport;
	}
	//----------------	Map-API -------------------//

	@Override
	public V put(final K key, final V value) {
		final V res = super.put(key, value);
		fireValueChanged("put", res, value);
		return res;
	}
	@Override
	public void putAll(final Map<? extends K, ? extends V> m) {
		super.putAll(m);
		fireValueChanged("putAll", null, this);
	}
	@Override
	public V putIfAbsent(final K key, final V value) {
		final V res = super.putIfAbsent(key, value);
		fireValueChanged("putIfAbsent", res, value);
		return res;
	}
	@Override
	public V remove(final Object key) {
		final V res = super.remove(key);
		fireValueChanged("remove", res, null);
		return res;
	}
	@Override
	public boolean remove(final Object key, final Object value) {
		final boolean res = super.remove(key, value);
		fireValueChanged("remove", value, null);
		return res;
	}
	@Override
	public boolean replace(final K key, final V oldValue, final V newValue) {
		final boolean res = super.replace(key, oldValue, newValue);
		fireValueChanged("replace", oldValue, newValue);
		return res;
	}
	@Override
	public V replace(final K key, final V value) {
		final V res = super.replace(key, value);
		fireValueChanged("replace", res, value);
		return res;
	}
	@Override
	public V merge(final K key, final V value, final BiFunction<? super V, ? super V, ? extends V> remappingFunction) {
		final V m = super.merge(key, value, remappingFunction);
		fireValueChanged("merge", null, m);
		return m;
	}

}
