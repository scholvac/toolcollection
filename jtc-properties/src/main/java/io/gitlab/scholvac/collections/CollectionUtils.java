package io.gitlab.scholvac.collections;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CollectionUtils {
	@SafeVarargs
	public static <T> List<T> concat(List<T>... lists) {
		return Stream.of(lists).flatMap(List::stream).collect(Collectors.toList());
	}

	@SafeVarargs
	public static <T> Collection<T> combine(Collection<T>... lists) {
		ArrayList<T> out = new ArrayList<>();
		if (lists != null && lists.length > 0) {
			for (Collection<T> list : lists)
				out.addAll(list);
		}
		return out;
	}

	public static Collection concatObj(Collection... lists) {
		ArrayList out = new ArrayList();
		if (lists != null && lists.length > 0) {
			for (Collection list : lists)
				out.addAll(list);
		}
		return out;
	}

}
