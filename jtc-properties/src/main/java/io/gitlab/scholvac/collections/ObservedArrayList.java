package io.gitlab.scholvac.collections;

import java.util.ArrayList;
import java.util.Collection;

import io.gitlab.scholvac.observable.IObservableObject;
import io.gitlab.scholvac.observable.ObservableSupport;
import io.gitlab.scholvac.observable.ObservableSupport.AbstractObservableSupport;

public class ObservedArrayList<E> extends ArrayList<E> implements IObservableObject<E>, AbstractObservableSupport<E>
{

	protected transient ObservableSupport<E> 	mListenerSupport = new ObservableSupport<>();

	public ObservedArrayList() { }

	public ObservedArrayList(final Collection<? extends E> c) {
		super(c);
	}

	public ObservedArrayList(final int initialCapacity) {
		super(initialCapacity);
	}

	@Override
	public ObservableSupport<E> getObservableSupport() {
		return mListenerSupport;
	}

	//----------------	List-API -------------------//

	@Override
	public void add(final int index, final E element) {
		super.add(index, element);
		if (mListenerSupport != null)
			mListenerSupport.fireIndexedValueChaned("add", index, element, element);
	}
	@Override
	public E remove(final int index) {
		final E removed = super.remove(index);
		if (mListenerSupport != null)
			mListenerSupport.fireIndexedValueChaned("remove", index, removed, null);
		return removed;
	}
	@Override
	public E set(final int index, final E element) {
		final E previous = super.set(index, element);
		if (mListenerSupport != null)
			mListenerSupport.fireIndexedValueChaned("set", index, previous, element);
		return previous;
	}

	@Override
	public void clear() {
		super.clear();
		if (mListenerSupport != null)
			mListenerSupport.fireValueChanged("clear", null, null);
	}
}
