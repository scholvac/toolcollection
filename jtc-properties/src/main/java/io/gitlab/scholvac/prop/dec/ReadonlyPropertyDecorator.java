package io.gitlab.scholvac.prop.dec;

import io.gitlab.scholvac.prop.IProperty;
import io.gitlab.scholvac.prop.impl.DelegateProperty;

public class ReadonlyPropertyDecorator<ValueT> extends DelegateProperty<ValueT> {

	public static <ValueT> IProperty<ValueT> decorate(final IProperty<ValueT> property){ return new ReadonlyPropertyDecorator<>(property);}

	public ReadonlyPropertyDecorator(final IProperty<ValueT> prop) {
		super(prop);
	}

	@Override
	public boolean isEditable() {
		return false;
	}

}
