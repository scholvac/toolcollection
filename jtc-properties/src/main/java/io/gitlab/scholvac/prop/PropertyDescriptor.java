package io.gitlab.scholvac.prop;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

import io.gitlab.scholvac.QualifiedName;
import io.gitlab.scholvac.prop.impl.PropertyImpl;

public class PropertyDescriptor<ValueT> {
	private final String	 				mName;


	private String 							mDescription;
	private Boolean 						mEditable;
	private Class<ValueT>					mValueType;

	private Map<String, Object>				mAnnotations;
	private Supplier<ValueT>				mInitialValue;
	private QualifiedName					mPath;

	public PropertyDescriptor(final String name, final String desc, final Boolean edt) {
		mName = name;
		mDescription = desc;
		mEditable = edt;
	}
	public static <ValueT> PropertyDescriptor<ValueT> create(final String name) {
		return create(name, null, null, null);
	}
	public static <ValueT> PropertyDescriptor<ValueT> createFQN(final String qn) {
		return createFQN(QualifiedName.fromSeperator(qn), null, null, null);
	}
	public static <ValueT> PropertyDescriptor<ValueT> createFQN(final QualifiedName qn) {
		return createFQN(qn, null, null, null);
	}

	public static <ValueT> PropertyDescriptor<ValueT> create(final String name, final Class<ValueT> clazz) {
		return create(name, null, clazz);
	}
	public static <ValueT> PropertyDescriptor<ValueT> createFQN(final QualifiedName qn, final Class<ValueT> clazz) {
		return createFQN(qn, null, clazz);
	}
	public static <ValueT> PropertyDescriptor<ValueT> create(final String name, final String description, final Class<ValueT> clazz) {
		return create(name, description, null, clazz);
	}
	public static <ValueT> PropertyDescriptor<ValueT> createFQN(final QualifiedName qn, final String description, final Class<ValueT> clazz) {
		return (PropertyDescriptor<ValueT>) createFQN(qn, description, null, clazz);
	}
	public static <ValueT> PropertyDescriptor<ValueT> create(final String name, final ValueT defaultValue) {
		return create(name, null, null, defaultValue);
	}
	public static <ValueT> PropertyDescriptor<ValueT> createFQN(final QualifiedName qn, final ValueT defaultValue) {
		return createFQN(qn, null, null, defaultValue);
	}

	public static <ValueT> PropertyDescriptor<ValueT> create(final String name, final String description, final Boolean editable, final Class<ValueT> clazz) {
		return new PropertyDescriptor<ValueT>(name, description, editable).valueType(clazz);
	}
	public static <ValueT> PropertyDescriptor<ValueT> create(final String name, final String description, final Boolean editable, final ValueT defaultValue) {
		return new PropertyDescriptor<ValueT>(name, description, editable).defaultValue(defaultValue);
	}
	public static <ValueT> PropertyDescriptor<ValueT> createFQN(final QualifiedName qn, final String description, final Boolean editable, final ValueT defaultValue) {
		PropertyDescriptor<ValueT> desc = new PropertyDescriptor<ValueT>(qn.last(), description, editable).defaultValue(defaultValue);
		if (qn.size() > 1)
			desc = desc.path(qn.skipLast());
		return desc;
	}
	public PropertyDescriptor<ValueT> path(final String...pathSegments) { return path(QualifiedName.from(pathSegments)); }
	public PropertyDescriptor<ValueT> path(final QualifiedName path) { mPath = path; return this; }
	public PropertyDescriptor<ValueT> valueType(final Class<ValueT> clazz) { this.mValueType = clazz; return this;}
	public PropertyDescriptor<ValueT> description(final String desc){ this.mDescription = desc; return this;}
	public PropertyDescriptor<ValueT> editable(final Boolean editable){ this.mEditable = editable; return this;}
	public PropertyDescriptor<ValueT> readOnly(final boolean readOnly){ return editable(!readOnly);}
	public PropertyDescriptor<ValueT> defaultValue(final ValueT value){ return defaultValue(() -> value); }
	public PropertyDescriptor<ValueT> defaultValue(final Supplier<ValueT> value) { mInitialValue = value; return this;}

	public <T> PropertyDescriptor<ValueT> annotate(final String key, final T value) {
		if (mAnnotations == null) mAnnotations = new HashMap<>();
		mAnnotations.put(key, value);
		return this;
	}
	public <T> T getAnnotationValue(final String source, final T defaultValue) {
		if (mAnnotations != null && mAnnotations.containsKey(source))
			return (T)mAnnotations.get(source);
		return defaultValue;
	}

	//TODO: create a property info create method that supports translations
	public String getName() { return mName; }
	public Supplier<ValueT> getInitialValueSupplier() { return mInitialValue; }
	public String getDescription() { return mDescription;}
	public Boolean isEditable() { return mEditable; }
	public Map<String, Object> getAnnotations() { return mAnnotations;}
	public Class<ValueT> getValueType() { return mValueType; }
	public QualifiedName getPath() { return mPath; }


	public PropertyDescriptor<ValueT> template(final QualifiedName qn){
		final String name = qn.last();
		final PropertyDescriptor<ValueT> desc = template(name);
		if (qn.size() > 1)
			desc.path(qn.skipLast());
		return desc;
	}

	/**
	 * @return a full copy of this descriptor (e.g a != b but a.equals(b) == true)
	 */
	public PropertyDescriptor<ValueT> copy() {
		return template(getName());
	}

	public PropertyDescriptor<ValueT> template(final String newName){
		final PropertyDescriptor<ValueT> desc = PropertyDescriptor.create(newName, getDescription(), getValueType())
				.defaultValue(getInitialValueSupplier());
		if (getAnnotations() != null  && getAnnotations().isEmpty() == false)
			desc.mAnnotations = new HashMap<>(getAnnotations());
		return desc;
	}



	public IProperty<ValueT> createNewInstance() {
		final Supplier<ValueT> supplier = getInitialValueSupplier();
		final ValueT initialValue = supplier != null ? supplier.get() : null;
		Class<ValueT> valueType = getValueType();
		if (valueType == null && initialValue != null)
			valueType = (Class<ValueT>) initialValue.getClass();
		final boolean editable = isEditable() == null || isEditable() == true;

		final IProperty<ValueT> property = new PropertyImpl<>(getName(), getDescription(), editable, initialValue, valueType);
		if (getAnnotations() != null)
			for (final Map.Entry<String, Object> e : getAnnotations().entrySet())
				property.addAnnotation(e.getKey(), e.getValue());
		return property;
	}
	//	public RestrictedPropertyPair<ValueT> createRestrictedInstance() {
	//		final Supplier<ValueT> supplier = getInitialValueSupplier();
	//		final ValueT initValue = supplier != null ? supplier.get() : null;
	//		final RestrictedPropertyPair<ValueT> pair = new RestrictedPropertyPair<>(getName(), getDescription(), initValue);
	//		if (getAnnotations() != null)
	//			for (final Map.Entry<String, Object> e : getAnnotations().entrySet())
	//				pair.property.addAnnotation(e.getKey(), e.getValue());
	//		return pair;
	//	}
}