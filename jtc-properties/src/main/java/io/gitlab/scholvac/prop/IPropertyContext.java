package io.gitlab.scholvac.prop;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

import io.gitlab.scholvac.QualifiedName;
import io.gitlab.scholvac.observable.IObservableObject;
import io.gitlab.scholvac.prop.PropertyDecomposer.IPropertyDecomposer;

public interface IPropertyContext extends IPropertyAccess, IContextAccess, IObservableObject, Serializable, IPropertyDecomposer {

	String getName();
	String getDescription();


	default <ValueT> IProperty<ValueT> getLocalProperty(final String name, final ValueT defaultValue){
		return getLocalProperty(PropertyDescriptor.create(name, defaultValue));
	}

	/**
	 * Equals to {@link IPropertyAccess#getProperty(PropertyDescriptor)} but ignores the path of the descriptor (if set)
	 * @param <ValueT>
	 * @param descriptor
	 * @return
	 */
	default <ValueT> IProperty<ValueT> getLocalProperty(final PropertyDescriptor<ValueT> descriptor){
		return getProperty(descriptor.copy().path((QualifiedName)null));
	}


	default boolean hasLocalProperty(final String name){
		return hasLocalProperty(PropertyDescriptor.create(name));
	}

	/**
	 * Equals to {@link IPropertyAccess#getProperty(PropertyDescriptor)} but ignores the path of the descriptor (if set)
	 * @param <ValueT>
	 * @param descriptor
	 * @return
	 */
	default boolean hasLocalProperty(final PropertyDescriptor<?> descriptor){
		return hasProperty(descriptor.copy().path((QualifiedName)null));
	}


	@Override
	default Collection<IProperty<?>> decompose(final Object value) {
		final HashSet<IProperty<?>> out = new HashSet<>();
		if (value != this)
			getAllProperties(true).forEach(it -> out.add(it));
		return Collections.EMPTY_LIST;
	}

	default <ValueT> void addAlias(final PropertyDescriptor<ValueT> desc, final String ... qualifiedName) {
		addAlias(desc, QualifiedName.from(qualifiedName));
	}
	<ValueT> void addAlias(PropertyDescriptor<ValueT> desc, QualifiedName from);
	<ValueT> IProperty<ValueT> getAliasOf(QualifiedName qn);

}
