package io.gitlab.scholvac.prop.exceptions;

import io.gitlab.scholvac.prop.IProperty;

public class PropertyException extends RuntimeException {

	private final IProperty<?> mProperty;

	public PropertyException(final IProperty<?> property, final String message) {
		this(property, message, null);
	}

	public PropertyException(final IProperty<?> property, final Throwable cause) {
		this(property, null, cause);
	}

	public PropertyException(final IProperty<?> property, final String message, final Throwable cause) {
		super(message, cause);
		mProperty = property;
	}

	public IProperty<?> getProperty() { return mProperty;}


}
