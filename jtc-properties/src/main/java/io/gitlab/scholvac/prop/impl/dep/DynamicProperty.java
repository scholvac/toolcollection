package io.gitlab.scholvac.prop.impl.dep;

import java.util.function.Consumer;
import java.util.function.Supplier;

import io.gitlab.scholvac.prop.impl.FunctionalProperty;

/**
 * @deprecated for backward compability. Bad name, the property is not dynamic, thus use {@link FunctionalProperty} instead
 */
@Deprecated
public class DynamicProperty<ValueT> extends FunctionalProperty<ValueT>{

	public DynamicProperty(final String name, final Supplier<ValueT> getter, final Consumer<ValueT> setter) {
		super(name, getter, setter);
		// TODO Auto-generated constructor stub
	}

}
