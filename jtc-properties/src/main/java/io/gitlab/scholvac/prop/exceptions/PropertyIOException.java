package io.gitlab.scholvac.prop.exceptions;

import io.gitlab.scholvac.prop.IProperty;

public class PropertyIOException extends PropertyException {

	public PropertyIOException(final IProperty<?> candidate, final String message) {
		this(candidate, message, null);
	}

	public PropertyIOException(final IProperty<?> candidate, final String message, final Throwable cause) {
		super(candidate, message, cause);
	}


}
