package io.gitlab.scholvac.prop.io.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamClass;
import java.io.OutputStream;

import io.gitlab.scholvac.prop.IPropertyContext;
import io.gitlab.scholvac.prop.io.IPropertyFileFormat;
import io.gitlab.scholvac.prop.utils.ContextUtils;

public class SerializeableFormat implements IPropertyFileFormat {

	@Override
	public void write(final IPropertyContext ctx, final OutputStream stream) throws IOException {
		final ObjectOutputStream oos = new ObjectOutputStream(stream);
		oos.writeObject(ctx);
		oos.close();
	}

	@Override
	public void load(final InputStream stream, final IPropertyContext root) throws IOException {
		final ObjectInputStream ois = new ObjectInputStream(stream) {
			@Override
			protected Class<?> resolveClass(final ObjectStreamClass desc) throws IOException, ClassNotFoundException {
				try {
					return super.resolveClass(desc);
				}catch(final Exception e) {
					final ClassLoader cll = getClass().getClassLoader();
					final ClassLoader tcll = Thread.currentThread().getContextClassLoader();
					final Class<?> cl = tcll.loadClass(desc.getName());
					if (cl != null)
						return cl;
					e.printStackTrace();
				}
				return null;
			}
		};
		try {
			final IPropertyContext ctx = (IPropertyContext) ois.readObject();
			ContextUtils.merge(ctx, root);
		} catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
			throw new IOException(e);
		}
	}

	@Override
	public boolean supports(final String encodingHint) {
		return "sp".equalsIgnoreCase(encodingHint);
	}

}
