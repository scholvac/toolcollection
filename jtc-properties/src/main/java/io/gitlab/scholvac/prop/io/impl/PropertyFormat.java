package io.gitlab.scholvac.prop.io.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.function.Function;
import java.util.stream.Stream;

import io.gitlab.scholvac.QualifiedName;
import io.gitlab.scholvac.StringUtils;
import io.gitlab.scholvac.prop.IProperty;
import io.gitlab.scholvac.prop.IPropertyContext;
import io.gitlab.scholvac.prop.PropertyDescriptor;
import io.gitlab.scholvac.prop.exceptions.PropertyIOException;
import io.gitlab.scholvac.prop.io.IPropertyFileFormat;

public class PropertyFormat implements IPropertyFileFormat {

	public static final String				DEFAULT_PATH_SEPERATOR = "\\.";


	private final static ArrayList<Function<PropertyFormat, Function<String, Object>>> sCasters;
	private final static Map<Class, Function<PropertyFormat, Function<Object, String>>> sWriters;
	//
	static {
		sCasters = new ArrayList<>();
		sCasters.add(pf -> PropertyFormat::parseBoolean);
		sCasters.add(pf -> PropertyFormat::parseByte);
		sCasters.add(pf -> PropertyFormat::parseShort);
		sCasters.add(pf -> PropertyFormat::parseLong);
		sCasters.add(pf -> Integer::valueOf);
		sCasters.add(PropertyFormat::createFloatParser);
		sCasters.add(PropertyFormat::createDoubleParser);
		sCasters.add(pf -> PropertyFormat::parseString);

		sWriters = new HashMap<>();
		sWriters.put(Boolean.class, pf -> obj -> ((Boolean)obj).toString());
		sWriters.put(Byte.class, pf -> obj -> String.format("%db", obj));
		sWriters.put(Short.class, pf -> obj -> String.format("%ds", obj));
		sWriters.put(Integer.class, pf -> obj -> String.format("%d", obj));
		sWriters.put(Long.class, pf -> obj -> String.format("%dl", obj));
		sWriters.put(Float.class, PropertyFormat::createFloatWriter);
		sWriters.put(Double.class,PropertyFormat::createDoubleWriter);
		sWriters.put(String.class, pf -> obj -> String.format("%s", obj));
	}

	private static boolean parseBoolean(final String str) {
		if ("true".equalsIgnoreCase(str) || "on".equalsIgnoreCase(str))
			return true;
		if ("false".equalsIgnoreCase(str) || "off".equalsIgnoreCase(str))
			return false;
		throw new RuntimeException();
	}

	private static byte parseByte(final String str) {
		if (Character.toLowerCase(str.charAt(str.length()-1)) == 'b')
			return Byte.parseByte(str.substring(0, str.length()-1));
		throw new RuntimeException();
	}
	private static short parseShort(final String str) {
		if (Character.toLowerCase(str.charAt(str.length()-1)) == 's')
			return Short.parseShort(str.substring(0, str.length()-1));
		throw new RuntimeException();
	}
	private static long parseLong(final String str) {
		if (Character.toLowerCase(str.charAt(str.length()-1)) == 'l')
			return Long.parseLong(str.substring(0, str.length()-1));
		throw new RuntimeException();
	}
	private static Function<String, Object> createFloatParser(final PropertyFormat pf) {
		final DecimalFormat df =  pf.getFloatFormat();
		return str -> {
			if (Character.toLowerCase(str.charAt(str.length()-1)) == 'f'){
				try {
					final Number d = df.parse(str);
					return d.floatValue();
				}catch(final Exception e) {}
			}
			throw new RuntimeException();
		};
	}
	private static Function<Object, String> createFloatWriter(final PropertyFormat pf) {
		final DecimalFormat df =  pf.getFloatFormat();
		return val -> df.format(val);
	}
	private static Function<String, Object> createDoubleParser(final PropertyFormat pf) {
		final DecimalFormat df =  pf.getDoubleFormat();
		final char s = df.getDecimalFormatSymbols().getDecimalSeparator();
		return str -> {
			try{
				final Number n = df.parse(str);
				return n.doubleValue();
			}catch(final Exception e) {}
			throw new RuntimeException();
		};
	}
	private static Function<Object, String> createDoubleWriter(final PropertyFormat pf) {
		final DecimalFormat df =  pf.getDoubleFormat();
		return val -> df.format(val);
	}
	private static String parseString(final String str) {
		if (str.startsWith("\"") && str.endsWith("\""))
			return str.substring(1, str.length()-1);
		if (str.startsWith("'") && str.endsWith("'"))
			return str.substring(1, str.length()-1);
		return str;

	}

	private PropertyDescriptor						mTemplate;
	private Locale									mLocale = Locale.US;

	private List<Function<String, Object>> 			mCasters;
	private Map<Class, Function<Object, String>> 	mWriters =new HashMap<>();


	@Override
	public boolean supports(final String encodingHint) {
		return "properties".equalsIgnoreCase(encodingHint);
	}
	@Override
	public void load(final InputStream stream, final IPropertyContext root) throws IOException {
		final Properties properties = new Properties();
		properties.load(stream);
		stream.close();

		final String pathSeperator = getPathSeperator();
		for (final Object k : properties.keySet()) {
			final String key = (String)k;
			final String strValue = properties.getProperty(key);

			final Object value = parseValue(strValue);
			final PropertyDescriptor desc = getTemplate()
					.template(QualifiedName.fromSeperator(key, pathSeperator))
					.defaultValue(parseValue(strValue));
			root.set(desc, value);
		}
	}

	private Object parseValue(final String strValue) {
		if (strValue == null || "null".equals(strValue))
			return null;
		final List<Function<String, Object>> casters = getCasters();
		for (int i = 0; i < casters.size(); i++) {
			try {
				final Object val = casters.get(i).apply(strValue);
				if (val != null)
					return val;
			}catch(final RuntimeException ex) {} //ignore on purpose
		}
		return strValue;
	}


	protected List<Function<String, Object>> getCasters(){
		if (mCasters == null) {
			mCasters = buildMemberCasters();
		}
		return mCasters;
	}

	private List<Function<String, Object>> buildMemberCasters() {
		final List<Function<String, Object>> out = new ArrayList<>(sCasters.size());
		for (final Function<PropertyFormat, Function<String, Object>> sup : sCasters) {
			out.add(sup.apply(this));
		}
		return out;
	}


	private PropertyDescriptor getTemplate() {
		if (mTemplate == null)
			mTemplate = createTemplateDescriptor();
		return mTemplate;
	}

	protected PropertyDescriptor createTemplateDescriptor() {
		return PropertyDescriptor.create((String)null);
	}
	protected String getPathSeperator() {
		return DEFAULT_PATH_SEPERATOR;
	}

	@Override
	public void write(final IPropertyContext ctx, final OutputStream stream) throws IOException {
		if (ctx == null) throw new NullPointerException("Context to save may not be null");
		if (stream == null) throw new NullPointerException("No target to write property context");

		final StringBuilder sb = new StringBuilder();
		final Stream<IPropertyContext> ctxStream =
				ctx.getAllContexts(true)
				.sorted((a, b) -> a.getQualifiedName().compareTo(b.getQualifiedName())) //sort to have comparable results - since saved in maps.
				;
		Stream.concat(Stream.of(ctx), ctxStream)
		.forEach(childCtx -> {
			final QualifiedName path = childCtx.getQualifiedName();
			childCtx.getAllProperties(false)
			.sorted((a,b)->a.getName().compareTo(b.getName()))
			.forEach(prop -> write(path, prop, sb));
		});
		stream.write(sb.toString().getBytes());
	}

	public void write(final QualifiedName path, final IProperty prop, final StringBuilder sb) {
		if (prop == null) throw new NullPointerException("property to save may not be null");


		final Function<Object, String> writer = getWriter(prop.getType());
		if (writer == null) throw new PropertyIOException(prop, "Could not find a writer for type: " + prop.getType());

		final Object value = prop.get();
		if (value == null)
			return ;
		final String strValue = writer.apply(prop.get());

		final QualifiedName fullPath = path.append(prop.getName());
		final String desc = prop.getDescription();
		if (StringUtils.notNullOrEmpty(desc))
			sb.append(formatDescription(desc)).append("\n");

		if (fullPath.first().isEmpty())
			sb.append(fullPath.skipFirst().toString("."));
		else
			sb.append(fullPath.toString("."));
		sb.append("=").append(strValue).append("\n");
	}


	protected Function<Object, String> getWriter(final Class type) {
		return mWriters.computeIfAbsent(type, t -> {
			final Function<PropertyFormat, Function<Object, String>> creator = sWriters.get(type);
			if (creator == null)
				return null;
			return creator.apply(this);
		});
	}
	protected String formatDescription(final String desc) {
		return StringUtils.appendPrefixToEachLine(getCommentSymbol(), desc);
	}
	protected String getCommentSymbol() {
		return "# ";
	}

	public DecimalFormat getDoubleFormat() {
		return new DecimalFormat("#.0####", new DecimalFormatSymbols(getLocale()));
	}
	public DecimalFormat getFloatFormat() {
		return new DecimalFormat("#.0#f", new DecimalFormatSymbols(getLocale()));
	}

	public void setLocale(final Locale l) { mLocale = l;}
	public Locale getLocale() { return mLocale;}

}
