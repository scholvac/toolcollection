package io.gitlab.scholvac.prop.impl;

import java.util.function.Consumer;
import java.util.function.Supplier;

import io.gitlab.scholvac.prop.IProperty;

/**
 * An {@link IProperty} implementation that uses {@link Consumer} and {@link Supplier} instead of a own hosted value.
 *
 * Since the property has no control over the underlying value, notifications need to be triggered externally.
 */
public class FunctionalProperty<ValueT> extends AbstractProperty<ValueT> implements IProperty<ValueT> {

	private Supplier<ValueT> mGetter;
	private Consumer<ValueT> mSetter;


	/** An optional value that is used to store the last vaulue type. The variable may comes in action if the current value is null and thus the currently active type can not be determined*/
	private Class<ValueT> mPrevType;

	public FunctionalProperty(final String name, final Supplier<ValueT> getter, final Consumer<ValueT> setter) {
		this(name, null, setter != null, getter, setter);
	}
	public FunctionalProperty(final String name, final String desc, final boolean editable, final Supplier<ValueT> getter, final Consumer<ValueT> setter) {
		super(name, desc, editable);
		mGetter = getter;
		mSetter = setter;
	}

	protected Consumer<ValueT> getSetter() { return mSetter;}
	protected Supplier<ValueT> getGetter() { return mGetter;}

	@Override
	public ValueT get() {
		return mGetter.get();
	}

	@Override
	protected void internalSetValue(final ValueT newValue) {
		getSetter().accept(newValue);
	}
	@Override
	public Class<ValueT> getType() {
		final ValueT val = get();
		if (val != null)
			return mPrevType = (Class<ValueT>) val.getClass();
		return mPrevType;
	}
}
