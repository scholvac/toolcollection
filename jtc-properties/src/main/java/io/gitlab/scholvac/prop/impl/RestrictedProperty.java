package io.gitlab.scholvac.prop.impl;

import io.gitlab.scholvac.prop.IProperty;


/* A restricted property is a readonly property that provides (during creation) a setter, thus that all
 * instance that have access to the property setter, can manipulate the proeprty but for all other objects, that may
 * work on the property, the property apears to be readonly.
 */
public class RestrictedProperty<ValueT> extends PropertyImpl<ValueT> implements IProperty<ValueT>{

	/** The setter to access a restricted property */
	@FunctionalInterface
	public interface IRestrictedProeprtySetter<ValueT> {
		void set(final ValueT value);
	}
	public static class RestrictedPropertyPair<ValueT>{
		public final IProperty<ValueT> 						property;
		public final IRestrictedProeprtySetter<ValueT>		setter;
		public RestrictedPropertyPair(final IProperty<ValueT> p, final IRestrictedProeprtySetter<ValueT> s) {
			property = p;
			setter = s;
		}

		public void set(final ValueT newValue) {
			setter.set(newValue);
		}

		public ValueT get() {
			return property.get();
		}
	}

	private final static class DefaultSetter<ValueT> implements IRestrictedProeprtySetter<ValueT> {
		RestrictedProperty<ValueT>	mProp;
		DefaultSetter(final RestrictedProperty<ValueT> p){ mProp = p;}
		@Override
		public void set(final ValueT value) {
			mProp.uncheckedSet(value);
		}
	}
	public static <ValueT> RestrictedPropertyPair<ValueT> createRestrictedProperty(final String name, final String desc, final ValueT value){
		return createRestrictedProperty(name, desc, value, (Class<ValueT>)value.getClass());
	}
	public static <ValueT> RestrictedPropertyPair<ValueT> createRestrictedProperty(final String name, final String desc, final ValueT value, final Class<ValueT> valueType){
		final RestrictedProperty<ValueT> prop = new RestrictedProperty<ValueT>(name, desc, value, valueType);
		return new RestrictedPropertyPair<>(prop, new DefaultSetter<>(prop));
	}

	public RestrictedProperty(final String name, final String desc, final ValueT value, final Class<ValueT> valueType) {
		super(name, desc, false, value, valueType);
	}
}
