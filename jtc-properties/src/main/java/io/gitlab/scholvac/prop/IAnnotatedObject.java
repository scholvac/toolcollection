package io.gitlab.scholvac.prop;

import java.io.Serializable;
import java.util.Map;

public interface IAnnotatedObject extends Serializable {
	Map<String, Object> getAnnotations();
	<AnnotationType> void addAnnotation(final String key, final AnnotationType value);
	<AnnotationType> AnnotationType getAnnotation(final String key);
	<AnnotationType> AnnotationType removeAnnotation(final String key);

	default <AnnotationType> void annotate(final String key) {
		addAnnotation(key, null);
	}
	default <AnnotationType> void annotate(final String key, final AnnotationType value) {
		addAnnotation(key, value);
	}
	default boolean hasAnnotation(final String key) {
		return getAnnotation(key) != null;
	}
	default <AnnotationType> AnnotationType getAnnotation(final String key, final AnnotationType defaultValue) {
		if (hasAnnotation(key))
			return getAnnotation(key);
		return defaultValue;
	}
	default <AnnotationType> AnnotationType getAnnotation(final String key, final Class<AnnotationType> type) {
		return getAnnotation(key); //type is "just" used for correct cast
	}
}
