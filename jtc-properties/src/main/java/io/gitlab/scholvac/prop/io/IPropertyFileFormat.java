package io.gitlab.scholvac.prop.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import io.gitlab.scholvac.prop.IPropertyContext;
import io.gitlab.scholvac.prop.impl.PropertyContext;
import io.gitlab.scholvac.service.IExtension;

public interface IPropertyFileFormat extends IExtension {


	void write(final IPropertyContext ctx, final OutputStream stream) throws IOException;

	/**
	 * Checks whether a stream can be encoded or not.
	 * @param encodingHint A hint of what kind of stream to expect, e.g. in for example the file extension
	 * @return true if a stream of that type can be decoded
	 * @throws IOException
	 */
	boolean supports(String encodingHint) ;
	void load(InputStream stream, IPropertyContext root) throws IOException;


	//////////////////////////////////////////////////////////////////////////////////////////////////////
	//				Defaults
	//////////////////////////////////////////////////////////////////////////////////////////////////////

	default IPropertyContext load(final InputStream stream) throws IOException {
		final IPropertyContext empty = new PropertyContext();
		load(stream, empty);
		return empty;
	}

}
