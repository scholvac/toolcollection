package io.gitlab.scholvac.prop;

import java.util.stream.Stream;

import io.gitlab.scholvac.QualifiedName;
import io.gitlab.scholvac.observable.IObservableObject.IValueChangeListener;
import io.gitlab.scholvac.observable.IObservableObject.ValueChangeEvent;
import io.gitlab.scholvac.prop.exceptions.PropertyCreationException;

public interface IPropertyAccess {

	/**
	 * Checks if a certain property (currently) exists
	 **/
	<ValueT> boolean hasProperty(PropertyDescriptor<ValueT> descriptor);

	/** Returns a property that matches the given descriptor.
	 *
	 * If no such property exists yet, the property is created.<br>
	 * If a property exists, that machtes the descriptor but does not have a valid description, the description of this descriptor is assigned.
	 * <br>
	 * If this descriptor contains annotations that are not part of the property, the missing annotations will be added to the property.
	 * However no annotation will be removed.
	 *
	 * @param <ValueT>
	 * @param descriptor
	 * @return
	 * @throws PropertyCreationException if a property with same name but different type exists.
	 **/
	<ValueT> IProperty<ValueT> getProperty(PropertyDescriptor<ValueT> descriptor);

	/**
	 * Returns a stream of all properties of this Property context and may all its child contexts.
	 * @param recursive Whether the stream shall contain the properties of the child contexts or not
	 * @return A stream that contains all properties of this and may all child contexts.
	 */
	Stream<IProperty> getAllProperties(boolean recursive);


	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * @param name Name of the {@link IProperty} to search.
	 * @return true if the property exists, false otherwise.
	 */
	default boolean hasProperty(final String name) { return hasProperty(PropertyDescriptor.create(name));}

	/**
	 * @param name Name of the {@link IProperty} to search.
	 * @return true if the property exists, false otherwise.
	 */
	default boolean hasPropertyFQN(final String name) { return hasPropertyFQN(QualifiedName.fromSeperator(name));}
	/**
	 * @param name Name of the {@link IProperty} to search.
	 * @return true if the property exists, false otherwise.
	 */
	default boolean hasPropertyFQN(final QualifiedName name) { return hasProperty(PropertyDescriptor.createFQN(name));}

	/**
	 * @param name Name of the {@link IProperty} to search.
	 * @return A property with given name. If the property did not exists yet, it will have the defaultValue.
	 */
	default <ValueT> IProperty<ValueT> getProperty(final String name){
		return getProperty(PropertyDescriptor.create(name, null));
	}
	/**
	 * @param name Name of the {@link IProperty} to search.
	 * @return A property with given name. If the property did not exists yet, it will have the defaultValue.
	 */
	default <ValueT> IProperty<ValueT> getProperty(final String name, final ValueT defaultValue){
		return getProperty(PropertyDescriptor.create(name, defaultValue)); //may be optimized by specialized class
	}

	/**
	 * @param name Name of the {@link IProperty} to search.
	 * @return A property with given name. If the property did not exists yet, it will have the defaultValue.
	 */
	default <ValueT> IProperty<ValueT> getPropertyFQN(final String name){
		return getPropertyFQN(QualifiedName.fromSeperator(name), null);
	}
	/**
	 * @param name Name of the {@link IProperty} to search.
	 * @return A property with given name. If the property did not exists yet, it will have the defaultValue.
	 */
	default <ValueT> IProperty<ValueT> getPropertyFQN(final String name, final ValueT defaultValue){
		return getPropertyFQN(QualifiedName.fromSeperator(name), defaultValue); //may be optimized by specialized class
	}


	/**
	 * @param name Name of the {@link IProperty} to search.
	 * @return A property with given name. If the property did not exists yet, it will have the defaultValue.
	 */
	default <ValueT> IProperty<ValueT> getPropertyFQN(final QualifiedName name, final ValueT defaultValue){
		return getProperty(PropertyDescriptor.createFQN(name, defaultValue)); //may be optimized by specialized class
	}


	/////////////////////////////////////////////////////////////////////////////////////////////////
	/**
	 * Sets a property value
	 * @param <ValueT>
	 * @param desc The property to be manipulated
	 * @param value The new value
	 */
	default <ValueT> void set(final PropertyDescriptor<ValueT> desc, final ValueT value) {
		getProperty(desc).set(value);
	}
	default <ValueT> void set(final String localName, final ValueT newValue) {
		getProperty(localName, newValue).set(newValue);
	}

	/**
	 * Sets a property value
	 * @param <ValueT>
	 * @param path The property to be manipulated
	 * @param value The new value
	 */
	default <ValueT> void setFQN(final String path, final ValueT value) {
		getPropertyFQN(QualifiedName.fromSeperator(path), value).set(value);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	default <ValueT> ValueT get(final PropertyDescriptor<ValueT> desc) {
		return getProperty(desc).get();
	}
	default <ValueT> ValueT get(final QualifiedName name, final ValueT defaultValue) {
		return getFQN(name, defaultValue);
	}

	default <ValueT> ValueT get(final String localName, final ValueT defaultValue) {
		return getProperty(localName, defaultValue).get();
	}
	default <ValueT> ValueT getFQN(final String qualifiedName, final ValueT defaultValue) {
		return getFQN(QualifiedName.fromSeperator(qualifiedName), defaultValue);
	}
	default <ValueT> ValueT getFQN(final QualifiedName qualifiedName, final ValueT defaultValue) {
		return getPropertyFQN(qualifiedName, defaultValue).get();
	}

	///////////////////////////////////////////////////////////////////////////////////////////////

	default <ValueT> ValueT listenFQN(final String qualifiedName, final ValueT defaultValue, final IValueChangeListener<ValueT> listener) {
		return listenFQN(qualifiedName, defaultValue, listener, false);
	}
	default <ValueT> ValueT listenFQN(final String qualifiedName, final ValueT defaultValue, final IValueChangeListener<ValueT> listener, final boolean fireInitial) {
		final IProperty<ValueT> prop = getPropertyFQN(qualifiedName, defaultValue);
		prop.addValueChangeListener(listener);
		if (fireInitial) {
			listener.valueChanged(new ValueChangeEvent<>(prop, null, prop.get()));
		}
		return prop.get();
	}


	default <ValueT> ValueT listen(final String localName, final ValueT defaultValue, final IValueChangeListener<ValueT> listener) {
		return listen(localName, defaultValue, listener, false);
	}
	default <ValueT> ValueT listen(final String localName, final ValueT defaultValue, final IValueChangeListener<ValueT> listener, final boolean fireInitial) {
		final IProperty<ValueT> prop = getProperty(localName, defaultValue);
		prop.addValueChangeListener(listener);
		if (fireInitial) {
			listener.valueChanged(new ValueChangeEvent<>(prop, defaultValue, prop.get()));
		}
		return prop.get();
	}

	default <ValueT> ValueT listen(final PropertyDescriptor<ValueT> desc, final IValueChangeListener<ValueT> listener) {
		return listen(desc, listener, false);
	}
	default <ValueT> ValueT listen(final PropertyDescriptor<ValueT> desc, final IValueChangeListener<ValueT> listener, final boolean fireInitial) {
		final IProperty<ValueT> prop = getProperty(desc);
		prop.addValueChangeListener(listener);
		if (fireInitial) {
			listener.valueChanged(new ValueChangeEvent<>(prop, desc.getInitialValueSupplier().get(), prop.get()));
		}
		return prop.get();
	}
}
