package io.gitlab.scholvac.prop.impl;

import java.util.Map;

import io.gitlab.scholvac.IDisposeable;
import io.gitlab.scholvac.prop.IProperty;

public class DelegateProperty<ValueT> implements IProperty<ValueT>{


	private IProperty<ValueT> mDelegate;

	public DelegateProperty(final IProperty<ValueT> prop) {
		mDelegate = prop;
	}

	protected IProperty<ValueT> getDelegate() {
		return mDelegate;
	}

	@Override
	public Map<String, Object> getAnnotations() {
		return mDelegate.getAnnotations();
	}

	@Override
	public <AnnotationType> void addAnnotation(final String key, final AnnotationType value) {
		mDelegate.addAnnotation(key, value);
	}

	@Override
	public <AnnotationType> AnnotationType getAnnotation(final String key) {
		return mDelegate.getAnnotation(key);
	}

	@Override
	public <AnnotationType> AnnotationType removeAnnotation(final String key) {
		return mDelegate.removeAnnotation(key);
	}

	@Override
	public IDisposeable addValueChangeListener(final IValueChangeListener<ValueT> listener) {
		return mDelegate.addValueChangeListener(listener);
	}

	@Override
	public void removeValueChangeListener(final IValueChangeListener<ValueT> listener) {
		mDelegate.removeValueChangeListener(listener);
	}

	@Override
	public String getName() {
		return mDelegate.getName();
	}

	@Override
	public String getDescription() {
		return mDelegate.getDescription();
	}

	@Override
	public boolean isEditable() {
		return mDelegate.isEditable();
	}

	@Override
	public ValueT get() {
		return mDelegate.get();
	}

	@Override
	public void set(final ValueT newValue) {
		mDelegate.set(newValue);
	}

	@Override
	public Class<ValueT> getType() {
		return mDelegate.getType();
	}

}
