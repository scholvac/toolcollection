package io.gitlab.scholvac.prop.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import io.gitlab.scholvac.QualifiedName;
import io.gitlab.scholvac.StringUtils;
import io.gitlab.scholvac.observable.ObservableSupport;
import io.gitlab.scholvac.observable.ObservableSupport.AbstractObservableSupport;
import io.gitlab.scholvac.prop.IProperty;
import io.gitlab.scholvac.prop.IPropertyContext;
import io.gitlab.scholvac.prop.PropertyDescriptor;
import io.gitlab.scholvac.prop.exceptions.PropertyCreationException;

public class PropertyContext implements IPropertyContext, AbstractObservableSupport {

	private final Map<String, IProperty>			mProperties = new HashMap<>();
	private final Map<String, IPropertyContext> 	mContexts = new HashMap<>();
	private transient ObservableSupport 			mObservableSupport;

	private final String							mName;
	private final IPropertyContext 					mParent;
	private final QualifiedName 					mQualifiedName;

	private String									mDescription = null;

	private Map<QualifiedName, PropertyDescriptor<?>> mAliasses = new HashMap<>();

	public PropertyContext() {
		this("");
	}
	public PropertyContext(final String name) {
		this(name, null);
	}
	protected PropertyContext(final String name, final IPropertyContext parent) {
		if (name == null)
			throw new IllegalArgumentException("Require a non null name");
		mParent = parent;
		mName = name;
		mQualifiedName = mParent != null ? mParent.getQualifiedName().append(name) : QualifiedName.from(name);
	}

	@Override
	public <ValueT> void addAlias(final PropertyDescriptor<ValueT> desc, final QualifiedName from) {
		mAliasses.put(from, desc);
	}
	@Override
	public <ValueT> IProperty<ValueT> getAliasOf(final QualifiedName qn) {
		final PropertyDescriptor<?> desc = mAliasses.get(qn.skipFirst());
		if (desc != null) {
			return (IProperty<ValueT>) getProperty(desc);
		}
		return null;
	}

	@Override
	public ObservableSupport getObservableSupport() {
		if (mObservableSupport == null) mObservableSupport  = new ObservableSupport<>();
		return mObservableSupport;
	}


	@Override
	public String toString() {
		final String contexts = String.join(", ", mContexts.keySet().stream().sorted().collect(Collectors.toList()));
		final String properties = String.join(", ", mProperties.keySet().stream().sorted().collect(Collectors.toList()));
		return "PropertyContext [Path = (" + getQualifiedName() + ") Contexts = ( " +contexts + ") Properties = (" + properties + ")]";
	}


	@Override
	public String getName() { return mName; }
	@Override
	public String getDescription() {return mDescription; }
	public void setDescription(final String desc) { mDescription = desc;}
	@Override
	public IPropertyContext getParent(){ return mParent;}
	@Override
	public QualifiedName getQualifiedName() { return mQualifiedName; }
	@Override
	public boolean hasContext(final String name) { return mContexts.containsKey(name); }

	@Override
	public IPropertyContext getContext(final QualifiedName path) {
		if (path == null || path.isEmpty()) throw new IllegalArgumentException("Expect a non empty path");
		if (path.equals(getQualifiedName()))
			return this;

		final String nextName = path.first();
		IPropertyContext next = null;

		if (nextName.equals(getName()))
			next = this;
		else {
			if (nextName.isEmpty())
				next = getRootContext();
			else
				next = mContexts.get(nextName);
			if (next == null) {
				next = createChildContext(nextName, this);
				mContexts.put(nextName, next);
				next.addValueChangeListener(this::fireValueChanged);
				fireValueChanged(null, next);
			}
		}
		if (path.size() == 1)
			return next;
		return next.getContext(path.skipFirst());
	}
	protected IPropertyContext createChildContext(final String nextName, final PropertyContext propertyContext) {
		return new PropertyContext(nextName, this);
	}
	@Override
	public IPropertyContext getRootContext() {
		return getParent() == null ? this : getParent().getRootContext();
	}



	@Override
	public <ValueT> boolean hasProperty(final PropertyDescriptor<ValueT> descriptor) {
		if (descriptor.getPath() != null) {
			return getContext(descriptor.getPath()).hasLocalProperty(descriptor);
		}
		return hasLocalProperty(descriptor);
	}
	@Override
	public boolean hasLocalProperty(final PropertyDescriptor<?> descriptor) {
		final String key = descriptor.getName();
		if (mProperties.containsKey(key) == false)
			return false;
		final IProperty<?> property = mProperties.get(key);
		return checkPropertyTypeAndReadable(property, descriptor);
	}

	@Override
	public boolean hasProperty(final String name) {
		return mProperties.containsKey(name);
	}
	@Override
	public boolean hasPropertyFQN(final QualifiedName qn) {
		final IPropertyContext ctx = qn.size() > 1 ? getContext(qn.skipLast()) : this;
		return ctx.hasProperty(qn.last());
	}

	@Override
	public <ValueT> IProperty<ValueT> getPropertyFQN(final QualifiedName name, final ValueT defaultValue) { //shortcut - fast access
		final IPropertyContext ctx = name.size() > 1 ? getContext(name.skipLast()) : this;
		return ctx.getLocalProperty(name.last(), defaultValue);
	}

	@Override
	public <ValueT> IProperty<ValueT> getProperty(final String name, final ValueT defaultValue){ //shortcut - fast access
		return getLocalProperty(name, defaultValue);
	}
	@Override
	public <ValueT> IProperty<ValueT> getLocalProperty(final String name, final ValueT defaultValue){ //shortcut - fast access
		final IProperty<ValueT> candidate = mProperties.get(name);
		if (candidate == null)
			return getOrCreateProperty(PropertyDescriptor.create(name, defaultValue), true);
		final Class<?> clazz = defaultValue != null ? defaultValue.getClass() : null;
		if (checkPropertyType(candidate, clazz) == false)
			throw new PropertyCreationException(candidate, PropertyDescriptor.create(name, defaultValue), "Failed to create property from descriptor a property with same name already exists but does not match the expectation");
		return candidate;
	}

	@Override
	public <ValueT> IProperty<ValueT> getProperty(final PropertyDescriptor<ValueT> descriptor) {
		return getOrCreateProperty(descriptor, true);
	}

	protected <ValueT> IProperty<ValueT> getOrCreateProperty(final PropertyDescriptor<ValueT> descriptor, final boolean forceCreation) {
		final QualifiedName path = descriptor.getPath();
		if (path != null && path.isEmpty() == false) {
			final IPropertyContext ctx = getContext(path);
			return ctx.getLocalProperty(descriptor);
		}
		return getLocalProperty(descriptor);
	}

	@Override
	public <ValueT> IProperty<ValueT> getLocalProperty(final PropertyDescriptor<ValueT> descriptor) {
		return getLocalProperty(descriptor, true);
	}
	public <ValueT> IProperty<ValueT> getLocalProperty(final PropertyDescriptor<ValueT> descriptor, final boolean forceCreation) {
		final IProperty<ValueT> aliasProp = getAliasProperty(descriptor);
		if (aliasProp != null)
			return aliasProp;
		synchronized (mProperties) {
			final IProperty<?> candidate = mProperties.get(descriptor.getName());
			if (candidate != null) {
				if (checkPropertyTypeAndReadable(candidate, descriptor) == false)
					throw new PropertyCreationException(candidate, descriptor, "Failed to create property from descriptor a property with same name already exists but does not match the expectation");
				final IProperty<ValueT> typedCandiate = (IProperty<ValueT>)candidate;

				assignDescription(typedCandiate, descriptor);
				combineAnnotations(typedCandiate, descriptor);
				return typedCandiate;
			}else {
				if (forceCreation == false)
					return null;
				final IProperty<ValueT> property = descriptor.createNewInstance();
				mProperties.put(property.getName(), property);
				property.addValueChangeListener(this::fireValueChanged);
				fireValueChanged(null, property);
				return property;
			}
		}
	}

	protected <ValueT> IProperty<ValueT> getAliasProperty(final PropertyDescriptor<ValueT> descriptor) {
		final QualifiedName qn = getQualifiedName().append(descriptor.getName());
		final IPropertyContext root = getRootContext();
		return root.getAliasOf(qn);
	}


	private static <ValueT> void assignDescription(final IProperty<ValueT> property, final PropertyDescriptor<ValueT> descriptor) {
		if (StringUtils.isNullOrEmpty(property.getDescription()))
			if (StringUtils.notNullOrEmpty(descriptor.getDescription()))
				if (property instanceof AbstractProperty)
					((AbstractProperty)property).lateAssignDescription(descriptor.getDescription());
	}
	private static <ValueT> void combineAnnotations(final IProperty<ValueT> property, final PropertyDescriptor<ValueT> descriptor) {
		final Map<String, Object> newAnnotations = descriptor.getAnnotations();
		if (newAnnotations != null && newAnnotations.isEmpty() == false) {
			for (final Entry<String, Object> newAnno : newAnnotations.entrySet()) {
				if (property.hasAnnotation(newAnno.getKey()) == false)
					property.annotate(newAnno.getKey(), newAnno.getValue());
			}
		}
	}
	private static boolean checkPropertyTypeAndReadable(final IProperty<?> candidate, final PropertyDescriptor<?> expectedDescriptor) {
		final Class<?> dType = expectedDescriptor.getValueType();
		if (dType != null) { //if it's null we accept it - it's not specified
			if (checkPropertyType(candidate, dType) == false)
				return false;
		}
		if (candidate == null || expectedDescriptor == null) throw new IllegalArgumentException("Expect a non null property and non null descriptor");
		final Boolean expEdt = expectedDescriptor.isEditable();
		if (expEdt != null) // null if not further specified
			if (candidate.isEditable() == false && expEdt)
				return false;
		return true;
	}

	/**
	 * Checks if the canditate match the expected type (<code>clazz</code>).
	 * @param candidate The property to check
	 * @param clazz The expected type or null. If null, each type is accepted.
	 * @return
	 */
	private static boolean checkPropertyType(final IProperty<?> candidate, final Class<?> clazz) {
		if (candidate == null) throw new IllegalArgumentException("Expect a non null property");
		if (clazz == null)
			return true; //null class allows everything
		final Class<?> cType = candidate.getType();
		if (cType == null) throw new NullPointerException("Could not match type of property " + candidate.toString());
		if ( false == cType.isAssignableFrom(clazz))
			return false;
		return true;
	}

	@Override
	public Stream<IProperty> getAllProperties(final boolean recursive) {
		Stream<IProperty> stream = mProperties.values().stream();
		if (recursive) {
			for (final IPropertyContext ctx : mContexts.values()) {
				stream = Stream.concat(stream, ctx.getAllProperties(recursive));
			}
		}
		return stream;
	}
	@Override
	public Stream<IPropertyContext> getAllContexts(final boolean recursive) {
		Stream<IPropertyContext> stream = mContexts.values().stream();
		if (recursive) {
			for (final IPropertyContext ctx : mContexts.values()) {
				stream = Stream.concat(stream, ctx.getAllContexts(recursive));
			}
		}
		return stream;
	}


}
