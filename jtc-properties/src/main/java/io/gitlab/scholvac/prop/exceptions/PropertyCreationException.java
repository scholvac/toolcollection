package io.gitlab.scholvac.prop.exceptions;

import io.gitlab.scholvac.prop.IProperty;
import io.gitlab.scholvac.prop.PropertyDescriptor;

public class PropertyCreationException extends PropertyException {

	private final PropertyDescriptor<?> mDescriptor;

	public PropertyCreationException(final IProperty<?> candidate, final PropertyDescriptor<?> descriptor, final String message) {
		this(candidate, descriptor, message, null);
	}

	public PropertyCreationException(final IProperty<?> candidate, final PropertyDescriptor<?> descriptor, final String message, final Throwable cause) {
		super(candidate, message, cause);
		mDescriptor = descriptor;
	}

	public PropertyDescriptor<?> getDescriptor() { return mDescriptor;}

}
