package io.gitlab.scholvac.prop.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import io.gitlab.scholvac.prop.IAnnotatedObject;

/**
 * Provides methods of {@link IAnnotatedObject} as external methods
 */
public class AnnotationSupport implements IAnnotatedObject {

	private Map<String, Object>				mAnnotations = null; //lazy initialized

	@Override
	public <AnnotationType> void addAnnotation(final String key, final AnnotationType value) {
		if (mAnnotations == null) mAnnotations = new HashMap<>();
		mAnnotations.put(key, value);
	}
	@Override
	public boolean hasAnnotation(final String key) {
		return mAnnotations != null && mAnnotations.containsKey(key);
	}
	@SuppressWarnings("unchecked")
	@Override
	public <AnnotationType> AnnotationType getAnnotation(final String key) {
		if (mAnnotations == null) return null;
		return (AnnotationType) mAnnotations.get(key);
	}
	@SuppressWarnings("unchecked")
	@Override
	public <AnnotationType> AnnotationType removeAnnotation(final String key) {
		if (mAnnotations == null) return null;
		return (AnnotationType) mAnnotations.remove(key);
	}
	@Override
	public Map<String, Object> getAnnotations() {
		if (mAnnotations == null) return new HashMap<>();
		return Collections.unmodifiableMap(mAnnotations);
	}


	/**
	 * Default implementation of IAnnotated object.
	 * <br>
	 * The interface reduces the IAnnotated object to an provider of an {@link AnnotationSupport} instance.
	 */
	public interface AbstractAnnotationSupport<ThisT extends IAnnotatedObject> extends IAnnotatedObject{
		AnnotationSupport getAnnotationSupport();

		@Override
		default Map<String, Object> getAnnotations() { return getAnnotationSupport().getAnnotations(); }
		@Override
		default <AnnotationType> void addAnnotation(final String key, final AnnotationType value) { getAnnotationSupport().addAnnotation(key, value);}
		@Override
		default <AnnotationType> AnnotationType getAnnotation(final String key) { return getAnnotationSupport().getAnnotation(key);}
		@Override
		default <AnnotationType> AnnotationType removeAnnotation(final String key) { return getAnnotationSupport().removeAnnotation(key);}
	}



}