package io.gitlab.scholvac.prop.io;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import io.gitlab.scholvac.prop.IPropertyContext;
import io.gitlab.scholvac.prop.impl.PropertyContext;
import io.gitlab.scholvac.prop.io.impl.PropertyFormat;
import io.gitlab.scholvac.prop.io.impl.SerializeableFormat;
import io.gitlab.scholvac.resources.FileUtils;
import io.gitlab.scholvac.resources.ResourceManager;
import io.gitlab.scholvac.service.IService;
import io.gitlab.scholvac.service.ServiceManager;
import io.gitlab.scholvac.service.impl.ExtensionPointSupport;
import io.gitlab.scholvac.service.impl.ExtensionPointSupport.AbstractExtensionPointSupport;

public class PropertyIO implements IService, AbstractExtensionPointSupport<IPropertyFileFormat> {

	public static PropertyIO get() { return ServiceManager.get(PropertyIO.class); }

	private static class PropertyFile {
		final String		key;
		boolean 			readOnly;
		boolean 			saveOnExit;
		IPropertyContext	store;

		public PropertyFile(final String key, final IPropertyContext store, final boolean readOnly2) {
			this.key = key;
			this.store = store;
			this.readOnly = readOnly2;
		}
	}


	private final ExtensionPointSupport<IPropertyFileFormat> 	mExtensionSupport = new ExtensionPointSupport<>();

	private final Map<String, PropertyFile> 				mLoadedContexts = new HashMap<>();


	public PropertyIO() { this(true);}
	public PropertyIO(final boolean initDefaults) {
		if (initDefaults) {
			register(new PropertyFormat());
			register(new SerializeableFormat());
		}
	}

	@Override
	public ExtensionPointSupport<IPropertyFileFormat> getExtensionPointSupport() { return mExtensionSupport; }



	public IPropertyContext load(final File file) throws IOException {
		return load(file, false, false);
	}
	public IPropertyContext load(final File file, final boolean readOnly) throws IOException {
		return load(file, false, readOnly);
	}
	public IPropertyContext load(final File file, final boolean saveOnExit, final boolean readOnly) throws IOException {
		final PropertyFile pf = getPropertyFile(file.getCanonicalPath(), readOnly);
		if (saveOnExit && pf.saveOnExit == false) { //we are the first to request save on exit
			Runtime.getRuntime().addShutdownHook(new Thread(()-> {
				try {
					save(pf.store, file);
				} catch (final IOException e) {
					e.printStackTrace();
				}
			}));
			pf.saveOnExit = true;
		}
		return pf.store;
	}

	private PropertyFile getPropertyFile(final String resource, final boolean readOnly) {
		final String key = readOnly ? resource + "_ro" : resource;
		final PropertyFile pf = mLoadedContexts.computeIfAbsent(key, r -> {
			final ResourceManager rmgr = ResourceManager.get();
			final URL url = rmgr.getResource(resource);
			IPropertyContext ctx = null;
			if (url == null) {
				ctx = new PropertyContext();
			}else {
				final String extension = FileUtils.getFileExtension(resource);//get the part after the last dot...
				try {
					ctx = load(extension, url.openStream());
				} catch (final IOException e) {
					e.printStackTrace();
					return null;
				}
			}
			return new PropertyFile(resource, ctx, readOnly);
		});
		return pf;
	}




	public IPropertyContext load(final String ecodingHint, final InputStream stream) {
		for (final IPropertyFileFormat format : getExtensions()) {
			if (format.supports(ecodingHint)) {
				try {
					return format.load(stream);
				}catch(final Exception e) {
					e.printStackTrace(); //do nothing, just try the next one
				}
			}
		}
		return null;
	}

	public void save(final IPropertyContext context, final File file) throws IOException {
		final FileOutputStream stream = new FileOutputStream(file);
		final String extension = FileUtils.getFileExtension(file);
		save(context, extension, stream);
	}

	public void save(final IPropertyContext context, final String encodingHint, final OutputStream stream) throws IOException {
		for (final IPropertyFileFormat format : getExtensions()) {
			if (format.supports(encodingHint)) {
				format.write(context, stream);
				return ;
			}
		}
		throw new UnsupportedOperationException("Could not find a valid property format for encoding: " + encodingHint);
	}
}
