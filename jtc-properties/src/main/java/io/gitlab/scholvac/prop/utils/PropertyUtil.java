package io.gitlab.scholvac.prop.utils;

import io.gitlab.scholvac.IDisposeable;
import io.gitlab.scholvac.prop.IProperty;
import io.gitlab.scholvac.prop.PropertyDescriptor;
import io.gitlab.scholvac.prop.dec.ReadonlyPropertyDecorator;

public class PropertyUtil {

	public static <ValueT> PropertyDescriptor<ValueT> getDescriptor(final IProperty<ValueT> prop) {
		final PropertyDescriptor<ValueT> desc = PropertyDescriptor.create(prop.getName(), prop.getType())
				.defaultValue(prop.get())
				.description(prop.getDescription())
				.editable(prop.isEditable());
		prop.getAnnotations().entrySet().forEach(it -> desc.annotate(it.getKey(), it.getValue()));
		return desc;
	}

	public static <ValueT> IProperty<ValueT> decorateReadOnly(final IProperty<ValueT> property) {
		return ReadonlyPropertyDecorator.decorate(property);
	}


	public static <ValueT> IDisposeable bind(final IProperty<ValueT> from, final IProperty<ValueT> to) {
		to.	set(from.get());
		return from.addValueChangeListener(pcl -> to.set(from.get()));
	}

	public static <ValueT> IProperty<ValueT> copy(final IProperty<ValueT> original) {
		final PropertyDescriptor<ValueT> desc = getDescriptor(original);
		return desc.createNewInstance();
	}

}
