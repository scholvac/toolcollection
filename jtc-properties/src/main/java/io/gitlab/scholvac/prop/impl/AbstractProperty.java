package io.gitlab.scholvac.prop.impl;

import java.util.Objects;

import io.gitlab.scholvac.IDisposeable;
import io.gitlab.scholvac.observable.IObservableObject;
import io.gitlab.scholvac.observable.ObservableSupport;
import io.gitlab.scholvac.observable.ObservableSupport.AbstractObservableSupport;
import io.gitlab.scholvac.prop.IProperty;
import io.gitlab.scholvac.prop.impl.AnnotationSupport.AbstractAnnotationSupport;

public abstract class AbstractProperty<ValueT> implements IProperty<ValueT>, AbstractAnnotationSupport, AbstractObservableSupport<ValueT>
{
	private final String							mName;
	private String	 								mDescription;
	private	boolean									mEditable;

	// The following variables will NOT be saved, they are program dependend and will be added during runtime and with support of
	// the PropertyDescriptor
	private transient final boolean 				mObserveComplexValues = true; //for now final...
	private transient ObservableSupport<ValueT> 	mObservableSupport;
	private transient AnnotationSupport				mAnnotationSupport;
	private transient IDisposeable 					mComplexValueDisposable;

	protected AbstractProperty(final String name) {
		this(name, null, true);
	}

	public AbstractProperty(final String name, final String desc, final boolean editable) {
		mName = name;
		mDescription = desc;
		mEditable = editable;
	}

	@Override
	public ObservableSupport<ValueT> getObservableSupport() {
		if (mObservableSupport == null)
			mObservableSupport = new ObservableSupport<>();
		return mObservableSupport;
	}

	@Override
	public AnnotationSupport getAnnotationSupport() {
		if (mAnnotationSupport == null)
			mAnnotationSupport = new AnnotationSupport();
		return mAnnotationSupport;
	}

	@Override
	public String getName() {
		return mName;
	}

	@Override
	public String getDescription() {
		return mDescription;
	}
	void lateAssignDescription(final String desc) { setDescription(desc);} //used by PropertyContextImpl to late fill in a description if it does not yet exists.
	protected void setDescription(final String description) {
		mDescription = description;
	}

	@Override
	public boolean isEditable() {
		return mEditable;
	}


	@Override
	public void set(final ValueT newValue) {
		if (isEditable() == false)
			throw new UnsupportedOperationException("Property " + getName() + " is not editable");

		if (isSetRequired(newValue) == false)
			return ; //nothing to do...

		uncheckedSet(newValue);
	}

	protected void uncheckedSet(final ValueT newValue) {
		final ValueT oldValue = get();
		internalSetValue(newValue);
		observeComplexValues(newValue);
		fireValueChanged(oldValue, newValue);
	}

	protected void observeComplexValues(final ValueT newValue) {
		IDisposeable.dispose(mComplexValueDisposable);
		if (mObserveComplexValues && newValue != null) {
			if (newValue instanceof IObservableObject) {
				mComplexValueDisposable = ((IObservableObject) newValue).addValueChangeListener( evt -> fireValueChanged(null, newValue));
			}
		}
	}

	/**
	 * persists the new value. This Method does not need to do any (property related) checks, they have been done from the caller.
	 * Performed Checks:
	 * <ul>
	 * <li> {@link #isEditable()} : The property is not readonly
	 * <li> {@link #isSetRequired(Object)} : The new value differs from the currently set value
	 * </ul>
	 * Additionally no notification need to be done, thats done by the caller as well.
	 * @param newValue The value to persist
	 */
	protected abstract void internalSetValue(ValueT newValue);

	protected boolean isSetRequired(final ValueT newValue) {
		return Objects.equals(get(), newValue) == false;
	}
}
