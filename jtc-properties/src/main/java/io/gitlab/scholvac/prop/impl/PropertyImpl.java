package io.gitlab.scholvac.prop.impl;

import io.gitlab.scholvac.prop.IProperty;
import io.gitlab.scholvac.validate.IValidator;
import io.gitlab.scholvac.validate.IValidator.ExceptionReporter;
import io.gitlab.scholvac.validate.IValidator.SEVERITY;
import io.gitlab.scholvac.validate.IValidator.ValidationResult;
import io.gitlab.scholvac.validate.RuntimeCheck;

public class PropertyImpl<ValueT> extends AbstractProperty<ValueT> implements IProperty<ValueT> {

	private ValueT 						mValue;
	private Class<ValueT>				mValueType;
	private RuntimeCheck<ValueT>		mValidator = null;

	public PropertyImpl(final String name, final String desc, final boolean editable, final ValueT value, final Class<ValueT> valueType) {
		super(name, desc, editable);
		mValueType = valueType;

		if (mValueType == null) {
			if (value != null)
				mValueType = (Class<ValueT>) value.getClass();
			else
				mValueType = (Class<ValueT>) Object.class;
		}
		uncheckedSet(value);
	}


	@Override
	public ValueT get() {
		return mValue;
	}
	@Override
	public Class<ValueT> getType() {
		return mValueType;
	}



	@Override
	protected void internalSetValue(final ValueT newValue) {
		mValue = newValue;
	}

	public void checkValidator(final ValueT newValue) {
		final ValidationResult result = evaluateNewValue(newValue);
		if (result != null) {
			IValidator.report(getValidationReporter(newValue), result);
		}
	}

	protected <ValidationReporterT extends ExceptionReporter> ExceptionReporter  getValidationReporter(final ValueT newValue) {
		return new ExceptionReporter(reason -> new UnsupportedOperationException("Property " + getName() + " did not accept value: " + newValue + ". Reason: " + reason));
	}

	public boolean isValidValue(final ValueT newValue) {
		return evaluateNewValue(newValue) == null;
	}
	public ValidationResult evaluateNewValue(final ValueT newValue) {
		if (mValidator == null)
			return null;
		final ValidationResult result = mValidator.evaluate(newValue);
		if (result.hasOrWorse(getDenialSeverity()))
			return result;
		return null;
	}

	protected SEVERITY getDenialSeverity() {return SEVERITY.ERROR; }
}
