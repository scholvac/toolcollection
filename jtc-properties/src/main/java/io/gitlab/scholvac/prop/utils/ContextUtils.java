package io.gitlab.scholvac.prop.utils;

import java.util.function.BiConsumer;
import java.util.stream.Stream;

import io.gitlab.scholvac.QualifiedName;
import io.gitlab.scholvac.prop.IProperty;
import io.gitlab.scholvac.prop.IPropertyContext;
import io.gitlab.scholvac.prop.PropertyDescriptor;

public class ContextUtils {

	public static void merge(final IPropertyContext addon, final IPropertyContext target) {
		final QualifiedName basePath = target.getQualifiedName();
		Stream.concat(Stream.of(addon), addon.getAllContexts(true))
		.forEach(ctx -> {
			final QualifiedName path = (basePath.isEmpty() || basePath.first().isEmpty()) ? ctx.getQualifiedName() : basePath.append(ctx.getQualifiedName());
			ctx.getAllProperties(false).forEach(prop -> {
				final PropertyDescriptor desc = PropertyUtil.getDescriptor(prop);
				desc.path(path);
				target.set(desc, prop.get());
			});
		});
	}

	public static void iterateAll(final IPropertyContext context, final BiConsumer<IPropertyContext, IProperty> consumer) {
		Stream.concat(Stream.of(context), context.getAllContexts(true)).forEach((ctx -> {
			ctx.getAllProperties(false).forEach(prop -> consumer.accept(ctx, prop));
		}));
	}

	public static boolean isEmpty(final IPropertyContext ctx) {
		final IProperty first = ctx.getAllProperties(true).findFirst().orElse(null);
		return first == null;
	}
}
