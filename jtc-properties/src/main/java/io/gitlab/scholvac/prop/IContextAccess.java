package io.gitlab.scholvac.prop;

import java.util.stream.Stream;

import io.gitlab.scholvac.QualifiedName;

public interface IContextAccess {

	/**
	 * @return The qualified name of the context, that is the qualified name of it's parent context combined with it's name.
	 */
	QualifiedName getQualifiedName();

	boolean hasContext(final String name);

	/**
	 * @param name The name of the {@link IPropertyContext}
	 * @return Either an existing {@link IPropertyContext} or creates a new
	 * context with the given name.
	 */
	IPropertyContext getContext(final QualifiedName name);

	IPropertyContext getParent();
	IPropertyContext getRootContext();

	/**
	 * Returns a stream of all (child) contexts of this property context and may all its child contexts.
	 * @param recursive Whether the stream shall contain the {@link IPropertyContext}'s of the child contexts or not
	 * @return A stream that contains all {@link IPropertyContext}'s of this and may all child contexts.
	 */
	Stream<IPropertyContext> getAllContexts(boolean recursive);
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	/**
	 * Get or create a child property context with the given name.
	 * @param names List of {@link QualifiedName} segments for the child context
	 * @return a not null property context
	 */
	default IPropertyContext getContext(final String ...names) { return getContext(QualifiedName.from(names));}
}
