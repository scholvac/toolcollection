package io.gitlab.scholvac.prop;

import java.io.Serializable;
import java.util.Comparator;

import io.gitlab.scholvac.IDisposeable;
import io.gitlab.scholvac.observable.IObservableObject;
import io.gitlab.scholvac.prop.impl.PropertyImpl;

public interface IProperty<ValueT> extends IAnnotatedObject, IObservableObject<ValueT>, Serializable{
	Comparator<IProperty<?>> NAME_COMPARATOR = (a, b) -> a.getName().compareTo(b.getName());

	String getName();
	String getDescription();
	boolean isEditable();

	ValueT get();
	void set(ValueT newValue);

	Class<ValueT> getType();





	/**
	 * Create a new editable Property with a given value, name and description are null
	 * @param <ValueT>
	 * @param value The initial value of the property
	 * @return a new IProperty instance
	 */
	static <ValueT> IProperty<ValueT> of(final ValueT value) {
		return of(null, null, true, value, null);
	}
	/**
	 * Create a new editable Property with a given value, name and description are null
	 * @param <ValueT>
	 * @param value The initial value of the property
	 * @return a new IProperty instance
	 */
	static <ValueT> IProperty<ValueT> of(final Class<ValueT> clazz) {
		return of(null, null, true, null, clazz);
	}
	/**
	 * Create a new editable Property with a given value, name and description are null
	 * @param <ValueT>
	 * @param value The initial value of the property
	 * @param editable whether the property can be changed or not
	 * @return a new IProperty instance
	 */
	static <ValueT> IProperty<ValueT> of(final ValueT value, final boolean editable) {
		return of(null, null, editable, value, null);
	}
	/**
	 * Create a new editable Property with a given value and name. The description is null
	 * @param <ValueT>
	 * @param name	name of the property
	 * @param value initial value of the property
	 * @return a new property instance
	 */
	static <ValueT> IProperty<ValueT> of(final String name, final ValueT value) {
		return of(name, null, true, value, null);
	}

	/**
	 * Create a new editable Property with a given value and name. The description is null
	 * @param <ValueT>
	 * @param name	name of the property
	 * @param editable whether the property can be changed or not
	 * @param value the initial value
	 * @return a new property instance
	 */
	static <ValueT> IProperty<ValueT> of(final String name, final boolean editable, final ValueT value) {
		return of(name, null, editable, value, null);
	}

	/**
	 * Create a new editable Property with a given value and name. The description is null
	 * @param <ValueT>
	 * @param name	name of the property
	 * @param editable whether the property can be changed or not
	 * @param value the initial value
	 * @return a new property instance
	 */
	static <ValueT> IProperty<ValueT> of(final String name, final String description, final boolean editable, final ValueT value) {
		return of(name, description, editable, value, null);
	}
	/**
	 * Create a new (editable) property with a given data type, value, name and description are null
	 * @param clazz, the data type
	 * @return a new property instance
	 */
	static <ValueT> IProperty<ValueT> ofType(final Class<ValueT> clazz) {
		return of(null, null, true, null, clazz);
	}

	static <ValueT> IProperty<ValueT> of(final String name, final String desc, final boolean  editable, final ValueT value, final Class<ValueT> type) {
		return new PropertyImpl<>(name, desc, editable, value, type);
	}


	default IDisposeable addValueChangeListener(final IValueChangeListener<ValueT> listener, final boolean fireInitial) {
		final IDisposeable disp = addValueChangeListener(listener);
		if (fireInitial) {
			listener.valueChanged(new ValueChangeEvent<>(this, null, get()));
		}
		return disp;
	}
}
