package io.gitlab.scholvac.prop;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import io.gitlab.scholvac.log.ILogger;
import io.gitlab.scholvac.log.SLog;
import io.gitlab.scholvac.service.IService;
import io.gitlab.scholvac.service.ServiceManager;

public class PropertyDecomposer implements IService {
	private static final ILogger LOG = SLog.getLogger(PropertyDecomposer.class);

	public static PropertyDecomposer get() { return ServiceManager.get(PropertyDecomposer.class); }

	public interface IPropertyDecomposer {
		Collection<IProperty<?>> decompose(final Object value);

		/** @note: dublicated names will be ignored */
		default Map<String, IProperty<?>> getNameMap(final Collection<IProperty<?>> col){
			if (col == null || col.isEmpty()) return new HashMap<>();
			return col.stream().collect(Collectors.toMap(IProperty::getName, it -> it, (m1, m2) -> m1));
		}
	}

	private final List<IPropertyDecomposer> 	mProviderList = new LinkedList<>();

	public void addProvider(final IPropertyDecomposer provider) {
		if (mProviderList != null && !mProviderList.contains(provider))
			mProviderList.add(provider);
	}
	public void removeProvider(final IPropertyDecomposer provider) {
		mProviderList.remove(provider);
	}

	public List<IProperty<?>> listProperties(final Object value, final Comparator<IProperty<?>> comparator){
		final List<IProperty<?>> list = decompose(value).collect(Collectors.toList());
		list.sort(comparator);
		return list;
	}
	public List<IProperty<?>> listProperties(final Object value) {
		return decompose(value).collect(Collectors.toList());
	}


	public Stream<IProperty<?>> decompose(final Object value){
		if (value == null) return Stream.empty();
		if (value instanceof IPropertyDecomposer) {
			final Collection<IProperty<?>> decomp = ((IPropertyDecomposer)value).decompose(value);
			if (decomp == null)
				return null;
			return decomp.stream();
		}
		return mProviderList.stream()
				.map(provider -> {
					try {
						return provider.decompose(value);
					}catch(final Exception e) {
						e.printStackTrace();
						LOG.error("Property Provider: " + provider + " failed with exception", e);
					}
					return null;
				})
				.filter(it -> it != null)
				.flatMap(Collection::stream);
	}

}

