package io.gitlab.scholvac;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import io.gitlab.scholvac.log.ILogger;
import io.gitlab.scholvac.log.SLog;
import io.gitlab.scholvac.rcore.RClassifier;
import io.gitlab.scholvac.rcore.RClassifier.RClass;
import io.gitlab.scholvac.rcore.RClassifier.RComboundClassifier;
import io.gitlab.scholvac.rcore.RClassifier.REnum;
import io.gitlab.scholvac.rcore.RClassifier.RInterface;
import io.gitlab.scholvac.rcore.RContainer;
import io.gitlab.scholvac.rcore.RObject;
import io.gitlab.scholvac.service.IService;
import io.gitlab.scholvac.service.ServiceManager;

public class ReflectionManager implements IService {
	private static final ILogger				LOG = SLog.getLogger(ReflectionManager.class);

	public static ReflectionManager get() { return ServiceManager.get(ReflectionManager.class);}


	private final Map<String, RClassifier> 					mNameMap = new HashMap<>();
	private final Map<Class<?>, RClassifier>	 			mClazzMap = new HashMap<>();

	private final Map<Class<?>, RClassifier>				mClazzLookup = new HashMap<>();
	private final Map<String, RClassifier>					mComboundClassifiers = new HashMap<>();

	public ReflectionManager() {
		register(RObject.class, RContainer.class);

		register(TimeUnit.class);
	}


	public boolean containsClassifier(final Class<?> clazz) {
		return mClazzLookup.containsKey(clazz);
	}

	public RClassifier getClassifier(final Object value) {
		if (value == null)
			return null;
		return getClassifier(value.getClass());
	}

	public RClassifier getClassifier(final Class<?> clazz) {
		if (clazz == null)
			return null;
		if (mClazzLookup.containsKey(clazz))
			return mClazzLookup.get(clazz);

		final Set<RClassifier> set = new HashSet<>();
		findClassifierByClass(clazz, set);

		RClassifier result = null;
		if (set.isEmpty()) {
			result = mClazzMap.get(RObject.class);
		}else if (set.size() == 1) {
			result = set.iterator().next();
		}else {
			final String key = RComboundClassifier.key(set);
			if (mComboundClassifiers.containsKey(key)) {
				result = mComboundClassifiers.get(key);
			}else
				mComboundClassifiers.put(key, result = new RComboundClassifier(set));
		}
		mClazzLookup.put(clazz, result);
		return result;
	}

	public void register(final Class<?> ...classes) {
		for (final Class<?> cl : classes){
			if (cl.isInterface())
				register(new RInterface(cl));
			else if (cl.isEnum())
				register(new REnum(cl));
			else
				register(new RClass(cl));
		}
	}
	public void registerConstructor(final Class<?> cl, final Supplier<?> supplier) {
		final RClassifier classifier = getClassifier(cl);
		registerConstructor(classifier, supplier);
	}
	public static void registerConstructor(final RClassifier cl, final Supplier<?> supplier) {
		cl.setConstructor(supplier);
	}
	public static void removeConstructor(final RClassifier cl) {
		cl.setConstructor(null);
	}

	public static Object createNewInstance(final RClassifier cl) {
		return cl.createNewInstance();
	}

	private void register(final RClassifier cl) {
		LOG.debug("Register " + cl);
		mNameMap.put(cl.getName(), cl);
		mClazzMap.put(cl.getNative(), cl);

		mClazzLookup.clear();
	}
	private void findClassifierByClass(final Class<?> clazz, final Set<RClassifier> classifiers) {
		if (clazz == null)
			return ;
		if (mClazzMap.containsKey(clazz))
			classifiers.add(mClazzMap.get(clazz));

		findClassifierByClass(clazz.getSuperclass(), classifiers);
		for (final Class<?> interf : clazz.getInterfaces()) {
			findClassifierByClass(interf, classifiers);
		}
	}

	public List<RClassifier> getInstantiableClasses(final Class<?> type) {
		for (final RClassifier v : mClazzMap.values()) {
			if (!canInstantiate(v) || !v.inherits(type))
				continue;
		}
		return mClazzMap.values().stream()
				.filter(this::canInstantiate)
				.filter(rcl -> rcl.inherits(type))
				.collect(Collectors.toList());
	}

	public Stream<RClassifier> getClassifierStream(){
		return mClazzMap.values().stream();
	}

	public boolean canInstantiate(final RClassifier rcl) {
		return rcl.canInstantiate();
	}
}
