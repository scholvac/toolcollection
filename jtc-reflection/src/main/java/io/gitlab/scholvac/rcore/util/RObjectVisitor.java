package io.gitlab.scholvac.rcore.util;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import io.gitlab.scholvac.ReflectionManager;
import io.gitlab.scholvac.rcore.RClassifier;
import io.gitlab.scholvac.rcore.RClassifier.RField;

public class RObjectVisitor {

	public interface IRVisitor{
		void enter(Object obj, RClassifier cl);

		boolean shallVisitField(RClassifier cl, RField field, Object fieldValue);

		void leave(Object obj, RClassifier cl);
	}

	public abstract static class RVisitorAdapter implements IRVisitor{

		@Override
		public void enter(final Object obj, final RClassifier cl) {
			// TODO Auto-generated method stub

		}
		@Override
		public boolean shallVisitField(final RClassifier cl, final RField field, final Object fieldValue) {
			return fieldValue != null;
		}

		@Override
		public void leave(final Object obj, final RClassifier cl) {
		}
	}


	public static void visit(final Object obj, final IRVisitor visitor) {
		final RClassifier cl = ReflectionManager.get().getClassifier(obj);
		if (cl != null)
			visit(obj, cl, visitor, new HashSet<>());
	}
	private static void visit(final Object obj, final IRVisitor visitor, final Set<Object> alreadyVisited) {
		if (obj == null || alreadyVisited.contains(obj))
			return ;
		if (obj instanceof Collection) {
			final Collection collection = (Collection)obj;
			for (final Object child : collection) {
				visit(child, visitor, alreadyVisited);
			}
		}else {
			final RClassifier cl = ReflectionManager.get().getClassifier(obj);
			if (cl != null)
				visit(obj, cl, visitor, alreadyVisited);
		}
	}

	private static void visit(final Object obj, final RClassifier cl, final IRVisitor visitor, final Set<Object> alreadyVisited) {
		visitor.enter(obj, cl);

		final Collection<RField> fields = cl.getAllFields();
		for (final RField field : fields) {
			final Object fieldValue = field.get(obj);
			if (visitor.shallVisitField(cl, field, fieldValue)) {
				if (fieldValue != null) {
					visit(fieldValue, visitor, alreadyVisited);
				}
			}
		}

		visitor.leave(obj, cl);
	}


	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//						Tools
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public static <T> Collection<T> collectInstances(final Object root, final Class<T> clazz){
		final Set<T> out = new HashSet<>();
		final IRVisitor visitor = new RVisitorAdapter() {
			@Override
			public void enter(final Object obj, final RClassifier cl) {
				final Class<?> objClazz = obj.getClass();
				if (clazz.isAssignableFrom(objClazz))
					out.add(clazz.cast(obj));
			}
		};
		visit(root, visitor);
		return out;
	}
}
