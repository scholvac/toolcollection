package io.gitlab.scholvac.rcore;

import java.util.ArrayList;
import java.util.List;

import io.gitlab.scholvac.anno.uml.UMLName;

public class RContainer extends RObject {

	@UMLName(name = "children", description = "generic list of children, to build up a model")
	List<Object>		mChildren;

	public List<Object> getChildren() { if (mChildren == null) mChildren = new ArrayList<>(); return mChildren;}

}
