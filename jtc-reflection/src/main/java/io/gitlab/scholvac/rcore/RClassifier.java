package io.gitlab.scholvac.rcore;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import io.gitlab.scholvac.AnnotationUtil;
import io.gitlab.scholvac.ReflectionManager;
import io.gitlab.scholvac.ReflectionUtils;
import io.gitlab.scholvac.exceptions.ReflectionsException;
import io.gitlab.scholvac.rcore.RCore.RAnnotatedElement;

public abstract class RClassifier extends RAnnotatedElement {


	enum RClassifierType {
		CLASS, INTERFACE, ENUM, COMBOUND
	}

	protected final RClassifierType 		mType;
	protected final Class<?> 				mNative;
	protected String 						mName;

	protected LinkedHashMap<String, RField> mAllFields;
	protected LinkedHashMap<String, RField>	mOwnFields;
	private List<RClassifier> 				mOwnParents;

	private IConstructor<?> 				mConstructor;

	protected RClassifier(final RClassifierType type, final Class<?> clazz) {
		mType = type;
		mNative = clazz;
		AnnotationUtil.checkForAnnotations(clazz, this);
	}

	public Class<?> getNative() {
		return mNative;
	}

	public String getName() {
		if (mName == null) {
			mName = AnnotationUtil.getName(mNative);
			if (mName == null)
				mName = getNative().getSimpleName();
		}
		return mName;
	}

	@Override
	public String toString() {
		return mType + "(" + getName() + ")";
	}
	public Collection<RField> getAllFields(){return _getAllFields().values();}
	private LinkedHashMap<String, RField> _getAllFields() {
		if (mAllFields == null)
			collectFields();
		return mAllFields;
	}


	public RField getField(final String fieldName) {
		if (mAllFields == null)
			collectFields();
		return mAllFields.get(fieldName);
	}

	public Object get(final Object instance, final String fieldName) {
		final RField field = getField(fieldName);
		if (field != null)
			return field.get(instance);
		return null;
	}

	public boolean set(final Object instance, final String fieldName, final Object value) {
		final RField field = getField(fieldName);
		if (field == null)
			return false;
		return field.set(instance, value);
	}






	public interface ISetter {
		boolean set(Object instance, final Object value);
	}
	public interface IGetter {
		Object get(final Object instance);
	}
	static class FieldSetterGetter implements ISetter, IGetter {

		private final Field mField;

		public FieldSetterGetter(final Field f) {
			mField = f;
			mField.setAccessible(true);
		}

		@Override
		public Object get(final Object instance) {
			try {
				return mField.get(instance);
			} catch (IllegalArgumentException | IllegalAccessException e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		public boolean set(final Object instance, final Object value) {
			try {
				mField.set(instance, value);
				return true;
			} catch (IllegalArgumentException | IllegalAccessException e) {
				e.printStackTrace();
			}
			return false;
		}
	}

	static class MethodGetter implements IGetter {
		private final Method mMethod;
		public MethodGetter(final Method m) {
			mMethod = m;
		}
		@Override
		public Object get(final Object instance) {
			try {
				return mMethod.invoke(instance, null);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				e.printStackTrace();
			}
			return null;
		}
	}

	static class MethodSetter implements ISetter {
		private final Method mMethod;
		public MethodSetter(final Method m) {
			mMethod = m;
		}
		@Override
		public boolean set(final Object instance, final Object value) {
			try {
				mMethod.invoke(instance, value);
				return true;
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				e.printStackTrace();
			}
			return false;
		}
	}

	private void collectFields() {
		mOwnFields = collectOwnFields();
		final LinkedHashMap<String, RField> allFields = new LinkedHashMap<>(mOwnFields);
		for (final RClassifier parent : getOwnParents()) {
			if (parent == null || parent == this)
				continue;
			final Map<String, RField> paf = parent._getAllFields();
			if (paf != null)
				allFields.putAll(paf);
		}
		mAllFields = allFields;
	}


	private Collection<RClassifier> getOwnParents() {
		if (mOwnParents == null) {
			mOwnParents = collectOwnParents();
		}
		return mOwnParents;
	}

	protected List<RClassifier> collectOwnParents() {
		final ReflectionManager rMgr = ReflectionManager.get();
		final List<RClassifier> out = new ArrayList<>();
		final RClassifier superClass = rMgr.getClassifier(mNative.getSuperclass());
		if (superClass != null)
			out.add(superClass);
		for (final Class<?> p : mNative.getInterfaces()) {
			final RClassifier rinterf = rMgr.getClassifier(p);
			if (rinterf != null && out.contains(rinterf) == false)
				out.add(rinterf);
		}
		return out;
	}
	protected LinkedHashMap<String, RField> collectOwnFields() {
		final LinkedHashMap<String, RField> out = new LinkedHashMap<>();
		final Field[] fields = mNative.getDeclaredFields();
		for (final Field f : fields) {

			if (AnnotationUtil.ignore(f))
				continue;
			final int modifiers = f.getModifiers();
			if (Modifier.isStatic(modifiers))
				continue;
			final String name = AnnotationUtil.getName(f, f.getName());
			final String desc = AnnotationUtil.getDescription(f, null);

			ISetter setter = null;
			IGetter getter = null;
			if (Modifier.isPublic(modifiers)) {
				final FieldSetterGetter psg = new FieldSetterGetter(f);
				getter = psg;
				setter = psg;
			}else {
				final Method getterMethod = ReflectionUtils.findFirstMethod(mNative, buildNames(name, "get", "is", ""), f.getType(), null);
				if (getterMethod != null){
					getter = new MethodGetter(getterMethod);

					final Method setterMethod = ReflectionUtils.findFirstMethod(mNative, buildNames(name, "set", ""), void.class, f.getType());
					if (setterMethod != null)
						setter = new MethodSetter(setterMethod);
				}else {
					//if the fieldAccessible flag is set to true, we get allow access as well
					if (AnnotationUtil.isFieldAccessible(f, false)) {
						final FieldSetterGetter psg = new FieldSetterGetter(f);
						getter = psg;
						setter = psg;
					}
				}
			}

			if (getter == null)
				continue;

			final RField rf = new RField(name, f, desc, setter, getter);
			AnnotationUtil.checkForAnnotations(f, rf);

			out.put(name, rf);
		}
		return out;
	}

	static String[] buildNames(final String _name, final String ...prefixes) {
		final String[] result = new String[prefixes.length];
		//check for ungary notation, e.g. mName -> name
		String firstUpperName = _name;
		if (firstUpperName.charAt(0) == 'm' && Character.isUpperCase(firstUpperName.charAt(1)))
			firstUpperName = firstUpperName.substring(1);
		else
			firstUpperName = Character.toUpperCase(firstUpperName.charAt(0)) + firstUpperName.substring(1);
		for (int i = 0; i < result.length; i++) {
			result[i] = prefixes[i] + firstUpperName;
			if (prefixes[i].isEmpty())
				result[i] = _name;
		}
		return result;
	}

	public interface IConstructor<T> {
		default boolean canInstantiate() { return true;}
		Object createNewInstance();
	}
	public static class SupplierConstructor<T> implements IConstructor<T> {
		private final Supplier<T> 			mSupplier;
		public SupplierConstructor(final Supplier<T> s) { mSupplier = s; }
		@Override
		public boolean canInstantiate() { return mSupplier != null; }
		@Override
		public Object createNewInstance() { return mSupplier.get(); }
	}
	public static class NonInstantiatableConstructor<T> implements IConstructor<T> {
		@Override
		public boolean canInstantiate() { return false; }
		@Override
		public Object createNewInstance() { return null; }
	}
	public static class DefaultNonArgConstructor<T> implements IConstructor<T> {
		private final Constructor<T> 	mConstructor;
		private final Class<T> 			mClass;
		public DefaultNonArgConstructor(final Class<T> c)  {
			mClass = c;
			try {
				mConstructor = c.getConstructor();
			} catch (NoSuchMethodException | SecurityException e) {
				throw new ReflectionsException(e);
			}
		}
		@Override
		public Object createNewInstance() {
			try {
				return mConstructor.newInstance();
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				throw new ReflectionsException(e);
			}
		}
	}


	public interface ITypeProvider {
		Class<?> getType();
		Type getGenericType();
	}
	private static ITypeProvider fromField(final Field f) {
		return new ITypeProvider() {
			@Override
			public Class<?> getType() {
				return f.getType();
			}

			@Override
			public Type getGenericType() {
				return f.getGenericType();
			}
		};
	}
	private static ITypeProvider fromMethod(final Method m) {
		return new ITypeProvider() {
			@Override
			public Class<?> getType() {
				return m.getReturnType();
			}

			@Override
			public Type getGenericType() {
				return m.getGenericReturnType();
			}
		};
	}

	public static class RField extends RAnnotatedElement implements IGetter, ISetter {
		public static final Comparator<RField> NAME_COMPARATOR = new Comparator<RField>() { @Override public int compare(final RField o1, final RField o2) { return String.CASE_INSENSITIVE_ORDER.compare(o1.getName(), o2.getName()); } };

		private final String			mName;
		private final Class<?>			mType;
		private final String			mDesc;
		private final ISetter			mSetter;
		private final IGetter			mGetter;
		private final boolean 			mReadOnly;
		private final boolean 			mIsCollection;
		private final Class<?>			mSingleType;

		public RField(final String name, final Field f, final String desc, final ISetter setter, final IGetter getter) {
			this(name, fromField(f), f, desc, setter, getter);
		}
		public RField(final String mName, final ITypeProvider typeProvider, final AnnotatedElement anoEl, final String desc, final ISetter mSetter, final IGetter mGetter) {
			this.mName = mName;
			mType = typeProvider.getType();
			this.mDesc = desc;
			this.mSetter = mSetter;
			this.mGetter = mGetter;
			mIsCollection = ReflectionUtils.inherits(mType, Collection.class);
			mReadOnly = mSetter == null && !mIsCollection;

			Class<?> singleType = null;
			if (mIsCollection) {
				final Type t = typeProvider.getGenericType();
				if (t instanceof ParameterizedType) {
					final Type[] ata = ((ParameterizedType)t).getActualTypeArguments();
					singleType = (Class<?>) ata[ata.length-1];
				}else if (t instanceof Class<?>)
					singleType = (Class<?>)t;
				else
					singleType = AnnotationUtil.getType(anoEl, Object.class);
			}
			else
				singleType = null;

			mSingleType = singleType;
		}

		public String getName() { return mName; }

		@Override
		public boolean set(final Object instance, final Object value) {
			if (mReadOnly)
				return false;
			if (isCollection()) {
				return add(instance, value);
			}
			if (mSetter != null && !mReadOnly)
				return mSetter.set(instance, value);
			return false;
		}
		public boolean unset(final Object instance, final Object newValue) {
			if (isReadOnly())
				return false;
			if (isCollection()) {
				return remove(instance, newValue);
			}
			if (get(instance) == newValue){
				return set(instance, null);
			}
			return false;
		}

		private boolean remove(final Object instance, final Object newValue) {
			final Collection c = (Collection) get(instance);
			return c.remove(newValue);
		}

		private boolean add(final Object instance, final Object value) {
			final Collection c = (Collection) get(instance);
			return c.add(value);
		}

		@Override
		public Object get(final Object instance) {
			return mGetter.get(instance);
		}

		public boolean isReadOnly() { return mReadOnly; }

		public boolean canSet(final Object instance) {
			if (isReadOnly()) return false;
			if (isCollection() || get(instance) == null)
				return true;
			return false;
		}

		public boolean isCollection() {
			return mIsCollection;
		}

		public Class<?> getSingleType() {
			if (mSingleType != null)
				return mSingleType;
			return mType;
		}
		public Class<?> getType() { return mType; }
		public boolean isPrimitive() {
			return getSingleType().isPrimitive();
		}

		@Override
		public String toString() {
			return String.format("Field[%s:%s%s:%s]", getName(), getSingleType().getName(), isCollection()?"*":"", Boolean.toString(!isReadOnly()));
		}
	}


	public static class RComboundClassifier extends RClassifier {

		public static String key(final Collection<RClassifier> list) {
			final List<String> names = list.stream().map(RClassifier::getName).sorted().collect(Collectors.toList());
			return String.join("&", names);
		}
		private final List<RClassifier> mBaseList;

		public RComboundClassifier(final Collection<RClassifier> list) {
			super(RClassifierType.COMBOUND, Object.class);
			mBaseList = new ArrayList<>(list);
			mBaseList.sort((a,b) -> {
				if (a.isAbstract() && !b.isAbstract())
					return 1;
				if (!a.isAbstract() && b.isAbstract())
					return -1;
				return a.getName().compareTo(b.getName());
			});
			mName = key(list);
		}

		@Override
		public boolean isAbstract() {
			for (int i = 0; i < mBaseList.size(); i++)
				if (mBaseList.get(i).isAbstract() == false)
					return false;
			return true;
		}



		@Override
		protected LinkedHashMap<String, RField> collectOwnFields() {
			final LinkedHashMap<String, RField> out = new LinkedHashMap<>();
			for (final RClassifier cl : mBaseList) {
				out.putAll(cl.collectOwnFields());
			}
			return out;
		}
		@Override
		protected List<RClassifier> collectOwnParents() {
			final List<RClassifier> out = new ArrayList<>();
			for (final RClassifier cl : mBaseList) {
				final List<RClassifier> tmp = cl.collectOwnParents();
				for (final RClassifier parent : tmp) {
					if (out.contains(parent)==false)
						out.add(parent);
				}
			}
			return out;
		}
		@Override
		public <T> T getAnnotationValue(final String key, final T defaultValue) {
			for (int i = 0; i < mBaseList.size(); i++){
				final T value = mBaseList.get(i).getAnnotationValue(key, defaultValue);
				if (value != defaultValue)
					return value;
			}
			return defaultValue;
		}

		@Override
		public IConstructor<?> getConstructor() {
			if (mBaseList.get(0).isAbstract() == false)
				return mBaseList.get(0).getConstructor();
			return super.getConstructor();
		}
	}


	public static class RClass extends RClassifier {
		private final boolean mAbstract;

		public RClass(final Class<?> clazz) {
			super(RClassifierType.CLASS, clazz);
			mAbstract = Modifier.isAbstract(mNative.getModifiers());
		}
		@Override
		public boolean isAbstract() {
			return mAbstract;
		}

		@Override
		protected IConstructor<?> createDefaultConstructor() {
			if (!isAbstract()) {
				try {
					return new DefaultNonArgConstructor(getNative());
				}catch(final ReflectionsException e) {
					//fallthrough
				}
			}
			return super.createDefaultConstructor();
		}
	}

	public static class RInterface extends RClassifier {
		private static final String[] sGetterPrefixes = {"get", "is", "has"};

		public RInterface(final Class<?> clazz) {
			super(RClassifierType.INTERFACE, clazz);
		}
		@Override
		public boolean isAbstract() { return true; }

		@Override
		protected LinkedHashMap<String, RField> collectOwnFields() {
			//interfaces do not have fields, thus we are looking for getter (and if there is one, also look for the setter)
			final LinkedHashMap<String, RField> out = new LinkedHashMap<>();
			final Method[] methods = mNative.getMethods();
			for (final Method method : methods) {
				if (AnnotationUtil.ignore(method))
					continue;
				final int modifiers = method.getModifiers();
				if (Modifier.isStatic(modifiers))
					continue;
				final String fieldName = getFieldName(AnnotationUtil.getName(method, method.getName()));
				if (fieldName == null)
					continue;
				//ignore methods with parameters even if they are getter (get...|is...)
				if (method.getParameterCount() > 0)
					continue;
				final String desc = AnnotationUtil.getDescription(method, null);

				final IGetter getter = new MethodGetter(method);
				final Method setterMethod = ReflectionUtils.findFirstMethod(mNative, buildNames(fieldName, "set", ""), void.class, method.getReturnType());
				ISetter setter = null;
				if (setterMethod != null)
					setter = new MethodSetter(setterMethod);

				final RField rf = new RField(fieldName, fromMethod(method), method, desc, setter, getter);
				AnnotationUtil.checkForAnnotations(method, rf);

				out.put(fieldName, rf);
			}
			return out;
		}

		private String getFieldName(final String name) {
			if (name == null || name.isEmpty() ) return null;
			final String lc = name.toLowerCase();
			for (int i = 0; i < sGetterPrefixes.length; i++) {
				if (lc.startsWith(sGetterPrefixes[i])) return name.substring(sGetterPrefixes[i].length());
			}
			return null;
		}
	}

	public static class REnum extends RClassifier {
		public REnum(final Class<?> clazz) {
			super(RClassifierType.ENUM, clazz);
		}
		@Override
		public boolean isAbstract() { return false; }
		public Object[] getEnumConstants() { return getNative().getEnumConstants();}
	}

	public abstract boolean isAbstract();

	public boolean inherits(final Class<?> type) {
		return ReflectionUtils.inherits(mNative, type);
	}

	public boolean canInstantiate() {
		return getConstructor().canInstantiate();
	}

	public IConstructor<?> getConstructor() {
		if (mConstructor != null)
			return mConstructor;
		return mConstructor = createDefaultConstructor();
	}

	protected IConstructor<?> createDefaultConstructor(){
		return new NonInstantiatableConstructor();
	}

	public void setConstructor(final Supplier<?> supplier) {
		mConstructor = new SupplierConstructor<>(supplier);
	}

	public Object createNewInstance() {
		final IConstructor<?> c = getConstructor();
		if (c != null && c.canInstantiate())
			return c.createNewInstance();
		return null;
	}

	public boolean hasAnnotation(final Class<? extends Annotation> clazz) {
		return getNative().isAnnotationPresent(clazz);
	}
}
