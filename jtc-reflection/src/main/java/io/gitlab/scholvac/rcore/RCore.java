package io.gitlab.scholvac.rcore;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

public class RCore {
	public static class RAnnotation {
		private final String mKey;
		private final Object mValue;
		public RAnnotation(final String k) { this(k, null);}
		public RAnnotation(final String k, final Object v) {
			mValue = v;
			mKey = k;
		}
		public String getKey() { return mKey;}
		public Object getValue() { return mValue;}
		public <T> T get() {
			try {
				return (T)getValue();
			}catch(final Throwable t) {
				return null;
			}
		}
	}

	public static abstract class RAnnotatedElement {
		private Map<String, RAnnotation> mAnnotations;

		public <T> void addAnnotation(final String key, final T value) {
			addAnnotation(new RAnnotation(key, value));
		}

		public void addAnnotation(final RAnnotation annotation) {
			if (annotation == null) return ;
			if (mAnnotations == null) mAnnotations = new HashMap<>();
			mAnnotations.put(annotation.getKey(), annotation);
		}

		public void addAnnotations(final RAnnotation...annotations) {
			if (annotations != null)
				Stream.of(annotations).forEach(this::addAnnotation);
		}
		public RAnnotation getAnnotation(final String key) {
			return mAnnotations != null ? mAnnotations.get(key) : null;
		}
		public boolean hasAnnotation(final String key) {
			return mAnnotations != null && mAnnotations.containsKey(key);
		}
		public <T> T getAnnotationValue(final String key, final T defaultValue) {
			if (hasAnnotation(key))
				return getAnnotation(key).get();
			return defaultValue;
		}

	}
}
