package io.gitlab.scholvac;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;

import io.gitlab.scholvac.StringUtils;
import io.gitlab.scholvac.anno.uml.ReflectionSupport;
import io.gitlab.scholvac.anno.uml.UMLDescription;
import io.gitlab.scholvac.anno.uml.UMLName;
import io.gitlab.scholvac.rcore.RClassifier;
import io.gitlab.scholvac.rcore.RClassifier.RField;

public class AnnotationUtil {

	public static String getName(final AnnotatedElement element) { return getName(element, null);}
	public static String getName(final AnnotatedElement element, final String defaultValue) {
		final UMLName anno = element.getAnnotation(UMLName.class);
		if (anno != null && StringUtils.isNullOrEmpty(anno.name()) == false)
			return anno.name();
		final UMLDescription desc = element.getAnnotation(UMLDescription.class);
		if (desc != null)
			return desc.name();
		final ReflectionSupport rs = element.getAnnotation(ReflectionSupport.class);
		if (rs != null && StringUtils.isNullOrEmpty(rs.name()) == false)
			return rs.name();
		return defaultValue;
	}
	public static String getDescription(final AnnotatedElement element) { return getDescription(element, null); }
	public static String getDescription(final AnnotatedElement element, final String defaultValue) {
		final UMLName anno = element.getAnnotation(UMLName.class);
		if (anno != null)
			return anno.description();
		final UMLDescription desc = element.getAnnotation(UMLDescription.class);
		if (desc != null)
			return desc.description();
		return defaultValue;
	}
	public static boolean isFieldAccessible(final AnnotatedElement element, final boolean defaultValue) {
		final UMLName anno = element.getAnnotation(UMLName.class);
		if (anno != null)
			return anno.fieldAccessible();
		final ReflectionSupport rs = element.getAnnotation(ReflectionSupport.class);
		if (rs != null)
			return rs.fieldAccessible();
		return defaultValue;
	}
	public static boolean ignore(final AnnotatedElement element) {
		final ReflectionSupport rs = element.getAnnotation(ReflectionSupport.class);
		if (rs != null)
			return rs.ignore();
		return false;
	}
	public static Class<?> getType(final AnnotatedElement element, final Class<?> defaultValue) {
		final UMLName anno = element.getAnnotation(UMLName.class);
		if (anno != null && anno.type() != Object.class)
			return anno.type();
		final ReflectionSupport rs = element.getAnnotation(ReflectionSupport.class);
		if (rs != null && rs.type() != Object.class)
			return rs.type();
		return defaultValue;
	}

	public static void checkForAnnotations(final AnnotatedElement f, final RField rf) {
		final Annotation[] annotations = f.getAnnotations();
		for (final Annotation anno : annotations) {
			rf.addAnnotation(anno.annotationType().getSimpleName(), anno);
		}
	}
	public static void checkForAnnotations(final Class<?> clazz, final RClassifier cl) {
		final Annotation[] annotations = clazz.getAnnotations();
		for (final Annotation anno : annotations) {
			cl.addAnnotation(anno.annotationType().getSimpleName(), anno);
		}
	}
}
