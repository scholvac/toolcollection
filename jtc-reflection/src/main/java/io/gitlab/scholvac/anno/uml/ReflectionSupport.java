package io.gitlab.scholvac.anno.uml;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ TYPE, FIELD, METHOD, PARAMETER })
public @interface ReflectionSupport {
	String name() default "";
	/** if set to true, the field becomes accessible via reflection api (RClassifier) */
	boolean fieldAccessible() default true;

	Class<?> type() default Object.class;
	boolean ignore() default false;
}
