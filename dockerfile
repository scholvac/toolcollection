# Basis-Image für Java und Maven
#FROM maven:3.8.1-jdk-11
FROM maven:3.9.0-eclipse-temurin-8

# Arbeitsverzeichnis setzen
WORKDIR /app

# Create Maven repository directory
RUN mkdir -p .mvn/repository

# POM und src vorab kopieren, damit Abhängigkeiten gecached werden
COPY . .
RUN mvn -Dmaven.repo.local=/app/.mvn/repository clean compile package -DskipTests